﻿using UnityEngine;
using System.Collections;
using System;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace DI.Proformer {
	[AttributeUsage(AttributeTargets.Field, Inherited = true)]
	public class HelpBoxAttribute : PropertyAttribute {

		public string note;
#if UNITY_EDITOR
		public MessageType messageType = MessageType.None;
#endif

		public HelpBoxAttribute(string note) {
			this.note = note;
		}
		public HelpBoxAttribute(string note, int messageType) {
			this.note = note;
#if UNITY_EDITOR
			this.messageType = (MessageType)messageType;
#endif
		}
	}
	
#if UNITY_EDITOR
	[CustomPropertyDrawer(typeof(HelpBoxAttribute))]
	public class HelpBoxDrawer : PropertyDrawer {
		/*Rect helpBoxRect;
		public override void OnGUI(Rect position)
		{
			helpBoxRect = new Rect(position.x, position.y, EditorGUIUtility.currentViewWidth, EditorStyles.helpBox.CalcHeight(new GUIContent(((HelpBoxAttribute)attribute).note), EditorGUIUtility.currentViewWidth));
			EditorGUI.HelpBox(helpBoxRect, ((HelpBoxAttribute)attribute).note, (UnityEditor.MessageType)((int)((HelpBoxAttribute)attribute).messageType));
			Rect propertyRect = position;
			propertyRect.height = EditorGUIUtility.singleLineHeight;
			propertyRect.y += helpBoxRect.height;
			base.OnGUI(propertyRect);
		}

		public override float GetHeight()
		{
			return helpBoxRect.height;
		}*/
		public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
		{
			Rect helpBoxRect = new Rect(position.x, position.y, EditorGUIUtility.currentViewWidth, EditorStyles.helpBox.CalcHeight(new GUIContent(((HelpBoxAttribute)attribute).note), EditorGUIUtility.currentViewWidth));
			Rect propertyRect = position;
			propertyRect.y = position.y + helpBoxRect.height + EditorGUIUtility.singleLineHeight * .2f;
			propertyRect.height = EditorGUIUtility.singleLineHeight;
			EditorGUI.HelpBox(helpBoxRect, ((HelpBoxAttribute)attribute).note, (UnityEditor.MessageType)((int)((HelpBoxAttribute)attribute).messageType));
			EditorGUI.PropertyField(propertyRect, property);
		}

		public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
		{
			return (EditorGUIUtility.singleLineHeight + EditorStyles.helpBox.CalcHeight(new GUIContent(((HelpBoxAttribute)attribute).note), EditorGUIUtility.currentViewWidth)) * 1.2f;
		}
	}
#endif
}
