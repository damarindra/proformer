﻿using UnityEngine;
using System.Collections;
using System;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace DI.Proformer.ToolHelper
{
	/// <summary>Draw Popup field with Tag option</summary>
	[AttributeUsage(AttributeTargets.Field | AttributeTargets.Property | AttributeTargets.Struct, Inherited = true)]
	public class TagAttribute : AddIndentAttribute
	{
		/// <summary>Draw Popup field with Tag option</summary>
		public TagAttribute() { }
	}

#if UNITY_EDITOR
	[CustomPropertyDrawer(typeof(TagAttribute))]
	public class TagPropertyDrawer : AddIndentDrawer {

		public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
		{
			TagAttribute att = (TagAttribute)attribute;
			EditorGUI.indentLevel += att.indentAddition;
			if (property.type != "string") {
				Debug.LogError("Target type not string variable");
				base.OnGUI(position, property, label);
			}else
				property.stringValue = EditorGUI.TagField(position, label, property.stringValue);
			EditorGUI.indentLevel -= att.indentAddition;
		}
	}
#endif
}