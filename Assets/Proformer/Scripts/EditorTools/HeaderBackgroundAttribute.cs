﻿using UnityEngine;
using System.Collections;
using System;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace DI.Proformer.ToolHelper {
	[AttributeUsage(AttributeTargets.Field, Inherited = true)]
	public class HeaderBackgroundAttribute : PropertyAttribute
	{
		public int yCount = 1;
		public float height = -1;
		public float offsetBot = 0;
		public string headerLabel = "";
		public RectOffset margin = new RectOffset();
		public bool border =true;

		public HeaderBackgroundAttribute() { }
		public HeaderBackgroundAttribute(string headerLabel) { this.headerLabel = headerLabel; }
		public HeaderBackgroundAttribute(int yCount) { this.yCount = yCount; }
		public HeaderBackgroundAttribute(int yCount, string headerLabel) { this.yCount = yCount; this.headerLabel = headerLabel; }
		public HeaderBackgroundAttribute(int yCount, float offsetBot) { this.yCount = yCount; this.offsetBot = offsetBot; }
		public HeaderBackgroundAttribute(int yCount, float offsetBot, string headerLabel) { this.yCount = yCount; this.offsetBot = offsetBot; this.headerLabel = headerLabel; }
		public HeaderBackgroundAttribute(float height) { this.height = height; }
	}
}