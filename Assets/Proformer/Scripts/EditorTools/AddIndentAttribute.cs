﻿using UnityEngine;
using System.Collections;
using System;

#if UNITY_EDITOR
using UnityEditor;
#endif
namespace DI.Proformer.ToolHelper
{
	[AttributeUsage(AttributeTargets.Field | AttributeTargets.Property |
		AttributeTargets.Class | AttributeTargets.Struct, Inherited = true)]
	public class AddIndentAttribute : PropertyAttribute
	{
		public int indentAddition = 0;
		public AddIndentAttribute() { }
		public AddIndentAttribute(int indentAddition) {
			this.indentAddition = indentAddition;
		}
	}

#if UNITY_EDITOR
	[CustomPropertyDrawer(typeof(AddIndentAttribute), true)]
	public class AddIndentDrawer : PropertyDrawer {

		public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
		{
			AddIndentAttribute att = (AddIndentAttribute)attribute;
			EditorGUI.indentLevel += att.indentAddition;
			EditorGUI.PropertyField(position, property);
			EditorGUI.indentLevel -= att.indentAddition;
		}
	}
#endif
}