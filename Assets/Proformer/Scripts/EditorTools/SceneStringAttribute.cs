﻿using UnityEngine;
using System.Collections;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace DI.Proformer.ToolHelper
{
	/// <summary>Draw Popup field with Scene option (gather from build settings)</summary>
	public class SceneStringAttribute : PropertyAttribute
	{
		/// <summary>Draw Popup field with Scene option (gather from build settings)</summary>
		public SceneStringAttribute() { }
	}

#if UNITY_EDITOR
	[CustomPropertyDrawer(typeof(SceneStringAttribute))]
	public class ScenePropertyDrawer : PropertyDrawer
	{

		public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
		{
			if (!CheckPropertyString(property))
			{
				Debug.LogError(property.name + "are not string variable");
				return;
			}

			GameData.Instance.CheckAndUpdateIfNecessary();

			if (GameData.Instance.scenes.Contains(property.stringValue))
			{
				int selected = GameData.Instance.scenes.IndexOf(property.stringValue);
				selected = EditorGUI.Popup(position, property.name, selected, GameData.Instance.scenes.ToArray());
				property.stringValue = GameData.Instance.scenes[selected];
			}
			else if (GameData.Instance.scenes.Count != 0)
				property.stringValue = GameData.Instance.scenes[0];
		}


		private bool CheckPropertyString(SerializedProperty sourcePropertyValue)
		{
			switch (sourcePropertyValue.propertyType)
			{
				case SerializedPropertyType.String:
					return true;
				default:
					Debug.LogError("Data type of the property used for string [" + sourcePropertyValue.propertyType + "] is currently not supported");
					return false;
			}
		}
	}
#endif
}