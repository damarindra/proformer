﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using System.Linq;
#if UNITY_EDITOR
using UnityEditor;
#endif
namespace DI.Proformer.ToolHelper
{
	/// <summary>Draw custom field with script popup option</summary>
	[AttributeUsage(AttributeTargets.Field , Inherited = true)]
	public class MonoScriptAttribute : AddIndentAttribute {
		public string targetGameObject;
		
		/// <summary>Draw custom field with script popup option</summary>
		/// <param name="targetGameObject">Target GameObject will be inspect and gather all MonoBehaviour script to popup menu</param>
		/// <returns>Draw custom field</returns>
		public MonoScriptAttribute(string targetGameObject) {
			this.targetGameObject = targetGameObject;
		}
	}

#if UNITY_EDITOR
	[CustomPropertyDrawer(typeof(MonoScriptAttribute), true)]
	public class MonoScriptDrawer : AddIndentDrawer {

		public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
		{
			MonoScriptAttribute att = (MonoScriptAttribute)attribute;
			EditorGUI.indentLevel += att.indentAddition;
			
			SerializedProperty target = property.serializedObject.FindProperty(att.targetGameObject);
			if (target != null) {
				GameObject targetGO = property.serializedObject.FindProperty(att.targetGameObject).objectReferenceValue as GameObject;
				if (string.IsNullOrEmpty(att.targetGameObject) || !targetGO)
				{
					GUI.enabled = false;
					EditorGUI.Popup(position, label.text, 0, new string[0]);
					GUI.enabled = true;
				}
				else if (!property.type.Contains("$MonoBehaviour"))
				{
					Debug.LogError("Target type is not MonoBehaviour : " + property.name);
					EditorGUI.PropertyField(position, property);
				}
				else
				{
					//SCRIPT INFO
					List<MonoBehaviour> monoBehaviours = targetGO.GetComponents<MonoBehaviour>().ToList();
					if (monoBehaviours.Count > 0)
					{
						var monoStrings = monoBehaviours.Select(i => i.ToString()).ToList();
						monoStrings.Insert(0, "Null");
						int iMonoScript = monoBehaviours.IndexOf(property.objectReferenceValue as MonoBehaviour);
						if (iMonoScript == -1)
							iMonoScript = 0;
						else
							iMonoScript += 1;
						//Adds 1 because we insert a null string
						iMonoScript = EditorGUI.Popup(position, "Script", iMonoScript, monoStrings.ToArray());
						if (iMonoScript == 0)
							property.objectReferenceValue = null;
						else if (iMonoScript <= monoBehaviours.Count)
						{
							if (property.objectReferenceValue != monoBehaviours[iMonoScript - 1])
								property.objectReferenceValue = monoBehaviours[iMonoScript - 1];
						}
						else
							iMonoScript = 0;
					}
					else
						property.objectReferenceValue = null;
				}
			}
			
			
			EditorGUI.indentLevel -= att.indentAddition;
		}
	}
#endif
}
