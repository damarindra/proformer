﻿using UnityEngine;
using System;
using System.Collections;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace DI.Proformer.ToolHelper {
	[AttributeUsage(AttributeTargets.Field | AttributeTargets.Property |
		AttributeTargets.Class | AttributeTargets.Struct, Inherited = true)]
	public class ConditionalAttribute : PropertyAttribute
	{
		public string code = "";
		public string ConditionalSourceField = "";
		public string ConditionalSourceField2 = "";
		public bool HideInInspector = false;
		public bool Inverse = false;
		//Random number
		public int comparableInt = -12576423;

		// Use this for initialization
		public ConditionalAttribute(string code, string conditionalSourceField)
		{
			this.code = code;
			this.ConditionalSourceField = conditionalSourceField;
			this.HideInInspector = false;
			this.Inverse = false;
		}
		public ConditionalAttribute(string code, string conditionalSourceField, string conditionalSourceField2)
		{
			this.code = code;
			this.ConditionalSourceField = conditionalSourceField;
			this.ConditionalSourceField2 = conditionalSourceField2;
			this.HideInInspector = false;
			this.Inverse = false;
		}

		public ConditionalAttribute(string code, string conditionalSourceField, int conditionalSourceField2)
		{
			this.code = code;
			this.ConditionalSourceField = conditionalSourceField;
			this.comparableInt = conditionalSourceField2;
			this.HideInInspector = false;
			this.Inverse = false;
		}
		public ConditionalAttribute(string code, string conditionalSourceField, bool hideInInspector)
		{
			this.code = code;
			this.ConditionalSourceField = conditionalSourceField;
			this.HideInInspector = hideInInspector;
			this.Inverse = false;
		}

		public ConditionalAttribute(string code, string conditionalSourceField, bool hideInInspector, bool inverse)
		{
			this.code = code;
			this.ConditionalSourceField = conditionalSourceField;
			this.HideInInspector = hideInInspector;
			this.Inverse = inverse;
		}

	}


#if UNITY_EDITOR

	[CustomPropertyDrawer(typeof(ConditionalAttribute))]
	public class ConditionalPropertyDrawer : PropertyDrawer
	{
		public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
		{
			ConditionalAttribute condHAtt = (ConditionalAttribute)attribute;
			bool enabled = GetConditionalHideAttributeResult(condHAtt, property);

			bool wasEnabled = GUI.enabled;
			GUI.enabled = enabled;
			if (!condHAtt.HideInInspector || enabled)
			{
				EditorGUI.PropertyField(position, property, label, true);
			}

			GUI.enabled = wasEnabled;
		}

		public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
		{
			ConditionalAttribute condHAtt = (ConditionalAttribute)attribute;
			if (condHAtt.code == "Hide")
			{
				bool enabled = GetConditionalHideAttributeResult(condHAtt, property);

				if (!condHAtt.HideInInspector || enabled)
				{
					return EditorGUI.GetPropertyHeight(property, label);
				}
				else
				{
					//The property is not being drawn
					//We want to undo the spacing added before and after the property
					return -EditorGUIUtility.standardVerticalSpacing;
					//return 0.0f;
				}

			}
			else
			{
				return EditorGUI.GetPropertyHeight(property, label);
			}
		}

		private bool GetConditionalHideAttributeResult(ConditionalAttribute condHAtt, SerializedProperty property)
		{
			bool enabled = true;

			string path = property.propertyPath;
			string[] paths = path.Split(new char[1] { '.' });
			path = "";
			foreach (string p in paths)
			{
				if (p != paths[paths.Length - 1])
					path += p;
			}

			string completePath = condHAtt.ConditionalSourceField;
			if (!string.IsNullOrEmpty(path))
				completePath = (path + "." + condHAtt.ConditionalSourceField);
			string[] pathProperty1 = completePath.Split(new char[1] { '.' });
			SerializedProperty sourcePropertyValue = null;
			int i = 0;
			sourcePropertyValue = property.serializedObject.FindProperty(pathProperty1[0]);

			i = 1;
			while (i <= pathProperty1.Length - 1)
			{
				if (string.IsNullOrEmpty(pathProperty1[i]))
				{ }
				else if (sourcePropertyValue != null)
					sourcePropertyValue = sourcePropertyValue.FindPropertyRelative(pathProperty1[i]);
				else
					break;
				i += 1;
			}

			completePath = condHAtt.ConditionalSourceField2;
			if (!string.IsNullOrEmpty(path))
				completePath = (path + "." + condHAtt.ConditionalSourceField2);
			string[] pathProperty2 = (completePath).Split(new char[1] { '.' });
			SerializedProperty sourcePropertyValue2 = property.serializedObject.FindProperty(pathProperty2[0]);
			i = 1;
			while (i <= pathProperty2.Length - 1)
			{
				if (sourcePropertyValue2 != null)
				{
					sourcePropertyValue2 = sourcePropertyValue2.FindPropertyRelative(pathProperty2[i]);
				}
				else
					break;
				i += 1;
			}

			if (condHAtt.code == "Hide")
			{
				if (sourcePropertyValue != null && sourcePropertyValue2 == null && condHAtt.comparableInt == -12576423)
				{
					enabled = CheckPropertyType(sourcePropertyValue);
				}
				else if (sourcePropertyValue != null && sourcePropertyValue2 != null)
				{
					enabled = ComparePropertyValue(sourcePropertyValue, sourcePropertyValue2);
				}
				else if (sourcePropertyValue != null && condHAtt.comparableInt != -12576423)
				{
					enabled = sourcePropertyValue.enumValueIndex == condHAtt.comparableInt;
				}
				else
				{
					//Debug.Log("Failed : " + sourcePropertyValue);
				}
			}

			if (condHAtt.Inverse) enabled = !enabled;

			return enabled;
		}

		private bool ComparePropertyValue(SerializedProperty sourcePropertyValue, SerializedProperty sourcePropertyValue2)
		{
			if (sourcePropertyValue.propertyType == SerializedPropertyType.Integer)
			{
				if (sourcePropertyValue2.propertyType == SerializedPropertyType.Integer)
					return sourcePropertyValue.intValue == sourcePropertyValue2.intValue;
				else
				{
					Debug.LogError("2 property type is not same!");
					return false;
				}
			}
			else if (sourcePropertyValue.propertyType == SerializedPropertyType.Enum)
			{
				if (sourcePropertyValue2.propertyType == SerializedPropertyType.Enum)
				{
					return sourcePropertyValue.enumValueIndex == sourcePropertyValue2.enumValueIndex;
				}
				else
				{
					Debug.LogError("2 property type is not same!");
					return false;
				}
			}
			return false;
		}

		private bool CheckPropertyType(SerializedProperty sourcePropertyValue)
		{
			switch (sourcePropertyValue.propertyType)
			{
				case SerializedPropertyType.Boolean:
					return sourcePropertyValue.boolValue;
				case SerializedPropertyType.ObjectReference:
					return sourcePropertyValue.objectReferenceValue != null;
				default:
					Debug.LogError("Data type of the property used for conditional hiding [" + sourcePropertyValue.propertyType + "] is currently not supported");
					return true;
			}
		}
	}
#endif
}
