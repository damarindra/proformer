﻿using UnityEngine;
using System.Collections;
using System;
using System.Linq;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
#endif
using System.Reflection;

namespace DI.Proformer.ToolHelper {
	/// <summary>
	/// Method Attribute will draw popup field with method option.
	/// </summary>
	[AttributeUsage(AttributeTargets.Field | AttributeTargets.Property |
		AttributeTargets.Class | AttributeTargets.Struct, Inherited = true)]
	public class MethodAttribute : AddIndentAttribute
	{
		public string condition = "";
		
		/// <summary> Method Attribute will draw popup field with method option.</summary>
		/// <param name="condition">Target Script variable (Support MonoBehaviour and TargetMethodInfo)</param>
		/// <returns>if param condition type proper, then will draw an popup menu show all method</returns>
		public MethodAttribute(string condition) {
			this.condition = condition;
		}
	}

#if UNITY_EDITOR
	[CustomPropertyDrawer(typeof(MethodAttribute))]
	public class MethodPropertyDrawer : AddIndentDrawer
	{
		public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
		{
			MethodAttribute att = (MethodAttribute)attribute;
			EditorGUI.indentLevel += att.indentAddition;
			if (!CheckPropertyType(property))
			{
				EditorGUI.PropertyField(position, property);
				return;
			}

			if (!string.IsNullOrEmpty(att.condition))
			{
				bool tmiMode = property.serializedObject.FindProperty(att.condition).type == "TargetMethodInfo" ? true : false;
				bool scriptMode = property.serializedObject.FindProperty(att.condition).type.Contains("MonoBehaviour");

				if (tmiMode)
				{
					if (property.serializedObject.FindProperty(att.condition).FindPropertyRelative("monoScript").objectReferenceValue != null)
					{
						BindingFlags flags = BindingFlags.Public | BindingFlags.DeclaredOnly | BindingFlags.Instance;
						var methods = property.serializedObject.FindProperty(att.condition).FindPropertyRelative("monoScript").objectReferenceValue.GetType().GetMethods(flags).ToList().Select(i => i.Name.ToString() ).ToList();
						int totalRemoved = 0;
						int count = methods.Count;
						for (int i = 0; i < count; i++) {
							if (methods[i-totalRemoved].Contains("get_") || methods[i - totalRemoved].Contains("set_"))
							{
								methods.RemoveAt(i - totalRemoved);
								totalRemoved += 1;
							}
						}
						ShowMethodsPopup(methods, property, position, label);
					}
					else
					{
						GUI.enabled = false;
						EditorGUI.Popup(position, label.text, 0, new string[0] { });
						property.stringValue = null;
						GUI.enabled = true;
					}
				}
				else if (scriptMode)
				{
					if (property.serializedObject.FindProperty(att.condition).objectReferenceValue != null)
					{
						var methods = ((MonoBehaviour)property.serializedObject.FindProperty(att.condition).objectReferenceValue).GetType().GetMethods(BindingFlags.Public | BindingFlags.DeclaredOnly | BindingFlags.Instance).ToList().Select(i => i.Name.ToString()).ToList();
						int totalRemoved = 0;
						int count = methods.Count;
						for (int i = 0; i < count; i++)
						{
							if (methods[i - totalRemoved].Contains("get_") || methods[i - totalRemoved].Contains("set_"))
							{
								methods.RemoveAt(i - totalRemoved);
								totalRemoved += 1;
							}
						}
						ShowMethodsPopup(methods, property, position, label);
					}
					else
					{
						GUI.enabled = false;
						property.stringValue = null;
						EditorGUI.Popup(position, label.text, 0, new string[0] { });
						GUI.enabled = true;
					}
				}
				else
					EditorGUI.PropertyField(position, property);

			}
			EditorGUI.indentLevel -= att.indentAddition;
		}

		private void ShowMethodsPopup(List<string> methods, SerializedProperty property, Rect position, GUIContent label) {

			if (methods.Count > 0)
			{
				methods.Insert(0, "Null");
				int iInfos = string.IsNullOrEmpty(property.stringValue) ? 0 : methods.IndexOf(property.stringValue);
				if (iInfos == -1)
					iInfos = 0;
				

				iInfos = EditorGUI.Popup(position, label.text, iInfos, methods.ToArray());
				if (iInfos < methods.Count)
				{
					if (iInfos == 0)
						property.stringValue = null;
					else if (property.stringValue != methods[iInfos])
					{
						property.stringValue = methods[iInfos];
					}
				}
				else
					iInfos = 0;
			}
			else
			{
				GUI.enabled = false;
				EditorGUI.Popup(position, label.text, 0, methods.ToArray());
				GUI.enabled = true;
			}
		}

		private bool CheckPropertyType(SerializedProperty sourcePropertyValue)
		{
			switch (sourcePropertyValue.propertyType)
			{
				case SerializedPropertyType.String:
					return true;
				default:
					Debug.LogError("Data type of the property used for Method Property [" + sourcePropertyValue.propertyType + "] is only supported for string");
					return true;
			}
		}
	}
#endif
}
