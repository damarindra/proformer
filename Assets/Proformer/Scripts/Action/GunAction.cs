﻿using UnityEngine;
using System.Collections;
using DI.PoolingSystem;
using DI.Proformer.Manager;
using DI.Proformer.ToolHelper;

namespace DI.Proformer.Action
{
	/// <summary>
	/// Gun Weapon\n
	/// targetInput.input.down : shoot start \n
	/// targetInput.input.up : shoot stop
	/// </summary>
	public class GunAction : ProformerAction
	{
		/// <summary>Projectile </summary>
		public GameObject projectile;
		[Tooltip("Local Position, relative to gameobject rotation. Basically z is front direction")]
		/// <summary>Projectile spawn Position </summary>
		public Vector3 projectilePosition = Vector3.forward;
		/// <summary>Set true if you can fire when input still pressed </summary>
		public bool continuous = false;
		//[Conditional("Hide", "continous", false)]
		[SerializeField]
		/// <summary>Fire rate </summary>
		private float fireRate = .2f;
		[SerializeField]
		/// <summary>Shoot SFX </summary>
		private AudioClip shootClip;
		[SerializeField]
		/// <summary>Pool size. Maximum projectil can be spawned together in one scene </summary>
		private int projectilePoolSize = 40;
		[SerializeField]
		/// <summary>Set true if you want unlimited ammo </summary>
		private bool unlimitedAmmo = true;
		[Conditional("Hide", "unlimitedAmmo", true, true)]
		[SerializeField]
		/// <summary>Set true if you want to add reload </summary>
		private bool canReload = false;
		[Conditional("Hide", "canReload", true, false)]
		[SerializeField]
		/// <summary>set true if you want auto reloading when current magazine is out </summary>
		private bool autoReload = true;
		[Conditional("Hide", "canReload", true, false)]
		[SerializeField]
		/// <summary>Reload time </summary>
		private float reloadTime = 2f;
		[Conditional("Hide", "canReload", true, false)]
		[SerializeField]
		/// <summary>Total Magazine </summary>
		private int totalMagazine = 90;
		[Conditional("Hide", "unlimitedAmmo", true, true)]
		[SerializeField]
		/// <summary>Each magazine </summary>
		private int maxEachMagazine = 30;
		[SerializeField]
		/// <summary>reload input </summary>
		private TargetInput reloadInput = new TargetInput();
		/// <summary>the current ammo (in game)</summary>
		private int currentAmmo;
		/// <summary>total ammo (in game)</summary>
		private int currentTotalAmmo;
		
		DIControllerMotor controller;
		bool canShootAgain = true;

		#region ProformerAction
		public override void Start()
		{
			base.Start();
			controller = GetComponent<DIControllerMotor>();
			if (canReload) {
				currentAmmo = maxEachMagazine;
				currentTotalAmmo = totalMagazine;
			}

			PoolingSystem.PoolingManager.Instance.CreatePool(projectile, projectilePoolSize);
		}

		public override void Act()
		{
			bool permissionCondition = permissionAction.CheckCondition(motor);
			if (targetInput.input.down && permissionCondition)
			{
				isExecute = true;
				motor.animator.SetBool("Shooting", true);
				targetInput.input.down = false;
			}
			else if (targetInput.input.up || !permissionCondition)
			{
				isExecute = false;
				motor.animator.SetBool("Shooting", false);
				targetInput.input.up = false;
			}
			else if (!permissionCondition && isExecute)
				isExecute = false;

			if (reloadInput.input != null) {
				if (permissionCondition && canReload && reloadInput.input.down)
				{
					StartCoroutine(Reload());
					return;
				}
			}

			if (isExecute)
				Shoot();
			if (!continuous)
				isExecute = false;
		}
		#endregion

		#region Gun
		/// <summary>
		/// Function to Start Shooting
		/// if Continuous shoot, you can stop it by set isShooting to false
		/// </summary>
		public void Shoot()
		{
			if (!canShootAgain)
				return;

			if (!unlimitedAmmo) {
				if (currentAmmo <= 0) {
					if (canReload && autoReload)
						StartCoroutine(Reload());
					return;
				}
				else
					currentAmmo -= 1;
			}

			ShootRaw();
			StartCoroutine(WaitNextShoot());
		}
		IEnumerator WaitNextShoot() {
			canShootAgain = false;
			yield return new WaitForSeconds(fireRate);
			canShootAgain = true;
		}
		public void ShootRaw()
		{
			Vector3 posOffset = projectilePosition;// controller.rawDirection == 1 ? projectilePosition : new Vector3(-projectilePosition.x, projectilePosition.y, -projectilePosition.z);
			posOffset *= motor.stateAdv.isWallSliding ? -1 : 1;
			Vector3 projectilePos = transform.position + transform.TransformDirection(posOffset);

			PoolingManager.Instance.ReuseObject(projectile, projectilePos, Quaternion.identity, controller);
			SoundManager.Instance.PlaySFX(shootClip);
		}
		IEnumerator Reload() {
			if (currentTotalAmmo <= 0) {
				yield return null;
			}
			else
			{
				canShootAgain = false;
				AnimatorTool.SetBool(motor.animator, "Reload", true);
				yield return new WaitForSeconds(reloadTime);
				int deltaMagazineNeed = maxEachMagazine - currentAmmo;
				currentAmmo += currentTotalAmmo > 0 && currentTotalAmmo < maxEachMagazine ? currentTotalAmmo : deltaMagazineNeed;
				currentTotalAmmo -= deltaMagazineNeed;
				if (currentTotalAmmo < 0)
					currentTotalAmmo = 0;
				canShootAgain = true;
				AnimatorTool.SetBool(motor.animator, "Reload", false);
			}
		}
		#endregion

		void OnDrawGizmosSelected()
		{
			if (Application.isPlaying)
			{
				Gizmos.DrawWireSphere(transform.position + transform.TransformDirection(projectilePosition), .1f);
#if UNITY_EDITOR
				UnityEditor.Handles.Label(transform.position + transform.TransformDirection(projectilePosition) + Vector3.up * .15f, "Projectile");
#endif
			}
			else
			{
				Gizmos.DrawWireSphere(transform.position + transform.TransformDirection(projectilePosition), .1f);
#if UNITY_EDITOR
				UnityEditor.Handles.Label(transform.position + transform.TransformDirection(projectilePosition) + Vector3.up * .15f, "Projectile");
#endif

			}

		}

	}
}

