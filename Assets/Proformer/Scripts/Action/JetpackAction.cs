﻿using UnityEngine;
using System.Collections;

namespace DI.Proformer.Action {
	/// <summary>Jetpack</summary>
	public class JetpackAction : ProformerAction {

		/// <summary>Fly power</summary>
		public float flyPower = 10;
		/// <summary>Fly time in seconds</summary>
		public float flyTime = 3;
		float currentFlyTime = 0;
		/// <summary>Particle system will be played</summary>
		public ParticleSystem particle;
		ParticleSystem.EmissionModule emitModule;

		public override void Start()
		{
			base.Start();
			emitModule = particle.emission;
			emitModule.enabled = false;
			particle.Play();
		}

		public override void Act()
		{
			base.Act();
			bool isCanExecute = permissionAction.CheckCondition(motor);
			if (isCanExecute)
			{
				if (targetInput.input.pressed && currentFlyTime > 0)
				{
					if (motor.jumpLeft <= 0)
					{
						motor.velocity.y += flyPower * Time.deltaTime;
						currentFlyTime -= Time.deltaTime;
						isExecute = true;
					}
				}
				else
					isExecute = false;
			}
			else
				isExecute = false;

			if (!isExecute)
				StopParticle();
			else if (isExecute)
				PlayParticle();

			if (motor.stateAdv.isClimbingLadder || motor.stateAdv.isWallSliding || motor.state.colliderBelow)
				currentFlyTime = flyTime;

		}

		void StopParticle()
		{
			emitModule.enabled = false;
		}
		void PlayParticle() {
			emitModule.enabled = true;
		}
	}
}
