﻿using UnityEngine;
using System.Collections;
using DI.Proformer.Manager;
using DI.Proformer.ToolHelper;

namespace DI.Proformer.Action
{
	/// <summary>Dash, add this component to Character type and create the dash control</summary>
	public class DashAction : ProformerAction {
		
		/// <summary>Dash Speed</summary>
		public float dashSpeed = 18;
		/// <summary>Dash Time </summary>
		public float dashTime = .3f;
		/// <summary>Dash Cooldown </summary>
		public float timeDashCooldown = .5f;
		private bool canDash = true;
		/// <summary>Set true if you want to make your character can dash again when touch the ground </summary>
		public bool canDashAgainWhenGrounded = true;
		/// <summary>Dash SFX </summary>
		public AudioClip dashClip;

		public override void Start()
		{
			base.Start();
		}

		public override void Act() {
			if (!permissionAction.CheckCondition(motor))
			{
				if (motor.animator.GetBool("Dashing")) {
					isExecute = false;
					motor.stateAdv.isDashing = isExecute;
					motor.animator.SetBool("Dashing", false);
				}
				return;
			}

			if (targetInput.input.down && canDash && motor.standartInput.directionalInput.x != 0)
			{
				StartCoroutine(Dash());
				SoundManager.Instance.PlaySFX(dashClip);
			}
			else if (targetInput.input.up)
			{
				isExecute = false;
			}

			AnimatorTool.SetBool(motor.animator, "Dashing", isExecute);

			if (isExecute && !motor.state.collisionBelow && !motor.state.collisionForward)
				motor.velocity.y = 0;
		}

		IEnumerator Dash()
		{
			float timer = 0;
			canDash = false;
			isExecute = true;
			motor.stateAdv.isDashing = isExecute;
			if (targetInput.input.pressed)
			{
				while (timer < dashTime)
				{
					motor.velocity.x = motor.standartInput.directionalInput.x * dashSpeed;
					timer += Time.deltaTime;
					if (!permissionAction.CheckCondition(motor))
						break;
					if (targetInput.input.up)
						break;
					if (motor.state.collisionWall)
						break;
					yield return null;
				}
				isExecute = false;
				motor.stateAdv.isDashing = isExecute;
				yield return new WaitForSeconds(timeDashCooldown);
				while (!canDash)
				{
					if (!canDashAgainWhenGrounded || (canDashAgainWhenGrounded && motor.state.collisionBelow) || motor.stateAdv.isWallSliding)
						canDash = true;
					yield return null;
				}
			}
		}
	}

}
