﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DI.Proformer.ToolHelper;
using System;

namespace DI.Proformer.Action {
	[RequireComponent(typeof(DIControllerMotor))]
	public class ProformerAction : MonoBehaviour {
		
		[HideInInspector]
		public PermissionsAction permissionAction = new PermissionsAction();
		[HideInInspector]
		public TargetInput targetInput = new TargetInput();

		protected DIControllerMotor motor;
		[HideInInspector]
		public bool isExecute = false;

		public virtual void Start() {
			motor = GetComponent<DIControllerMotor>();
		}

		public virtual void Act() {
		}

		[HideInInspector]
		public bool _killComponent = false;
		public void KillComponent()
		{
			_killComponent = true;
		}

		[System.Serializable]
		public class PermissionsAction {
			public bool grounded = true, jumping = true, wallSliding, ladder, crouch;
			public List<ProformerAction> otherActions = new List<ProformerAction>();
			public List<bool> otherActionPermission = new List<bool>();

			public PermissionsAction() {
				otherActions = new List<ProformerAction>();
				otherActionPermission = new List<bool>();
			}

			public bool CheckCondition(DIControllerMotor motor)
			{
				if (!grounded && motor.state.collisionBelow)
					return false;
				if (!jumping && !motor.state.collisionBelow)
					return false;
				if (!wallSliding && motor.stateAdv.isWallSliding)
					return false;
				if (!ladder && motor.stateAdv.isClimbingLadder)
					return false;
				if (!crouch && motor.stateAdv.isCrouching)
					return false;
				for (int i = 0; i < otherActions.Count; i++) {
					if (otherActions[i] == null)
						Debug.LogError("Other Actions not setup properly, please click button 'Flush Custom Action' before playing or testing");
					else if (!otherActionPermission[i] && otherActions[i].isExecute) 
						return false;
				}
				return true;
			}
		}
	}
}
