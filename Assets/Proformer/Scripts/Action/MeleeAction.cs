﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DI.Proformer.Manager;
using DI.Proformer.ToolHelper;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace DI.Proformer.Action {
	/// <summary>Melee</summary>
	public class MeleeAction : ProformerAction
	{
		/// <summary></summary>
		[Tooltip("Time Cooldown when Combo Melee is done")]
		public float attackRestTime = 1f;
		public List<AttackCombo> attackCombo = new List<AttackCombo>();
		Animator anim;

		bool canGoToNextCombo = false, isWaitingNextCombo = false, isResting = false;
		int currentCombo = 0;

		/// <summary>
		/// Stop When Attacking
		/// </summary>
		[Tooltip("Stop When Attacking")]
		public bool stopWhenAttacking = true;

		public override void Start()
		{
			base.Start();
			anim = GetComponent<Animator>();
			motor.onAfterCalculation += StopWhenAttacking;
			if (anim == null)
				anim = GetComponentInChildren<Animator>();

			foreach (AttackCombo a in attackCombo)
			{
				if (a.areaEffect != null)
				{
					a.areaEffect.isTrigger = true;
					a.areaEffect.enabled = false;
				}
				else
					Debug.LogError("Area Effect is Null");
			}
		}

		private void StopWhenAttacking(ref Vector3 velocity, DIController motor)
		{
			if (isExecute && stopWhenAttacking) {
				velocity.x = 0;
				velocity.z = 0;
			}
		}

		public override void Act()
		{
			bool permissionCondition = permissionAction.CheckCondition(motor);
			if (permissionCondition) {
				if (targetInput.input.down) {
					Attacking();
				}
			}
		}

		public void Attacking()
		{
			currentCombo += 1;
			//End of attack
			if (currentCombo > attackCombo.Count || isResting)
			{
				if (!isResting && canGoToNextCombo)
				{
					StartCoroutine("RestAttack");
				}
				if (currentCombo > attackCombo.Count)
					currentCombo -= 1;
				return;
			}
			if (attackCombo[currentCombo - 1].areaEffect == null)
			{
				Debug.LogError("Area Affected Not Signed");
				return;
			}
			if (currentCombo != 1)
			{

				if (canGoToNextCombo && !isWaitingNextCombo)
				{
					anim.SetTrigger("Combo");
					attackCombo[currentCombo - 2].areaEffect.enabled = false;
					StopCoroutine("AttackCo");
					StartCoroutine("AttackCo");
				}
				else if (!isWaitingNextCombo)
				{
					currentCombo -= 1;
					isWaitingNextCombo = true;
					StopCoroutine("WaitTillNextComboActivated");
					StartCoroutine("WaitTillNextComboActivated");
				}
				else if (isWaitingNextCombo)
					currentCombo -= 1;
			}
			else
			{
				isExecute = true;
				anim.SetBool("Melee", true);
				StopCoroutine("AttackCo");
				StartCoroutine("AttackCo");
			}
		}

		IEnumerator RestAttack()
		{
			isResting = true;
			anim.SetBool("Melee", false);
			isExecute = false;
			yield return new WaitForSeconds(attackRestTime);
			currentCombo = 0;
			isResting = false;
		}

		IEnumerator WaitTillNextComboActivated()
		{

			while (!canGoToNextCombo)
				yield return null;
			attackCombo[currentCombo - 1].areaEffect.enabled = false;
			currentCombo += 1;
			StopCoroutine("AttackCo");
			StartCoroutine("AttackCo");
			anim.SetTrigger("Combo");
			yield return new WaitForEndOfFrame();
			isWaitingNextCombo = false;
		}

		IEnumerator AttackCo()
		{
			canGoToNextCombo = false;
			float timer = 0;
			int indexUsed = currentCombo - 1;
			SoundManager.Instance.PlaySFX(attackCombo[indexUsed].attackClip);
			if (attackCombo[indexUsed].areaEffect.enabled)
				attackCombo[indexUsed].areaEffect.enabled = false;
			while (timer < attackCombo[indexUsed].attackTime)
			{
				if (timer > attackCombo[indexUsed].triggerStartTime && !attackCombo[indexUsed].areaEffect.enabled)
				{
					attackCombo[indexUsed].areaEffect.enabled = true;
				}
				if (timer > attackCombo[indexUsed].transitionToNextAttack)
					canGoToNextCombo = true;
				if (timer > attackCombo[indexUsed].triggerEndTime && attackCombo[indexUsed].areaEffect.enabled) {
					attackCombo[indexUsed].areaEffect.enabled = false;
				}

				timer += Time.deltaTime;
				yield return null;
			}

			currentCombo = 0;
			anim.SetBool("Melee", false);
			attackCombo[indexUsed].areaEffect.enabled = false;
			StopAllCoroutines();
			StartCoroutine("RestAttack");
		}


		[System.Serializable]
		public class AttackCombo
		{
			/// <summary>
			/// Total AttackTime
			/// </summary>
			public float attackTime = .8f;
			/// <summary>
			/// Time when attack Trigger started
			/// </summary>
			public float triggerStartTime = .2f;
			/// <summary>
			/// Time when player can go to next combo
			/// </summary>
			public float transitionToNextAttack = .5f;
			/// <summary>
			/// Time when attack Trigger End
			/// </summary>
			public float triggerEndTime = .7f;
			/// <summary>
			/// Damage Taken
			/// </summary>
			public int damage = 30;
			/// <summary>
			/// Area Effect
			/// </summary>
#if PROFORMER3D
			public Collider areaEffect;
#else
			public Collider2D areaEffect;
#endif
			public bool comboExecuted = false;
			/// <summary>
			/// SFX
			/// </summary>
			public AudioClip attackClip;

#if UNITY_EDITOR
			/// <summary>
			/// EDITOR THINGS
			/// </summary>
			[SerializeField, HideInInspector]
			public bool showOptions = false;
#endif
		}
	}
}
