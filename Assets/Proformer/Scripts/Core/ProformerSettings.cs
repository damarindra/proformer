﻿using UnityEngine;
using System.Collections;

#if UNITY_EDITOR
using UnityEditor;
namespace DI.Proformer {

	public class ProformerSettings {

		public enum WorldType {
			ThreeD, TwoD
		}
		[HideInInspector]
		public static WorldType worldType { get {
				if (InternalTool.isContainsDefines("PROFORMER3D"))
					return WorldType.ThreeD;
				else
					return WorldType.TwoD;
			}
			protected set { } }
		public static bool debugRay { get { return EditorPrefs.GetBool("ProformerDebugRay", true); } protected set { EditorPrefs.SetBool("ProformerDebugRay", value); } }
		public static bool showHierarchyIcon { get { return EditorPrefs.GetBool("ProformerHierarchyIcon", true); } protected set { EditorPrefs.SetBool("ProformerHierarchyIcon", value); } }
		public static bool visualizeLogic { get { return EditorPrefs.GetBool("ProformerVisualizeLogic", true); } protected set { EditorPrefs.SetBool("ProformerVisualizeLogic", value); } }
		public static bool showLevelBoundsInfo { get { return EditorPrefs.GetBool("ProformerLevelBoundsInfo", true); } protected set { EditorPrefs.SetBool("ProformerLevelBoundsInfo", value); } }
		public static bool creationPopup { get { return EditorPrefs.GetBool("ProformerCreationEntityPopup", true); } protected set { EditorPrefs.SetBool("ProformerCreationEntityPopup", value); } }
		
		public static Texture2D arrowImage {
			get {
				if (_arrowImage != null)
					return _arrowImage;
				else {
					_arrowImage = AssetDatabase.LoadAssetAtPath("Assets/Proformer/Resources/Images/arrow.png", typeof(Texture2D)) as Texture2D;
					return _arrowImage;
				}
			}
		}

		static bool isCompleteInit = false;
		static Texture2D _arrowImage;


		enum SettingMenu { World, Preferences }
		static SettingMenu settingMenu = SettingMenu.World;
		public static string[] MonoBehaviourPublicMethod = new string[23] {
			"Invoke","InvokeRepeating","CancelInvoke","IsInvoking","StartCoroutine","StartCoroutine_Auto","StopCoroutine","StopAllCoroutines","GetComponent","GetComponentInChildren","GetComponentsInChildren","GetComponentInParent","GetComponentsInParent","GetComponents","CompareTag","SendMessageUpwards","SendMessage","BroadcastMessage","ToString","Equals","GetHashCode","GetInstanceID","GetType"
		};
		[PreferenceItem("Proformer")]
		static void OnGUI()
		{
			if (!isCompleteInit)
			{
				if (EditorPrefs.GetBool("ProformerImportInitialize", false)) {
					if (!InternalTool.isContainsDefines("PROFORMER3D"))
						InternalTool.SetDefine("PROFORMER3D");
					EditorPrefs.SetBool("ProformerImportInitialize", true);
				}
				isCompleteInit = true;
			}

			EditorGUI.BeginChangeCheck();

			settingMenu = (SettingMenu)GUILayout.Toolbar((int)settingMenu, new string[2] { "World", "Preferences" });
			Rect lastRect = GUILayoutUtility.GetLastRect();
			EditorGUI.DrawRect(new Rect(lastRect.x, lastRect.y + EditorGUIUtility.singleLineHeight * 1.4f, lastRect.width, EditorGUIUtility.singleLineHeight * .3f), new Color(0, 0, 0, .4f));
			GUILayout.Space(10);

			if (settingMenu == SettingMenu.World)
			{
				if (EditorApplication.isCompiling)
					GUI.enabled = false;
				EditorGUILayout.LabelField("WORLD TYPE", EditorStyles.boldLabel);

				WorldType _wType = (WorldType)GUILayout.SelectionGrid((int)worldType, new string[2] { "3D", "2D" }, 2, GUILayout.Height(40));

				EditorGUILayout.HelpBox("WARNING!! Changing world type in the middle development will be make all gameobject created incompatible. Setup World Type only at the first time", MessageType.Warning);
				if (worldType != _wType)
				{
					if (_wType == WorldType.ThreeD)
						InternalTool.SetDefine("PROFORMER3D");
					else if (_wType == WorldType.TwoD)
						InternalTool.UnSetDefine("PROFORMER3D");
				}
				GUI.enabled = true;
			}
			else
			{
				EditorGUILayout.HelpBox("Hover to show tooltip", MessageType.Info);
				
				debugRay = EditorGUILayout.Toggle(new GUIContent("Debug Ray", "Show Debug Ray when game is playing in Editor"), debugRay);
				showHierarchyIcon = EditorGUILayout.Toggle(new GUIContent("Show Hierarchy Icon", "Showing unique Icon on Hierarchy"), showHierarchyIcon);
				visualizeLogic = EditorGUILayout.Toggle(new GUIContent("Visualize Logic", "Show logic direction with bezier curve when selected. Can be found at AreaTrigger, InputAreaTrigger, IntCompare, etc"), visualizeLogic);
				showLevelBoundsInfo = EditorGUILayout.Toggle(new GUIContent("Show Level Bounds Info", "Show level bounds info"), showLevelBoundsInfo);
				creationPopup = EditorGUILayout.Toggle(new GUIContent("Creation Popup", "When create a new Proformer Entity Object, showing Popup for configure the Level Path Position before create a new GameObject and if any gameobject selected, set as renderer option will be show"), creationPopup);
			}
			

		}
	}

}
#endif
