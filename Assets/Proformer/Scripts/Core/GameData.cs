﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DI.PoolingSystem;
using System;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace DI.Proformer.ToolHelper {
	[System.Serializable]
	public class GameData : ScriptableObject {
		public static string filePath { get {
				return "Assets/Proformer/Resources/" + folderName + "/" + fileName + ".asset";
			}
		}
		static string folderName = "GamePreferences";
		static string fileName = "GameData";

		public static string resourceFilePath = "GamePreferences/GameData";
		public static GameData Instance
		{get {
				if (_gameData)
					return _gameData;
				else {
#if UNITY_EDITOR
					_gameData = AssetDatabase.LoadAssetAtPath(filePath, typeof(GameData)) as GameData;
					if (_gameData == null)
					{
						_gameData = CreateInstance<GameData>();
						if (!AssetDatabase.IsValidFolder("Assets/Proformer/Resources/" + folderName))
							AssetDatabase.CreateFolder("Assets/Proformer/Resources", folderName);
						AssetDatabase.CreateAsset(_gameData, filePath);
						AssetDatabase.SaveAssets();
					}
#else
					_gameData = Resources.Load(resourceFilePath, typeof(GameData)) as GameData;
#endif
					return _gameData;
				}
			}
			protected set {
				_gameData = value;
			}
		}
		static GameData _gameData;
		
		public bool showGameScreen, showBGM, showCheckPoint, showPoolCandidate;
		public List<string> scenes = new List<string>();
		public float fadeDuration = .5f;
		public bool coroutineLoad = true;

		public enum GameScreenType
		{
			MainMenu, Gameplay
		}
		//[SerializeField]
		//public Dictionary<string, GameScreenType> gameScreen = new Dictionary<string, GameScreenType>();
		public List<GameScreen> gameScreen = new List<GameScreen>();


		[System.Serializable]
		public class GameScreen {
			public string scene;
			public GameScreenType gameScreenType;

			public GameScreen(string scene, GameScreenType type) {
				this.scene = scene;
				gameScreenType = type;
			}
		}
		
		public enum CheckPointType
		{
			LastStand, Placing
		}
		public CheckPointType checkPointType = CheckPointType.LastStand;

		[System.Serializable]
		public class BackgroundMusic {

			public string scene;
			public AudioClip backgroundMusic;

			public BackgroundMusic(string scene, AudioClip music)
			{
				this.scene = scene;
				backgroundMusic = music;
			}
		}
		public List<BackgroundMusic> backgroundMusics = new List<BackgroundMusic>();
		
		public List<PoolingManager.PoolCandidate> objectWillBePooled = new List<PoolingManager.PoolCandidate>();
		public GameObject sfxTemplate;

#if UNITY_EDITOR
		public void CheckAndUpdateIfNecessary() {
			if (scenes.Count != EditorBuildSettings.scenes.Length) {
				UpdateScenes();
				UpdateGameScreenType();
				UpdateBGM();
			}
		}
		public void UpdateScenes()
		{
			scenes.Clear();
			for (int i = 0; i < EditorBuildSettings.scenes.Length; i++)
			{
				if (!EditorBuildSettings.scenes[i].enabled) continue;

				string scenePath = EditorBuildSettings.scenes[i].path;
				string[] seperators = new string[2] { "/", "." };
				string[] splitPath = scenePath.Split(seperators, System.StringSplitOptions.None);

				scenes.Add(splitPath[splitPath.Length - 2]);
			}
		}
#endif

		public void UpdateGameScreenType()
		{
			List<GameData.GameScreen> oldGameScreen = new List<GameData.GameScreen>(gameScreen);
			gameScreen.Clear();
			//Updating
			for (int i = 0; i < scenes.Count; i++)
			{
				bool isSameScene = false;
				//Check Same Scene
				foreach (GameData.GameScreen gs in oldGameScreen.ToArray())
				{
					if (gs.scene.Equals(scenes[i]))
					{
						gameScreen.Add(new GameData.GameScreen(scenes[i], gs.gameScreenType));
						isSameScene = true;
						break;
					}

				}
				if (!isSameScene)
					gameScreen.Add(new GameData.GameScreen(scenes[i], GameData.GameScreenType.Gameplay));
			}
		}
		public void UpdateBGM()
		{
			List<GameData.BackgroundMusic> oldGameScreen = new List<GameData.BackgroundMusic>(backgroundMusics);
			backgroundMusics.Clear();
			//Updating
			for (int i = 0; i < scenes.Count; i++)
			{
				bool isSameScene = false;
				//Check Same Scene
				foreach (GameData.BackgroundMusic gs in oldGameScreen.ToArray())
				{
					if (gs.scene.Equals(scenes[i]))
					{
						backgroundMusics.Add(new GameData.BackgroundMusic(scenes[i], gs.backgroundMusic));
						isSameScene = true;
						break;
					}

				}
				if (!isSameScene)
					backgroundMusics.Add(new GameData.BackgroundMusic(scenes[i], null));

			}
		}

		[SerializeField]
		public List<Type> customActionLib() {
			List<Type> actions = new List<Type>();
			foreach (string actStr in customActionLibString) {
				Type type = Type.GetType(actStr);
				actions.Add(type);
			}
			return actions;
		}
		public List<string> customActionLibString = new List<string>();
	}
}
