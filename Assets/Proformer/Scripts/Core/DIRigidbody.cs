﻿using UnityEngine;
using System.Collections;
using DI.Proformer.ToolHelper;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace DI.Proformer {
	[System.Serializable]
	public class DIRigidbody {
	#if PROFORMER3D
		public Rigidbody rb;
		public DIRigidbody(Rigidbody rb) {
			this.rb = rb;
		}
#else
		public Rigidbody2D rb;
		public DIRigidbody(Rigidbody2D rb) {
			this.rb = rb;
		}
#endif
	}
}
