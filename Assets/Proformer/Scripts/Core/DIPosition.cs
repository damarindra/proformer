﻿using UnityEngine;
using System.Collections;
using DI.Proformer.Manager;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace DI.Proformer {
	[System.Serializable]
	public class DIPosition {

		/// <summary>
		/// DONT EDIT THIS
		/// </summary>
		//Used in Editor too/
		public float rawHorizontal { get { return getRawHorizontalFromWorld(); } }
		public float rawVertical { get { return getRawVerticalFromWorld(); } }
		public Vector3 position {
			get {
				if (transform != null) {
					if (transform.position != _position)
						_position = transform.position;
				}
				return _position;
			} 
			set{
				if (transform != null) {
					transform.position = value;
				}
				_position = value;
			}}
		[SerializeField]
		private Vector3 _position;
		[SerializeField]
		private Transform transform;
		public int levelPathPosition { set { _levelPathPosition = value; }
			get {
				if (LevelBounds.instance) {
					if (LevelBounds.instance.levelPath.Count == 0)
						LevelBounds.instance.GatherLevelPath();
					if (_levelPathPosition > LevelBounds.instance.levelPath.Count - 1)
						_levelPathPosition = LevelBounds.instance.levelPath.Count - 1;
					else if (_levelPathPosition < 0)
						_levelPathPosition = 0;
				}

				return _levelPathPosition;
			}
		}
		[SerializeField]
		private int _levelPathPosition = 0;
		public LevelPath levelPath { get {
				if (LevelBounds.instance)
				{
					return LevelBounds.instance.levelPath[levelPathPosition];
				}
				else return null;
			}
		}

		public DIPosition(Transform transform) {
			this.transform = transform;
		}
		public DIPosition() { }
		public DIPosition(Vector3 position) {
			_position = position;
		}
		public DIPosition(int levelPathPosition, Vector3 position)
		{
			this.levelPathPosition = levelPathPosition;
			_position = position;
		}
		public void Init(Transform transform) {
			this.transform = transform;
		}

		/// <summary>Updating DIPosition.position with transform.position</summary>
		/// <returns>return false if failed</returns>
		public bool UpdatePosition() {
			if (transform) {
				position = transform.position;
				return true;
			}
			return false;
		}

		/// <summary>
		/// move level path position with add index
		/// </summary>
		/// <param name="indexAddition">Addition Index</param>
		/// <param name="self">Transform that will be automatically set the position</param>
		public void MoveLevelPathPosition(int indexAddition) {
			SetLevelPathPosition(levelPathPosition + indexAddition);
		}
		/// <summary>
		/// Set the level Position with auto fix position with old rawHorizontal and rawVertical
		/// </summary>
		/// <param name="position">Level Path Position</param>
		/// <param name="self">Transform that will be automatically set the position</param>
		public void SetLevelPathPosition(int position) {
			float horizontal = rawHorizontal;
			float vertical = rawVertical;
			levelPathPosition = position;
			this.position = GetPositionFromRaw(horizontal, vertical, levelPathPosition);
		}
		public void SetPosition(int levelPathPosition, Vector3 position) {
			this.levelPathPosition = levelPathPosition;
			this.position = position;
		}

		/// <summary>
		/// This function is for fix the position if out of levelpath direction. Get from current position
		/// </summary>
		/// <param name="updateCurrentPosition">Set new value to position</param>
		/// <returns>result position</returns>
		public Vector3 FixPositionRelativeToLevelPath(bool updateCurrentPosition = true)
		{
			if (LevelBounds.instance == null)
				return Vector3.zero;
			Vector3 v = GetPositionFromRaw(rawHorizontal, rawVertical, levelPathPosition);

			if (updateCurrentPosition) {
				position = v;
			}

			return v;
		}
		/// <summary>
		/// Calculate Position, Fix it, and set the position
		/// </summary>
		/// <param name="position">Target position will be recalculate</param>
		/// <returns>result position</returns>
		public Vector3 SetPositionAndFixIt(Vector3 position)
		{
			if (LevelBounds.instance == null)
				return Vector3.zero;
			this.position = position;

			Vector3 v = GetPositionFromRaw(rawHorizontal, rawVertical, levelPathPosition);
			this.position = v;

			return v;
		}
		/// <summary>
		/// Calculate Position and Fix It, without set the position
		/// </summary>
		/// <param name="position">Target position will be recalculate</param>
		/// <returns>result position</returns>
		public Vector3 CalculatePositionAndFixIt(Vector3 position)
		{
			if (LevelBounds.instance == null)
				return Vector3.zero;
			return GetPositionFromRaw(getRawHorizontal(position), getRawVertical(position), levelPathPosition);
		}

		public Vector3 GetPositionFromRaw(float rawHorizontal, float rawVertical, int levelPathPosition)
		{
			if (LevelBounds.instance == null)
				return Vector3.zero;
			Vector3 lbPDir = LevelBounds.instance.levelPath[levelPathPosition].direction;
			Vector3 v = levelPath.from;
			v += lbPDir * rawHorizontal * levelPath.length;
			v.y = LevelBounds.instance.bottom + ((LevelBounds.instance.top - LevelBounds.instance.bottom) * rawVertical);
			return v;
		}

		float getRawHorizontalFromWorld()
		{
			if (LevelBounds.instance == null || levelPath == null)
				return 0;

			Vector3 from = levelPath.from;
			from.y = 0;
			Vector3 currentPos = position;
			currentPos.y = 0;
			float length = Vector3.Distance(from, currentPos);
			float result = length / Vector3.Distance(levelPath.from, levelPath.to);

			if (result > 1)
				result = 1;
			else if (Vector3.Angle((currentPos - from).normalized, levelPath.direction) > 90)
				result = 0;

			return result;
		}
		float getRawVerticalFromWorld()
		{
			float result;
			if (LevelBounds.instance)
				result = (position.y - LevelBounds.instance.bottom) / (LevelBounds.instance.top - LevelBounds.instance.bottom);
			else
				return 0;

			if (result > 1)
				result = 1;
			else if (result < 0)
				result = 0;

			return result;
		}
		float getRawHorizontal(Vector3 position)
		{
			if (LevelBounds.instance == null || levelPath == null)
				return 0;

			Vector3 from = levelPath.from;
			from.y = 0;
			Vector3 currentPos = position;
			currentPos.y = 0;
			float length = Vector3.Distance(from, currentPos);
			float result = length / Vector3.Distance(levelPath.from, levelPath.to);

			if (result > 1)
				result = 1;
			else if (Vector3.Angle((currentPos - from).normalized, levelPath.direction) > 90)
				result = 0;

			return result;
		}
		float getRawVertical(Vector3 position)
		{
			float result;
			if (LevelBounds.instance)
				result = (position.y - LevelBounds.instance.bottom) / (LevelBounds.instance.top - LevelBounds.instance.bottom);
			else
				return 0;

			if (result > 1)
				result = 1;
			else if (result < 0)
				result = 0;

			return result;
		}
	}
	
}
