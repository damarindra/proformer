﻿using UnityEngine;
using System.Collections;

namespace DI.Proformer {
	[System.Serializable]
	public class DICharacterCollider {

		private GameObject body = null;

		public bool enabled
		{
			get
			{
				if (VerifySettings()) { return collider.enabled; }
				else
				{
#if PROFORMER3D
					Debug.LogError("Your Object is incompatible. Current settings is set as 3D, but your object is 2D type!");
#else
					Debug.LogError("Your Object is incompatible. Current settings is set as 2D, but your object is 3D type!");
#endif
#if UNITY_EDITOR
					if (UnityEditor.EditorApplication.isPlaying)
						UnityEditor.EditorApplication.isPaused = true;
#endif
					return false;
				}
			}
			set
			{
				if (VerifySettings()) { collider.enabled = value; }
				else
				{
#if PROFORMER3D
					Debug.LogError("Your Object is incompatible. Current settings is set as 3D, but your object is 2D type!");
#else
					Debug.LogError("Your Object is incompatible. Current settings is set as 2D, but your object is 3D type!");
#endif

#if UNITY_EDITOR
					if (UnityEditor.EditorApplication.isPlaying)
						UnityEditor.EditorApplication.isPaused = true;
#endif
				}
			}
		}
#if !PROFORMER3D

		public BoxCollider2D collider;
		public Vector3 center
		{
			get
			{
				if (VerifySettings())
				{
					return collider.offset;
				}
				else
				{
					Debug.LogError("Your Object is incompatible. Current settings is set as 2D, but your object is 3D type!");
#if UNITY_EDITOR
					if (UnityEditor.EditorApplication.isPlaying)
						UnityEditor.EditorApplication.isPaused = true;
#endif
					return Vector3.zero;
				}
			}
			set
			{
				if (VerifySettings())
				{
					collider.offset = value;
				}
				else
				{
					Debug.LogError("Your Object is incompatible. Current settings is set as 2D, but your object is 3D type!");

#if UNITY_EDITOR
					if (UnityEditor.EditorApplication.isPlaying)
						UnityEditor.EditorApplication.isPaused = true;
#endif
				}
			}
		}
		public float height {
			get {

				if (VerifySettings())
				{ return collider.size.y; }
				else
				{
					Debug.LogError("Your Object is incompatible. Current settings is set as 2D, but your object is 3D type!");
#if UNITY_EDITOR
					if (UnityEditor.EditorApplication.isPlaying)
						UnityEditor.EditorApplication.isPaused = true;
#endif
					return -1;
				}
			} set
			{
				if (VerifySettings())
					collider.size = new Vector2(collider.size.x, value);
				else
				{
					Debug.LogError("Your Object is incompatible. Current settings is set as 2D, but your object is 3D type!");

#if UNITY_EDITOR
					if (UnityEditor.EditorApplication.isPlaying)
						UnityEditor.EditorApplication.isPaused = true;
#endif
				}
			}
		}
		public float width {
			get {
				if (VerifySettings()) return collider.size.x;
				else
				{
					Debug.LogError("Your Object is incompatible. Current settings is set as 2D, but your object is 3D type!");
#if UNITY_EDITOR
					if (UnityEditor.EditorApplication.isPlaying)
						UnityEditor.EditorApplication.isPaused = true;
#endif
					return -1;
				}
			}
			set {
				if (VerifySettings()) collider.size = new Vector2(value, collider.size.y);
				else
				{
					Debug.LogError("Your Object is incompatible. Current settings is set as 2D, but your object is 3D type!");

#if UNITY_EDITOR
					if (UnityEditor.EditorApplication.isPlaying)
						UnityEditor.EditorApplication.isPaused = true;
#endif
				}
			} }
		/// <summary>
		/// This just raw in 2D, useless
		/// </summary>
		public float depth { get { return 0; } }

		public DICharacterCollider(){ }
		public DICharacterCollider(BoxCollider2D bc) {
			this.collider = bc;
		}
		public DICharacterCollider(GameObject body)
		{
			this.body = body;
			this.collider = body.AddComponent<BoxCollider2D>();
		}
		public bool VerifySettings() {
			bool result = true;
			if (collider == null && body != null)
			{
				collider = body.GetComponent<BoxCollider2D>();
				if (collider == null && body.GetComponent<BoxCollider>() != null)
					result = false;
			}
			else if(collider != null){
				if(collider.ToString() == collider.gameObject.name + " (UnityEngine.BoxCollider)"){
					result = false;
				}
			}
			if (result == false) {
				collider = null;
			}
			return result;
		}
		
#else

		public BoxCollider collider;
		public Vector3 center {
			get
			{
				if(VerifySettings())
				{
					return collider.center;
				}
				else
				{
					Debug.LogError("Your Object is incompatible. Current settings is set as 3D, but your object is 2D type!");
#if UNITY_EDITOR
					if (UnityEditor.EditorApplication.isPlaying)
						UnityEditor.EditorApplication.isPaused = true;
#endif
					return Vector3.zero;
				}
			}
			set
			{
				if(VerifySettings())
				{
					collider.center = value;
				}
				else
				{
					Debug.LogError("Your Object is incompatible. Current settings is set as 3D, but your object is 2D type!");

#if UNITY_EDITOR
					if (UnityEditor.EditorApplication.isPlaying)
						UnityEditor.EditorApplication.isPaused = true;
#endif
				}
			}
		}
		public float height {
			get {
				if(VerifySettings())
				{
					return collider.size.y;
				}
				else
				{
					Debug.LogError("Your Object is incompatible. Current settings is set as 3D, but your object is 2D type!");
#if UNITY_EDITOR
					if (UnityEditor.EditorApplication.isPlaying)
						UnityEditor.EditorApplication.isPaused = true;
#endif
					return -1;
				}
			} set
			{
				if(VerifySettings()) {
					collider.size = new Vector3(collider.size.x, value, collider.size.z);
				}
				else
				{
					Debug.LogError("Your Object is incompatible. Current settings is set as 3D, but your object is 2D type!");

#if UNITY_EDITOR
					if (UnityEditor.EditorApplication.isPlaying)
						UnityEditor.EditorApplication.isPaused = true;
#endif
				}
			}
		}
		public float depth
		{
			get
			{
				if(VerifySettings())
				{
					return collider.size.z;
				}
				else
				{
					Debug.LogError("Your Object is incompatible. Current settings is set as 3D, but your object is 2D type!");

#if UNITY_EDITOR
					if (UnityEditor.EditorApplication.isPlaying)
						UnityEditor.EditorApplication.isPaused = true;
#endif
					return -1;
				}
			}
			set
			{
				if(VerifySettings()) {
					collider.size = new Vector3(collider.size.x, collider.size.y, value);
				}
				else
				{
					Debug.LogError("Your Object is incompatible. Current settings is set as 3D, but your object is 2D type!");

#if UNITY_EDITOR
					if (UnityEditor.EditorApplication.isPlaying)
						UnityEditor.EditorApplication.isPaused = true;
#endif
				}
			}
		}
		//public float depth { get { return collider.size.x; } set { collider.size = new Vector3(value, collider.size.y, collider.size.z); } }
		public float width {
			get { if(VerifySettings()) { return collider.size.x; }
				else
				{
					Debug.LogError("Your Object is incompatible. Current settings is set as 3D, but your object is 2D type!");

#if UNITY_EDITOR
					if (UnityEditor.EditorApplication.isPlaying)
						UnityEditor.EditorApplication.isPaused = true;
#endif
					return -1;
				}
			} set { if(VerifySettings()) { collider.size = new Vector3(value, collider.size.y, collider.size.z); }
				else
				{
					Debug.LogError("Your Object is incompatible. Current settings is set as 3D, but your object is 2D type!");

#if UNITY_EDITOR
					if (UnityEditor.EditorApplication.isPlaying)
						UnityEditor.EditorApplication.isPaused = true;
#endif
				}
			}
		}
		//public float width { get { return collider.size.z; } set { collider.size = new Vector3(collider.size.x, collider.size.y, value); } }
		
		public DICharacterCollider() { }
		public DICharacterCollider(BoxCollider collider) {
			this.collider = collider;
		}
		public DICharacterCollider(GameObject body)
		{
			this.body = body;
			this.collider = body.AddComponent<BoxCollider>();
		}
		
		public bool VerifySettings() {
			bool result = true;
			if (collider == null && body != null)
			{
				collider = body.GetComponent<BoxCollider>();
				if (collider == null && body.GetComponent<BoxCollider2D>() != null)
					result = false;
			}
			else if (collider != null) {
				if (collider.ToString() == collider.gameObject.name + " (UnityEngine.BoxCollider2D)")
				{
					result = false;
				}
			}
			if (result == false) {
				collider = null;
			}
			return result;
		}
#endif
	}
}
