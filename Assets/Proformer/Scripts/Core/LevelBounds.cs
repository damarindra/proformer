﻿using UnityEngine;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
using UnityEditorInternal;
#endif

namespace DI.Proformer.Manager{
	/// <summary>
	/// Level Bounds, set up your level Path with this script
	/// </summary>
	[System.Serializable]
	public class LevelBounds : MonoBehaviour {
		public static LevelBounds instance { get {
				if (_instance == null)
					_instance = FindObjectOfType<LevelBounds>();
				return _instance;
			} }
		static LevelBounds _instance = null;

		public bool loop = false;
		public Color lineColor = Color.white;
		/// <summary>
		/// All Path in Level Bounds
		/// </summary>
		public List<Vector3> points = new List<Vector3> {new Vector3(-10, 0), new Vector3(10, 0) };

		[HideInInspector]
		public Vector3 startPosition;

		/// <summary>
		/// All Level Path
		/// </summary>
		public List<LevelPath> levelPath = new List<LevelPath>();

		[SerializeField]
		public Dictionary<int, LevelPath> objectDictionary = new Dictionary<int, LevelPath>();

		public float top = 5f;
		public float bottom = -5f;
		public int defaultLevelPathIndex = 0;
		public enum BoundReach {
			Constrait, Dead
		}
		public BoundReach topBound = BoundReach.Constrait, bottomBound = BoundReach.Dead;

		void Awake(){
			if (_instance == null)
				_instance = this;
			else if (_instance != this)
				Destroy(gameObject);

		}

		void Start() {
			GatherLevelPath();
		}

		/// <summary>
		/// Gather all Level Path
		/// </summary>
		public void GatherLevelPath() {
			levelPath.Clear();
			int i = 0;

			while (i < points.Count - 1)
			{
				levelPath.Add(new LevelPath(points[i], points[i + 1]));
				i++;
			}
			if(loop)
				levelPath.Add(new LevelPath(points[points.Count - 1], points[0]));
		}

		void OnDrawGizmos(){
			Gizmos.color = lineColor;

			if (points.Count == 0)
				return;

			int i = 0;


			while (i < points.Count - 1) {
				Gizmos.DrawLine (points [i], points [i + 1]);
				Gizmos.DrawLine (new Vector3 (points [i].x, top, points [i].z), new Vector3 (points [i].x, bottom, points [i].z));
				Gizmos.DrawLine (new Vector3 (points [i].x, top, points [i].z), new Vector3 (points [i+1].x, top, points [i+1].z));
				Gizmos.DrawLine (new Vector3 (points [i+1].x, top, points [i+1].z), new Vector3 (points [i+1].x, bottom, points [i+1].z));
				Gizmos.DrawLine (new Vector3 (points [i].x, bottom, points [i].z), new Vector3 (points [i+1].x, bottom, points [i+1].z));

				i++;
			}

			if (loop) {
				Gizmos.DrawLine(points[0], points[points.Count-1]);
				Gizmos.DrawLine(new Vector3(points[0].x, top, points[0].z), new Vector3(points[0].x, bottom, points[0].z));
				Gizmos.DrawLine(new Vector3(points[0].x, top, points[0].z), new Vector3(points[points.Count - 1].x, top, points[points.Count - 1].z));
				Gizmos.DrawLine(new Vector3(points[points.Count-1].x, top, points[points.Count - 1].z), new Vector3(points[points.Count - 1].x, bottom, points[points.Count - 1].z));
				Gizmos.DrawLine(new Vector3(points[0].x, bottom, points[0].z), new Vector3(points[points.Count - 1].x, bottom, points[points.Count - 1].z));
				
			}
		}


#region static
		public static float YToRawVertical(float y) {
			return y / Mathf.Abs(instance.top - instance.bottom);
		}
		public static float XToRawHorizontal(LevelPath levelPath, float x) {
			return x / Vector3.Distance(levelPath.from, levelPath.to);
		}
		public static Vector3 RawToWorldVector(LevelPath lp, float horizontal, float vertical)
		{
			Vector3 lbPDir = lp.direction;
			Vector3 v = lp.from;
			v += lbPDir * horizontal * lp.length;
			if (LevelBounds.instance != null)
				v.y = LevelBounds.instance.bottom + ((LevelBounds.instance.top - LevelBounds.instance.bottom) * vertical);
			else
			{
				LevelBounds lb = GameObject.FindObjectOfType<LevelBounds>();
				v.y = lb.bottom + ((lb.top - lb.bottom) * vertical);
			}

			return v;
		}
		public static Vector3 RawToWorldVector(DIEntity entity)
		{
			Vector3 lbPDir = entity.position.levelPath.direction;
			Vector3 v = entity.position.levelPath.from;
			v += lbPDir * entity.position.rawHorizontal * entity.position.levelPath.length;
			if (LevelBounds.instance == null)
				v.y = LevelBounds.instance.bottom + ((LevelBounds.instance.top - LevelBounds.instance.bottom) * entity.position.rawVertical);
			else
			{
				LevelBounds lb = GameObject.FindObjectOfType<LevelBounds>();
				v.y = lb.bottom + ((lb.top - lb.bottom) * entity.position.rawVertical);
			}

			return v;
		}
		public static Vector2 WorldVectorToRaw(LevelPath lp, Vector3 position)
		{
			Vector2 raw = new Vector2();
			if (LevelBounds.instance == null)
				raw.y = (position.y - FindObjectOfType<LevelBounds>().bottom) / (FindObjectOfType<LevelBounds>().top - FindObjectOfType<LevelBounds>().bottom);
			else
				raw.y = (position.y - LevelBounds.instance.bottom) / (LevelBounds.instance.top - LevelBounds.instance.bottom);
			raw.y = raw.y > 1 ? 1 : raw.y < 0 ? 0 : raw.y;

			Vector3 from = lp.from;
			from.y = 0;
			Vector3 currentPos = position;
			currentPos.y = 0;
			float length = Vector3.Distance(from, currentPos);
			raw.x = length / Vector3.Distance(lp.from, lp.to);
			raw.x = raw.x > 1 ? 1 : raw.x < 0 ? 0 : raw.x;

			return raw;
		}
		public static Vector2 WorldVectorToRaw(DIEntity entity)
		{
			Vector2 raw = new Vector2();
			if (LevelBounds.instance != null)
				raw.y = (entity.transform.position.y - FindObjectOfType<LevelBounds>().bottom) / (FindObjectOfType<LevelBounds>().top - FindObjectOfType<LevelBounds>().bottom);
			else
				raw.y = (entity.transform.position.y - LevelBounds.instance.bottom) / (LevelBounds.instance.top - LevelBounds.instance.bottom);
			raw.y = raw.y > 1 ? 1 : raw.y < 0 ? 0 : raw.y;

			Vector3 from = entity.position.levelPath.from;
			from.y = 0;
			Vector3 currentPos = entity.transform.position;
			currentPos.y = 0;
			float length = Vector3.Distance(from, currentPos);
			raw.x = length / Vector3.Distance(entity.position.levelPath.from, entity.position.levelPath.to);
			raw.x = raw.x > 1 ? 1 : raw.x < 0 ? 0 : raw.x;

			return raw;
		}
#endregion
	}
}