﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

#if UNITY_EDITOR
using UnityEditor;
using UnityEditorInternal;
#endif

namespace DI.Proformer.ToolHelper {
	/// <summary>
	/// Simple Draw Mesh Collider
	/// </summary>
	[System.Obsolete("In the future will be removed. No need to create this messy things. Focus on Platformer!")]
	[RequireComponent(typeof(MeshFilter)), RequireComponent(typeof(MeshCollider))]
	public class ProformerSimpleCollider : MonoBehaviour {

		//public List<Vector3> coreVertex = new List<Vector3>() { new Vector3(-1, 0, 0), new Vector3(1, 0, 0) };
		public List<FaceVertex> coreVertex = new List<FaceVertex>() { new FaceVertex(new Vector3(-1, 0, 0), 1), new FaceVertex(new Vector3(1, 0, 0), 1) };

		public int resolution = 4;

		[System.Serializable]
		public class FaceVertex {
			public Vector3 vertex;
			public float height;
			public Vector3 directionWidth = Vector3.forward;
			public Vector3 directionHeight = Vector3.down;

			public FaceVertex(Vector3 vertex, float height)
			{
				this.vertex = vertex;
				this.height = height;
			}
		}
		public float width = 5f;

		public void CreateMesh() {
			// You can change that line to provide another MeshFilter
			MeshFilter meshFilter = GetComponent<MeshFilter>();
			if (meshFilter == null)
			{
				Debug.LogError("MeshFilter not found!");
				return;
			}

			Mesh mesh = new Mesh();
			//meshFilter.mesh = new Mesh();
			meshFilter.sharedMesh = mesh;
			mesh.Clear();

			for (int i = 0; i < coreVertex.Count - 1; i++) {
				Vector3[] points = new Vector3[8];

				points[0] = coreVertex[i].vertex + coreVertex[i].directionWidth * width + coreVertex[i].directionHeight * coreVertex[i].height;
				points[1] = coreVertex[i + 1].vertex + coreVertex[i + 1].directionWidth * width + coreVertex[i].directionHeight * coreVertex[i + 1].height;
				points[2] = coreVertex[i + 1].vertex - coreVertex[i + 1].directionWidth * width + coreVertex[i].directionHeight * coreVertex[i + 1].height;
				points[3] = coreVertex[i].vertex - coreVertex[i].directionWidth * width + coreVertex[i].directionHeight * coreVertex[i].height;

				points[4] = coreVertex[i].vertex + coreVertex[i].directionWidth * width;
				points[5] = coreVertex[i + 1].vertex + coreVertex[i + 1].directionWidth * width;
				points[6] = coreVertex[i + 1].vertex - coreVertex[i + 1].directionWidth * width;
				points[7] = coreVertex[i].vertex - coreVertex[i].directionWidth * width;

				if (coreVertex.Count == 2)
					CreateBox(ref mesh, points, i);
				else if (i == 0)
					CreateBox(ref mesh, points, i, true, false);
				else if (i == coreVertex.Count - 2)
					CreateBox(ref mesh, points, i, false, true);
				else
					CreateBox(ref mesh, points, i, false, false);
			}


			mesh.RecalculateBounds();
			;


			GetComponent<MeshCollider>().sharedMesh = mesh;


		}
		
		public void CreateBox(ref Mesh mesh, Vector3[] points, int indexBox, bool leftTriangle = true, bool rightTriangle = true, bool bottomTriangle = true, bool topTriangle = true, bool frontTriangle = true, bool backTriangle = true) {

			#region Vertices
			Vector3[] vertices;
			//if (leftTriangle && rightTriangle)
				vertices = new Vector3[]
				{
				// Bottom
				points[0], points[1], points[2], points[3],
 
				// Left
				points[7], points[4], points[0], points[3],
 
				// Front
				points[4], points[5], points[1], points[0],
 
				// Back
				points[6], points[7], points[3], points[2],
 
				// Right
				points[5], points[6], points[2], points[1],
 
				// Top
				points[7], points[6], points[5], points[4]
				};
			/*else if (!leftTriangle && rightTriangle)
				vertices = new Vector3[]
				{
				// Bottom
				points[0], points[1], points[2], points[3],
 
				// Left
				//points[7], points[4], points[0], points[3],
 
				// Front
				points[4], points[5], points[1], points[0],
 
				// Back
				points[6], points[7], points[3], points[2],
 
				// Right
				points[5], points[6], points[2], points[1],
 
				// Top
				points[7], points[6], points[5], points[4]
				};
			else if (leftTriangle && !rightTriangle)
				vertices = new Vector3[]
				{
				// Bottom
				points[0], points[1], points[2], points[3],
 
				// Left
				points[7], points[4], points[0], points[3],
 
				// Front
				points[4], points[5], points[1], points[0],
 
				// Back
				points[6], points[7], points[3], points[2],
 
				// Right
				//points[5], points[6], points[2], points[1],
 
				// Top
				points[7], points[6], points[5], points[4]
				};
			else
				vertices = new Vector3[]
				{
				// Bottom
				points[0], points[1], points[2], points[3],
 
				// Left
				//points[7], points[4], points[0], points[3],
 
				// Front
				points[4], points[5], points[1], points[0],
 
				// Back
				points[6], points[7], points[3], points[2],
 
				// Right
				//points[5], points[6], points[2], points[1],
 
				// Top
				points[7], points[6], points[5], points[4]
				};*/
			#endregion

			#region Triangles
			int startedIndexTriangle = indexBox * 24;
			/*List<int> triangles = new List<int>();
			if (bottomTriangle)
			{
				// Bottom
				triangles.Add(startedIndexTriangle + 3);
				triangles.Add(startedIndexTriangle + 1);
				triangles.Add(startedIndexTriangle + 0);

				triangles.Add(startedIndexTriangle + 3);
				triangles.Add(startedIndexTriangle + 2);
				triangles.Add(startedIndexTriangle + 1);
			}
			if (leftTriangle) {
				// Left
				triangles.Add(startedIndexTriangle + 3 + 4 * 1);
				triangles.Add(startedIndexTriangle + 1 + 4 * 1);
				triangles.Add(startedIndexTriangle + 0 + 4 * 1);

				triangles.Add(startedIndexTriangle + 3 + 4 * 1);
				triangles.Add(startedIndexTriangle + 2 + 4 * 1);
				triangles.Add(startedIndexTriangle + 1 + 4 * 1);
			}
			if (frontTriangle) {
				// Front
				triangles.Add(startedIndexTriangle + 3 + 4 * 2);
				triangles.Add(startedIndexTriangle + 1 + 4 * 2);
				triangles.Add(startedIndexTriangle + 0 + 4 * 2);

				triangles.Add(startedIndexTriangle + 3 + 4 * 2);
				triangles.Add(startedIndexTriangle + 2 + 4 * 2);
				triangles.Add(startedIndexTriangle + 1 + 4 * 2);
			}
			if (backTriangle)
			{
				// Back
				triangles.Add(startedIndexTriangle + 3 + 4 * 3);
				triangles.Add(startedIndexTriangle + 1 + 4 * 3);
				triangles.Add(startedIndexTriangle + 0 + 4 * 3);

				triangles.Add(startedIndexTriangle + 3 + 4 * 3);
				triangles.Add(startedIndexTriangle + 2 + 4 * 3);
				triangles.Add(startedIndexTriangle + 1 + 4 * 3);
			}
			if (rightTriangle) {
				// Right
				triangles.Add(startedIndexTriangle + 3 + 4 * 4);
				triangles.Add(startedIndexTriangle + 1 + 4 * 4);
				triangles.Add(startedIndexTriangle + 0 + 4 * 4);

				triangles.Add(startedIndexTriangle + 3 + 4 * 4);
				triangles.Add(startedIndexTriangle + 2 + 4 * 4);
				triangles.Add(startedIndexTriangle + 1 + 4 * 4);
			}
			if (topTriangle) {
				// Top
				triangles.Add(startedIndexTriangle + 3 + 4 * 5);
				triangles.Add(startedIndexTriangle + 1 + 4 * 5);
				triangles.Add(startedIndexTriangle + 0 + 4 * 5);

				triangles.Add(startedIndexTriangle + 3 + 4 * 5);
				triangles.Add(startedIndexTriangle + 2 + 4 * 5);
				triangles.Add(startedIndexTriangle + 1 + 4 * 5);
			}
			*/
			int[] triangles;
			if (leftTriangle && rightTriangle)
				triangles = new int[]
				{
				// Bottom
				startedIndexTriangle + 3, startedIndexTriangle + 1, startedIndexTriangle + 0,
				startedIndexTriangle + 3, startedIndexTriangle +  2, startedIndexTriangle + 1,			
 
				// Left
				startedIndexTriangle + 3 + 4 * 1, startedIndexTriangle + 1 + 4 * 1, startedIndexTriangle + 0 + 4 * 1,
				startedIndexTriangle + 3 + 4 * 1, startedIndexTriangle + 2 + 4 * 1, startedIndexTriangle + 1 + 4 * 1,
 
				// Front
				startedIndexTriangle + 3 + 4 * 2, startedIndexTriangle + 1 + 4 * 2, startedIndexTriangle + 0 + 4 * 2,
				startedIndexTriangle + 3 + 4 * 2, startedIndexTriangle + 2 + 4 * 2, startedIndexTriangle + 1 + 4 * 2,
 
				// Back
				startedIndexTriangle + 3 + 4 * 3,startedIndexTriangle +  1 + 4 * 3, startedIndexTriangle + 0 + 4 * 3,
				startedIndexTriangle + 3 + 4 * 3, startedIndexTriangle + 2 + 4 * 3, startedIndexTriangle + 1 + 4 * 3,
 
				// Right
				startedIndexTriangle + 3 + 4 * 4, startedIndexTriangle + 1 + 4 * 4, startedIndexTriangle + 0 + 4 * 4,
				startedIndexTriangle + 3 + 4 * 4, startedIndexTriangle + 2 + 4 * 4, startedIndexTriangle + 1 + 4 * 4,
 
				// Top
				startedIndexTriangle + 3 + 4 * 5, startedIndexTriangle + 1 + 4 * 5, startedIndexTriangle + 0 + 4 * 5,
				startedIndexTriangle + 3 + 4 * 5, startedIndexTriangle + 2 + 4 * 5, startedIndexTriangle + 1 + 4 * 5,

				};
			else if (!leftTriangle && rightTriangle && bottomTriangle && topTriangle)
				triangles = new int[]
				{
				// Bottom
				startedIndexTriangle + 3, startedIndexTriangle + 1, startedIndexTriangle + 0,
				startedIndexTriangle + 3, startedIndexTriangle +  2, startedIndexTriangle + 1,			
 
				// Left
				//startedIndexTriangle + 3 + 4 * 1, startedIndexTriangle + 1 + 4 * 1, startedIndexTriangle + 0 + 4 * 1,
				//startedIndexTriangle + 3 + 4 * 1, startedIndexTriangle + 2 + 4 * 1, startedIndexTriangle + 1 + 4 * 1,
 
				// Front
				startedIndexTriangle + 3 + 4 * 2, startedIndexTriangle + 1 + 4 * 2, startedIndexTriangle + 0 + 4 * 2,
				startedIndexTriangle + 3 + 4 * 2, startedIndexTriangle + 2 + 4 * 2, startedIndexTriangle + 1 + 4 * 2,
 
				// Back
				startedIndexTriangle + 3 + 4 * 3,startedIndexTriangle +  1 + 4 * 3, startedIndexTriangle + 0 + 4 * 3,
				startedIndexTriangle + 3 + 4 * 3, startedIndexTriangle + 2 + 4 * 3, startedIndexTriangle + 1 + 4 * 3,
 
				// Right
				startedIndexTriangle + 3 + 4 * 4, startedIndexTriangle + 1 + 4 * 4, startedIndexTriangle + 0 + 4 * 4,
				startedIndexTriangle + 3 + 4 * 4, startedIndexTriangle + 2 + 4 * 4, startedIndexTriangle + 1 + 4 * 4,
 
				// Top
				startedIndexTriangle + 3 + 4 * 5, startedIndexTriangle + 1 + 4 * 5, startedIndexTriangle + 0 + 4 * 5,
				startedIndexTriangle + 3 + 4 * 5, startedIndexTriangle + 2 + 4 * 5, startedIndexTriangle + 1 + 4 * 5,

				};
			else if (leftTriangle && !rightTriangle && bottomTriangle && topTriangle)
				triangles = new int[]
				{
				// Bottom
				startedIndexTriangle + 3, startedIndexTriangle + 1, startedIndexTriangle + 0,
				startedIndexTriangle + 3, startedIndexTriangle +  2, startedIndexTriangle + 1,			
 
				// Left
				startedIndexTriangle + 3 + 4 * 1, startedIndexTriangle + 1 + 4 * 1, startedIndexTriangle + 0 + 4 * 1,
				startedIndexTriangle + 3 + 4 * 1, startedIndexTriangle + 2 + 4 * 1, startedIndexTriangle + 1 + 4 * 1,
 
				// Front
				startedIndexTriangle + 3 + 4 * 2, startedIndexTriangle + 1 + 4 * 2, startedIndexTriangle + 0 + 4 * 2,
				startedIndexTriangle + 3 + 4 * 2, startedIndexTriangle + 2 + 4 * 2, startedIndexTriangle + 1 + 4 * 2,
 
				// Back
				startedIndexTriangle + 3 + 4 * 3,startedIndexTriangle +  1 + 4 * 3, startedIndexTriangle + 0 + 4 * 3,
				startedIndexTriangle + 3 + 4 * 3, startedIndexTriangle + 2 + 4 * 3, startedIndexTriangle + 1 + 4 * 3,
 
				// Right
				//startedIndexTriangle + 3 + 4 * 4, startedIndexTriangle + 1 + 4 * 4, startedIndexTriangle + 0 + 4 * 4,
				//startedIndexTriangle + 3 + 4 * 4, startedIndexTriangle + 2 + 4 * 4, startedIndexTriangle + 1 + 4 * 4,
 
				// Top
				startedIndexTriangle + 3 + 4 * 5, startedIndexTriangle + 1 + 4 * 5, startedIndexTriangle + 0 + 4 * 5,
				startedIndexTriangle + 3 + 4 * 5, startedIndexTriangle + 2 + 4 * 5, startedIndexTriangle + 1 + 4 * 5,

				};
			else //if(!leftTriangle && !rightTriangle && bottomTriangle && topTriangle)
				triangles = new int[]
				{
				// Bottom
				startedIndexTriangle + 3, startedIndexTriangle + 1, startedIndexTriangle + 0,
				startedIndexTriangle + 3, startedIndexTriangle +  2, startedIndexTriangle + 1,			
 
				// Left
				//startedIndexTriangle + 3 + 4 * 1, startedIndexTriangle + 1 + 4 * 1, startedIndexTriangle + 0 + 4 * 1,
				//startedIndexTriangle + 3 + 4 * 1, startedIndexTriangle + 2 + 4 * 1, startedIndexTriangle + 1 + 4 * 1,
 
				// Front
				startedIndexTriangle + 3 + 4 * 2, startedIndexTriangle + 1 + 4 * 2, startedIndexTriangle + 0 + 4 * 2,
				startedIndexTriangle + 3 + 4 * 2, startedIndexTriangle + 2 + 4 * 2, startedIndexTriangle + 1 + 4 * 2,
 
				// Back
				startedIndexTriangle + 3 + 4 * 3,startedIndexTriangle +  1 + 4 * 3, startedIndexTriangle + 0 + 4 * 3,
				startedIndexTriangle + 3 + 4 * 3, startedIndexTriangle + 2 + 4 * 3, startedIndexTriangle + 1 + 4 * 3,
 
				// Right
				//startedIndexTriangle + 3 + 4 * 4, startedIndexTriangle + 1 + 4 * 4, startedIndexTriangle + 0 + 4 * 4,
				//startedIndexTriangle + 3 + 4 * 4, startedIndexTriangle + 2 + 4 * 4, startedIndexTriangle + 1 + 4 * 4,
 
				// Top
				startedIndexTriangle + 3 + 4 * 5, startedIndexTriangle + 1 + 4 * 5, startedIndexTriangle + 0 + 4 * 5,
				startedIndexTriangle + 3 + 4 * 5, startedIndexTriangle + 2 + 4 * 5, startedIndexTriangle + 1 + 4 * 5,

				};

			#endregion

			List<Vector3> verticesConcat = new List<Vector3>(mesh.vertices);
			List<Vector3> verticeResult = new List<Vector3>(verticesConcat.Concat(vertices.ToList()));
			mesh.vertices = verticeResult.ToArray();
			//mesh.vertices = vertices;
			//mesh.normals = normales;
			//mesh.uv = uvs;
			List<int> trianglesConcat = new List<int>(mesh.triangles);
			var triangleResult = trianglesConcat.Concat(triangles.ToList());
			mesh.triangles = triangleResult.ToArray();
			//mesh.triangles = triangles;
		}
	}
}




#if UNITY_EDITOR
namespace DI.Proformer.ToolHelper
{
	[System.Obsolete("In the future will be removed. No need to create this messy things. Focus on Platformer!")]
	[CustomEditor(typeof(ProformerSimpleCollider))]
	public class ProformerSimpleColliderEditor : Editor
	{
		public ProformerSimpleCollider pmc;
		private GUIContent upButtonContent = new GUIContent("\u25B2", "Up");
		private GUIContent downButtonContent = new GUIContent("\u25BC", "Up");

		int controlID;
		//SerializedProperty coreVertex;
		ReorderableList coreVertex;

		bool moveMode = true;
		bool forceUpdate = true;
		bool anyChangeInspector = false;
		bool removeMode = false;

		public void OnEnable()
		{
			pmc = (ProformerSimpleCollider)target;
			controlID = GUIUtility.GetControlID(FocusType.Passive);
			//coreVertex = serializedObject.FindProperty("coreVertex");
			coreVertex = new ReorderableList(serializedObject, serializedObject.FindProperty("coreVertex"), true, true, true, true);
			coreVertex.drawHeaderCallback = (Rect rect) =>
			{
				EditorGUI.LabelField(rect, "Core Vertex");
			};
			coreVertex.drawElementCallback =
				(Rect rect, int index, bool isActive, bool isFocused) => {
					var element = coreVertex.serializedProperty.GetArrayElementAtIndex(index);
					Vector3 oldOneVertex = element.FindPropertyRelative("vertex").vector3Value;
					float oldOneHeight = pmc.coreVertex[index].height;
					rect.y += 2;
					//EditorGUI.PropertyField(rect, element, true);
					float currentWidth = rect.width / 5;
					EditorGUI.LabelField(new Rect(rect.x, rect.y, currentWidth, EditorGUIUtility.singleLineHeight), "Vertex " + index.ToString());

					EditorGUI.LabelField(new Rect(rect.x + currentWidth, rect.y, 30, EditorGUIUtility.singleLineHeight), "X");
					Undo.RecordObject(pmc, "Undo vertex");
					pmc.coreVertex[index].vertex.x = InternalTool.DragableFloatProperty(new Rect(rect.x + currentWidth + 15, rect.y, currentWidth - 15, EditorGUIUtility.singleLineHeight),
						new Rect(rect.x + currentWidth, rect.y, 30, EditorGUIUtility.singleLineHeight),
						pmc.coreVertex[index].vertex.x,
						EditorStyles.numberField);
					//pmc.coreVertex[index].vertex.x = EditorGUI.FloatField(new Rect(rect.x + currentWidth + 15, rect.y, currentWidth - 15, EditorGUIUtility.singleLineHeight), pmc.coreVertex[index].vertex.x);

					//EditorGUI.PropertyField(new Rect(rect.x + currentWidth + 15, rect.y, currentWidth - 15, EditorGUIUtility.singleLineHeight), element.FindPropertyRelative("vertex").FindPropertyRelative("x"));
					//EditorGUI.PropertyField(new Rect(rect.x, rect.y, currentWidth * 4, EditorGUIUtility.singleLineHeight), element.FindPropertyRelative("vertex"));

					EditorGUI.LabelField(new Rect(rect.x + (currentWidth) * 2, rect.y, 30, EditorGUIUtility.singleLineHeight), "  Y ");
					Undo.RecordObject(pmc, "Undo vertex");
					pmc.coreVertex[index].vertex.y = InternalTool.DragableFloatProperty(new Rect(rect.x + (currentWidth) * 2 + 20, rect.y, currentWidth - 20, EditorGUIUtility.singleLineHeight),
						new Rect(rect.x + (currentWidth) * 2, rect.y, 30, EditorGUIUtility.singleLineHeight),
						pmc.coreVertex[index].vertex.y,
						EditorStyles.numberField);
					//pmc.coreVertex[index].vertex.y = EditorGUI.FloatField(new Rect(rect.x + (currentWidth) * 2 + 20 , rect.y, currentWidth - 20, EditorGUIUtility.singleLineHeight), pmc.coreVertex[index].vertex.y);
					//EditorGUI.PropertyField(new Rect(rect.x + (currentWidth) * 2 + 20, rect.y, currentWidth - 20, EditorGUIUtility.singleLineHeight), element.FindPropertyRelative("vertex").FindPropertyRelative("y"));


					EditorGUI.LabelField(new Rect(rect.x + (currentWidth) * 3, rect.y, 30, EditorGUIUtility.singleLineHeight), "  Z ");
					Undo.RecordObject(pmc, "Undo vertex");
					pmc.coreVertex[index].vertex.z = InternalTool.DragableFloatProperty(new Rect(rect.x + (currentWidth) * 3 + 20, rect.y, currentWidth - 20, EditorGUIUtility.singleLineHeight),
						new Rect(rect.x + (currentWidth) * 3, rect.y, 30, EditorGUIUtility.singleLineHeight),
						pmc.coreVertex[index].vertex.z,
						EditorStyles.numberField);
					//pmc.coreVertex[index].vertex.z = EditorGUI.FloatField(new Rect(rect.x + (currentWidth) * 3 + 20 , rect.y, currentWidth - 20, EditorGUIUtility.singleLineHeight), pmc.coreVertex[index].vertex.z);
					//EditorGUI.PropertyField(new Rect(rect.x + (currentWidth) * 3 + 20, rect.y, currentWidth - 20, EditorGUIUtility.singleLineHeight), element.FindPropertyRelative("vertex").FindPropertyRelative("z"));

					EditorGUI.LabelField(new Rect(rect.x + (currentWidth) * 4, rect.y, 30, EditorGUIUtility.singleLineHeight), "  H ");
					Undo.RecordObject(pmc, "Undo vertex");
					pmc.coreVertex[index].height = InternalTool.DragableFloatProperty(new Rect(rect.x + (currentWidth) * 4 + 20, rect.y, currentWidth - 20, EditorGUIUtility.singleLineHeight),
						new Rect(rect.x + (currentWidth) * 4, rect.y, 30, EditorGUIUtility.singleLineHeight),
						pmc.coreVertex[index].height,
						EditorStyles.numberField);
					//pmc.coreVertex[index].height = EditorGUI.FloatField(new Rect(rect.x + (currentWidth) * 4 + 20 , rect.y, currentWidth - 20, EditorGUIUtility.singleLineHeight), pmc.coreVertex[index].height);
					//EditorGUI.PropertyField(new Rect(rect.x + (currentWidth) * 4 + 20, rect.y, currentWidth - 20, EditorGUIUtility.singleLineHeight), element.FindPropertyRelative("height"));
					if (oldOneVertex != pmc.coreVertex[index].vertex || oldOneHeight != pmc.coreVertex[index].height)
						anyChangeInspector = true;
				};
		}
		public void OnDisable()
		{

		}
		public override void OnInspectorGUI()
		{
			anyChangeInspector = false;

			serializedObject.Update();
			EditorGUI.BeginChangeCheck();
			Undo.RecordObject(pmc, "Width");
			float w = EditorGUILayout.FloatField("Width", pmc.width);
			if (w != pmc.width)
			{
				anyChangeInspector = true;
				pmc.width = w;
			}
			//ReorderableList
			coreVertex.DoLayoutList();
			//ShowContent(pmc.coreVertex.ToArray());
			if (GUILayout.Button(!moveMode ? "Move Mode" : "Rotate Mode  (Experimental)"))
			{
				moveMode = !moveMode;
				pmc.transform.position += Vector3.up;
				pmc.transform.position -= Vector3.up;
			}
			if (!forceUpdate)
			{
				if (GUILayout.Button("Force Update"))
				{
					forceUpdate = true;
				}
				if (GUILayout.Button("Create Collider"))
					pmc.CreateMesh();

			}
			else
			{
				if (GUILayout.Button("Disable Force Update"))
				{
					forceUpdate = false;
				}
			}
			/*EditorGUILayout.HelpBox("If you got an error 'cleaning the mesh failed', please use this button to recrete the mesh", MessageType.Info);
			if (GUILayout.Button("Clean Mesh")) {
				DestroyImmediate(pmc.GetComponent<MeshCollider>());
				DestroyImmediate(pmc.GetComponent<MeshFilter>());

				pmc.gameObject.AddComponent<MeshFilter>();
				pmc.gameObject.AddComponent<MeshCollider>();
			}*/

			if (anyChangeInspector && forceUpdate)
			{
				pmc.CreateMesh();
			}

			if (EditorGUI.EndChangeCheck()) {
				serializedObject.ApplyModifiedProperties();
				EditorUtility.SetDirty(pmc);
			}
		}

		void ShowContent(ProformerSimpleCollider.FaceVertex[] faceVertex)
		{
			EditorGUILayout.BeginHorizontal();
			EditorGUILayout.PrefixLabel("Face Vertex", EditorStyles.boldLabel);
			if (GUILayout.Button("ADD"))
			{
				Undo.RecordObject(pmc, "Add");
				pmc.coreVertex.Add(new ProformerSimpleCollider.FaceVertex(pmc.coreVertex[faceVertex.Length - 1].vertex + Vector3.right, 1));
				pmc.transform.position += Vector3.up;
				pmc.transform.position -= Vector3.up;
				anyChangeInspector = true;
			}
			EditorGUILayout.EndHorizontal();
			//foreach (ProformerMeshCollider.FaceVertex face in faceVertex) {
			for (int i = 0; i < faceVertex.Length; i++)
			{
				Undo.RecordObject(pmc, "Vertex");
				//EditorGUI.indentLevel += 1;
				EditorGUILayout.BeginHorizontal();
				float currentWidth = (EditorGUIUtility.currentViewWidth - 60) / 5 - 15;

				if (GUILayout.Button(upButtonContent, GUILayout.Width(20)) && i != 0)
				{
					Undo.RecordObject(pmc, "Move");
					ProformerSimpleCollider.FaceVertex temp = faceVertex[i - 1];
					pmc.coreVertex[i - 1] = faceVertex[i];
					pmc.coreVertex[i] = temp;
					anyChangeInspector = true;
				}
				if (GUILayout.Button(downButtonContent, GUILayout.Width(20)) && i != faceVertex.Length - 1)
				{
					Undo.RecordObject(pmc, "Move");
					ProformerSimpleCollider.FaceVertex temp = faceVertex[i + 1];
					pmc.coreVertex[i + 1] = faceVertex[i];
					pmc.coreVertex[i] = temp;
					anyChangeInspector = true;
				}
				Undo.RecordObject(pmc, "Vertex");
				EditorGUILayout.LabelField("X", GUILayout.Width(15));
				float x = EditorGUILayout.FloatField(faceVertex[i].vertex.x, GUILayout.Width(currentWidth));
				EditorGUILayout.LabelField("Y", GUILayout.Width(15));
				float y = EditorGUILayout.FloatField(faceVertex[i].vertex.y, GUILayout.Width(currentWidth));
				EditorGUILayout.LabelField("Z", GUILayout.Width(15));
				float z = EditorGUILayout.FloatField(faceVertex[i].vertex.z, GUILayout.Width(currentWidth));
				//EditorGUILayout.PropertyField(this.coreVertex.GetArrayElementAtIndex(i).FindPropertyRelative("vertex"));
				EditorGUILayout.LabelField("H", GUILayout.Width(15));
				float h = EditorGUILayout.FloatField(faceVertex[i].height, GUILayout.Width(currentWidth));

				if (x != faceVertex[i].vertex.x || y != faceVertex[i].vertex.y || z != faceVertex[i].vertex.z || h != faceVertex[i].height)
					anyChangeInspector = true;

				faceVertex[i].vertex.x = x;
				faceVertex[i].vertex.y = y;
				faceVertex[i].vertex.z = z;
				faceVertex[i].height = h;

				if (GUILayout.Button("-", GUILayout.Width(20)))
				{
					Undo.RecordObject(pmc, "Remove");
					pmc.coreVertex.RemoveAt(i);
					pmc.transform.position += Vector3.up;
					pmc.transform.position -= Vector3.up;
					anyChangeInspector = true;
				}
				EditorGUILayout.EndHorizontal();
				//EditorGUI.indentLevel -= 1;

			}
		}

		public void OnSceneGUI()
		{
			bool anyChange = false;

			Event e = Event.current;

			for (int i = 0; i < pmc.coreVertex.Count; i++)
			{
				/*Vector3 newDirectionHeightPos = Handles.FreeMoveHandle(LocalToGlobal(pmc.coreVertex[i].vertex) + pmc.coreVertex[i].directionHeight * (pmc.coreVertex[i].height), Quaternion.identity, HandleUtility.GetHandleSize(LocalToGlobal(pmc.coreVertex[i].vertex)) * .1f, Vector3.one * .1f, Handles.SphereCap);
				Vector3 newDirection = (newDirectionHeightPos - LocalToGlobal(pmc.coreVertex[i].vertex)).normalized;
				if (newDirection != pmc.coreVertex[i].directionHeight)
				{
					pmc.coreVertex[i].directionHeight = newDirection;
				}*/

				Handles.color = Color.blue;
				Vector3 newPosHeight = Handles.FreeMoveHandle(LocalToGlobal(pmc.coreVertex[i].vertex) + pmc.coreVertex[i].directionHeight * pmc.coreVertex[i].height, Quaternion.identity, HandleUtility.GetHandleSize(LocalToGlobal(pmc.coreVertex[i].vertex)) * .1f, InternalTool.GetSnapValue(), Handles.CubeCap);
				float height = Vector3.Distance(newPosHeight, LocalToGlobal(pmc.coreVertex[i].vertex));
				if (height != pmc.coreVertex[i].height)
				{
					Undo.RecordObject(pmc, "Vertex");
					pmc.coreVertex[i].height = height;
					anyChange = true;
				}

				if (moveMode && !removeMode)
				{
					Handles.color = Color.green;
					Vector3 newPosVertex = Handles.FreeMoveHandle(LocalToGlobal(pmc.coreVertex[i].vertex), Quaternion.identity, HandleUtility.GetHandleSize(LocalToGlobal(pmc.coreVertex[i].vertex)) * .2f, InternalTool.GetSnapValue(), Handles.SphereCap);
					if (GlobalToLocal(newPosVertex) != pmc.coreVertex[i].vertex)
					{
						Undo.RecordObject(pmc, "Vertex");
						pmc.coreVertex[i].vertex = GlobalToLocal(newPosVertex);
						anyChange = true;
					}

				}
				/*Vector3 newDirectionPos = Handles.FreeMoveHandle(newPosVertex + pmc.coreVertex[i].direction * pmc.width, Quaternion.identity, HandleUtility.GetHandleSize(LocalToGlobal(pmc.coreVertex[i].vertex)) * .1f, Vector3.one * .1f, Handles.SphereCap);
				newDirectionPos.y = newPosVertex.y;
				Vector3 newDirection = (newDirectionPos - newPosVertex).normalized;
				if (newDirectionPos != pmc.coreVertex[i].direction)
				{
					pmc.coreVertex[i].direction = newDirection;
				}*/
				if (!moveMode && !removeMode)
				{
					Handles.color = Color.red;
					Quaternion yRotation = Handles.RotationHandle(Quaternion.LookRotation(pmc.coreVertex[i].directionWidth), LocalToGlobal(pmc.coreVertex[i].vertex));
					Vector3 direction = yRotation * Vector3.forward;
					if (direction != pmc.coreVertex[i].directionWidth)
					{
						Undo.RecordObject(pmc, "Vertex");
						pmc.coreVertex[i].directionWidth = direction;
						anyChange = true;
					}
				}
				//Debug.Log(direction +", " + pmc.coreVertex[i].direction);
				/*Quaternion currentRotation = Quaternion.Euler(pmc.coreVertex[i].direction);
				Quaternion newRotation = Handles.FreeRotateHandle(currentRotation, newPosVertex, HandleUtility.GetHandleSize(LocalToGlobal(pmc.coreVertex[i].vertex)) * .4f);
				if (currentRotation != newRotation) {
					pmc.coreVertex[i].direction = newRotation * Vector3.forward;
				}*/
			}

			if (e.control)
			{
				removeMode = false;
				if (e.GetTypeForControl(controlID) == EventType.MouseDown)
				{
					Undo.RecordObject(pmc, "Create");
					Vector3 dir = (pmc.coreVertex[pmc.coreVertex.Count - 1].vertex - pmc.coreVertex[pmc.coreVertex.Count - 2].vertex).normalized;
					pmc.coreVertex.Add(new ProformerSimpleCollider.FaceVertex(pmc.coreVertex[pmc.coreVertex.Count - 1].vertex + dir * 1, 1));
					anyChange = true;
					//return;
				}
			}
			else if (e.alt)
			{
				removeMode = true;
				int i = 0;
				while (i < pmc.coreVertex.Count)
				{
					Vector3 currentVertex = LocalToGlobal(pmc.coreVertex[i].vertex);
					Handles.DrawSolidArc(currentVertex, (Camera.current.transform.position - currentVertex).normalized, currentVertex - Vector3.right, 360, HandleUtility.GetHandleSize(LocalToGlobal(pmc.coreVertex[i].vertex)) * .2f);
					if (Vector2.Distance((Vector2)Camera.current.WorldToScreenPoint(currentVertex), new Vector2(e.mousePosition.x, Camera.current.pixelHeight - e.mousePosition.y)) < 10f)
					{
						Handles.color = Color.red;
						if (e.GetTypeForControl(controlID) == EventType.MouseDown)
						{
							Undo.RecordObject(pmc, "Delete");
							pmc.coreVertex.RemoveAt(i);
							pmc.transform.position += Vector3.up;
							pmc.transform.position -= Vector3.up;
							anyChange = true;
							//return;
						}
					}
					else
					{
						Handles.color = Color.blue;
					}

					i++;
				}
			}
			else if (removeMode)
				removeMode = false;
			if (anyChange && forceUpdate)
			{
				pmc.CreateMesh();
			}
		}
		Vector3 LocalToGlobal(Vector3 v)
		{
			Vector3 gv = v + pmc.transform.position;
			return gv;
		}
		Vector3 GlobalToLocal(Vector3 v)
		{
			Vector3 gv = v - pmc.transform.position;
			return gv;
		}
	}
}
#endif
