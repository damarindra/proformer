﻿using UnityEngine;
using System.Collections.Generic;

namespace DI.Proformer.ToolHelper
{
	/// <summary>
	/// Path
	/// </summary>
	public class Path : MonoBehaviour
	{
		public List<DIPosition> pathList = new List<DIPosition>();
		public bool connetLastPoint;
	}
}