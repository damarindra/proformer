﻿using UnityEngine;
using UnityEngine.Internal;
using System;
using System.Linq;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditorInternal;
using UnityEditor;
#endif
using System.Reflection;


namespace DI.Proformer {
	/// <summary>
	/// Editor Helper
	/// </summary>
	public class InternalTool {

		public static void DrawRay(Vector3 origin, Vector3 direction, Color color) {
#if UNITY_EDITOR
			if(ProformerSettings.debugRay)
				Debug.DrawRay(origin, direction, color);
#endif
		}
		public static void DrawRay(Vector3 origin, Vector3 direction)
		{
#if UNITY_EDITOR
			if(ProformerSettings.debugRay)
				Debug.DrawRay(origin, direction);
#endif
		}
		internal static void DrawLine(Vector3 vector31, Vector3 vector32)
		{
#if UNITY_EDITOR
			if (ProformerSettings.debugRay)
				Debug.DrawLine(vector31, vector32);
#endif
		}

		Texture2D FlipTexture(Texture2D original)
		{
			Texture2D flipped = new Texture2D(original.width, original.height);

			int xN = original.width;
			int yN = original.height;

			for (int i = 0; i < xN; i++)
			{
				for (int j = 0; j < yN; j++)
				{
					flipped.SetPixel(xN - i - 1, j, original.GetPixel(i, j));
				}
			}
			flipped.Apply();

			return flipped;
		}

		/// <summary>
		/// Will cancel invoke when methodInfo is null
		/// </summary>
		public static void CallMethod(string methodName, MonoBehaviour monoScript)
		{
			if (monoScript.GetType().GetMethod(methodName) != null)
				monoScript.GetType().GetMethod(methodName).Invoke(monoScript, new object[0]);
		}
#if UNITY_EDITOR
		public static float DragableFloatProperty(Rect position, Rect dragHotZone, float value, [DefaultValue("EditorStyles.numberField")]GUIStyle style)
		{
			int controlID = GUIUtility.GetControlID("EditorTextField".GetHashCode(), FocusType.Keyboard, position);
			Type editorGUIType = typeof(EditorGUI);

			Type RecycledTextEditorType = Assembly.GetAssembly(editorGUIType).GetType("UnityEditor.EditorGUI+RecycledTextEditor");
			Type[] argumentTypes = new Type[] { RecycledTextEditorType, typeof(Rect), typeof(Rect), typeof(int), typeof(float), typeof(string), typeof(GUIStyle), typeof(bool) };
			MethodInfo doFloatFieldMethod = editorGUIType.GetMethod("DoFloatField", BindingFlags.NonPublic | BindingFlags.Static, null, argumentTypes, null);

			object[] parameters;
			FieldInfo fieldInfo = editorGUIType.GetField("s_RecycledEditor", BindingFlags.NonPublic | BindingFlags.Static);
			object recycledEditor = fieldInfo.GetValue(null);
			parameters = new object[] { recycledEditor, position, dragHotZone, controlID, value, "g7", style, true };

			return (float)doFloatFieldMethod.Invoke(null, parameters);
		}
		public static bool isContainsDefines(string define)
		{
			var defines = PlayerSettings.GetScriptingDefineSymbolsForGroup(EditorUserBuildSettings.selectedBuildTargetGroup);
			string newDefine = define;
			return defines.Contains(newDefine);
		}
		public static void SetDefine(string define)
		{
			var defines = PlayerSettings.GetScriptingDefineSymbolsForGroup(EditorUserBuildSettings.selectedBuildTargetGroup);
			string newDefine = define;
			if (!defines.Contains(newDefine))
			{
				defines = string.Format("{0},{1}", defines, newDefine);
				PlayerSettings.SetScriptingDefineSymbolsForGroup(EditorUserBuildSettings.selectedBuildTargetGroup, defines);
				AssetDatabase.SaveAssets();
			}
		}
		public static void UnSetDefine(string define)
		{
			var defines = PlayerSettings.GetScriptingDefineSymbolsForGroup(EditorUserBuildSettings.selectedBuildTargetGroup);
			string newDefine = define;
			if (defines.Contains(newDefine))
			{
				defines = defines.Replace(newDefine, "");
				PlayerSettings.SetScriptingDefineSymbolsForGroup(EditorUserBuildSettings.selectedBuildTargetGroup, defines);
				AssetDatabase.SaveAssets();
			}
		}
		public static Vector3 GetSnapValue() {
			return new Vector3(EditorPrefs.GetFloat("MoveSnapX"), EditorPrefs.GetFloat("MoveSnapY"), EditorPrefs.GetFloat("MoveSnapZ"));
		}
#endif

		public static void ForceUpdateView(Transform transform) {
			transform.position += Vector3.up;
			transform.position -= Vector3.up;
		}

		public static Vector3 RotateDirection(Vector3 direction, Vector3 rotation) {
			return Quaternion.Euler(rotation) * direction;
		}
		public static Vector3 RotationToDirection(Quaternion rotation) {
			return rotation * Vector3.forward;
		}
		public static Vector3 RotationToDirection(Quaternion rotation, Vector3 direction) {
			return rotation * direction;
		}
		public static int NormalToLevelPathDirection(Vector3 normal, LevelPath levelPath) {
			return Vector3.Angle(normal, levelPath.direction) <= 90 ? 1 : -1;
		}

		public static void ProformerRaycast(Ray ray, out ProformerRaycastHit hit, float rayLength, LayerMask mask)
		{
#if PROFORMER3D
			RaycastHit rawHit;
			Physics.Raycast(ray, out rawHit, rayLength, mask);
#else
			RaycastHit2D rawHit;
			rawHit = Physics2D.Raycast(ray.origin, ray.direction, rayLength, mask);
#endif
			hit = new ProformerRaycastHit(rawHit);
		}
		public static ProformerRaycastHit[] ProformerRaycastAll(Ray ray, float rayLength, LayerMask mask)
		{
#if PROFORMER3D
			RaycastHit[] rawHit;
			rawHit = Physics.RaycastAll(ray, rayLength, mask);
#else
			RaycastHit2D[] rawHit;
			rawHit = Physics2D.RaycastAll(ray.origin, ray.direction, rayLength, mask);
#endif
			ProformerRaycastHit[] hits = new ProformerRaycastHit[rawHit.Length];
			for(int i = 0; i < hits.Length; i++)
				hits[i] = new ProformerRaycastHit(rawHit[i]);
			return hits;
		}

		public static bool IsFartherThan(Vector3 position, Vector3 compare, Vector3 anchor) {
			return Vector3.Distance(anchor, position) > Vector3.Distance(anchor, compare);
		}

		public static void ProformerDebugError(string title, string error, UnityEngine.Object target) {
			Debug.LogError(title + " : " + error + ". " + "Target : " + target, target);
		}
		public static void ProformerDebugWarning(string title, string error, UnityEngine.Object target)
		{
			Debug.LogWarning(title + " : " + error + ". " + "Target : " + target, target);
		}
		public static void ProformerDebugLog(string title, string error, UnityEngine.Object target)
		{
			Debug.Log(title + " : " + error + ". " + "Target : " + target, target);
		}

#if UNITY_EDITOR

		public static string[] GetSortingLayerNames()
		{
			Type internalEditorUtilityType = typeof(InternalEditorUtility);
			PropertyInfo sortingLayersProperty = internalEditorUtilityType.GetProperty("sortingLayerNames", BindingFlags.Static | BindingFlags.NonPublic);
			return (string[])sortingLayersProperty.GetValue(null, new object[0]);
		}

		public static int[] GetSortingLayerUniqueIDs()
		{
			Type internalEditorUtilityType = typeof(InternalEditorUtility);
			PropertyInfo sortingLayerUniqueIDsProperty = internalEditorUtilityType.GetProperty("sortingLayerUniqueIDs", BindingFlags.Static | BindingFlags.NonPublic);
			return (int[])sortingLayerUniqueIDsProperty.GetValue(null, new object[0]);
		}

		public static int GetSortingLayerIndex(string sortingLayerName) {
			var sortingLayers = GetSortingLayerNames().ToList();
			return sortingLayers.IndexOf(sortingLayerName);
		}

		public static Type[] GetScriptAssetsOfType<T>()
		{
			//MonoScript[] scripts = (MonoScript[])UnityEngine.Object.FindObjectsOfTypeIncludingAssets(typeof(MonoScript));
			MonoScript[] scripts = (MonoScript[])Resources.FindObjectsOfTypeAll(typeof(MonoScript));

			List<Type> result = new List<Type>();

			foreach (MonoScript m in scripts)
			{
				if (m.GetClass() != null && m.GetClass().IsSubclassOf(typeof(T)))
				{
					result.Add(m.GetClass());
				}
			}
			return result.ToArray();
		}
#endif

	}

	public class ProformerRaycastHit {
#if PROFORMER3D
		public Collider collider;
		public RaycastHit hit;
#else
		public Collider2D collider;
		public RaycastHit2D hit;
#endif
		public float distance;
		public Vector3 normal, point;

		public ProformerRaycastHit() { }
#if PROFORMER3D
		public ProformerRaycastHit(RaycastHit hit) {
#else
		public ProformerRaycastHit(RaycastHit2D hit) {
#endif
			this.hit = hit;
			collider = hit.collider;
			distance = hit.distance;
			normal = hit.normal;
			point = hit.point;
		}

		public void Reset()
		{
			collider = null;
			distance = 0;
			normal = point = Vector3.zero;
		}

	}
}
