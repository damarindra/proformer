﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace DI.Proformer {
	public class DIAreaCollector : MonoBehaviour {

		[HideInInspector]
		public LayerMask layerTarget;
		[SerializeField]
		protected DIRigidbody rb;
		[HideInInspector]
		public List<GameObject> gameObjectCollected = new List<GameObject>();

		public delegate void OnGameObjectTrigger(GameObject go);
		public OnGameObjectTrigger onGameObjectEnter, onGameObjectExit;

		protected const float skinWidth = .05f;

#if PROFORMER3D
		public void Init(BoxCollider baseCollider) {
#else
		public void Init(BoxCollider2D baseCollider) {
#endif
			transform.localPosition = new Vector3(transform.localPosition.x, baseCollider.size.y / 2 + skinWidth / 2, transform.localPosition.z);
			if (transform.parent == baseCollider.transform)
				transform.localScale = Vector3.one;
			else
				transform.localScale = baseCollider.transform.localScale;
#if PROFORMER3D
			rb = new DIRigidbody(gameObject.AddComponent<Rigidbody>());
			rb.rb.isKinematic = true;
			rb.rb.useGravity = false;

			BoxCollider bc = gameObject.AddComponent<BoxCollider>();
			bc.center = baseCollider.center;
			bc.size = new Vector3(baseCollider.size.x - skinWidth * 2, skinWidth, baseCollider.size.z - skinWidth * 2);
#else
			transform.localRotation = Quaternion.Euler(Vector3.zero);
			transform.rotation = Quaternion.Euler(transform.eulerAngles.x, 0, transform.eulerAngles.z);
			rb = new DIRigidbody(gameObject.AddComponent<Rigidbody2D>());
			rb.rb.isKinematic = true;

			BoxCollider2D bc = gameObject.AddComponent<BoxCollider2D>();
			bc.offset = baseCollider.offset;
			bc.size = new Vector2(baseCollider.size.x - skinWidth*2, skinWidth);
#endif
			bc.isTrigger = true;

		}
#if PROFORMER3D
		public virtual void OnTriggerEnter(Collider collider){
#else
		public virtual void OnTriggerEnter2D(Collider2D collider) {
#endif
			int colLayer = 1 << collider.gameObject.layer;
			if ((layerTarget.value & colLayer) > 0) {
				if (!gameObjectCollected.Contains(collider.gameObject)) {
					gameObjectCollected.Add(collider.gameObject);
					if (onGameObjectEnter != null)
						onGameObjectEnter(collider.gameObject);
				}
			}
		}

#if PROFORMER3D
		public virtual void OnTriggerExit(Collider collider)
#else
		public virtual void OnTriggerExit2D(Collider2D collider)
#endif
		{
			int colLayer = 1 << collider.gameObject.layer;
			if ((layerTarget.value & colLayer) > 0)
			{
				if (gameObjectCollected.Contains(collider.gameObject)) {
					gameObjectCollected.Remove(collider.gameObject);
					if (onGameObjectExit != null)
						onGameObjectExit(collider.gameObject);
				}
			}
		}


	}
}
