﻿using UnityEngine;
using System.Collections;

namespace DI.Proformer.ToolHelper {
	/// <summary>
	/// Renderer Helper, Transition
	/// </summary>
	public static class RendererHelper {
		public static IEnumerator FadeOutRendererCo(Renderer renderer, float smoothing) {
			while (renderer.materials[renderer.materials.Length - 1].color.a > 0.05f)
			{
				foreach (Material m in renderer
					.materials)
				{
					m.color = Color.Lerp(m.color, new Color(1, 1, 1, 0), smoothing * Time.deltaTime);
				}
				yield return null;
			}
			foreach (Material m in renderer
					.materials)
			{
				m.color = new Color(1, 1, 1, 0);
			}
		}

		public static void SetColor(Renderer renderer, Color[] color) {
			for(int i = 0; i < renderer.materials.Length; i++)
				renderer.materials[i].color = color[i];
		}

		public static IEnumerator FlickeringColorCo(Renderer renderer, Color[] trueColor, Color flickeringColor, float transitionTime, float totalTime) {

			float t = 0;
			while (t < totalTime)
			{
				for (int i = 0; i < trueColor.Length; i++) {
					if(renderer.materials[i].color.Equals(flickeringColor))
						renderer.materials[i].color = trueColor[i];
					else if (renderer.materials[i].color.Equals(trueColor[i]))
						renderer.materials[i].color = flickeringColor;
				//	yield return null;
				}
				t += transitionTime;
				yield return new WaitForSeconds(transitionTime);
			}
			for (int i = 0; i < trueColor.Length; i++)
			{
				renderer.materials[i].color = trueColor[i];
				yield return null;
			}
		}
		public static IEnumerator FlickeringColorCo(Renderer renderer, float transitionTime, float totalTime)
		{

			float t = 0;
			while (t < totalTime)
			{
				if (!renderer.gameObject.activeSelf)
					renderer.gameObject.SetActive(true);
				else if (renderer.gameObject.activeSelf)
					renderer.gameObject.SetActive(false);
				//	yield return null;

				t += transitionTime;
				yield return new WaitForSeconds(transitionTime);
			}
			renderer.gameObject.SetActive(true);
		}
	}

}
