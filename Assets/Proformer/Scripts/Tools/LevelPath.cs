﻿using UnityEngine;
using System.Collections;
using DI.Proformer.Manager;

namespace DI.Proformer {
	/// <summary>
	/// Level Path
	/// </summary>
	public class LevelPath
	{
		public Vector3 direction;
		public Vector3 from, to;
		public float length { get {return Vector2.Distance(new Vector2(from.x, from.z), new Vector2(to.x, to.z)); } } 
		public float edgeOffset = .0025f;
		public Vector3 edgeFrom, edgeTo;
		public int index { get { return LevelBounds.instance ? LevelBounds.instance.levelPath.IndexOf(this) : -1; } }

		public LevelPath(Vector3 from, Vector3 to)
		{
			this.from = from;
			this.to = to;
			to.y = 0; from.y = 0;
			direction = (to - from).normalized;
			edgeFrom = from + direction * edgeOffset;
			edgeTo = to + direction * edgeOffset * -1;
		}

		public Vector3 GetFromRaw(float horizontal, float vertical) {
			if (!LevelBounds.instance)
				return Vector3.zero;
			Vector3 result = Vector3.zero;
			result = this.from + direction * length * horizontal;
			result.y = LevelBounds.instance.bottom + (LevelBounds.instance.top - LevelBounds.instance.bottom) * vertical;
			return result;
		}
		public Vector2 GetRaw(Vector3 position) {
			if (!LevelBounds.instance)
				return Vector3.zero;
			Vector2 result = Vector2.zero;
			result.x = Vector3.Distance(from, new Vector3(position.x, from.y, position.z));
			result.x /= length;
			result.y = position.y - LevelBounds.instance.bottom;
			result.y /= LevelBounds.instance.top - LevelBounds.instance.bottom;
			return result;
		}
	}
}
