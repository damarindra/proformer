﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DI.Proformer
{

	public static class Globals
	{
		public static string animatorTemplatePath = "Assets/Proformer/Resources/Template";
		public static string animatorName = "animTemplate.controller";
		public static string animationClipPath = "Assets/Proformer/Resources/Template/animclips";
		//Edit this must followed by edit MotorAnimationClips.cs
		//public static string[] animationClips = { "idle" , "run", "jump", "fall", "crouch", "crouchIdle", "dash", "wallSlide", "gotDamage", "climbLadder", "climbLadderIdle", "dead"};
	}

	public enum IsTriggerType
	{
		Chooseable, False, True
	}
}
