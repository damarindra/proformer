﻿using UnityEngine;
using System.Collections;

namespace DI.Proformer.ToolHelper {
	/// <summary>
	/// Tool helper for checking Animator Parameter
	/// </summary>
	public static class AnimatorTool {

		/// <summary>
		/// Updates the animator bool.
		/// </summary>
		/// <param name="animator">Animator.</param>
		/// <param name="parameterName">Parameter name.</param>
		/// <param name="value">If set to <c>true</c> value.</param>
		public static void SetBool(Animator animator, string parameterName, bool value)
		{
			if (animator == null)
				return;
			if (animator.HasParameterOfType(parameterName, AnimatorControllerParameterType.Bool))
				animator.SetBool(parameterName, value);
		}

		/// <summary>
		/// Triggers an animator trigger.
		/// </summary>
		/// <param name="animator">Animator.</param>
		/// <param name="parameterName">Parameter name.</param>
		/// <param name="value">If set to <c>true</c> value.</param>
		public static void SetTrigger(Animator animator, string parameterName)
		{
			if (animator == null)
				return;
			if (animator.HasParameterOfType(parameterName, AnimatorControllerParameterType.Trigger))
				animator.SetTrigger(parameterName);
		}

		/// <summary>
		/// Updates the animator float.
		/// </summary>
		/// <param name="animator">Animator.</param>
		/// <param name="parameterName">Parameter name.</param>
		/// <param name="value">Value.</param>
		public static void SetFloat(Animator animator, string parameterName, float value)
		{
			if (animator == null)
				return;
			if (animator.HasParameterOfType(parameterName, AnimatorControllerParameterType.Float))
				animator.SetFloat(parameterName, value);
		}

		/// <summary>
		/// Updates the animator integer.
		/// </summary>
		/// <param name="animator">Animator.</param>
		/// <param name="parameterName">Parameter name.</param>
		/// <param name="value">Value.</param>
		public static void SetInt(Animator animator, string parameterName, int value)
		{
			if (animator == null)
				return;
			if (animator.HasParameterOfType(parameterName, AnimatorControllerParameterType.Int))
				animator.SetInteger(parameterName, value);
		}

		public static bool HasParameterOfType(this Animator self, string name, AnimatorControllerParameterType type)
		{
			var parameters = self.parameters;
			foreach (var currParam in parameters)
			{
				if (currParam.type == type && currParam.name == name)
				{
					return true;
				}
			}
			return false;
		}


		public static void ChangeAnimationClip(this Animator self, AnimationClip useClip, string replaceThis) // where replaceThis is the name of the clip/node that already exists in your Animation Controller.
		{
			RuntimeAnimatorController myController = self.runtimeAnimatorController;

			if (myController is AnimatorOverrideController) myController = (myController as AnimatorOverrideController).runtimeAnimatorController;

			AnimatorOverrideController animatorOverride = new AnimatorOverrideController();
			animatorOverride.runtimeAnimatorController = myController;

			animatorOverride[replaceThis] = useClip;

			self.runtimeAnimatorController = animatorOverride;
		}

		public static AnimationClip GetAnimationClip(this Animator self, string node)
		{
			RuntimeAnimatorController myController = self.runtimeAnimatorController;

			if (myController is AnimatorOverrideController) myController = (myController as AnimatorOverrideController).runtimeAnimatorController;

			AnimatorOverrideController animatorOverride = new AnimatorOverrideController();
			animatorOverride.runtimeAnimatorController = myController;
			
			return animatorOverride[node];
		}
	}
}
