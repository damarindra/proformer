﻿using UnityEngine;
using System.Collections;

namespace DI.Proformer{
	public enum ExecuteMode {
		Start, CallMethod
	}
}
