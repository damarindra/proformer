﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace DI.Proformer {
	[System.Serializable]
	public class SimpleGUITarget {
		public string id = "Rename_Here";
		//public DIGetSetVariable targetVariable;
		public Text targetText;
		public string prefixText;
	}
}
