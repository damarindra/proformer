﻿using UnityEngine;
using System.Collections;
using System.Reflection;
using System.Collections.Generic;
using System.Linq;
using DI.Proformer.ToolHelper;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace DI.Proformer
{
	[System.Serializable]
	public class TargetMethodInfo 
	{
		public GameObject target;
		public MonoBehaviour monoScript;
		public string methodInfoString;
		public List<string> parameters = new List<string>();
		private MethodInfo _methodInfo;

		public MethodInfo methodInfo { get {
				if (_methodInfo == null && !string.IsNullOrEmpty(methodInfoString) && monoScript != null) {
					if (methodInfoString == "Disable GameObject")
						_methodInfo = GetType().GetMethod("DisableGameObject");
					else if (methodInfoString == "Enable GameObject")
						_methodInfo = GetType().GetMethod("EnableGameObject");
					else if (methodInfoString == "Disable Script")
						_methodInfo = GetType().GetMethod("DisableScript");
					else if (methodInfoString == "Enable Script")
						_methodInfo = GetType().GetMethod("EnableScript");
					else 
						_methodInfo = monoScript.GetType().GetMethod(methodInfoString);
				}
				return _methodInfo;
			}
		}

		/// <summary>
		/// Will cancel invoke when methodInfo is null
		/// </summary>
		public void CallMethod() {
			if (methodInfoString == "Disable GameObject")
				DisableGameObject();
			else if (methodInfoString == "Enable GameObject")
				EnableGameObject();
			else if (methodInfoString == "Disable Script")
				DisableScript();
			else if (methodInfoString == "Enable Script")
				EnableScript();
			else if(methodInfo != null)
				methodInfo.Invoke(monoScript, new object[0]);
		}

		public void DisableGameObject() {
			if (target != null)
				target.SetActive(false);
		}
		public void EnableGameObject()
		{
			if (target != null) {
				target.SetActive(true);
			}
		}
		public void DisableScript()
		{
			if (monoScript != null)
				monoScript.enabled = false;
		}
		public void EnableScript()
		{
			if (monoScript != null)
				monoScript.enabled = true;
		}
	}
}