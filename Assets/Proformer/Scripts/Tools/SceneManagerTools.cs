﻿using UnityEngine;
using System.Collections;
using System;
#if UNITY_5_0 || UNITY_5_1 || UNITY_5_2

#else
using UnityEngine.SceneManagement;
#endif

namespace DI.Proformer.ToolHelper {
	public class SceneManagerTools {
		public static string GetCurrentSceneName() {
#if UNITY_5_0 || UNITY_5_1 || UNITY_5_2
			return Application.loadedLevelName;
#else
			return SceneManager.GetActiveScene().name;
#endif
		}
		public static AsyncOperation LoadLevelAsync(string levelName) {
#if UNITY_5_0 || UNITY_5_1 || UNITY_5_2
			return Application.LoadLevelAsync(levelName);
#else
			return SceneManager.LoadSceneAsync(levelName);
#endif
		}

		public static void LoadScene(string levelName)
		{
#if UNITY_5_0 || UNITY_5_1 || UNITY_5_2
			Application.LoadLevel(levelName);
#else
			SceneManager.LoadScene(levelName);
#endif
		}
	}
}
