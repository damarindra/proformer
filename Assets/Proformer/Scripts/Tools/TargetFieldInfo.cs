﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Reflection;
using System.Collections.Generic;
using System.Linq;
using System;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace DI.Proformer {
	[System.Serializable, Obsolete("Target Info Field now replaced with DIGetSetVariable. Target Info Field will be delete soon")]
	public class TargetFieldInfo {

		public GameObject target;
		public MonoBehaviour monoScript;
		public string fieldInfoName;
		private FieldInfo fieldInfo;
		public string typeAllowed = "";

		public object GetValue()
		{
			if (fieldInfo == null)
			{
				SetupFieldInfo();
				if (fieldInfo == null)
				{
					Debug.LogError("Field Info of is Null!!");
					return .00000001f;
				}
			}
			return fieldInfo.GetValue(monoScript);
		}
		public FieldInfo GetField()
		{
			if (fieldInfo == null)
			{
				SetupFieldInfo();
				if (fieldInfo == null)
				{
					Debug.LogError("Property Info is Null!!");
					return fieldInfo;
				}
			}
			return fieldInfo;
		}

		void SetupFieldInfo()
		{
			fieldInfo = monoScript.GetType().GetField(fieldInfoName);
		}
	}

#if UNITY_EDITOR
	//[CustomPropertyDrawer(typeof(TargetFieldInfo), true)]
	public class TargetFieldInfoDrawer : PropertyDrawer
	{
		//Rect idRect;
		Rect titleRect;
		Rect gObjectRect;
		Rect scriptRect;
		Rect varRect;

		public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
		{
			titleRect = new Rect(position.x, position.y, position.width, EditorGUIUtility.singleLineHeight);
			gObjectRect = new Rect(position.x, titleRect.y + EditorGUIUtility.singleLineHeight * 1.1f, position.width, EditorGUIUtility.singleLineHeight);
			scriptRect = new Rect(position.x, gObjectRect.y + EditorGUIUtility.singleLineHeight * 1.1f, position.width, EditorGUIUtility.singleLineHeight);
			varRect = new Rect(position.x, scriptRect.y + EditorGUIUtility.singleLineHeight * 1.1f, position.width, EditorGUIUtility.singleLineHeight);

			EditorGUI.LabelField(titleRect, "Target Variable", EditorStyles.boldLabel);
			EditorGUI.indentLevel += 1;
			ShowValues(property);
			EditorGUI.indentLevel -= 1;

		}
		public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
		{
			return EditorGUIUtility.singleLineHeight * 4 * 1.1f;
		}
		void ShowValues(SerializedProperty value) {
			GameObject oldGO = value.FindPropertyRelative("target").objectReferenceValue as GameObject;
			EditorGUI.PropertyField(gObjectRect, value.FindPropertyRelative("target"));
			if (value.FindPropertyRelative("target").objectReferenceValue as GameObject != oldGO)
				value.FindPropertyRelative("monoScript").objectReferenceValue = null;

			if (value.FindPropertyRelative("target").objectReferenceValue != null)
			{
				//SCRIPT INFO
				MonoBehaviour oldMono = value.FindPropertyRelative("monoScript").objectReferenceValue as MonoBehaviour;
				List <MonoBehaviour> monoBehaviours = ((GameObject)value.FindPropertyRelative("target").objectReferenceValue).GetComponents<MonoBehaviour>().ToList();
				if (monoBehaviours.Count > 0)
				{
					if (value.FindPropertyRelative("monoScript").objectReferenceValue as MonoBehaviour == null && monoBehaviours.Count != 0)
						value.FindPropertyRelative("monoScript").objectReferenceValue = monoBehaviours[0];
					int iMonoScript = monoBehaviours.IndexOf(value.FindPropertyRelative("monoScript").objectReferenceValue as MonoBehaviour);
					iMonoScript = EditorGUI.Popup(scriptRect, "Script", iMonoScript, monoBehaviours.Select(i => i.ToString()).ToArray());
					if (iMonoScript < monoBehaviours.Count)
					{
						if (value.FindPropertyRelative("monoScript").objectReferenceValue as MonoBehaviour != monoBehaviours[iMonoScript]) 
							value.FindPropertyRelative("monoScript").objectReferenceValue = monoBehaviours[iMonoScript];
					}
					else
						iMonoScript = 0;
					if (value.FindPropertyRelative("monoScript").objectReferenceValue as MonoBehaviour != oldMono) 
						value.FindPropertyRelative("fieldInfoName").stringValue = "";
				}
				else
					value.FindPropertyRelative("monoScript").objectReferenceValue = null;
					

				if (value.FindPropertyRelative("monoScript").objectReferenceValue != null) {
					//FIELD INFO
					List<string> infos = value.FindPropertyRelative("monoScript").objectReferenceValue.GetType().GetFields(BindingFlags.Public | BindingFlags.Instance).ToList().Select(i => i.Name.ToString()).ToList();
					#region TYPEALLOWED
					/*
					if (!string.IsNullOrEmpty(value.FindPropertyRelative("typeAllowed").stringValue))
					{
						int totalRemoved = 0;
						List<string> types = value.FindPropertyRelative("monoScript").objectReferenceValue.GetType().GetFields(BindingFlags.Public | BindingFlags.Instance).ToList().Select(i => i.FieldType.ToString()).ToList();
						for (int i = 0; i < types.Count; i++)
						{
							string[] typeNames = types[i].Split(new char[1] { '.' });
							string result = typeNames[typeNames.Length - 1];
							if (result.Substring(result.Length - 1) == "]")
								result.Remove(result.Length - 1);
							if (result != value.FindPropertyRelative("typeAllowed").stringValue)
							{
								infos.RemoveAt(i - totalRemoved);
								totalRemoved += 1;
							}
						}
					}
					value.FindPropertyRelative("typeAllowed").stringValue = EditorGUI.TextField(allowedType, "Type", value.FindPropertyRelative("typeAllowed").stringValue);
					*/
					#endregion

					int totalRemoved = 0;
					List<string> types = value.FindPropertyRelative("monoScript").objectReferenceValue.GetType().GetFields(BindingFlags.Public | BindingFlags.Instance).ToList().Select(i => i.FieldType.ToString()).ToList();
					for (int i = 0; i < types.Count; i++)
					{
						string[] typeNames = types[i].Split(new char[1] { '.' });
						string result = typeNames[typeNames.Length - 1];
						if (result.Substring(result.Length - 1) == "]")
							result.Remove(result.Length - 1);
						if (result != "Int32" && result != "Single")
						{
							infos.RemoveAt(i - totalRemoved);
							totalRemoved += 1;
						}
					}


					if (infos.Count > 0)
					{
						if ((value.FindPropertyRelative("fieldInfoName").stringValue == null || value.FindPropertyRelative("fieldInfoName").stringValue == "") && infos.Count != 0)
							value.FindPropertyRelative("fieldInfoName").stringValue = infos[0].ToString();
						int iInfos = infos.IndexOf(value.FindPropertyRelative("fieldInfoName").stringValue);
						iInfos = EditorGUI.Popup(varRect, "Variable", iInfos, infos.ToArray());
						if (iInfos < infos.Count && iInfos != -1)
						{
							if (value.FindPropertyRelative("fieldInfoName").stringValue != infos[iInfos])
							{
								value.FindPropertyRelative("fieldInfoName").stringValue = infos[iInfos];
							}
						}
						else
							iInfos = 0;
					}
					else {
						value.FindPropertyRelative("fieldInfoName").stringValue = "";
						EditorGUI.Popup(varRect, "Variable", 0, new string[] {"" });
					}

				}
			}
			else
			{
				GUI.enabled = false;
				value.FindPropertyRelative("monoScript").objectReferenceValue = EditorGUI.ObjectField(scriptRect, "Script", value.FindPropertyRelative("monoScript").objectReferenceValue, typeof(MonoBehaviour), true) as MonoBehaviour;
				//value.FindPropertyRelative("typeAllowed").stringValue = EditorGUI.TextField(allowedType, "Type", value.FindPropertyRelative("typeAllowed").stringValue);
				EditorGUI.Popup(varRect, "Variable", 0, new string[] {"" });
				GUI.enabled = true;
			}
		}

	}
#endif
}
