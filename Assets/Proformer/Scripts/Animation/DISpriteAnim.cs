﻿using UnityEngine;
using System.Collections.Generic;
using System;
using System.Linq;

namespace DI.Proformer {
	/// <summary>Sprite Anim </summary>
	[ExecuteInEditMode]
	[RequireComponent(typeof(SpriteRenderer))]
	public class DISpriteAnim : MonoBehaviour {

		[HideInInspector]
		/// <summary>Sprites will be played </summary>
		public List<Sprite> sprites = new List<Sprite>();
		/// <summary>Frame per seconds</summary>
		public float fps = 8;
		/// <summary>get time needed at one frame in seconds</summary>
		public float frameTime { get { return 1f / fps; } }

		public enum PlayMode { Looping, PingPong, Once }
		/// <summary>Play Mode </summary>
		public PlayMode playMode = PlayMode.Looping;
		private enum CurrentState { Playing, Paused, Stop, Finish }
		[HideInInspector]
		CurrentState state = CurrentState.Stop;

		/// <summary>Play on start</summary>
		public bool playOnStart = true;

		[HideInInspector]
		public SpriteRenderer sr;
		float timer;

		// Use this for initialization
		void Start() {
			sr = GetComponent<SpriteRenderer>();
			if (sprites.Count == 0)
				sprites = new List<Sprite>(1) { sr.sprite };
			sr.sprite = sprites[0];
			if (playOnStart)
				state = CurrentState.Playing;
		}

		// Update is called once per frame
		void Update() {
			if (!Application.isPlaying)
				return;
			if (state == CurrentState.Playing)
				Animate();
		}

		void Animate() {
			if (timer >= frameTime) {
				int currentIndex = sprites.IndexOf(sr.sprite);
				currentIndex += 1;
				if (currentIndex >= sprites.Count) {
					if (playMode == PlayMode.Looping)
						currentIndex = 0;
					else if (playMode == PlayMode.PingPong)
					{
						Sprite[] newList = sprites.ToArray();
						Array.Reverse(newList);
						sprites = newList.ToList();
						if (sprites.Count > 1)
							currentIndex = 1;
						else
							currentIndex = 0;
					}
					else {
						state = CurrentState.Finish;
						return;
					}
				}
				sr.sprite = sprites[currentIndex];
				timer = 0;
			}
			timer += Time.deltaTime;
		}

		public void Play() { if (state == CurrentState.Finish) sr.sprite = sprites[0]; state = CurrentState.Playing; }
		public void Stop() { sr.sprite = sprites[0]; state = CurrentState.Stop; }
		public void Pause() { state = CurrentState.Paused; }

	}

}
