﻿using UnityEngine;
using DI.Proformer.ToolHelper;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace DI.Proformer {
	[System.Serializable]
	public class MotorAnimationClips {
		
		public AnimationClipSwitchable idle = new AnimationClipSwitchable(null, "idle"), 
			run = new AnimationClipSwitchable(null, "run"), 
			jump = new AnimationClipSwitchable(null, "jump"),
			fall = new AnimationClipSwitchable(null, "fall"),
			crouch = new AnimationClipSwitchable(null, "crouch"),
			crouchIdle = new AnimationClipSwitchable(null, "crouchIdle"),
			dash = new AnimationClipSwitchable(null, "dash"), 
			wallSlide = new AnimationClipSwitchable(null, "wallSlide"), 
			gotDamage = new AnimationClipSwitchable(null, "gotDamage"),
			climbLadder = new AnimationClipSwitchable(null, "climbLadder"), 
			climbLadderIdle = new AnimationClipSwitchable(null, "climbLadderIdle"),
			dead = new AnimationClipSwitchable(null, "dead");
		/*
		public AnimationClip idle, run, jump, fall, crouch, crouchIdle, dash, wallSlide, gotDamage, climbLadder, climbLadderIdle, dead;
		//Edit this must followed by edit Globals.cs
		[HideInInspector]
		public string _idleName = "idle", _runName = "run", _jumpName = "jump", _fallName = "fall", _crouchName = "crouch", _crouchIdleName = "crouchIdle"
			, _dashName = "dash", _wallSlideName = "wallSlide", _gotDamageName = "gotDamage", _climbLadderName = "climbLadder", _climbLadderIdleName = "climbLadderIdle", _deadName = "dead";*/
	}

	[System.Serializable]
	public class AnimationClipSwitchable
	{
		[HideInInspector]
		public AnimationClip clip;
		[HideInInspector]
		public string lastNameRegisteredInAnimatorController = "";
		
		public AnimationClipSwitchable(AnimationClip clip, string lastNameRegisteredInAnimatorController) {
			this.clip = clip;
			this.lastNameRegisteredInAnimatorController = lastNameRegisteredInAnimatorController;
		}

		public void Init(Animator animator) {
			if(clip != null && string.IsNullOrEmpty(lastNameRegisteredInAnimatorController))
				animator.ChangeAnimationClip(clip, lastNameRegisteredInAnimatorController);
		}
		public void SwitchWith(Animator animator, AnimationClip clip)
		{
			if (clip != null && string.IsNullOrEmpty(lastNameRegisteredInAnimatorController)) {
				animator.ChangeAnimationClip(clip, lastNameRegisteredInAnimatorController);
				this.clip = clip;
				lastNameRegisteredInAnimatorController = clip.name;
			}
		}
	}

#if UNITY_EDITOR
	[CustomPropertyDrawer(typeof(AnimationClipSwitchable))]
	public class AnimationClipSwitchableEditor : PropertyDrawer {

		public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
		{
			property.FindPropertyRelative("clip").objectReferenceValue = EditorGUI.ObjectField(position, property.displayName, property.FindPropertyRelative("clip").objectReferenceValue, typeof(AnimationClip), false);
		}
		
	}
#endif
}
