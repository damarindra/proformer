﻿using UnityEngine;
using System.Collections;
using DI.Proformer.ToolHelper;
using DI.Proformer.Action;
using DI.Proformer.Manager;
using UnityEngine.UI;
using System;
#if UNITY_EDITOR
using UnityEditor;
using UnityEditorInternal;
#endif

namespace DI.Proformer
{
	public class DIControllerMotor : DIController
	{
		//Setup Things
		public Renderer characterRenderer;
		public DIRigidbody rigidb;
		public GameObject rendererObject;
#if PROFORMER3D
		private Vector3 rendererRigthDirectionEuler = Vector3.zero;
#endif
		public Animator animator;
		[HideInInspector]
		public Color[] trueColors;
		public bool fadeOutWhenDie = true;
		public float fadeOutSmoothing = 5f;

		public int maxHealth = 100;
		public int health = 100;
		private bool autoFixWhenHealthMoreThanMaxHealth = true;
		public float invisibleTime = 1;
		[HideInInspector]
		public float recoverTime = 0;
		public bool canRespawn = false;
		[Conditional("Hide", "canRespawn")]
		public float respawnTime = 3f;
		[Conditional("Hide", "canRespawn", false, false)]
		public bool infiniteLives = true;
		[Conditional("Hide", "infiniteLives", false, true)]
		public int livesCount = 3;
		public AudioClip hitClip;
		public bool flickeringWhenGetDamage = true;
		[Conditional("Hide", "flickeringWhenGetDamage")]
		public bool disableEnableFlickerMethod = false;
		[Conditional("Hide", "flickeringWhenGetDamage")]
		public Color colorFlicker = new Color(1, 1, 1, 0);
		public float transitionTimeFlickering = .1f;
		
		public float moveSpeed = 6;
		public float accelerationTimeGrounded = .1f;
		public float accelerationTimeAirborne = .2f;

		public bool jumpPermission = true;
		[Conditional("Hide", "jumpPermission", false)]
		public float maxJumpHeight = 4;
		[Conditional("Hide", "jumpPermission", false)]
		public float minJumpHeight = 1;
		[Conditional("Hide", "jumpPermission", false)]
		public float timeToJumpApex = .4f;
		[Conditional("Hide", "jumpPermission", false)]
		public int numberJumpOnAir = 1;
		[Conditional("Hide", "jumpPermission", false)]
		public bool setToGlobalGravity = true;
		[Conditional("Hide", "jumpPermission", false)]
		public AudioClip jumpClip;
		public bool useGlobalGravity = false;

		public bool ladderPermission = true;
		[Conditional("Hide", "ladderPermission", false)]
		public float ladderSpeed = 4f;
		[Conditional("Hide", "ladderPermission", false)]
		public bool ladderJump = true;
		[Conditional("Hide", "ladderPermission", false)]
		public bool centeredWhenClimbing = true;
		[Conditional("Hide", "ladderPermission", false)]
		public bool unstickLadderWhenHorizontalMove = true;

		public bool wallSlidePermission = true;
		[Conditional("Hide", "wallSlidePermission", false)]
		public Vector2 wallJumpClimb = new Vector2(6, 20);
		[Conditional("Hide", "wallSlidePermission", false)]
		public Vector2 wallJumpOff = new Vector2(3, 0);
		[Conditional("Hide", "wallSlidePermission", false)]
		public Vector2 wallLeap = new Vector2(20, 14);
		[Conditional("Hide", "wallSlidePermission", false)]
		public float wallSlideSpeedMax = 3;
		[Conditional("Hide", "wallSlidePermission", false)]
		public float wallStickTime = .2f;
		float timeToWallUnstick;
		[Conditional("Hide", "wallSlidePermission", false)]
		public bool unstickWhenDirTouchUp = false;
		[Conditional("Hide", "wallSlidePermission", false)]
		public bool reverseWallSlide = true;
		public int jumpLeft;

		public bool crouchPermission = false;
		[Conditional("Hide", "crouchPermission", false)]
		public float speedCrouching = 3f;
		public float colliderScaller = .5f;
		private DICharacterCollider _ccCrouch;
		private float rayLengthStandingCrouch;
		private Vector3 frontTopCrouch;

		public float maxJumpVelocity;
		public float minJumpVelocity;

		public Vector3 velocity, oldVelocity;
		public float velocityXSmoothing;
		float velocityZSmoothing;
		/// <summary>store velocity on special ground, only when special ground modifier type == AccelerationModify.</summary>
		public float velocityXSmoothingSpecialGround;
		protected bool updating = true;
		[SerializeField]
		private float gravity;
		public float _gravity { get { return gravity; } set { gravity = value; } }

		public new MotorState stateAdv { set {_stateAdv = value;}
			get {base.stateAdv = _stateAdv;
				return _stateAdv;} }
		public MotorState _stateAdv;
		public MotorInput motorInput;
		public DICharacterBody characterBody;

		public delegate void OnDeadListener();
		public event OnDeadListener onDead, onRespawn;

		public string[] impactTag;
		public string[] takeDamageTag;
		public string[] pickableTag;

		[HideInInspector]
		public bool canMove = true;
		[HideInInspector]
		public bool isInvisible = false;
		[HideInInspector]
		public bool recovering = false;
		[HideInInspector]
		public bool isDead = false;
		protected Image healthBar;

		protected ProformerAction[] actions;

		[SerializeField, HideInInspector]
		private GiveDamage _giveDamage;
		[SerializeField, HideInInspector]
		private Impactable _impactable;
		[SerializeField, HideInInspector]
		private Stompable _stompable;
		[HideInInspector]
		public CheckPoint respawnPosition;

		public float getPercentageVelocityHorizontal() {
			return Mathf.Abs(velocity.x / moveSpeed);
		}


		public void CalculateJump()
		{
			gravity = -(2 * maxJumpHeight) / Mathf.Pow(timeToJumpApex, 2);
			if (setToGlobalGravity)
				Physics.gravity = new Vector3(0, gravity, 0);
			maxJumpVelocity = Mathf.Abs(gravity) * timeToJumpApex;
			minJumpVelocity = Mathf.Sqrt(2 * Mathf.Abs(gravity) * minJumpHeight);
		}

		

		// Use this for initialization
		protected override void Start()
		{
			canMove = true;
			base.Start();
			if (animator == null)
				animator = GetComponent<Animator>();
			if (animator == null)
				animator = GetComponentInChildren<Animator>();
			if (!useGlobalGravity)
				CalculateJump();
			if (characterBody == null)
				characterBody = bodyCollider.collider.GetComponent<DICharacterBody>();
			if (ladderPermission) {
				characterBody.onTriggerEnter += onLadderEnter;
				characterBody.onTriggerStay += onLadderStay;
				characterBody.onTriggerExit += onLadderExit;
			}
			
			stateAdv = new MotorState();
			motorInput = new MotorInput(standartInput);
			if (crouchPermission)
				CreateCrouchCollider();

			timeToWallUnstick = wallStickTime;

			actions = GetComponents<ProformerAction>();
			if (characterRenderer == null)
				InternalTool.ProformerDebugError("ProformerSetupNotComplete", "Character Renderer still null", this);
			else {
				int i = 0;
				trueColors = new Color[characterRenderer.materials.Length];
				foreach (Material m in characterRenderer.materials)
				{
					trueColors[i] = m.color;
					i++;
				}
#if PROFORMER3D
				rendererRigthDirectionEuler = rendererObject.transform.localEulerAngles;
#endif
			}
			StartCoroutine(AutoFixWhenHealthMoreThanMaxHealth());
		}

		IEnumerator AutoFixWhenHealthMoreThanMaxHealth()
		{
			while (autoFixWhenHealthMoreThanMaxHealth) {
				if (health > maxHealth)
					health = maxHealth;
				yield return null;
			}
		}

		// Update is called once per frame
		public virtual void Motoring()
		{
			if (isDead || !updating || isControllerDisabled || raycaster.pc.collider == null)
			{
				return;
			}
			stateAdv.Reset();

			standartInput.jumpDownOneWayTrigger = motorInput.jump.down;
			oldVelocity = velocity;

			if (state.stomping) {
				velocity.y = state.stompYVelocity;
			}

			//Crouching
			if (crouchPermission && !stateAdv.lastFrameLadder)
			{
				if (motorInput.standartInput.directionalInput.y < 0 && !stateAdv.isCrouching)
				{
					stateAdv.isCrouching = true;
				}
				else if (motorInput.standartInput.directionalInput.y >= 0 && stateAdv.isCrouching)
				{
					if (CanStandFromCrouching())
						stateAdv.isCrouching = false;
				}
			}
			float targetSpeedRaw = stateAdv.isCrouching ? speedCrouching : moveSpeed;

			float targetVelocityHorizontal = motorInput.standartInput.directionalInput.x * targetSpeedRaw;
			if (!canMove)
				targetVelocityHorizontal = 0;
			velocity.x = Mathf.SmoothDamp(velocity.x, targetVelocityHorizontal, ref velocityXSmoothing, (state.collisionBelow) ? accelerationTimeGrounded : accelerationTimeAirborne);

			//ClimbingLadder();
			Laderring();
			WallSliding();
			JumpLogic();
			
			if (!stateAdv.isClimbingLadder)
				velocity.y += gravity * Time.deltaTime;

			foreach (ProformerAction act in actions) 
				act.Act();

			if (animator != null)
				UpdateAnimation();

			//copy base.stateAdv
			base.stateAdv = stateAdv;

			//Apply Velocity
			if (stateAdv.isDownThroughOneWay || (stateAdv.isClimbingLadder && Mathf.Sign(motorInput.standartInput.directionalInput.y) == -1))
			{
			}
			else if (!stateAdv.isCrouching)
			{
				ApplyMovement(velocity * Time.deltaTime, null);
				if (!bodyCollider.enabled)
				{
					_ccCrouch.enabled = false;
					bodyCollider.enabled = true;
				}
			}
			else if (stateAdv.isCrouching)
			{
				ApplyMovement(velocity * Time.deltaTime, _ccCrouch);
				if (!_ccCrouch.enabled)
				{
					_ccCrouch.enabled = true;
					bodyCollider.enabled = false;
				}
			}

			//Reset
			if (state.collisionBelow && !stateAdv.isClimbingLadder)
			{
				velocity.y = 0;
				jumpLeft = numberJumpOnAir + 1;
			}
			if (stateAdv.lastFrameSliding && reverseWallSlide && !stateAdv.isWallSliding)
				rendererObject.transform.Rotate(Vector3.up, 180, Space.Self);
			if (state.collisionAbove && gravity <= 0)
				velocity.y = 0;

			if (transform.position.y < LevelBounds.instance.bottom && LevelBounds.instance.bottomBound == LevelBounds.BoundReach.Dead || transform.position.y > LevelBounds.instance.top && LevelBounds.instance.topBound == LevelBounds.BoundReach.Dead)
				Dead();
		}
		
		private void Laderring() {
			if (!ladderPermission || !stateAdv.canClimbingLadder || !stateAdv.currentLadder)
				return;

			float targetVelocityY = velocity.y;
			float targetVelocityX = velocity.x;
			if (state.colliderBelow && !stateAdv.isClimbingLadder)
			{
				if (motorInput.standartInput.directionalInput.y > 0)
				{
					//Climb
					targetVelocityY = LadderingOn();
				}
				else if (motorInput.standartInput.directionalInput.y < 0)
				{
					//we need to check if collider below is OneWay, so we can take ladder
					if (state.colliderBelow.tag.Equals("OneWay"))
					{
						targetVelocityY = LadderingOn();
					}
				}
			}
			else if (stateAdv.isClimbingLadder)
			{
				targetVelocityY = 0;
				targetVelocityX = 0;
				if (isTouchGround() && motorInput.standartInput.directionalInput.y < 0)
				{
					LadderingOff();
					targetVelocityX = 0;
				}
				else if (motorInput.standartInput.directionalInput.y != 0)
				{
					//Climb
					targetVelocityY = motorInput.standartInput.directionalInput.y * ladderSpeed;
				}

				if (motorInput.horizontal.down && motorInput.standartInput.directionalInput.x != 0 && unstickLadderWhenHorizontalMove) {
					targetVelocityX = velocity.x;
					LadderingOff();
					stateAdv.canClimbingLadder = false;
				}
			}
			else if(!stateAdv.isClimbingLadder) {
				if (motorInput.standartInput.directionalInput.y != 0)
				{
					//Climb
					targetVelocityY = LadderingOn();
					targetVelocityX = 0;
				}
			}
			velocity.y = targetVelocityY;
			velocity.x = targetVelocityX;

		}

		void LadderingOff() {
			stateAdv.isClimbingLadder = false;
			if (stateAdv.currentLadder != null)
			{
#if PROFORMER3D
				DILadder ladder = stateAdv.currentLadder.GetComponent<DILadder>();
				if (ladder)
				{
					/*
					int dir = 0;
					//Controlling the face direction when laddering off
					//if no input, then
					if (standartInput.directionalInput.x == 0) {
						//Check if ladder direction LEFT or RIGHT,then just add a little amount of velocity to auto rotate;
						if (ladder.ladderFaceDirection == DILadder.LadderDirection.Left) {
							velocity.x = .01f;
							dir = 1;
						}
						else if (ladder.ladderFaceDirection == DILadder.LadderDirection.Right) {
							velocity.x = -.01f;
							dir = -1;
						}
						//else, just use the old direction
						else
							dir = Vector3.Angle(direction, position.levelPath.direction) < 90 ? 1 : -1;
					}else
						dir = standartInput.directionalInput.x != 0 ? (int)Mathf.Abs(standartInput.directionalInput.x) : Vector3.Angle(direction, position.levelPath.direction) < 90 ? 1 : -1;
					rendererObject.transform.localEulerAngles = dir == 1 ? rendererRigthDirectionEuler : new Vector3(rendererRigthDirectionEuler.x, -rendererRigthDirectionEuler.y, rendererRigthDirectionEuler.z);
					ApplyRotation();*/
					rendererObject.transform.localEulerAngles = rendererRigthDirectionEuler;
				}
#endif
			}
		}

		float LadderingOn() {
			stateAdv.isClimbingLadder = true;
			float targetVelocityY = velocity.y;
			if (stateAdv.currentLadder != null)
			{
				DILadder ladder = stateAdv.currentLadder.GetComponent<DILadder>();
				if (ladder)
				{
					if (Mathf.Abs(ladder.getTopY() - raycaster.bottomCenterOrigin.y) < raycaster.height / 3 && motorInput.standartInput.directionalInput.y == 1)
						stateAdv.isClimbingLadder = false;
					else {
#if PROFORMER3D
						if (ladder.ladderFaceDirection == DILadder.LadderDirection.Left)
						{
							if (Vector3.Angle(direction, position.levelPath.direction) > 90)
								rendererObject.transform.Rotate(new Vector3(0, 180, 0));
						}
						else if (ladder.ladderFaceDirection == DILadder.LadderDirection.Forward)
						{
							rendererObject.transform.Rotate(new Vector3(0, 90 * rawDirection, 0));
						}
						else if (ladder.ladderFaceDirection == DILadder.LadderDirection.Right)
						{
							if(Vector3.Angle(direction, position.levelPath.direction) < 90)
								rendererObject.transform.Rotate(new Vector3(0, 180, 0));
						}
						else if (ladder.ladderFaceDirection == DILadder.LadderDirection.Backward)
						{
							rendererObject.transform.Rotate(new Vector3(0, -90 * rawDirection, 0));
						}
#endif
						Vector3 offset = Vector3.zero;
						if (centeredWhenClimbing) {
							offset = ladder._collider.bounds.center - transform.position;
							offset.y = 0;
						}
						transform.Translate(offset, Space.World);
						targetVelocityY = motorInput.standartInput.directionalInput.y * ladderSpeed;
					}
				}
			}
			return targetVelocityY;
		}

#if PROFORMER3D
		void onLadderEnter(Collider col)
#else
		void onLadderEnter(Collider2D col)
#endif
		{
			if (col.tag.Equals("Ladder"))
			{
				stateAdv.canClimbingLadder = true;
				stateAdv.currentLadder = col.gameObject;
			}

		}
#if PROFORMER3D
		void onLadderStay(Collider col)
#else
		void onLadderStay(Collider2D col)
#endif
		{
			if (col.tag.Equals("Ladder"))
			{
				if (!stateAdv.canClimbingLadder || !stateAdv.currentLadder)
				{
					if (velocity.y > 0)
						return;
					stateAdv.canClimbingLadder = true;
					stateAdv.currentLadder = col.gameObject;
				}
			}
		}
#if PROFORMER3D
		void onLadderExit(Collider col)
#else
		void onLadderExit(Collider2D col)
#endif
		{
			if (col.tag.Equals("Ladder"))
			{
				if (stateAdv.isClimbingLadder) {
					LadderingOff();
					stateAdv.isClimbingLadder = false;
				}
				stateAdv.canClimbingLadder = false;
				stateAdv.currentLadder = null;
			}
		}

		private void WallSliding() {

			//Wall Sliding
			if (wallSlidePermission && !stateAdv.isClimbingLadder && !stateAdv.canClimbingLadder)
			{
				if (state.collisionWall && !state.collisionBelow && velocity.y < 0)
				{
					stateAdv.isWallSliding = true;
					if (jumpLeft != numberJumpOnAir + 1)
						jumpLeft = numberJumpOnAir + 1;

					if (velocity.y < -wallSlideSpeedMax)
					{
						velocity.y = -wallSlideSpeedMax;
					}
					if (reverseWallSlide && !stateAdv.lastFrameSliding)
					{
						rendererObject.transform.Rotate(Vector3.up, 180, Space.Self);
					}

					if (timeToWallUnstick > 0)
					{
						velocityXSmoothing = 0;

						if ((motorInput.standartInput.directionalInput.x == state.wallDirection && motorInput.standartInput.directionalInput.x != 0) || (unstickWhenDirTouchUp && motorInput.standartInput.directionalInput.x == 0))
						{
							timeToWallUnstick -= Time.deltaTime;
						}
						else
						{
							timeToWallUnstick = wallStickTime;
						}
					}
					else
					{
						velocity.x = (state.wallDirection) * moveSpeed;
						timeToWallUnstick = wallStickTime;
						state.collisionWall = false;
					}

				}
				else if (stateAdv.isWallSliding) {
					stateAdv.isWallSliding = false;
				}
			}
		}

		private void JumpLogic()
		{
			if (jumpPermission && motorInput.jump.down && !stateAdv.isDashing)
			{
				if (wallSlidePermission && stateAdv.isWallSliding)
				{
					if (state.wallDirection == -motorInput.standartInput.directionalInput.x)
					{
						velocity.x = state.wallDirection * wallJumpClimb.x;
						velocity.y = wallJumpClimb.y;
					}
					else if (motorInput.standartInput.directionalInput.x == 0)
					{
						velocity.x = state.wallDirection * wallJumpOff.x;
						velocity.y = wallJumpOff.y;
					}
					else
					{
						velocity.x = state.wallDirection * wallLeap.x;
						velocity.y = wallLeap.y;
					}
					SoundManager.Instance.PlaySFX(jumpClip);
					jumpLeft -= 1;

				}
				else if (stateAdv.isClimbingLadder)
				{
					if (ladderJump)
					{
						LadderingOff();
						stateAdv.canClimbingLadder = false;
						velocity.y = maxJumpVelocity;
						SoundManager.Instance.PlaySFX(jumpClip);
						jumpLeft -= 1;
					}
				}
				else if (state.isSlidingSlope) {
					velocity.y = state.verticalHitNormal.y * maxJumpVelocity;
					float normalX = Vector3.Angle(state.verticalHitNormal, position.levelPath.direction) <= 90 ? Mathf.Abs(state.verticalHitNormal.x) : Mathf.Abs(state.verticalHitNormal.x) * -1;
					velocity.x = normalX * maxJumpVelocity;
					jumpLeft -= 1;
					SoundManager.Instance.PlaySFX(jumpClip);
				}
				else if (state.collisionBelow)
				{
					if (state.colliderBelow != null)
						//if collider below is oneway && directionInput.y == -1, then pass the one way, else jump as usually
						velocity.y = state.colliderBelow.tag.Equals("OneWay") && standartInput.directionalInput.y == -1 ? gravity * 2 * Time.deltaTime : maxJumpVelocity;
					//jump as usually, for handling if state is climbingSlope
					else if (state.collisionBelow)
						velocity.y = maxJumpVelocity;
					jumpLeft -= 1;
					
					SoundManager.Instance.PlaySFX(jumpClip);

				}
				else if (!state.collisionBelow && jumpLeft > 0)
				{
					velocity.y = maxJumpVelocity;
					jumpLeft -= 1;
					SoundManager.Instance.PlaySFX(jumpClip);

				}
			}
			if (motorInput.jump.up)
			{
				if (velocity.y > minJumpVelocity)
					velocity.y = minJumpVelocity;
			}
		}

		bool CanStandFromCrouching()
		{
			for (int i = 0; i < verticalRayCount; i++)
			{
				Vector3 rayOrigin = frontTopCrouch + transform.position;
				rayOrigin += direction == position.levelPath.direction ? -direction * raycaster.verticalSpace * i : direction * raycaster.verticalSpace * i;

				Ray ray = new Ray(rayOrigin, Vector3.up);

				ProformerRaycastHit hit;
				InternalTool.ProformerRaycast(ray, out hit, rayLengthStandingCrouch, groundMask);

				Debug.DrawLine(rayOrigin, rayOrigin + Vector3.up * (rayLengthStandingCrouch), Color.red);
				if (hit.collider != null)
					return false;
			}
			return true;
		}
		bool isTouchGround()
		{
			for (int i = 0; i < verticalRayCount; i++)
			{
				Vector3 rayOrigin;
				rayOrigin = raycaster.frontBottomOrigin + (-direction * raycaster.verticalSpace * i);

				Ray ray = new Ray(rayOrigin, Vector3.down);
				float rayLength = raycaster.skinWidth + Mathf.Abs(velocity.y * Time.deltaTime);

				ProformerRaycastHit hit;
				InternalTool.ProformerRaycast(ray, out hit, rayLength, groundMask);

				Debug.DrawLine(rayOrigin, rayOrigin + Vector3.down * (rayLength), Color.red);

				if (hit.collider != null)
				{
					if (hit.collider.tag.Equals("OneWay") && Mathf.Sign(motorInput.standartInput.directionalInput.y) == -1)
						return false;
					return true;
				}

			}
			return false;
		}
		void UpdateAnimation()
		{
			AnimatorTool.SetFloat(animator, "Speed", Mathf.Abs(velocity.x));
			AnimatorTool.SetBool(animator, "Grounded", state.collisionBelow);
			AnimatorTool.SetFloat(animator, "vSpeed", velocity.y);
			if (wallSlidePermission)
				AnimatorTool.SetBool(animator, "WallSliding", stateAdv.isWallSliding);
			AnimatorTool.SetBool(animator, "Crouching", stateAdv.isCrouching);
			AnimatorTool.SetBool(animator, "Ladder", stateAdv.isClimbingLadder);
			if (stateAdv.isClimbingLadder)
				AnimatorTool.SetFloat(animator, "LadderSpeed", Mathf.Abs(velocity.y));
			AnimatorTool.SetBool(animator, "Dead", isDead);
		}

		public virtual void TriggerEnterPickableObject(Pickable go)
		{
			go.Triggered();
		}
#if PROFORMER3D
		public virtual void TriggerEnterGetDamage(Collider collider)
#else
		public virtual void TriggerEnterGetDamage(Collider2D collider)
#endif
		{
			if (isDead)
				return;
			GiveDamage giveDamage = collider.GetComponent<GiveDamage>();
			health -= giveDamage.damageGiven;
			if (health <= 0)
			{
				health = 0;
				Dead();
			}
			characterBody.GotDamage(giveDamage.freezeTime, collider.GetComponent<Impactable>());
			SoundManager.Instance.PlaySFX(hitClip);
			if (giveDamage.triggered != null)
				giveDamage.triggered(collider.gameObject);
			AnimatorTool.SetBool(animator, "Hit", true);
			AnimatorTool.SetTrigger(animator, "HitTrigger");
			if(IsInvoking("ResetHitAnimator"))
				CancelInvoke("ResetHitAnimator");
			Invoke("ResetHitAnimator", giveDamage.freezeTime < .04f ? .3f : giveDamage.freezeTime);
		}
		void ResetHitAnimator() {
			AnimatorTool.SetBool(animator, "Hit", false);
		}


		/// <summary>
		/// Virtual Method for detecting Impact
		/// use base method when overriding for automatically Impact
		/// Implementation of this method at CharacterBody
		/// </summary>
		/// <param name="collider">Impact Collider</param>
#if PROFORMER3D
		public virtual void TriggerEnterImpact(Collider collider) 
#else
		public virtual void TriggerEnterImpact(Collider2D collider)
#endif
		{
			if (!canMove || isDead)
				return;
			Vector3 dir;
			Impactable effector = collider.GetComponent<Impactable>();
			if (effector.impactType == Impactable.ImpactType.LOCAL) {
				int flip = Vector3.Angle(effector.transform.right, position.levelPath.direction) < 90 ? 1 : -1;
				dir = (effector.offset * -1).normalized;
				dir.x *= flip;
				dir.z *= flip;
			}
			else { 
				dir = ((effector.transform.position + effector.offset) - transform.position).normalized;
				int flip = Vector3.Angle(effector.transform.right, position.levelPath.direction) < 90 ? -1 : 1;
				dir.x *= flip;
				dir.z *= flip;
			}

			if (Vector3.Angle(dir, position.levelPath.direction) < 90)
				dir = new Vector3(new Vector3(dir.x, 0, dir.z).magnitude, dir.y, 0);
			else
				dir = new Vector3(-new Vector3(dir.x, 0, dir.z).magnitude, dir.y, 0);

			velocity = dir * effector.power;
		}

		public void CreateCrouchCollider()
		{
#if PROFORMER3D
			_ccCrouch = new DICharacterCollider(bodyCollider.collider.gameObject.AddComponent<BoxCollider>());
#else
			_ccCrouch = new DICharacterCollider(bodyCollider.collider.gameObject.AddComponent<BoxCollider2D>());
#endif
			_ccCrouch.width = bodyCollider.width;
			_ccCrouch.height = bodyCollider.height * colliderScaller;
			_ccCrouch.center = new Vector3(bodyCollider.center.x, (bodyCollider.center.y - bodyCollider.height / 2) + _ccCrouch.height / 2, bodyCollider.center.z);
			rayLengthStandingCrouch = (bodyCollider.center.y + bodyCollider.height / 2) - (_ccCrouch.center.y + _ccCrouch.height / 2);
			frontTopCrouch = new Vector3(_ccCrouch.center.x + _ccCrouch.width / 2, _ccCrouch.center.y - .025f + _ccCrouch.height / 2, _ccCrouch.center.z);
			_ccCrouch.enabled = false;
		}

		public virtual IEnumerator Respawn()
		{
			if (fadeOutWhenDie)
			{
				if (!disableEnableFlickerMethod)
				{
					while (characterRenderer.material.color.a > .05f)
						yield return null;
				}
			}
			yield return new WaitForSeconds(respawnTime);
			isDead = false;
			AnimatorTool.SetBool(animator, "Dead", isDead);
			canMove = true;
			StartCoroutine(characterBody.Invisible());
			if (!disableEnableFlickerMethod)
				StartCoroutine(RendererHelper.FlickeringColorCo(characterRenderer, trueColors, new Color(1, 1, 1, 0), transitionTimeFlickering, invisibleTime));
			else
				StartCoroutine(RendererHelper.FlickeringColorCo(characterRenderer, transitionTimeFlickering, invisibleTime));
			health = maxHealth;
			velocity = Vector3.zero;
			if (healthBar != null)
				GUIManager.instance.UpdateImageFill(healthBar, health, maxHealth);
			bodyCollider.enabled = true;
			if (respawnPosition != null)
			{
				position.SetLevelPathPosition(respawnPosition.position.levelPath.index);
				transform.position = respawnPosition.transform.position;
			}

			if(!infiniteLives)
				livesCount -= 1;
			if (onRespawn != null)
				onRespawn();
		}
		/// <summary>
		/// Virtual method for implementation of character die
		/// </summary>
		public virtual void Dead()
		{
			canMove = false;
			//bodyCollider.enabled = false;
			StartCoroutine(disableColliderOnFixedUpdate());
			isDead = true;
			RendererHelper.SetColor(characterRenderer, trueColors);
			AnimatorTool.SetBool(animator, "Dead", isDead);
			if (fadeOutWhenDie)
			{
				if (!disableEnableFlickerMethod)
					StartCoroutine(RendererHelper.FadeOutRendererCo(characterRenderer, fadeOutSmoothing));
				else
					StartCoroutine(DisableRendererObject(respawnTime));
			}
			if (canRespawn && livesCount > 0)
				StartCoroutine(Respawn());

			if (onDead != null)
				onDead();
		}


		IEnumerator DisableRendererObject(float time) {
			rendererObject.SetActive(false);
			yield return new WaitForSeconds(time);
			rendererObject.SetActive(true);
		}

		IEnumerator disableColliderOnFixedUpdate()
		{
			yield return new WaitForFixedUpdate();
			bodyCollider.enabled = false;
		}

		public void DisableUpdate()
		{
			updating = false;
		}
		public void EnableUpdate()
		{
			updating = true;
		}

		public class MotorState : AdvanceState
		{
			public bool isWallSliding, lastFrameSliding;
			public bool isDashing;
			public bool isCrouching;
			public bool isDownThroughOneWay = false;
			public bool canClimbingLadder, lastFrameLadder;
			public GameObject currentLadder = null;

#if PROFORMER3D
			public Collider lastOneWay;
#else
			public Collider2D lastOneWay;
#endif

			public override void Reset()
			{
				lastFrameSliding = isWallSliding;
				lastFrameLadder = isClimbingLadder;
			}
		}
		public class MotorInput
		{
			public StandartInputController standartInput;
			public InputType horizontal = new InputType();
			public InputType jump = new InputType();
			public InputType dash = new InputType();
			public InputType shoot = new InputType();

			public MotorInput(StandartInputController standartInput) {
				this.standartInput = standartInput;
			}

			public void Reset()
			{
				standartInput.directionalInput = Vector2.zero;
				horizontal.Reset();
				jump.Reset();
				dash.Reset();
				shoot.Reset();
			}

			/// <summary>
			/// Input Type
			/// </summary>
			public class InputType
			{
				/// <summary>
				/// define as button down, will reset to false when frame completed
				/// </summary>
				public bool down = false;
				/// <summary>
				/// define as button up, will reset to false when frame completed
				/// </summary>
				public bool up = false;
				/// <summary>
				/// define as button pressed, value will not reset until change manually
				/// </summary>
				public bool pressed = false;
				/// <summary>
				/// Reset fuction : will reset down and up to false
				/// </summary>
				public void Reset()
				{
					down = false; up = false;
				}
			}
		}
	}
}