﻿using UnityEngine;
using System.Collections;
using DI.Proformer;
using System;
using DI.Proformer.Manager;
using System.Collections.Generic;
using DI.Proformer.ToolHelper;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace DI.Proformer
{
	public class DIController : DIEntity
	{
		public int verticalRayCount = 20, horizontalRayCount = 4;
		[Range(10, 85)]
		public float slopeLimit = 45;

		//enemy mask for stompable check
		public LayerMask groundMask, enemyMask;
		public DICharacterCollider bodyCollider;
		public Raycaster raycaster;
		public ControllerState state;
		public StandartInputController standartInput = new StandartInputController();
		protected bool isControllerDisabled;
		public float wallAngleMin = 89, wallAngleMax= 91;

		//Delegate
		public delegate void OnBeforeTranslate(ref Vector3 velocity, DIController controller);
		public event OnBeforeTranslate onBeforeCalculation, onAfterCalculation;

		public List<string> stompableTag = new List<string>();
		/// <summary>1 = right | -1 = Left. Defined before any process</summary>
		public int rawDirection { get; set; }

		protected AdvanceState stateAdv;

		Vector3 currentMovement;

		protected override void Start()
		{
			base.Start();
			rawDirection = 1;
			stateAdv = new AdvanceState();
			groundMask = 1 << LayerMask.NameToLayer("Grounds");
			enemyMask = 1 << LayerMask.NameToLayer("Enemy");
			state = new ControllerState();
			raycaster = new Raycaster(this, bodyCollider, horizontalRayCount, verticalRayCount);
		}

		public void ApplyMovement(Vector3 movement) {
			ApplyMovement(movement, null);
		}

		public void ApplyMovement(Vector3 movement, DICharacterCollider pcc = null)
		{
			if (movement == Vector3.zero || raycaster.pc.collider == null)
				return;
			state.Reset();
			currentMovement = movement;

			if (currentMovement.x != 0)
				rawDirection = (int)Mathf.Sign(currentMovement.x);
			ApplyRotation();

			state.velocityBeforeProcess = currentMovement;
			if (onBeforeCalculation != null)
				onBeforeCalculation(ref currentMovement, this);

			raycaster.UpdateDirection(VelocityToDirection(currentMovement));
			//UpdateLevelPath(ref velocity);

			SpecialGround(ref currentMovement, pcc);
			
			HorizontalRaycast(ref currentMovement, pcc);

			SlidingSlope(ref currentMovement);

			if (!state.isSlidingSlope)
				VerticalRaycast(ref currentMovement, pcc);
			if (!state.isClimbingSlope && !state.isSlidingSlope)
				DescendingSlope(ref currentMovement);

			AlongPath(ref currentMovement);

			UpdateLevelPath(ref currentMovement);

			if (LevelBounds.instance.bottomBound == LevelBounds.BoundReach.Constrait && transform.position.y < LevelBounds.instance.bottom && currentMovement.y < 0)
				currentMovement.y = 0;
			else if (LevelBounds.instance.topBound == LevelBounds.BoundReach.Constrait && transform.position.y > LevelBounds.instance.top && currentMovement.y > 0)
				currentMovement.y = 0;

			if (onAfterCalculation != null)
				onAfterCalculation(ref currentMovement, this);
			
			transform.Translate(currentMovement, Space.World);
			
			state.LateReset();
		}

		#region PROCESS_VELOCITY
		void HorizontalRaycast(ref Vector3 velocity, DICharacterCollider pcc) {
			Vector3 rayOrigin = VelocityToDirection(velocity) == position.levelPath.direction ? raycaster.rightOrigin : raycaster.leftOrigin;
			rayOrigin += Vector3.down * ((pcc != null ? pcc.height /2 : bodyCollider.height / 2) - raycaster.skinWidth);
			rayOrigin -= VelocityToDirection(velocity) * (raycaster.skinDepth);
			Ray ray = new Ray(rayOrigin, VelocityToDirection(velocity));

			HorizontalRaycast(ref velocity, ray, pcc);
		}
		void HorizontalRaycast(ref Vector3 velocity, Ray ray, DICharacterCollider pcc)
		{
			float rayLength = raycaster.skinWidth + Mathf.Abs(velocity.x) + raycaster.skinDepth;
			for (int i = 0; i < horizontalRayCount; i++)
			{
				ProformerRaycastHit hit;
				InternalTool.ProformerRaycast(ray, out hit, rayLength, groundMask);
				InternalTool.DrawRay(ray.origin, ray.direction * rayLength);

				if (hit.collider)
				{
					if (!state.lastFrameCollisionBelow && hit.collider.tag.Equals("OneWay"))
						continue;

					float angle = Vector3.Angle(hit.normal, Vector3.up);
					if (angle <= slopeLimit)
					{
						state.isClimbingSlope = true;
						state.horizontalHitNormal = hit.normal;
						state.slopeAngleHorizontal = angle;
						ClimbingSlope(ref velocity, hit);
						rayLength = hit.distance + Mathf.Abs(velocity.x);
					}
					else
					{
						velocity.x = (hit.distance - (raycaster.skinWidth + raycaster.skinDepth)) * rawDirection;
						if (state.isClimbingSlope)
						{
							velocity.y = 0;
							state.isClimbingSlope = false;
						}
						rayLength = hit.distance;
						state.collisionForward = true;
					}
					
					if (angle >= wallAngleMin && angle <= wallAngleMax) {
						state.collisionWall = true;
						state.wallDirection = Vector3.Angle(hit.normal, position.levelPath.direction) < 90 ? 1 : -1;
					}

				}
				ray.origin += Vector3.up * (pcc == null ? raycaster.horizontalSpace : raycaster.customHorizontalSpace(pcc));
			}
		}

		private void ClimbingSlope(ref Vector3 velocity, ProformerRaycastHit hit)
		{
			float climbingSlopeY = Mathf.Sin(state.slopeAngleHorizontal * Mathf.Deg2Rad) * Mathf.Abs(velocity.x);
			if (climbingSlopeY > velocity.y)
			{
				velocity.y = climbingSlopeY;
				state.collisionBelow = true;
				velocity.x = Mathf.Cos(state.slopeAngleHorizontal * Mathf.Deg2Rad) * Mathf.Abs(velocity.x) * Mathf.Sign(velocity.x);
			}
		}

		void VerticalRaycast(ref Vector3 velocity, DICharacterCollider pcc)
		{
			int directionY = (int)Mathf.Sign(velocity.y);
			Vector3 rayOrigin = directionY == -1 ? raycaster.backBottomOrigin + Vector3.up * raycaster.skinDepth : (pcc == null ? raycaster.backTopOrigin : raycaster.backTopOriginCustom(pcc))+ Vector3.down * raycaster.skinDepth;

			Vector3 offsetRayOrigin = velocity;
			offsetRayOrigin.y = 0;
			AlongPath(ref offsetRayOrigin);
			rayOrigin += offsetRayOrigin;

			float rayLength = raycaster.skinWidth + Mathf.Abs(velocity.y) + raycaster.skinDepth;

			for (int i = 0; i < verticalRayCount; i++)
			{
				Ray ray = new Ray(rayOrigin, Vector3.up * directionY);
				ProformerRaycastHit hitGround, hitEnemy;
				InternalTool.ProformerRaycast(ray, out hitGround, rayLength, groundMask);
				InternalTool.DrawRay(ray.origin, ray.direction * rayLength);

				if(directionY == -1 && !state.lastFrameCollisionBelow) {

					InternalTool.ProformerRaycast(ray, out hitEnemy, rayLength, enemyMask);
					if (hitEnemy.collider ) {
						//Check if stomped
						if ( hitEnemy.collider.GetComponent<Stompable>() && stompableTag.Contains(hitEnemy.collider.tag))
						{
							hitEnemy.collider.GetComponent<Stompable>().Stomped(ref velocity);
							state.stomping = true;
							state.stompYVelocity = velocity.y / Time.deltaTime;
							break;
						}
					}
				}

				if (hitGround.collider)
				{
					state.verticalHitNormal = hitGround.normal;
					//Jump Down OneWay platform
					if (state.passThisCollider != null)
					{
						if (hitGround.collider == state.passThisCollider)
							continue;
						else
							state.passThisCollider = null;
					}

					//Trigger Passing OneWay
					if (hitGround.collider.tag.Equals("OneWay") && standartInput.directionalInput.y == -1 && (standartInput.jumpDownOneWayTrigger || stateAdv.isClimbingLadder))
					{
						state.passThisCollider = hitGround.collider;
						continue;
					}
					//Passing OneWay Platform When Jumping
					else if (hitGround.collider.tag.Equals("OneWay") && directionY == 1)
						continue;

					velocity.y = (hitGround.distance - raycaster.skinWidth - raycaster.skinDepth) * directionY;
					rayLength = hitGround.distance < raycaster.skinWidth + raycaster.skinDepth ? raycaster.skinWidth + raycaster.skinDepth : hitGround.distance;

					/*
					if (state.isClimbingSlope || state.collisionForward)
					{
						velocity.x = 0;
					}*/

					if (!state.isClimbingSlope)
						state.collisionBelow = directionY == -1;
					state.collisionAbove = directionY == 1;

					if (state.collisionBelow)
						state.colliderBelow = !state.colliderBelow ? hitGround.collider : !state.colliderBelow.tag.Equals("Untagged") ? hitGround.collider : state.colliderBelow;

				}
				else if (state.passThisCollider != null)
					state.passThisCollider = null;
				
				rayOrigin += raycaster.faceDirection * raycaster.verticalSpace;
			}

		}
		private void DescendingSlope(ref Vector3 velocity)
		{
			Vector3 rayOrigin = raycaster.backBottomOrigin;
			if(state.isSpecialGrounded && Vector3.Angle(direction, VelocityToDirection(velocity)) > 90) { 
				rayOrigin = raycaster.frontBottomOrigin;
			}

			DescendingSlope(ref velocity, rayOrigin);
		}
		private void DescendingSlope(ref Vector3 velocity, Vector3 rayOrigin)
		{
			if (state.velocityBeforeProcess.y > 0 || state.collisionForward)
				return;
			
			Ray ray = new Ray(rayOrigin + Vector3.up * raycaster.skinDepth, Vector3.down);
			ProformerRaycastHit hit;
			InternalTool.ProformerRaycast(ray, out hit, Mathf.Infinity, groundMask);
			InternalTool.DrawRay(ray.origin, ray.direction * 3, Color.yellow);

			if (hit.collider)
			{
				//Store velocity that will be calculate, if special ground, we need to used current velocity, but if not special ground, used the velocityBeforeProcess
				Vector3 velocityUsed = state.isSpecialGrounded ? velocity : state.velocityBeforeProcess;

				float angle = Vector3.Angle(hit.normal, Vector3.up);
				if (state.oldSlopeAnglesVertical != angle)
				{
					ProformerRaycastHit hit2;
					InternalTool.ProformerRaycast(new Ray(raycaster.backOrigin, Vector3.down), out hit2, Mathf.Infinity, groundMask);
					angle = Vector3.Angle(hit2.normal, Vector3.up);
				}
				if (angle <= slopeLimit && angle > 1 && Vector3.Angle(hit.normal, VelocityToDirection(velocityUsed)) < 90)
				{
					if (hit.distance - raycaster.skinWidth - raycaster.skinDepth < Mathf.Tan(angle * Mathf.Deg2Rad) * Mathf.Abs(velocityUsed.x))
					{
						if (angle != state.oldSlopeAnglesVertical)
							velocityUsed.y = (hit.distance - raycaster.skinWidth - raycaster.skinDepth) * -1;
						else
							velocityUsed.y = 0;

						state.slopeAngleVertical = angle;
						state.isDescendingSlope = true;
						state.collisionBelow = true;
						velocityUsed.y += Mathf.Sin(state.slopeAngleVertical * Mathf.Deg2Rad) * Mathf.Abs(velocityUsed.x) * -1;
						velocityUsed.x = Mathf.Cos(state.slopeAngleVertical * Mathf.Deg2Rad) * Mathf.Abs(velocityUsed.x) * Mathf.Sign(velocityUsed.x);
						velocity = velocityUsed;
					}
				}
			}
		}

		void SpecialGround(ref Vector3 velocity, DICharacterCollider pcc) {
			Vector3 rayOrigin = raycaster.backBottomOrigin + Vector3.up * raycaster.skinDepth;

			Vector3 offsetRayOrigin = velocity;
			offsetRayOrigin.y = 0;
			AlongPath(ref offsetRayOrigin);
			rayOrigin += offsetRayOrigin;
			Vector3 velocityBeforeModify = velocity;

			float rayLength = raycaster.skinWidth + Mathf.Abs(velocity.y) + raycaster.skinDepth;

			//Check ray bottom if found special ground
			for (int i = 0; i < verticalRayCount; i++)
			{
				Ray ray = new Ray(rayOrigin, Vector3.down);
				ProformerRaycastHit hitGround;
				InternalTool.ProformerRaycast(ray, out hitGround, rayLength, groundMask);
				InternalTool.DrawRay(ray.origin, ray.direction * rayLength);

				if (hitGround.collider) {
					velocity = velocityBeforeModify;
					state.verticalHitNormal = hitGround.normal;
					if (!CheckIfSpecialGroundAndModifyVelocity(ref velocity, hitGround.collider.gameObject))
						velocity = velocityBeforeModify;
				}
				rayOrigin += raycaster.faceDirection * raycaster.verticalSpace;
			}
			//if not found special grounded, need to check again with raylength infinite. this process look like descending slope raycasting.
			if (!state.isSpecialGrounded) {
				Ray ray = new Ray(raycaster.backBottomOrigin + Vector3.up * raycaster.skinDepth, Vector3.down);
				ProformerRaycastHit hit;
				InternalTool.ProformerRaycast(ray, out hit, Mathf.Infinity, groundMask);
				InternalTool.DrawRay(ray.origin, ray.direction * 3, Color.yellow);

				if (hit.collider)
				{
					float angle = Vector3.Angle(hit.normal, Vector3.up);
					if (state.oldSlopeAnglesVertical != angle)
					{
						ProformerRaycastHit hit2;
						InternalTool.ProformerRaycast(new Ray(raycaster.backOrigin, Vector3.down), out hit2, Mathf.Infinity, groundMask);
						angle = Vector3.Angle(hit2.normal, Vector3.up);
					}
					if (angle <= slopeLimit && angle > 1 && Vector3.Angle(hit.normal, VelocityToDirection(velocityBeforeModify)) < 90)
					{
						if (hit.distance - raycaster.skinWidth - raycaster.skinDepth < Mathf.Tan(angle * Mathf.Deg2Rad) * Mathf.Abs(velocityBeforeModify.x))
						{
							if (hit.collider.gameObject.GetComponent<DISpecialGround>())
							{
								velocity = velocityBeforeModify;
								state.verticalHitNormal = hit.normal;
								if (!CheckIfSpecialGroundAndModifyVelocity(ref velocity, hit.collider.gameObject))
									velocity = velocityBeforeModify;
							}
						}
					}
				}
			}
		}

		bool CheckIfSpecialGroundAndModifyVelocity(ref Vector3 velocity, GameObject ground) {
			DISpecialGround specialGround = ground.GetComponent<DISpecialGround>();
			//if special ground, we need to calculate our velocity with modifier at special ground.
			if (specialGround)
			{
				specialGround.Modify(ref velocity, this);
				state.isSpecialGrounded = true;
				return true;
			}
			return false;
		}

		void SlidingSlope(ref Vector3 velocity)
		{
			if (velocity.y > 0)
				return;
			ProformerRaycastHit backHit, frontHit;
			Ray backRay = new Ray(raycaster.backBottomOrigin, Vector3.down);
			Ray frontRay = new Ray(raycaster.frontBottomOrigin, Vector3.down);

			InternalTool.ProformerRaycast(backRay, out backHit, raycaster.skinWidth + Mathf.Abs(velocity.y), groundMask);
			InternalTool.ProformerRaycast(frontRay, out frontHit, raycaster.skinWidth + Mathf.Abs(velocity.y), groundMask);

			InternalTool.DrawRay(backRay.origin, backRay.direction * 1f, Color.magenta);
			InternalTool.DrawRay(frontRay.origin, frontRay.direction * 1f, Color.magenta);

			if (backHit.collider != null ^ frontHit.collider != null)
			{
				//backRay + entityDirection != normal then not sliding
				if (backHit.collider != null && rawDirection != InternalTool.NormalToLevelPathDirection(backHit.normal, position.levelPath)) {
					return;
				}
				//frontRay + entityDirection == normal then not sliding
				else if (frontHit.collider != null && rawDirection == InternalTool.NormalToLevelPathDirection(frontHit.normal, position.levelPath))
				{
					return;
				}

				ApplySlidingSlope(backHit, ref velocity);
				ApplySlidingSlope(frontHit, ref velocity);
			}
		}

		void ApplySlidingSlope(ProformerRaycastHit hit, ref Vector3 velocity)
		{
			if (hit.collider)
			{
				float angle = Vector3.Angle(hit.normal, Vector3.up);
				if (angle > slopeLimit)
				{
					state.isSlidingSlope = true;
					state.colliderBelow = null;
					state.collisionBelow = false;

					float directionX = Vector3.Angle(hit.normal, position.levelPath.direction) < 90 ? -1 : 1;
					velocity.x = Mathf.Cos(angle * Mathf.Deg2Rad) * velocity.y * directionX;
					velocity.y = Mathf.Sin(angle * Mathf.Deg2Rad) * velocity.y;
				}
			}
		}
		#endregion
		public void UpdateLevelPath(ref Vector3 velocity) {
			Vector3 _direction = new Vector3(velocity.x, 0, velocity.z).normalized;
			if (_direction == Vector3.zero)
				_direction = this.direction;
			int direction = Vector3.Angle(_direction, position.levelPath.direction) < 90 ? 1 : -1;
			Vector3 nextPosition = transform.position + velocity;
			
			if (direction == 1)
			{
				if (Vector3.Angle((nextPosition - position.levelPath.to).normalized, position.levelPath.direction) < 90)
				{
					int nextLevelPathPos = (position.levelPathPosition + 1);
					if (nextLevelPathPos >= LevelBounds.instance.levelPath.Count && LevelBounds.instance.loop)
						nextLevelPathPos = 0;
					if (LevelBounds.instance.loop || nextLevelPathPos <= LevelBounds.instance.levelPath.Count - 1)
					{
						Vector3 velocityBefore = velocity;
						velocity.x = position.levelPath.to.x - transform.position.x;
						velocity.z = position.levelPath.to.z - transform.position.z;
						position.levelPathPosition = nextLevelPathPos;
						float leftMagnitude = velocityBefore.magnitude - Vector3.Magnitude(new Vector3(velocity.x, 0, velocity.z));
						Vector3 velocityLeft = Vector3.right * leftMagnitude;
						AlongPath(ref velocityLeft);
						velocity += velocityLeft;
						RotateTo(true);
					}
					//checking if not loop and currently at the edge of all levelpath / nextLevelPathPos == levelPath.Count-1
					else if (position.levelPathPosition == LevelBounds.instance.levelPath.Count - 1 && !LevelBounds.instance.loop)
					{
						velocity.x = position.levelPath.to.x - transform.position.x;
						velocity.z = position.levelPath.to.z - transform.position.z;
						RotateTo(true);
					}
				}
			}
			else if (direction == -1) {
				if (Vector3.Angle((nextPosition - position.levelPath.from).normalized, -position.levelPath.direction) < 90)
				{
					int nextLevelPathPos = (position.levelPathPosition - 1);
					
					if (nextLevelPathPos < 0 && LevelBounds.instance.loop)
						nextLevelPathPos = LevelBounds.instance.levelPath.Count - 1;
					if (LevelBounds.instance.loop || nextLevelPathPos >= 0)
					{
						Vector3 velocityBefore = velocity;
						velocity.x = position.levelPath.from.x - transform.position.x;
						velocity.z = position.levelPath.from.z - transform.position.z;
						position.levelPathPosition = nextLevelPathPos;
						float leftMagnitude = velocityBefore.magnitude - Vector3.Magnitude(new Vector3(velocity.x, 0, velocity.z));
						Vector3 velocityLeft = -Vector3.right * leftMagnitude;
						AlongPath(ref velocityLeft);
						velocity += velocityLeft; RotateTo(false);
					}
					//checking if not loop and currently at the edge of all levelpath / nextLevelPathPos == levelPath.Count-1
					else if (position.levelPathPosition == 0 && !LevelBounds.instance.loop)
					{
						velocity.x = position.levelPath.from.x - transform.position.x;
						velocity.z = position.levelPath.from.z - transform.position.z;
						RotateTo(false);
					}
				}
			}
		}
		/// <summary>
		/// Calculate next position with diference levelpath
		/// </summary>
		/// <param name="directionTo">if directionTo true, then entity will align to the new level path 'from' point. if false, entity will align to the new level path 'to' point</param>
		protected Vector3 GetPositionWhenTransitionLevelPath(bool directionTo, LevelPath levelPath, Vector3 velocity)
		{
			if (directionTo)
				return new Vector3(levelPath.from.x + velocity.x, transform.position.y + velocity.y, levelPath.from.z + velocity.z);
			else
				return new Vector3(levelPath.to.x + velocity.x, transform.position.y + velocity.y, levelPath.to.z + velocity.z);
		}
		public virtual void ApplyRotation()
		{
			if (rawDirection == 1 && Vector3.Angle(direction, position.levelPath.direction) > 90)
				RotateTo(true);
			else if (rawDirection == -1 && Vector3.Angle(direction, position.levelPath.direction) < 90)
				RotateTo(false);
		}

		/// <summary>Translate Velocity to Direction LevelPath</summary>
		/// <param name="velocity">Raw Velocity, before any process, x for right or left, y for up or down</param>
		/// <returns>Direction Level path</returns>
		Vector3 VelocityToDirection(Vector2 velocity)
		{
			if (velocity.x == 0)
				return direction;
			return (Mathf.Sign(velocity.x) == 1 ? position.levelPath.direction : -position.levelPath.direction);
		}

		public class AdvanceState {
			public bool isClimbingLadder;

			public virtual void Reset() {
			}
		}

		public class ControllerState
		{
			/// <summary>
			/// Determines controller collision state
			/// </summary>
			public bool collisionBelow, collisionAbove, collisionForward;
			public bool lastFrameCollisionBelow = false;
			public bool isClimbingSlope;
			public bool stomping;
			/// <summary>
			/// Determines hit normal direction
			/// </summary>
			public Vector3 horizontalHitNormal, verticalHitNormal;
			public float slopeAngleHorizontal = 0, oldSlopeAnglesHorizontal;
			public float slopeAngleVertical = 0, oldSlopeAnglesVertical;
			public bool isDescendingSlope;
			public float stompYVelocity;
			/// <summary>
			/// Determines velocity before any process
			/// </summary>
			internal Vector3 velocityBeforeProcess;
			/// <summary>
			/// Determines if controller collide with wall (90 degress platform)
			/// </summary>
			public bool collisionWall;
			/// <summary>
			/// Determines the wall direction. 1 = forward, -1 = backward
			/// </summary>
			public int wallDirection;
			public bool isSlidingSlope;
			public bool isSpecialGrounded, lastFrameSpecialGrounded;

#if PROFORMER3D
			public Collider colliderBelow;
			public Collider passThisCollider;
#else
			public Collider2D colliderBelow;
			public Collider2D passThisCollider;
#endif

			public ControllerState(){}

			public void Reset()
			{
				lastFrameCollisionBelow = collisionBelow;
				collisionBelow = collisionAbove = collisionForward = false;
				isClimbingSlope = isDescendingSlope = isSlidingSlope = false;
				horizontalHitNormal = verticalHitNormal = Vector3.zero;
				oldSlopeAnglesHorizontal = slopeAngleHorizontal;
				oldSlopeAnglesVertical = slopeAngleVertical;
				slopeAngleVertical = slopeAngleHorizontal = 0;
				colliderBelow = null;
				wallDirection = 0;
				collisionWall = false;
				stomping = false;
				lastFrameSpecialGrounded = isSpecialGrounded;
				isSpecialGrounded = false;
			}

			public void LateReset() {
			}
		}

		public class StandartInputController
		{
			public Vector2 directionalInput;
			public bool jumpDownOneWayTrigger;
		}

		[System.Serializable]
		public class Raycaster
		{
			DIController controller;
			public DICharacterCollider pc;
			Transform transform;
			Vector3 direction;
			public float skinWidth = .025f, skinDepth = .3f;
			int horizontalRayCount, verticalRayCount;

			public float height { get { return pc.height; } }
			public float width { get { return pc.width; } }
			public Vector3 center { get { return controller.rawDirection == 1 ? pc.center : new Vector3(pc.center.x * -1, pc.center.y, pc.center.z * -1); } }
			public float verticalSpace { get { return (pc.width / 2 - skinWidth) * 2 / (verticalRayCount - 1); } }
			public float horizontalSpace { get { return (pc.height - skinWidth * 2) / (horizontalRayCount - 1); } }
			public float horizontalSpaceModified { get { return (pc.height - 0.008f * 2) / (horizontalRayCount - 1); } }
			public float customHorizontalSpace(DICharacterCollider pc) { return (pc.height - skinWidth * 2) / (horizontalRayCount - 1); }

			public Raycaster(DIController controller, DICharacterCollider pc, int horizontalRayCount, int verticalRayCount)
			{
				this.controller = controller;
				this.pc = pc;
				try
				{
					transform = pc.collider.transform;
				}
				catch {
					InternalTool.ProformerDebugError("ProformerSetupNotComplete", "No Collider assigned", controller);
				}
				this.horizontalRayCount = horizontalRayCount;
				this.verticalRayCount = verticalRayCount;
			}

			public void UpdateDirection(Vector3 dir)
			{
				direction = dir;
				//direction90 = Quaternion.Euler(0, 90, 0) * direction;
			}
			public Vector3 rightDirection { get { return (frontOrigin - centerOrigin).normalized; } }
			public Vector3 leftDirection { get { return (backOrigin - centerOrigin).normalized; } }
			public Vector3 faceDirection { get { return (frontOrigin - backOrigin).normalized; } }
			public Vector3 centerOrigin { get { Vector3 v = transform.position + center; return v; } }
			public Vector3 frontOrigin
			{
				get { Vector3 v = transform.position + center + direction * (pc.width / 2 - skinWidth); return v; }
			}
			public Vector3 backOrigin
			{
				get { Vector3 v = transform.position + center - direction * (pc.width / 2 - skinWidth); return v; }
			}
			public Vector3 rightOrigin
			{
				get { Vector3 v = transform.position + center + controller.position.levelPath.direction * (pc.width / 2 - skinWidth); return v; }
			}
			public Vector3 leftOrigin
			{
				get { Vector3 v = transform.position + center - controller.position.levelPath.direction * (pc.width / 2 - skinWidth); return v; }
			}
			public Vector3 bottomCenterOrigin
			{
				get { Vector3 v = transform.position + center; v.y -= ((pc.height - skinWidth) * .5f); return v; }
			}
			public Vector3 topCenterOrigin
			{
				get { Vector3 v = transform.position + center; v.y += ((pc.height - skinWidth) * .5f); return v; }
			}
			public Vector3 frontBottomOrigin
			{
				get { Vector3 v = frontOrigin + Vector3.down * ((pc.height / 2) - skinWidth); return v; }
			}
			public Vector3 frontTopOrigin
			{
				get { Vector3 v = frontOrigin + Vector3.up * ((pc.height / 2) - skinWidth); return v; }
			}
			public Vector3 backBottomOrigin
			{
				get { Vector3 v = backOrigin + Vector3.down * ((pc.height / 2) - skinWidth); return v; }
			}
			public Vector3 backTopOrigin
			{
				get { Vector3 v = backOrigin + Vector3.up * ((pc.height / 2) - skinWidth); return v; }
			}
			//Temporary Fix
			public Vector3 frontBottomOriginHorizontal
			{
				get { Vector3 v = frontOrigin + Vector3.down * ((pc.height / 2) - 0.008f); return v; }
			}


			public Vector3 frontOriginCustom(DICharacterCollider c)
			{
				Vector3 v = c.collider.transform.position + (controller.rawDirection == 1 ? c.center : new Vector3(c.center.x * -1, c.center.y, c.center.z * -1) + direction * (c.width / 2 - skinWidth)); return v;
			}
			public Vector3 backTopOriginCustom(DICharacterCollider c) {
				Vector3 v = c.collider.transform.position + c.center - direction * (c.width / 2 - skinWidth); v += Vector3.up * (c.height / 2 - skinWidth); return v;
			}
		}

		#region Public Method
		public float getHorizontalMovement { get { return currentMovement.x; } }
		#endregion
	}
	
}