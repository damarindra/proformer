﻿using UnityEngine;
using System.Collections.Generic;
using DI.Proformer.ToolHelper;
using UnityEngine.SceneManagement;
using DI.Proformer.Manager;
using System.IO;
using System;

#if UNITY_EDITOR
using UnityEditorInternal;
using UnityEditor;
#endif



namespace DI.Proformer.Manager{
	/// <summary>
	/// Game Manager, control the game!.
	/// </summary>
	public class GameManager : Singleton<GameManager> {

		[HideInInspector]
		public DIPlayer player;
		[HideInInspector]
		public LevelBounds levelBound;
		
		public List<CheckPoint> checkPoints = new List<CheckPoint>();
		public CheckPoint currentCheckPoint;
		public GameObject checkPointTemplate;
		public bool showCheckPointProperty = true;

		[HideInInspector]
		private DICamera _mainCamera;
		public DICamera mainCamera { get { return _mainCamera; } set { _mainCamera = value; } }

		void Start() {
			Setup();
		}
#if UNITY_5_3
		void OnLevelWasLoaded(int index) {
			Setup();
		}
#else
		void OnEnable()
		{
			SceneManager.sceneLoaded += OnLevelFinishedLoading;
		}
		void OnDisable()
		{
			SceneManager.sceneLoaded -= OnLevelFinishedLoading;
		}
		void OnLevelFinishedLoading(Scene scene, LoadSceneMode mode)
		{
			Setup();
		}
#endif

		void Setup(){
			if (GameData.Instance.scenes.Contains(SceneManagerTools.GetCurrentSceneName())) {
				int indexScreen = GameData.Instance.scenes.IndexOf(SceneManagerTools.GetCurrentSceneName());
				bool isGameplayType = GameData.Instance.gameScreen[indexScreen].gameScreenType == GameData.GameScreenType.Gameplay ? true : false;
				if (!isGameplayType) {
					if (!GameData.Instance.scenes.Contains(SceneManagerTools.GetCurrentSceneName()))
						Debug.LogWarning("Please Add : " + SceneManagerTools.GetCurrentSceneName() + " Scene to Build Settings");
					return;
				}
			}else
				Debug.LogWarning("Please Add : " + SceneManagerTools.GetCurrentSceneName() + " Scene to Build Settings");

			if (player == null)
				player = GameObject.FindObjectOfType<DIPlayer>();
			if (levelBound == null)
				levelBound = GameObject.FindObjectOfType<LevelBounds> ();

			if(checkPointTemplate)
				checkPointTemplate.SetActive(false);

			if (GameData.Instance.checkPointType == GameData.CheckPointType.LastStand) {
				foreach (CheckPoint cp in checkPoints)
					cp.gameObject.SetActive(false);
				if (player != null)
				{
					GameObject cp = new GameObject("CheckPoint");
					cp.SetActive(false);
					currentCheckPoint = cp.AddComponent<CheckPoint>();
					currentCheckPoint.position.SetLevelPathPosition(0);
					currentCheckPoint.owner = player.controller;
					player.controller.respawnPosition = currentCheckPoint;
					cp.SetActive(true);
				}
				currentCheckPoint.followOwner = true;
			}
			else if (GameData.Instance.checkPointType == GameData.CheckPointType.Placing) {
				if (currentCheckPoint == null)
				{
					if (checkPoints.Count == 0)
					{
						if (player != null)
						{
							GameObject cp = new GameObject("CheckPoint");
							cp.SetActive(false);
							currentCheckPoint = cp.AddComponent<CheckPoint>();
							currentCheckPoint.position.SetLevelPathPosition(0);
							currentCheckPoint.owner = player.controller;
							player.controller.respawnPosition = currentCheckPoint;
							cp.SetActive(true);
						}
					}
					else
					{
						currentCheckPoint = checkPoints[0];
					}
				}
				player.controller.bodyCollider.collider.GetComponent<DICharacterBody>().onTriggerEnter += onCheckPoint;
			}

			//Get Main Camera
			mainCamera = FindObjectOfType<DICamera>();

		}

#if PROFORMER3D
		void onCheckPoint(Collider col)
#else
		void onCheckPoint(Collider2D col)
#endif
		{
			if (col.GetComponent<CheckPoint>()) {
				GameManager.Instance.currentCheckPoint = col.GetComponent<CheckPoint>();
				player.controller.respawnPosition = currentCheckPoint;
			}
		}


	}
}