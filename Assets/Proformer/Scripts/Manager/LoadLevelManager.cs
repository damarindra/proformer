﻿using UnityEngine;
using System.Collections;
using DI.Proformer.Manager;
using DI.Proformer.ToolHelper;
using UnityEngine.SceneManagement;

namespace DI.Proformer.Manager {
	public class LoadLevelManager : Singleton<LoadLevelManager> {
		public void LoadLevel(string levelName) {
			LoadLevel(levelName, GameData.Instance.fadeDuration);
		}
		public void LoadLevel(string levelName, float transitionDuration, bool coroutine = true)
		{
			if (GUIManager.instance != null) {
				if (GUIManager.instance._useLoading) {
					GUIManager.instance.CoverWithLoading();
					if (GUIManager.instance.loadingBG != null)
						GUIManager.instance.FadeImage(GUIManager.instance.loadingBG, 1, transitionDuration);
					if (GUIManager.instance.loadingBarBg != null)
						GUIManager.instance.FadeImage(GUIManager.instance.loadingBarBg, 1, transitionDuration);
					if (GUIManager.instance.loadingBar != null)
					{
						GUIManager.instance.FadeImage(GUIManager.instance.loadingBar, 1, transitionDuration);
						GUIManager.instance.loadingBar.fillAmount = 0;
					}
				}
			}

			StartCoroutine(LoadLevelCo(levelName, transitionDuration, coroutine));
		}

		IEnumerator LoadLevelCo(string levelName, float transitionDuration, bool coroutine)
		{
			yield return new WaitForSeconds(transitionDuration);
			if (coroutine)
			{
				AsyncOperation asyn = SceneManagerTools.LoadLevelAsync(levelName);
				while (!asyn.isDone)
				{
					if (GUIManager.instance != null) {
						if (GUIManager.instance.loadingBar != null && GUIManager.instance._useLoading)
						{
							GUIManager.instance.loadingBar.fillAmount = asyn.progress;
						}
					}
					yield return null;
				}
			}
			else
				SceneManagerTools.LoadScene(levelName);
		}
	}
}
