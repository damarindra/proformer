﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DI.Proformer.ToolHelper;
using UnityEngine.SceneManagement;

namespace DI.PoolingSystem{
	
	public class PoolingManager : Singleton<PoolingManager> {
		[System.Serializable]
		public class PoolCandidate
		{
			public GameObject go;
			public int size;
		}
		
		Dictionary<int, Queue<ObjectInstance>> poolDictionary = new Dictionary<int, Queue<ObjectInstance>>();
		Transform poolParent;
		public bool destroyOnLoad = true;

		public int defaultSize = 15;

		void Start(){
			InitPool();
		}

#if UNITY_5_3
		void OnLevelWasLoaded(int index) {
			if (Instance == this) {
				if (destroyOnLoad)
					InitPool();
				else
					DisableAllPooled();
			}
		}
#else
		void OnEnable()
		{
			SceneManager.sceneLoaded += OnLevelFinishedLoading;
		}
		void OnDisable()
		{
			SceneManager.sceneLoaded -= OnLevelFinishedLoading;
		}
		void OnLevelFinishedLoading(Scene scene, LoadSceneMode mode)
		{
			if (Instance == this)
			{
				if (destroyOnLoad)
					InitPool();
				else
					DisableAllPooled();
			}
		}
#endif

		private void DisableAllPooled()
		{
			foreach (Queue<ObjectInstance> obj in poolDictionary.Values) {
				for (int i = 0; i < obj.Count; i++) {
					ObjectInstance go = obj.Dequeue();
					go.Disable();
					obj.Enqueue(go);
				}
			}
		}

		void InitPool() {
			//Debug.Log("Times");
			poolParent = new GameObject("PoolParent").transform;
			if (!destroyOnLoad)
				DontDestroyOnLoad(poolParent);
			poolDictionary.Clear();

			foreach (PoolCandidate pc in GameData.Instance.objectWillBePooled)
			{
				CreatePool(pc.go, pc.size);
			}
		}
		public void DestroyPool() {
			poolDictionary.Clear();
		}

		public void CreatePool(GameObject prefab, int poolSize){
			int key = prefab.GetInstanceID ();

			if (!poolDictionary.ContainsKey (key)) {
				Transform poolHolder = new GameObject (prefab.name + " parent").transform;
				poolHolder.SetParent (poolParent);
				poolDictionary.Add (key, new Queue<ObjectInstance> ());

				for (int i = 0; i < poolSize; i++) {
					ObjectInstance go = new ObjectInstance( Instantiate (prefab) as GameObject);
					//Add to the last of queue
					poolDictionary [key].Enqueue (go);
					go.SetParent (poolHolder);
				}
			}
		}

		IEnumerator CreatePoolCo(GameObject prefab, int poolSize) {
			int key = prefab.GetInstanceID();

			if (!poolDictionary.ContainsKey(key))
			{
				Transform poolHolder = new GameObject(prefab.name + " parent").transform;
				poolHolder.SetParent(poolParent);
				poolDictionary.Add(key, new Queue<ObjectInstance>());

				for (int i = 0; i < poolSize; i++)
				{
					ObjectInstance go = new ObjectInstance(Instantiate(prefab) as GameObject);
					//Add to the last of queue
					poolDictionary[key].Enqueue(go);
					go.SetParent(poolHolder);
					yield return null;
				}
			}
		}

		public void ReuseObject(GameObject prefab, Vector3 position, Quaternion rotation) {
			ReuseObject(prefab, position, rotation, null);
		}
		public void ReuseObject(GameObject prefab, Vector3 position, Quaternion rotation, UnityEngine.Object obj){
			int key = prefab.GetInstanceID ();

			if (poolDictionary.ContainsKey(key))
			{
				//Get the first queue
				ObjectInstance go = poolDictionary[key].Dequeue();
				//Add to the last of queue
				poolDictionary[key].Enqueue(go);

				go.Reuse(position, rotation, obj);
			}
			else{
				CreatePool(prefab, defaultSize);
				ReuseObject(prefab, position, rotation, obj);
			}
		}

		public class ObjectInstance {
			GameObject go;
			Transform tr;

			bool hasPoolObjectComponent;
			IPoolObject poolObjectScript;

			public ObjectInstance(GameObject objectInstance){
				go = objectInstance;
				tr = objectInstance.transform;
				go.SetActive(false);

				if(go.GetComponent<IPoolObject>() != null){
					hasPoolObjectComponent = true;
					poolObjectScript = go.GetComponent<IPoolObject>();
				}
			}

			public void Reuse(Vector3 position, Quaternion rotation) {
				Reuse(position, rotation, null);
			}
			public void Reuse(Vector3 position, Quaternion rotation, UnityEngine.Object obj){
				go.SetActive (true);
				go.transform.position = position;
				go.transform.rotation = rotation;

				if (hasPoolObjectComponent) {
					poolObjectScript.OnObjectReuse (obj);
				}
			}

			public void SetParent(Transform parent){
				tr.SetParent (parent);
			}

			public void Disable() {
				poolObjectScript.Disable();
			}
		}
	}
}
