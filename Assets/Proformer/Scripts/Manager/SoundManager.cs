﻿using UnityEngine;
using System.Collections;
using DI.Proformer.ToolHelper;
using DI.PoolingSystem;
using UnityEngine.SceneManagement;

namespace DI.Proformer.Manager {
	/// <summary>
	/// Sound Manager, used for controlling sounds on the game
	/// </summary>
	[RequireComponent(typeof(AudioSource))]
	public class SoundManager : Singleton<SoundManager> {
		
		private AudioSource backgroundSource;

		// Use this for initialization
		void Start () {
			backgroundSource = GetComponent<AudioSource>();
			backgroundSource.loop = true;
			UpdateBGM();
		}

#if UNITY_5_3
		void OnLevelWasLoaded(int index) {
			if (Instance != this) {
				Destroy(gameObject);
				return;
			}
			UpdateBGM();
		}
#else
		void OnEnable()
		{
			SceneManager.sceneLoaded += OnLevelFinishedLoading;
		}
		void OnDisable()
		{
			SceneManager.sceneLoaded -= OnLevelFinishedLoading;
		}
		void OnLevelFinishedLoading(Scene scene, LoadSceneMode mode)
		{
			if (Instance != this)
			{
				Destroy(gameObject);
				return;
			}
			UpdateBGM();
		}
#endif

		void UpdateBGM() {
			if (backgroundSource == null)
				backgroundSource = GetComponent<AudioSource>();

			//string activeScene = SceneManager.GetActiveScene().name;
			if (GameData.Instance.scenes.Contains(SceneManagerTools.GetCurrentSceneName())) {
				int indexScene = GameData.Instance.scenes.IndexOf(SceneManagerTools.GetCurrentSceneName());
				if (backgroundSource.clip != GameData.Instance.backgroundMusics[indexScene].backgroundMusic) {
					ChangeBackgroundMusic(GameData.Instance.backgroundMusics[indexScene].backgroundMusic);
				}
			}
		}

		public void ChangeBackgroundMusic(AudioClip clip) {
			backgroundSource.Stop();
			backgroundSource.clip = clip;
			backgroundSource.Play();
		}
		public void PlaySFX(AudioClip clip) {
			if(clip != null)
				PoolingManager.Instance.ReuseObject(GameData.Instance.sfxTemplate, transform.position, Quaternion.identity, clip);
		}
	}
}
