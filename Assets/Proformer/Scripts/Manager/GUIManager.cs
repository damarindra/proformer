﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using DI.Proformer.ToolHelper;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System;

#if UNITY_EDITOR
using UnityEditor;
using UnityEditor.SceneManagement;
#endif


namespace DI.Proformer.Manager {
	public class GUIManager : MonoBehaviour {
		public static GUIManager instance = null;

		public bool _usePreviewCharacter = true, previewHide;
		public Image characterPreviewBG, characterPreview;

		public bool _useHealthBar = true, healthHide;
		public Image healthArea, healthValue;

		public bool _useLoading = true, loadingHide;
		public Image loadingBG, loadingBarBg, loadingBar;

		public GameObject gameOverPanel;
		
		public List<SimpleGUITarget> simpleGUITargets = new List<SimpleGUITarget>();

		/// <summary>
		/// Updating Health Value
		/// </summary>
		/// <param name="value">Current Value</param>
		/// <param name="maxValue">max Health Value</param>
		public void UpdateHealthUI(int value, int maxValue) {
			UpdateImageFill(healthValue, value, maxValue);
		}
		/// <summary>
		/// Update Image Fill Amount
		/// </summary>
		/// <param name="bar">Image</param>
		/// <param name="value">Current Value</param>
		/// <param name="maxValue">Max value</param>
		public void UpdateImageFill(Image bar, int value, int maxValue)
		{
			float target = value / (float)maxValue;
			if (target < 0)
				target = 0;
			bar.fillAmount = target;
		}
		void Start() {
			if (instance == null)
				instance = this;
			else if (instance != this)
				Destroy(gameObject);
			StartCoroutine(Setup());
		}

		IEnumerator Setup() {

			gameOverPanel.SetActive(false);

			GetComponentInChildren<Canvas>().sortingOrder = 99;

			if (previewHide)
			{
				characterPreviewBG.gameObject.SetActive(true);
				characterPreview.gameObject.SetActive(true);
			}
			if (healthHide)
			{
				healthArea.gameObject.SetActive(true);
				healthValue.gameObject.SetActive(true);
			}
			if (loadingHide)
			{
				loadingBG.gameObject.SetActive(true);
				loadingBarBg.gameObject.SetActive(true);
				loadingBar.gameObject.SetActive(true);
			}
			yield return new WaitForEndOfFrame();

			if (loadingBG != null)
			{
				if (loadingBG.color.a != 0)
					FadeImage(loadingBG, 0, GameData.Instance.fadeDuration);
				else loadingBG = null;
			}
			if (loadingBarBg != null)
			{
				if (loadingBarBg.color.a != 0)
					FadeImage(loadingBarBg, 0, GameData.Instance.fadeDuration);
				else loadingBarBg = null;
			}
			if (loadingBar != null)
			{
				if (loadingBar.color.a != 0)
					FadeImage(loadingBar, 0, GameData.Instance.fadeDuration);
				else loadingBar = null;
			}

			Image loadImgActive = loadingBG ? loadingBG : loadingBarBg ? loadingBarBg : loadingBar;
			if (loadImgActive != null)
			{
				while (loadImgActive.color.a > .03f)
					yield return null;
				GetComponentInChildren<Canvas>().sortingOrder = -5;

			}

		}

		void Update() {
			foreach (SimpleGUITarget sgt in simpleGUITargets) {
				if (sgt.targetText != null) {
					/*if (sgt.targetVariable.targetObjType != null)
					{
						//if(sgt.targetVariable.Value != null)
						if(sgt.targetVariable.typeAllowed == DIVariableType.Int)
							sgt.targetText.text = string.IsNullOrEmpty(sgt.prefixText) ? sgt.targetVariable.IntValue.ToString() : sgt.prefixText + sgt.targetVariable.IntValue.ToString();
						else if (sgt.targetVariable.typeAllowed == DIVariableType.Float)
							sgt.targetText.text = string.IsNullOrEmpty(sgt.prefixText) ? sgt.targetVariable.FloatValue.ToString() : sgt.prefixText + sgt.targetVariable.FloatValue.ToString();
						else if (sgt.targetVariable.typeAllowed == DIVariableType.String)
							sgt.targetText.text = string.IsNullOrEmpty(sgt.prefixText) ? sgt.targetVariable.StringValue : sgt.prefixText + sgt.targetVariable.StringValue;
					}*/
				}
			}
			if (GameManager.Instance.player) {
				UpdateHealthUI(GameManager.Instance.player.controller.health, GameManager.Instance.player.controller.maxHealth);
			}
			
		}

		public void CoverWithLoading()
		{
			Image loadImgActive = loadingBG ? loadingBG : loadingBarBg ? loadingBarBg : loadingBar;
			if (loadImgActive != null)
				GetComponentInChildren<Canvas>().sortingOrder = 99;

		}
		public void LoadLevel(string levelName)
		{
			if(LoadLevelManager.Instance)
				LoadLevelManager.Instance.LoadLevel(levelName, GameData.Instance.fadeDuration);
		}

		public void FadeImage(Image image, float alpha, float duration)
		{
			StartCoroutine(FadeImageCo(image, alpha, duration));
		}
		static IEnumerator FadeImageCo(Image image, float alpha, float duration)
		{
			/*Color startColor = image.color;
			Color targetColor = image.color;
			targetColor.a = alpha;
			for (float i = 0.0f; i < 1.0f; i += Time.deltaTime * duration) {
				image.color = Color.Lerp(startColor, targetColor, i);
				yield return null;
			}
			image.color = targetColor;
			*/
			float step = 1 / duration * Time.deltaTime;
			step *= ((image.color.a > alpha) ? 1 : -1);
			if (Mathf.Sign(step) == 1)
			{
				while (image.color.a >= alpha)
				{
					image.color = new Color(image.color.r, image.color.g, image.color.b, image.color.a - step);
					yield return null;
				}
			}
			else {
				while (image.color.a <= alpha)
				{
					image.color = new Color(image.color.r, image.color.g, image.color.b, image.color.a - step);
					yield return null;
				}
			}
			image.color = new Color(image.color.r, image.color.g, image.color.b, alpha);
		}

		public void ShowGameOverPanel()
		{
			gameOverPanel.SetActive(true);
		}
	}
}