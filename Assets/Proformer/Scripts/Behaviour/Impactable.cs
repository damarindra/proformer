﻿using UnityEngine;

namespace DI.Proformer {
	/// <summary>
	/// Impactable, attach this script for giving Impact Character Basic.
	/// On Character Basic side, set the Impactable Tag
	/// </summary>
	public class Impactable : MonoBehaviour {
		
		/// <summary>Impact Power</summary>
		public float power = 15f;
		[HideInInspector]
		public Vector3 worldSourceImpact;
		/// <summary>Local Position Impact</summary>
		public Vector3 offset = Vector3.down;

		public enum ImpactType {
			LOCAL, GLOBAL
		}
		public ImpactType impactType;

		void Start() {
#if PROFORMER3D
			GetComponent<Collider>().isTrigger = true;
#else
			GetComponent<Collider2D>().isTrigger = true;
#endif
		}

		void OnDrawGizmos() {
			Gizmos.color = Color.red;
			if (!Application.isPlaying)
				Gizmos.DrawWireSphere(transform.position + offset, .1f);
		}

#if UNITY_EDITOR
		/// <summary>
		/// EDITOR THINGS
		/// </summary>
		[HideInInspector]
		public bool editSources = false;
#endif
	}
}