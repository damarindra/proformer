﻿using UnityEngine;
using System.Collections;
using DI.Proformer.ToolHelper;

namespace DI.Proformer {
	public class Stompable : MonoBehaviour {
		
		/// <summary>Stomp Power</summary>
		public float stompPower = 10;
		/// <summary>Instant kill when stomped</summary>
		public bool killWhenStomped = true;
		/// <summary>Stomp damage, only active when killWhenStomped is false</summary>
		[Conditional("Hide","killWhenStomped", true, true)]
		public int stompDamage = 30;

		[HideInInspector]
		public DIControllerMotor motor;

		void Start() {
			motor = GetComponentInParent<DIControllerMotor>();
		}

		public void Stomped(ref Vector3 velocity) {
			velocity.y += stompPower * Time.deltaTime;
			if (killWhenStomped)
			{
				motor.health = 0;
			}
			else {
				motor.health -= stompDamage;
			}

			if (motor.health <= 0)
				motor.Dead();
		}
	}
}
