﻿using UnityEngine;
using System.Collections;

namespace DI.Proformer {
	/// <summary>
	/// Give Damage is used for giving damage to target.
	/// Object Give Damage must have a 'Tag'.
	/// On Character side must set Take Damage Tag to listen Give Damage Script.
	/// </summary>
	public class GiveDamage : MonoBehaviour {

		/// <summary>Total damage given</summary>
		public int damageGiven = 30;
		/// <summary>Freezing time, disable movement</summary>
		public float freezeTime = .2f;

		public delegate void Triggered(GameObject go);
		public Triggered triggered;

		void Start() {
#if PROFORMER3D
			GetComponent<Collider>().isTrigger = true;
#else
			GetComponent<Collider2D>().isTrigger = true;
#endif
		}
		
	}
}
