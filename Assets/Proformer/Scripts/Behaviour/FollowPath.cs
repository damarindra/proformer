﻿using UnityEngine;
using System.Collections.Generic;
using DI.Proformer.ToolHelper;
using DI.Proformer.Manager;

namespace DI.Proformer {
	/// <summary>
	/// Follow Path used if you want to make a gameobject following path\n
	/// Originally created by : Sebastian Lague\n
	/// Modified by : Damar Inderajati\n
	/// </summary>
	[RequireComponent(typeof(Path)), RequireComponent(typeof(DIEntity))]
	public class FollowPath : MonoBehaviour {
		/// <summary>Speed</summary>
		public float speed = 2;
		/// <summary>wait time each point</summary>
		public float waitTimeEachPoint;
		/// <summary>movement smoothing</summary>
		[Range(0, 5)]
		public float smoothMovement = 0f;
		
		int fromIndex = 0;
		float currentPercentageWaypoint;
		float nextMoveTime;
		/// <summary>Rotate the object</summary>
		public bool useLookAt = false;
		/// <summary>Looping</summary>
		public bool looping = true;
		public enum LoopMode { PingPong, Once }
		[Conditional("Hide", "looping", true, true)]
		/// <summary>Loop Mode</summary>
		public LoopMode loopMode = LoopMode.PingPong;
		public ExecuteMode executeMode = ExecuteMode.Start;
		public bool isMoving { get; set; }
		
		protected DIControllerMotor controller;
		protected Path path;
		[HideInInspector]
		public Vector3 velocity;

		List<Vector3> pathWay = new List<Vector3>();

		// Use this for initialization
		void Start () {
			path = GetComponent<Path>();
			controller = GetComponent<DIControllerMotor>();
			if (executeMode == ExecuteMode.Start)
				isMoving = true;
			foreach (DIPosition point in path.pathList)
				pathWay.Add(point.position);
			if (path.connetLastPoint && !looping)
				pathWay.Add(pathWay[0]);
		}

		// Update is called once per frame
		void Update()
		{
			if (isMoving) {
				if (controller)
				{
					if (controller.canMove && !controller.isDead)
					{
						velocity = CalculateMovement();
						AnimatorTool.SetBool(controller.animator, "Grounded", true);
						AnimatorTool.SetFloat(controller.animator, "Speed", velocity.magnitude / Time.deltaTime);

						transform.Translate(velocity, Space.World);
					}
				}
				else {
					velocity = CalculateMovement();
					transform.Translate(velocity, Space.World);
					
				}

				if (useLookAt) {
					controller.rawDirection = Vector3.Angle(velocity.normalized, controller.position.levelPath.direction) < 90 ? 1 : -1;
					controller.ApplyRotation();
				}
			}
		}

		public Vector2 VelocityToVector2(int LevelPathPosition) {
			Vector2 result = velocity;
			float horizontal = new Vector3(velocity.x, 0, velocity.z).magnitude;
			if (Vector3.Angle(LevelBounds.instance.levelPath[LevelPathPosition].direction, velocity) < 90)
			{
				result.x = horizontal;
			}
			else
				result.x = -horizontal;
			return result;
		}

		float Ease(float x)
		{
			float a = smoothMovement + 1;
			return Mathf.Pow(x, a) / (Mathf.Pow(x, a) + Mathf.Pow(1 - x, a));
		}

		Vector3 CalculateMovement()
		{

			if (Time.time < nextMoveTime)
			{
				return Vector3.zero;
			}

			fromIndex %= pathWay.Count;
			int toWaypointIndex = (fromIndex + 1) % pathWay.Count;
			float distanceBetweenWaypoints = Vector3.Distance(pathWay[fromIndex], pathWay[toWaypointIndex]);
			currentPercentageWaypoint += Time.deltaTime * speed / distanceBetweenWaypoints;
			currentPercentageWaypoint = Mathf.Clamp01(currentPercentageWaypoint);
			float ease = Ease(currentPercentageWaypoint);

			Vector3 newPos = Vector3.Lerp(pathWay[fromIndex], pathWay[toWaypointIndex], ease);

			if (currentPercentageWaypoint >= 1)
			{
				currentPercentageWaypoint = 0;
				fromIndex++;

				if (loopMode == LoopMode.PingPong && !looping)
				{
					if (fromIndex >= pathWay.Count - 1)
					{
						fromIndex = 0;
						Vector3[] newArray = pathWay.ToArray();
						System.Array.Reverse(newArray);
						pathWay = new List<Vector3>(newArray);
					}
				}
				nextMoveTime = Time.time + waitTimeEachPoint;

				if (loopMode == LoopMode.Once && fromIndex % (pathWay.Count - 1) == 0) {
					isMoving = false;
					return Vector3.zero;
				}
			}

			return newPos - transform.position;
		}

		public void Execute() {
			isMoving = true;
		}
		public void StopExecute() {
			isMoving = false;
		}
	}
}