﻿using UnityEngine;
using System.Collections;
using DI.Proformer.Action;
using DI.Proformer.ToolHelper;

namespace DI.Proformer {
	/// <summary>Character simple AI using FSM</summary>
	public class DICharacterFSM : MonoBehaviour
	{
		#region Base State
		public enum State {
			Idle,
			Moving,
			Shooting,

			Dead
		}

		State mState = State.Idle;

		public string GetStateName() {
			return mState.ToString();
		}
		public string UpdateState() {
			return "Running State : " + GetStateName();
		}
		public void SetState(State state) {
			mState = state;
		}
		public void NextState() {
			switch (mState) {
				case State.Idle:
					if (CanShootTarget())
						mState = State.Shooting;
					else {
						mState = State.Moving;
						nextStateTime = Time.time + UnityEngine.Random.Range(minMoveTime, maxMoveTime);
					}
					break;
				case State.Moving:
					if (CanShootTarget())
						mState = State.Shooting;
					else {
						mState = State.Idle;
						nextStateTime = Time.time + UnityEngine.Random.Range(minIdleTime, maxIdleTime);
					}
					break;
				case State.Shooting:
					mState = State.Idle;
					break;
				case State.Dead:
					break;

				default:
					mState = State.Idle;
					break;
			}
		}
		#endregion

		#region Field
		public bool movePermission;
		public float maxMoveTime = 5f, minMoveTime = 2f, maxMoveRange = 10;
		public float maxIdleTime = 5f, minIdleTime = 2f, maxIdleRange = 10;
		[SerializeField, Tooltip("Offset length wall detection")]
		private float offsetWallDetectorLength = 1f;
		[SerializeField, Tooltip("Offset position hole detection")]
		private float offsetHoleDetectorPosition = 0.5f;
		[SerializeField, Tooltip("Determines distance player can jumping down")]
		float maxGroundHigh = 2.5f;

		public float shootRange = 8;
		public LayerMask shootTargetMask;
		public StandartInput gunInput = new StandartInput();

		[System.Flags]
		enum UnreachableGroundState
		{
			Stop = 1 //000000
				, ReverseDirection = 2 //000001
				, DontStop = 4 //000010
				, Jumping = 8 //000100
				, Jumping_ReverseDirection = Jumping | ReverseDirection
				, Jumping_Stop = Jumping | Stop
		}
		/// <summary>
		/// Hole Detection Action
		/// </summary>
		[SerializeField]
		UnreachableGroundState holeDetection = UnreachableGroundState.ReverseDirection;
		[SerializeField]
		UnreachableGroundState wallDetection = UnreachableGroundState.ReverseDirection;

		bool _waitForReverseDirection = false;

		bool _isGun { get { return gun; } }
		GunAction gun;
		public DIControllerMotor controller;

		#endregion

		#region MonoBehaviour

		float nextStateTime = -1;
		void Start() {
			gun = GetComponent<GunAction>();
			controller = GetComponent<DIControllerMotor>();
			nextStateTime = Time.time;
		}

		void Update() {
			ApplyFSM();
		}
		void LateUpdate() {
			if (CanShootTarget())
				SetState(State.Shooting);

			switch (mState) {
				case State.Idle:
					if (Time.time >= nextStateTime)
						NextState();
					break;
				case State.Moving:
					if (Time.time >= nextStateTime)
						NextState();
					break;
				case State.Shooting:
					if (!CanShootTarget())
						NextState();
					else {
						Shoot();
						if (controller.standartInput.directionalInput.x != 0)
							controller.standartInput.directionalInput.x = 0;
					}
					break;
				case State.Dead:
					break;
			}
		}

		#endregion

		#region Advance


		static bool UnreachableHasFlag(UnreachableGroundState source, UnreachableGroundState isHasThis)
		{
			return (source & isHasThis) == isHasThis;
		}

		private void ApplyFSM()
		{
			switch (mState) {
				case State.Idle:
					if (controller.standartInput.directionalInput.x != 0)
						controller.standartInput.directionalInput.x = 0;
					break;
				case State.Moving:
					Move();
					break;
				case State.Shooting:
					break;
				case State.Dead:
					break;
			}

			controller.Motoring();
		}

		void Move() {
			if (!movePermission)
				return;
			//Random moving direction if not setup yet
			if (controller.standartInput.directionalInput.x == 0)
				controller.standartInput.directionalInput.x = UnityEngine.Random.Range(0, 2) == 0 ? -1 : 1;

			if (!_waitForReverseDirection && controller.state.collisionBelow) {
				WallCollision();
				HoleCollision();
			}
		}

		bool CanShootTarget() {
			if (!_isGun)
				return false;
			else
			{
				Ray ray = new Ray(transform.position + controller.direction * 1.5f, controller.direction);
				ProformerRaycastHit hit;
				InternalTool.ProformerRaycast(ray, out hit, shootRange, shootTargetMask);
				InternalTool.DrawRay(ray.origin, ray.direction * shootRange, Color.green);
				if (hit.collider)
					return true;
			}
			gun.targetInput.input.Reset();

			gun.targetInput.input.up = true;
			return false;
		}

		void Shoot() {
			if (_isGun) {
				gun.targetInput.input.down = true;
			}
		}

		void WallCollision() {
			if (isWallInFront())
			{
				if (UnreachableHasFlag(wallDetection, UnreachableGroundState.Jumping) && controller.state.collisionBelow)
				{
					if (canJumping())
					{
						DoJump(controller.timeToJumpApex * 2.2f);
					}
					else if (UnreachableHasFlag(wallDetection, UnreachableGroundState.ReverseDirection))
					{
						ChangeDirection();
					}
					else if (UnreachableHasFlag(wallDetection, UnreachableGroundState.Stop))
					{
						ChangeDirection();
					}
				}
				else if (UnreachableHasFlag(wallDetection, UnreachableGroundState.ReverseDirection))
				{
					ChangeDirection();
				}
				else if (UnreachableHasFlag(wallDetection, UnreachableGroundState.Stop))
				{
					ChangeDirection();
				}
			}
		}
		void HoleCollision() {

			if (controller.state.collisionBelow)
			{
				if (!isWallInFront())
				{
					Ray ray = new Ray(controller.raycaster.frontBottomOrigin + controller.direction * offsetHoleDetectorPosition, Vector3.down);
					ProformerRaycastHit hit;
					InternalTool.ProformerRaycast(ray, out hit, maxGroundHigh + controller.raycaster.skinWidth, 1 << LayerMask.NameToLayer("Grounds"));
					InternalTool.DrawRay(ray.origin, ray.direction * (maxGroundHigh + controller.raycaster.skinWidth), Color.cyan);
					if (hit.collider != null)
					{
						float angle = Vector3.Angle(Vector3.up, hit.normal);
						if (angle < controller.slopeLimit && controller.standartInput.directionalInput.x == 0)
						{
							//Move
							controller.standartInput.directionalInput.x = UnityEngine.Random.Range(0, 2) == 0 ? -1 : 1;
							controller.RotateTo(controller.standartInput.directionalInput.x == 1);
						}
						else if ((angle > controller.slopeLimit) && holeDetection != UnreachableGroundState.DontStop)
						{
							ChangeDirection();
						}
					}
					else if (holeDetection != UnreachableGroundState.DontStop)
					{
						//SEt current COllisionState
						if (UnreachableHasFlag(holeDetection, UnreachableGroundState.Jumping))
						{
							if (controller.state.collisionBelow)
							{
								//set jump initiate position
								//Check Jump Capability First
								//If can jump, then jump
								if (canJumping() && UnreachableHasFlag(holeDetection, UnreachableGroundState.Jumping))
								{
									DoJump();
								}
								//If not, execute the second condition
								else
									ChangeDirection();
							}
						}
						else if (UnreachableHasFlag(holeDetection, UnreachableGroundState.ReverseDirection) || UnreachableHasFlag(holeDetection, UnreachableGroundState.Stop))
						{
							ChangeDirection();
						}
					}
				}
			}
		}

		void DoJump(float resetTime)
		{
			controller.motorInput.jump.down = true;
			StartCoroutine(ResetJump(resetTime));
		}
		void DoJump()
		{
			DoJump(controller.timeToJumpApex);
		}
		void ChangeDirection() {
			controller.standartInput.directionalInput.x *= -1;
			_waitForReverseDirection = true;
			Invoke("ResetWaitForReverseDirection", controller.accelerationTimeGrounded * 1.5f);
		}
		void ResetWaitForReverseDirection() {
			_waitForReverseDirection = false;
		}
		IEnumerator ResetJump(float resetTime) {
			yield return new WaitForSeconds(resetTime);
			controller.motorInput.jump.down = false;
			controller.motorInput.jump.up = true;
			yield return new WaitForEndOfFrame();
			controller.motorInput.jump.up = false;
		}
		
		bool isWallInFront()
		{
			for (int i = 0; i < controller.horizontalRayCount; i++)
			{
				Vector3 rayOrigin = controller.raycaster.frontBottomOrigin;
				rayOrigin += Vector3.up * (controller.raycaster.horizontalSpace * i);
				float rayLength = controller.raycaster.skinWidth + offsetWallDetectorLength;
				Ray ray = new Ray(rayOrigin, controller.direction);
				ProformerRaycastHit hit;
				InternalTool.ProformerRaycast(ray, out hit, rayLength, controller.groundMask);

				InternalTool.DrawRay(rayOrigin, ray.direction * rayLength, Color.red);
				if (hit.collider != null)
				{
					if (Vector3.Angle(Vector3.up, hit.normal) > controller.slopeLimit)
						return true;
				}
			}
			return false;
		}

		bool canJumping()
		{
			if (!controller.jumpPermission || controller.velocity.magnitude < controller.moveSpeed / 2 || nextStateTime - Time.time < controller.timeToJumpApex * 1.5f)
				return false;
			int segmentCount = 10;
			float segmentScale = 1;
			Vector3[] segments = new Vector3[segmentCount];

			// The first line point is wherever the player's cannon, etc is
			segments[0] = controller.raycaster.backBottomOrigin;

			// The initial velocity
			Vector3 segVelocity = new Vector3(controller.velocity.x, controller.maxJumpVelocity, controller.velocity.z);

			for (int i = 1; i < segmentCount; i++)
			{
				// Time it takes to traverse one segment of length segScale (careful if velocity is zero)
				float segTime = (segVelocity.sqrMagnitude != 0) ? segmentScale / segVelocity.magnitude : 0;

				// Add velocity from gravity for this segment's timestep
				segVelocity = segVelocity + Physics.gravity * segTime;

				// RayLength
				float rayLength = Vector3.Distance(segments[i - 1], segments[i - 1] + segVelocity * segTime);

				// Check to see if we're going to hit a physics object
				Ray ray = new Ray(segments[i - 1], segVelocity * segTime);

				ProformerRaycastHit hit;
				InternalTool.ProformerRaycast(ray, out hit, rayLength, controller.groundMask);

				InternalTool.DrawRay(segments[i - 1], segVelocity * segTime);
				InternalTool.DrawLine(segments[i - 1] + Vector3.down * .2f, segments[i - 1] + Vector3.up * .2f);
				InternalTool.DrawLine(segments[i - 1] + Vector3.left * .2f, segments[i - 1] + Vector3.right * .2f);

				if (hit.collider != null)
				{
					if (hit.collider.tag != "OneWay")
					{
						// set next position to the position where we hit the physics object
						segments[i] = segments[i - 1] + segVelocity.normalized * hit.distance;
						// correct ending velocity, since we didn't actually travel an entire segment
						segVelocity = segVelocity - Physics.gravity * (segmentScale - hit.distance) / segVelocity.magnitude;
						// flip the velocity to simulate a bounce
						segVelocity = Vector3.Reflect(segVelocity, hit.normal);

						if (Vector3.Angle(hit.normal, Vector3.down) < 15)
							return false;
						else if (Vector3.Angle(hit.normal, Vector3.up) <= controller.slopeLimit && Mathf.Abs(hit.point.y - segments[0].y) < maxGroundHigh)
							return true;
						else if (Vector3.Angle(hit.normal, Vector3.up) > controller.slopeLimit)
							return false;
						else if (segVelocity.y < 0)
							return false;
						else if(Mathf.Abs(hit.point.y - segments[0].y) < maxGroundHigh)
							return true;
					}
					else
						segments[i] = segments[i - 1] + segVelocity * segTime;
					/*
					 * Here you could check if the object hit by the Raycast had some property - was 
					 * sticky, would cause the ball to explode, or was another ball in the air for 
					 * instance. You could then end the simulation by setting all further points to 
					 * this last point and then breaking this for loop.
					 */
				}
				// If our raycast hit no objects, then set the next position to the last one plus v*t
				else
				{
					segments[i] = segments[i - 1] + segVelocity * segTime;
				}
			}
			return false;

		}
		#endregion
	}
}
