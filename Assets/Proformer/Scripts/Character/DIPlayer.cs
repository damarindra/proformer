﻿using UnityEngine;
using DI.Proformer.ToolHelper;
using DI.Proformer.Manager;

namespace DI.Proformer {
	/// <summary>Player, take input control </summary>
	[RequireComponent(typeof(DIControllerMotor)), ExecuteInEditMode]
	public class DIPlayer : MonoBehaviour {

		[HideInInspector]
		public DIControllerMotor controller;
		
		public StandartInput horizontalInput, verticalInput, jumpInput, gunInput, meleeInput, dashInput;

		// Use this for initialization
		void Start () {
			controller = GetComponent<DIControllerMotor>();
			if (!Application.isPlaying) {
				SetupInput();
				return;
			}
			controller.onDead += onDead;
		}

		private void onDead()
		{
			if (GUIManager.instance && controller.livesCount <= 0)
				GUIManager.instance.ShowGameOverPanel();
		}

		void SetupInput()
		{
			horizontalInput = new StandartInput();
			verticalInput = new StandartInput();
			jumpInput = new StandartInput();
			gunInput = new StandartInput();
			dashInput = new StandartInput();
			meleeInput = new StandartInput();
			horizontalInput.axisName = "Horizontal";
			verticalInput.axisName = "Vertical";
			jumpInput.axisName = "Jump";
			gunInput.axisName = "Fire";
			dashInput.axisName = "Dash";
			meleeInput.axisName = "Melee";
		}
	
		// Update is called once per frame
		void Update ()
		{
			if (!Application.isPlaying)
				return;

			controller.motorInput.horizontal.down = horizontalInput.down = DIInputManager.GetButtonDown(horizontalInput.axisName);
			controller.motorInput.standartInput.directionalInput.x = DIInputManager.GetAxisRaw(horizontalInput.axisName);

			verticalInput.down = DIInputManager.GetButtonDown(verticalInput.axisName);
			controller.motorInput.standartInput.directionalInput.y = DIInputManager.GetAxisRaw(verticalInput.axisName);

			jumpInput.down = controller.motorInput.jump.down = DIInputManager.GetButtonDown(jumpInput.axisName);
			jumpInput.up = controller.motorInput.jump.up = DIInputManager.GetButtonUp(jumpInput.axisName);
			jumpInput.pressed = DIInputManager.GetButton(jumpInput.axisName);

			dashInput.down = DIInputManager.GetButtonDown(dashInput.axisName);
			dashInput.up = DIInputManager.GetButtonUp(dashInput.axisName);
			dashInput.pressed = DIInputManager.GetButton(dashInput.axisName);
			gunInput.down = DIInputManager.GetButtonDown(gunInput.axisName);
			gunInput.pressed = DIInputManager.GetButton(gunInput.axisName);
			gunInput.up = DIInputManager.GetButtonUp(gunInput.axisName);
			meleeInput.down = DIInputManager.GetButtonDown(meleeInput.axisName);
			
			
			

			controller.Motoring();
		}
	}
}
