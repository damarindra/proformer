﻿using UnityEngine;
using System.Collections;
using DI.Proformer.ToolHelper;
using DI.Proformer.Action;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace DI.Proformer {
	/// <summary>Obsolete, use DICharacterFSM instead </summary>
	[RequireComponent(typeof(DIControllerMotor)), System.Obsolete("Replaced with DICharacterFSM")]
	public class DICharacterAI : MonoBehaviour {

		[HideInInspector]
		public DIControllerMotor controller;
		
		public bool movePermission;
		public float maxMoveTime = 5f, minMoveTime = 2f, maxMoveRange = 10;
		public float maxIdleTime = 5f, minIdleTime = 2f, maxIdleRange = 10;

		public float shootRange = 8;
		public LayerMask shootTargetMask;
		public StandartInput gunInput = new StandartInput();

		float maxGroundHigh;

		[System.Flags]
		enum UnreachableGroundState {
				Stop = 1 //000000
				, ReverseDirection = 2 //000001
				, DontStop = 4 //000010
				, Jumping = 8 //000100
				, Jumping_ReverseDirection = Jumping | ReverseDirection
				, Jumping_Stop = Jumping | Stop
		}
		/// <summary>
		/// Hole Detection Action
		/// </summary>
		[SerializeField]
		UnreachableGroundState holeDetection = UnreachableGroundState.ReverseDirection;
		[SerializeField]
		private float offsetHoleDetectorPosition = 0.5f;

		/// <summary>
		/// Wall Detection Action
		/// </summary>
		[SerializeField]
		UnreachableGroundState wallDetection = UnreachableGroundState.ReverseDirection;
		[SerializeField]
		private float offsetWallDetectorLength = 1f;

		enum MoveState { Idle
				, Moving
				, ChangeDirection
				, Jumping}
		[SerializeField]
		/// <summary>
		/// Current Moving State
		/// </summary>
		MoveState currentMoveState = MoveState.Idle;

		enum MovingCollision { NothingHappen, WallCollision, HoleCollision }
		/// <summary>
		/// Check if there is any obstacle wall or hole
		/// </summary>
		[SerializeField]
		MovingCollision movingCollisionState = MovingCollision.NothingHappen;
		static bool UnreachableHasFlag(UnreachableGroundState source, UnreachableGroundState isHasThis) {
			return (source & isHasThis) == isHasThis;
		}

		[System.Flags]
		enum AIState { AIMove = 1
				, Shoot = 2}
		AIState aiState = AIState.AIMove;

		#region Jump_Var
		private Vector3 jumpInitiatePosition = Vector3.zero;
		#endregion

		float moveTime;
		GunAction gun;

		void Start()
		{
			controller = GetComponent<DIControllerMotor>();
			gun = GetComponent<GunAction>();
			maxGroundHigh = (controller.bodyCollider.width / Mathf.Sin(controller.slopeLimit)) * Mathf.Sin(90 - controller.slopeLimit);
			StartCoroutine("AICoroutine");
		}

		void Update() {
			if (gun) {
				//Check if enemy in range of shoot
				Ray ray = new Ray(transform.position + controller.direction * 1.5f, controller.direction);
				ProformerRaycastHit hit;
				InternalTool.ProformerRaycast(ray, out hit, shootRange, shootTargetMask);
				InternalTool.DrawRay(ray.origin, ray.direction * shootRange, Color.green);
				if (hit.collider && aiState != AIState.Shoot) {
					StopCoroutine("AICoroutine");
					gunInput.down = true;
					aiState = AIState.Shoot;
					StartCoroutine("AICoroutine");
				}
				else if(!hit.collider && aiState != AIState.AIMove){
					gunInput.up = true;
					StopCoroutine("AICoroutine");
					aiState = AIState.AIMove;
					StartCoroutine("AICoroutine");
				}
			}

			controller.Motoring();
		}

		IEnumerator AICoroutine() {
			#region AI MOVE
			if (aiState == AIState.AIMove)
			{
				//Loop all Move Condition
				while (true)
				{
					//Idle
					if (currentMoveState == MoveState.Idle)
					{
						//set direction to zero
						controller.standartInput.directionalInput.x = 0;
						//wait until
						yield return new WaitForSeconds(Random.Range(minIdleTime, maxIdleTime));
						//set moveState to Moving
						currentMoveState = MoveState.Moving;
						//forcing the character to move, to avoid character stuck at the edge of ground.
						MoveCharacter(true);
					}
					else if (currentMoveState == MoveState.Moving)
					{
						if (moveTime <= 0)
							moveTime = Random.Range(minMoveTime, maxMoveTime);

						//Loop Moving Only Condition (include Jump, change direction, detection
						while (moveTime > 0)
						{
							//condition if change direction
							if (currentMoveState == MoveState.ChangeDirection)
							{
								float directionX = Mathf.Sign(controller.standartInput.directionalInput.x);
								//make a little move to another direction, so not just stuck at the edge of ground
								if (Mathf.Sign(controller.velocity.x) == directionX)
								{
									//you want to know where the process changing movingCollisionState? See at MoveCharacter at the else of (currentMoveState == MoveState.ChangeDirection)
									//Changing State
									if (movingCollisionState == MovingCollision.WallCollision)
									{
										movingCollisionState = MovingCollision.NothingHappen;
										if (UnreachableHasFlag(wallDetection, UnreachableGroundState.Stop))
										{
											currentMoveState = MoveState.Idle;
											break;
										}
										else if (UnreachableHasFlag(wallDetection, UnreachableGroundState.ReverseDirection))
											currentMoveState = MoveState.Moving;
									}
									else if (movingCollisionState == MovingCollision.HoleCollision)
									{
										movingCollisionState = MovingCollision.NothingHappen;
										if (UnreachableHasFlag(holeDetection, UnreachableGroundState.Stop))
										{
											currentMoveState = MoveState.Idle;
											break;
										}
										else if (UnreachableHasFlag(holeDetection, UnreachableGroundState.ReverseDirection))
											currentMoveState = MoveState.Moving;
									}
								}
								moveTime -= Time.deltaTime;
							}
							else
							{
								controller.motorInput.jump.Reset();
								//set jump down to false when reach ground
								if (controller.state.collisionBelow && !controller.state.lastFrameCollisionBelow)
									controller.motorInput.jump.up = true;
								//movetime will continue counting when grounded only, just avoid if movetime running out and still jump, will dropping down. so this is the solution
								if (controller.state.collisionBelow)
									moveTime -= Time.deltaTime;
								MoveCharacter();
							}
							yield return null;
						}
						if (moveTime <= 0)
							currentMoveState = MoveState.Idle;
					}
					yield return null;
				}
			}
			#endregion
			#region AI SHOOT
			else if (aiState == AIState.Shoot) {
				Debug.Log("Shoot");
			}
			#endregion
		}
		void MoveCharacter(bool forcing = false) {
			if (forcing)
			{
				controller.standartInput.directionalInput.x = Random.Range(0, 2) == 0 ? -1 : 1;
				controller.RotateTo(controller.standartInput.directionalInput.x == 1);

				Ray ray = new Ray(controller.raycaster.frontBottomOrigin + controller.direction * offsetHoleDetectorPosition, Vector3.down);
				ProformerRaycastHit hit;
				InternalTool.ProformerRaycast(ray, out hit, maxGroundHigh + controller.raycaster.skinWidth, 1 << LayerMask.NameToLayer("Grounds"));
				InternalTool.DrawRay(ray.origin, ray.direction * (maxGroundHigh + controller.raycaster.skinWidth), Color.cyan);
				if (hit.collider == null)
				{
					controller.standartInput.directionalInput.x *= -1;
					controller.RotateTo(controller.standartInput.directionalInput.x == 1);
				}
			}
			else {
				#region CheckWall
				bool isFrontWall = isWallInFront();
				if (controller.state.collisionBelow)
				{
					if (isFrontWall && UnreachableHasFlag(wallDetection, UnreachableGroundState.Jumping)) {
						//set jump initiate position
						jumpInitiatePosition = controller.raycaster.backBottomOrigin + Vector3.up * controller.bodyCollider.height;
						//Check Jump Capability First
						//If can jump, then jump
						movingCollisionState = MovingCollision.WallCollision;
						if (canJumping() && UnreachableHasFlag(wallDetection, UnreachableGroundState.Jumping))
						{
							controller.motorInput.jump.down = true;
						}
						//If not, execute the second condition
						else if (UnreachableHasFlag(wallDetection, UnreachableGroundState.ReverseDirection) || UnreachableHasFlag(wallDetection, UnreachableGroundState.Stop))
							ChangeDirection();
					
					}
					else if (isFrontWall && (UnreachableHasFlag(wallDetection, UnreachableGroundState.ReverseDirection) || UnreachableHasFlag(wallDetection, UnreachableGroundState.Stop))){
						if (wallDetection != UnreachableGroundState.DontStop) {
							//SEt current COllisionState
							movingCollisionState = MovingCollision.WallCollision;
							
							ChangeDirection();
						}
					}
				}
				#endregion


				#region CheckGround and Hole
				if (controller.state.collisionBelow) {
					if (!isFrontWall) {
						Ray ray = new Ray(controller.raycaster.frontBottomOrigin + controller.direction * offsetHoleDetectorPosition, Vector3.down);
						ProformerRaycastHit hit;
						InternalTool.ProformerRaycast(ray, out hit, maxGroundHigh + controller.raycaster.skinWidth, 1 << LayerMask.NameToLayer("Grounds"));
						InternalTool.DrawRay(ray.origin, ray.direction * (maxGroundHigh + controller.raycaster.skinWidth), Color.cyan);
						if (hit.collider != null)
						{
							float angle = Vector3.Angle(Vector3.up, hit.normal);
							if (angle < controller.slopeLimit && controller.standartInput.directionalInput.x == 0)
							{
								//Move
								controller.standartInput.directionalInput.x = Random.Range(0, 2) == 0 ? -1 : 1;
								controller.RotateTo(controller.standartInput.directionalInput.x == 1);
							}
							else if ((angle > controller.slopeLimit) && holeDetection != UnreachableGroundState.DontStop)
							{
								//SEt current COllisionState
								movingCollisionState = MovingCollision.HoleCollision;
								//Stop or change direction
								ChangeDirection();
							}
						}
						else if (holeDetection != UnreachableGroundState.DontStop)
						{
							//SEt current COllisionState
							movingCollisionState = MovingCollision.HoleCollision;
							if (UnreachableHasFlag(holeDetection, UnreachableGroundState.Jumping)) {
								if (controller.state.collisionBelow)
								{
									//set jump initiate position
									jumpInitiatePosition = controller.raycaster.backBottomOrigin + Vector3.up * controller.bodyCollider.height;
									//Check Jump Capability First
									//If can jump, then jump
									if (canJumping() && UnreachableHasFlag(holeDetection, UnreachableGroundState.Jumping))
									{
										controller.motorInput.jump.down = true;
									}
									//If not, execute the second condition
									else
										ChangeDirection();
								}
							}
							else if (UnreachableHasFlag(holeDetection, UnreachableGroundState.ReverseDirection) || UnreachableHasFlag(holeDetection, UnreachableGroundState.Stop))
							{
								ChangeDirection();
							}
						}
					}
				}
				#endregion
			}

		}
		void ChangeDirection() {
			currentMoveState = MoveState.ChangeDirection;
			controller.standartInput.directionalInput *= -1;
		}

		bool isWallInFront() {
			for (int i = 0; i < controller.horizontalRayCount; i++)
			{
				Vector3 rayOrigin = controller.raycaster.frontBottomOrigin;
				rayOrigin += Vector3.up * (controller.raycaster.horizontalSpace * i);
				float rayLength = controller.raycaster.skinWidth + offsetWallDetectorLength;
				Ray ray = new Ray(rayOrigin, controller.direction);
				ProformerRaycastHit hit;
				InternalTool.ProformerRaycast(ray, out hit, rayLength, controller.groundMask);

				InternalTool.DrawRay(rayOrigin, ray.direction * rayLength, Color.red);
				if (hit.collider != null)
				{
					if(Vector3.Angle(Vector3.up, hit.normal) > controller.slopeLimit)
						return true;
				}
			}
			return false;
		}

		bool canJumping()
		{
			if (!controller.jumpPermission || controller.velocity.magnitude < controller.moveSpeed / 2 || moveTime < controller.timeToJumpApex * 1.5f)
				return false;
			int segmentCount = 10;
			float segmentScale = 1;
			Vector3[] segments = new Vector3[segmentCount];

			// The first line point is wherever the player's cannon, etc is
			segments[0] = jumpInitiatePosition;

			// The initial velocity
			Vector3 segVelocity = new Vector3(controller.velocity.x, controller.maxJumpVelocity, controller.velocity.z);

			for (int i = 1; i < segmentCount; i++)
			{
				// Time it takes to traverse one segment of length segScale (careful if velocity is zero)
				float segTime = (segVelocity.sqrMagnitude != 0) ? segmentScale / segVelocity.magnitude : 0;

				// Add velocity from gravity for this segment's timestep
				segVelocity = segVelocity + Physics.gravity * segTime;

				// RayLength
				float rayLength = Vector3.Distance(segments[i - 1], segments[i - 1] + segVelocity * segTime);

				// Check to see if we're going to hit a physics object
				Ray ray = new Ray(segments[i - 1], segVelocity * segTime);

				ProformerRaycastHit hit;
				InternalTool.ProformerRaycast(ray, out hit, rayLength, controller.groundMask);

				InternalTool.DrawRay(segments[i - 1], segVelocity * segTime);
				InternalTool.DrawLine(segments[i - 1] + Vector3.down * .2f, segments[i - 1] + Vector3.up * .2f);
				InternalTool.DrawLine(segments[i - 1] + Vector3.left * .2f, segments[i - 1] + Vector3.right * .2f);

				if (hit.collider != null)
				{
					if (hit.collider.tag != "OneWay")
					{
						// set next position to the position where we hit the physics object
						segments[i] = segments[i - 1] + segVelocity.normalized * hit.distance;
						// correct ending velocity, since we didn't actually travel an entire segment
						segVelocity = segVelocity - Physics.gravity * (segmentScale - hit.distance) / segVelocity.magnitude;
						// flip the velocity to simulate a bounce
						segVelocity = Vector3.Reflect(segVelocity, hit.normal);
						
						if (Vector3.Angle(hit.normal, Vector3.down) < 15)
							return false;
						else if (Vector3.Angle(hit.normal, Vector3.up) <= controller.slopeLimit)
							return true;
						else if (Vector3.Angle(hit.normal, Vector3.up) > controller.slopeLimit)
							return false;
						else if (segVelocity.y < 0)
							return false;
						else
							return true;
					}
					else
						segments[i] = segments[i - 1] + segVelocity * segTime;
					/*
					 * Here you could check if the object hit by the Raycast had some property - was 
					 * sticky, would cause the ball to explode, or was another ball in the air for 
					 * instance. You could then end the simulation by setting all further points to 
					 * this last point and then breaking this for loop.
					 */
				}
				// If our raycast hit no objects, then set the next position to the last one plus v*t
				else
				{
					segments[i] = segments[i - 1] + segVelocity * segTime;
				}
			}
			return false;

		}
	}

#if UNITY_EDITOR
	/*
	//[CustomEditor(typeof(DICharacterAI)), CanEditMultipleObjects]
	public class ProformerCharacterAIEditor : Editor {
		DICharacterAI ai;

		void OnEnable()
		{
			ai = (DICharacterAI)target;
			
		}

		public override void OnInspectorGUI()
		{
			EditorGUI.BeginChangeCheck();
			MovementInspector();
			GunInspector();

			if (EditorGUI.EndChangeCheck())
			{
				serializedObject.ApplyModifiedProperties();
				EditorUtility.SetDirty(ai);
			}
		}

		void MovementInspector()
		{
			EditorPrefs.SetBool("Movement AI Fold",
				EditorGUILayout.Foldout(EditorPrefs.GetBool("Movement AI Fold"), "Movement AI"));
			if (EditorPrefs.GetBool("Movement AI Fold"))
			{
				EditorGUI.indentLevel += 1;
				EditorGUILayout.PropertyField(serializedObject.FindProperty("movePermission"));
				if (!ai.movePermission)
					GUI.enabled = false;
				EditorGUILayout.PropertyField(serializedObject.FindProperty("holeDetection"));
				EditorGUILayout.PropertyField(serializedObject.FindProperty("offsetHoleDetectorPosition"));
				EditorGUILayout.PropertyField(serializedObject.FindProperty("wallDetection"));
				EditorGUILayout.PropertyField(serializedObject.FindProperty("offsetWallDetectorLength"));
				
				EditorGUILayout.Space();
				EditorGUILayout.LabelField("Move Range", EditorStyles.boldLabel);
				EditorGUILayout.PropertyField(serializedObject.FindProperty("maxMoveRange"));
				EditorGUILayout.BeginHorizontal();
				EditorGUILayout.LabelField("Move Range", GUILayout.Width(EditorGUIUtility.labelWidth));
				EditorGUILayout.MinMaxSlider(ref ai.minMoveTime, ref ai.maxMoveTime, 0, ai.maxMoveRange);
				EditorGUILayout.EndHorizontal();
				GUI.enabled = false;
				EditorGUILayout.PropertyField(serializedObject.FindProperty("maxMoveTime"));
				EditorGUILayout.PropertyField(serializedObject.FindProperty("minMoveTime"));
				if (ai.movePermission)
					GUI.enabled = true;

				EditorGUILayout.Space();
				EditorGUILayout.LabelField("Idle Range", EditorStyles.boldLabel);
				EditorGUILayout.PropertyField(serializedObject.FindProperty("maxIdleRange"));
				EditorGUILayout.BeginHorizontal();
				EditorGUILayout.LabelField("Idle Time Range", GUILayout.Width(EditorGUIUtility.labelWidth));
				EditorGUILayout.MinMaxSlider(ref ai.minIdleTime, ref ai.maxIdleTime, 0, ai.maxIdleRange);
				EditorGUILayout.EndHorizontal();
				GUI.enabled = false;
				EditorGUILayout.PropertyField(serializedObject.FindProperty("minIdleTime"));
				EditorGUILayout.PropertyField(serializedObject.FindProperty("maxIdleTime"));
				if (ai.movePermission) 
					GUI.enabled = true;

				EditorGUILayout.Space();
				EditorGUILayout.LabelField("State Watcher", EditorStyles.boldLabel);

				GUI.enabled = false;
				EditorGUILayout.PropertyField(serializedObject.FindProperty("currentMoveState"));
				EditorGUILayout.PropertyField(serializedObject.FindProperty("movingCollisionState"));
				GUI.enabled = true;

				EditorGUI.indentLevel -= 1;
				EditorGUILayout.Space();
				GUI.enabled = true;
			}
		}

		void GunInspector() {

			EditorPrefs.SetBool("Gun AI Fold",
				EditorGUILayout.Foldout(EditorPrefs.GetBool("Gun AI Fold"), "Gun AI"));
			if (EditorPrefs.GetBool("Gun AI Fold"))
			{
				if (!ai.GetComponent<GunAction>())
					GUI.enabled = false;
				EditorGUI.indentLevel += 1;
				EditorGUILayout.PropertyField(serializedObject.FindProperty("shootRange"));
				EditorGUILayout.PropertyField(serializedObject.FindProperty("shootTargetMask"));
				EditorGUI.indentLevel -= 1;
				GUI.enabled = true;
			}
		}
	}*/
#endif
}
