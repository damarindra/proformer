﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DI.Proformer.ToolHelper;

namespace DI.Proformer {
	[ExecuteInEditMode]
	/// <summary>
	/// Character Body is used for handling such controller and Trigger
	/// </summary>
	public class DICharacterBody : MonoBehaviour {

		[HideInInspector]
		public DIControllerMotor motor;
		[HideInInspector]
		public GiveDamage _gd;

#if PROFORMER3D
		public delegate void OnTriggerListener(Collider col);
#else
		public delegate void OnTriggerListener(Collider2D col);
#endif
		public event OnTriggerListener onTriggerEnter, onTriggerExit, onTriggerStay, onDamageEnter, onImpactEnter;

		private List<GameObject> giveDamageList = new List<GameObject>();

#region MonoBehaviour

		void Start() {
			if (!Application.isPlaying) {
				gameObject.tag = GetComponentInParent<DIControllerMotor>().gameObject.tag;
				return;
			}
			if (motor == null)
				motor = GetComponentInParent<DIControllerMotor>();
			onImpactEnter += motor.TriggerEnterImpact;
			onDamageEnter += motor.TriggerEnterGetDamage;
		}

#if PROFORMER3D
		void OnTriggerEnter(Collider col)
#else
		void OnTriggerEnter2D(Collider2D col)
#endif
		{
			if (!Application.isPlaying)
				return;
			if (col.tag.Equals("Melee")) {
				GiveDamage giveDamage = col.GetComponent<GiveDamage>();
				Impactable _impact = col.GetComponent<Impactable>();

				if (_impact ) {
					foreach (string t in motor.impactTag)
					{
						if (col.tag.Equals(t))
						{
							if (onImpactEnter != null)
								onImpactEnter(col);
							break;
						}
					}
				}

				if (giveDamage != null && !motor.isInvisible)
				{
					foreach (string t in motor.takeDamageTag)
					{
						if (col.tag.Equals(t))
						{
							if (onDamageEnter != null)
								onDamageEnter(col);
							break;
						}
					}
				}

				return;
			}

			Impactable impact = col.GetComponent<Impactable>();
			if (impact != null && !col.GetComponent<GiveDamage>())
			{
				foreach (string t in motor.impactTag)
				{
					if (col.tag.Equals(t))
					{
						if(onImpactEnter != null)
							onImpactEnter(col);
						break;
					}
				}
			}
			Pickable pickable = col.GetComponentInParent<Pickable>();
			if (pickable != null)
			{
				if (motor.pickableTag.Length != 0)
				{
					foreach (string t in motor.pickableTag)
					{
						if (col.tag.Equals(t))
						{
							motor.TriggerEnterPickableObject(pickable);
						}
					}
				}
			}

			if (onTriggerEnter != null)
				onTriggerEnter(col);
		}

		void Update()
		{
#if !PROFORMER3D
			if (!Application.isPlaying)
				transform.rotation = Quaternion.Euler(0, 0, 0);
#endif
		}

#if PROFORMER3D
		void OnTriggerStay(Collider col)
#else
		void OnTriggerStay2D(Collider2D col)
#endif
		{
			if (!Application.isPlaying)
				return;
			if (col.tag.Equals("Melee"))
				return;

			DICharacterBody targetCB = col.GetComponent<DICharacterBody>();

			if (targetCB)
			{
				if (targetCB.motor.isInvisible)
					return;
			}

			if (onTriggerStay != null)
				onTriggerStay(col);

			if(giveDamageList.Contains(col.gameObject)){
				return;
			}
			giveDamageList.Add(col.gameObject);

			Impactable impact = col.GetComponent<Impactable>();
			GiveDamage giveDamage = col.GetComponent<GiveDamage>();

			if (impact != null && giveDamage != null)
			{
				foreach (string t in motor.impactTag)
				{
					if (col.tag.Equals(t))
					{
						foreach (string tDamage in motor.takeDamageTag)
						{
							if (t.Equals(tDamage) && motor.isInvisible)
								return;
						}
						if(onImpactEnter != null)
							onImpactEnter(col);
						break;
					}
				}
			}

			if (giveDamage != null && !motor.isInvisible)
			{
				foreach (string t in motor.takeDamageTag)
				{
					if (col.tag.Equals(t))
					{
						if (onDamageEnter != null)
							onDamageEnter(col);
						break;
					}
				}
			}

			
		}

#if PROFORMER3D
		void OnTriggerExit(Collider col)
#else
		void OnTriggerExit2D(Collider2D col)
#endif
		{
			if (!Application.isPlaying)
				return;
			if (giveDamageList.Contains(col.gameObject))
				giveDamageList.Remove(col.gameObject);

			if (onTriggerExit != null)
				onTriggerExit(col);
		}


#endregion

		/// <summary>
		/// Implements Damage
		/// </summary>
		/// <param name="freezeTime">Freeze Time (Can't Move)</param>
		/// <param name="useImpact">Using Impact, if true => Effector must be has a Impactable Component</param>
		public void GotDamage(float freezeTime, bool useImpact)
		{
			motor.recoverTime = freezeTime;
			StartCoroutine(FreezeMovement(useImpact));
			StartCoroutine(Invisible());
		}
		IEnumerator FreezeMovement(bool useImpact)
		{
			motor.canMove = false;
			if(!useImpact)
				motor.velocity.x = 0;
			if (motor.flickeringWhenGetDamage && !motor.isDead)
			{
				if (!motor.disableEnableFlickerMethod)
					StartCoroutine(RendererHelper.FlickeringColorCo(motor.characterRenderer, motor.trueColors, motor.colorFlicker, motor.transitionTimeFlickering, motor.invisibleTime));
				else
					StartCoroutine(RendererHelper.FlickeringColorCo(motor.characterRenderer, motor.transitionTimeFlickering, motor.invisibleTime));
			}
			yield return new WaitForSeconds(motor.recoverTime);
			motor.canMove = true;
		}
		public IEnumerator Invisible()
		{
			motor.isInvisible = true;

			if (motor.invisibleTime > 0)
				yield return new WaitForSeconds(motor.invisibleTime);
			motor.isInvisible = false;
			giveDamageList.Clear();
		}
	}
}
