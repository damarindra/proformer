﻿using UnityEngine;
using System.Collections;
using DI.PoolingSystem;
using System;

namespace DI.Proformer.Sound {
	/// <summary>
	/// SFX is used for playing Sound Effect
	/// </summary>
	[RequireComponent(typeof(AudioSource))]
	public class SFX : MonoBehaviour, IPoolObject
	{
		AudioSource audioSource;
		bool isActive = false;
		void Start() {
			audioSource = GetComponent<AudioSource>();
			audioSource.loop = false;
			audioSource.playOnAwake = false;
		}
		void Update() {
			if (!audioSource.isPlaying && isActive)
				Disable();
		}
		public void Disable()
		{
			gameObject.SetActive(false);
			isActive = false;
		}

		public void OnSetup()
		{
		}

		public void OnObjectReuse(UnityEngine.Object obj)
		{
			if (obj.GetType() == typeof(AudioClip)) {
				if ((AudioClip)obj != null) {
					if (audioSource == null)
						audioSource = GetComponent<AudioSource>();
					audioSource.clip = (AudioClip)obj;
					audioSource.Play();
					isActive = true;
				}
			}
		}
	}
}
