﻿using UnityEngine;
using System.Collections;

namespace DI.Proformer.ToolHelper {
	[System.Serializable]
	public class StandartInput {
		/// <summary>Input Axis Name</summary>
		public string axisName;
		/// <summary>
		/// define as button down, will reset to false when frame completed
		/// </summary>
		[HideInInspector]
		public bool down = false;
		/// <summary>
		/// define as button up, will reset to false when frame completed
		/// </summary>
		[HideInInspector]
		public bool up = false;
		/// <summary>
		/// define as button pressed, value will not reset until change manually
		/// </summary>
		[HideInInspector]
		public bool pressed = false;
		/// <summary>
		/// Reset fuction : will reset down and up to false
		/// </summary>
		public void Reset()
		{
			down = false; up = false;
		}
	}
}
