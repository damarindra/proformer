﻿using UnityEngine;
using DI.Proformer.ToolHelper;

namespace DI.Proformer {
	public class DIInputUpdater : MonoBehaviour {

		public StandartInput inputAxis;
		private enum UpdateOn { Update, FixedUpdate }
		[SerializeField]
		private UpdateOn updateOn = UpdateOn.Update;
		[SerializeField]
		private bool recordDown = true;
		[SerializeField]
		private bool recordUp;
		[SerializeField]
		private bool recordPressed;

		// Update is called once per frame
		void Update () {
			if (updateOn == UpdateOn.Update) {
				if(recordDown)
					inputAxis.down = DIInputManager.GetButtonDown(inputAxis.axisName);
				if(recordUp)
					inputAxis.up = DIInputManager.GetButtonUp(inputAxis.axisName);
				if(recordPressed)
					inputAxis.pressed = DIInputManager.GetButtonDown(inputAxis.axisName);
			}
		}
		void FixedUpdate()
		{
			if (updateOn == UpdateOn.FixedUpdate)
			{
				if (recordDown)
					inputAxis.down = DIInputManager.GetButtonDown(inputAxis.axisName);
				if (recordUp)
					inputAxis.up = DIInputManager.GetButtonUp(inputAxis.axisName);
				if (recordPressed)
					inputAxis.pressed = DIInputManager.GetButtonDown(inputAxis.axisName);
			}
		}
	}
}
