﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace DI.Proformer.ToolHelper {
	[System.Serializable]
	public class TargetInput {

		public GameObject target;
		public MonoBehaviour script;
		public StandartInput input { get {
				if (_input == null)
					_input = getInput();
				return _input; } }
		private StandartInput _input;
		public string inputName;
		public enum InputTrigger { Down, Up }
		public InputTrigger inputTrigger = InputTrigger.Down;
#pragma warning disable 0414
		[SerializeField]
		private bool hideInputTrigger = true;
#pragma warning restore 0414

		StandartInput getInput() {
			if(script != null && !string.IsNullOrEmpty(inputName))
				return (StandartInput)script.GetType().GetField(inputName).GetValue(script);
			return null;
		}
		public TargetInput(bool hideInputTrigger = true) {
			this.hideInputTrigger = hideInputTrigger;
		}
	}
}
