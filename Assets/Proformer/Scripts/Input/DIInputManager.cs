﻿using UnityEngine;
using System.Collections;
#if CROSS_PLATFORM_INPUT
using UnityStandardAssets.CrossPlatformInput;
#endif

namespace DI {
	public class DIInputManager {

		public static bool GetButton(string axisName)
		{
#if CROSS_PLATFORM_INPUT
			return CrossPlatformInputManager.GetButton(axisName);
#else
			return Input.GetButton(axisName);
#endif
		}

		public static bool GetButtonDown(string axisName) {
#if CROSS_PLATFORM_INPUT
			return CrossPlatformInputManager.GetButtonDown(axisName);
#else
			return Input.GetButtonDown(axisName);
#endif
		}

		public static bool GetButtonUp(string axisName)
		{
#if CROSS_PLATFORM_INPUT
			return CrossPlatformInputManager.GetButtonUp(axisName);
#else
			return Input.GetButtonUp(axisName);
#endif
		}

		public static float GetAxis(string axisName)
		{
#if CROSS_PLATFORM_INPUT
			return CrossPlatformInputManager.GetAxis(axisName);
#else
			return Input.GetAxis(axisName);
#endif
		}

		public static float GetAxisRaw(string axisName)
		{
#if CROSS_PLATFORM_INPUT
			return CrossPlatformInputManager.GetAxisRaw(axisName);
#else
			return Input.GetAxisRaw(axisName);
#endif
		}
	}
}
