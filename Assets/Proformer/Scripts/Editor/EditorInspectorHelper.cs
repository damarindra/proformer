﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace DI.Proformer.ToolHelper {
	/// <summary>
	/// Use only in Editor
	/// </summary>
	public class EditorInspectorHelper {
#if UNITY_EDITOR
		public static bool GiveDamageInspector(GiveDamage giveDamage, SerializedProperty serializeableProperty)
		{
			using (EditorGUILayoutGroup group = new EditorGUILayoutGroup(new GUIContent("Give Damage", "Give Damage is a component that allow a gameobject to giving damage. For ex : Enemy character is need to giving damage to player, so enemy body collider need Give Damage component. ah don't forget to configure Give Damage Trigger Tag at player"), serializeableProperty.isExpanded, giveDamage)) {
				serializeableProperty.isExpanded = group.isExpanding;
				if (group.isExpanding) {
					if (!giveDamage)
					{
						GUI.enabled = false;
						EditorGUILayout.IntField("Damage Given", 0);
						EditorGUILayout.IntField("Freeze Time Given", 0);
					}
					else
					{
						EditorGUI.BeginChangeCheck();
						Undo.RecordObject(giveDamage, "damageGiven");
						giveDamage.damageGiven = EditorGUILayout.IntField("Damage Given", giveDamage.damageGiven);
						Undo.RecordObject(giveDamage, "freezeTime");
						giveDamage.freezeTime = EditorGUILayout.FloatField("Freeze Time Given", giveDamage.freezeTime);
						if(EditorGUI.EndChangeCheck())
							EditorUtility.SetDirty(giveDamage);
					}
					GUI.enabled = true;
				}
				return group.toggleValue;
			}
		}
		public static void GiveDamageInspector(GiveDamage giveDamage)
		{
			if (!giveDamage)
			{
				GUI.enabled = false;
				EditorGUILayout.IntField("Damage Given", 0);
				EditorGUILayout.IntField("Freeze Time Given", 0);
			}
			else
			{
				EditorGUI.BeginChangeCheck();
				Undo.RecordObject(giveDamage, "damageGiven");
				giveDamage.damageGiven = EditorGUILayout.IntField("Damage Given", giveDamage.damageGiven);
				Undo.RecordObject(giveDamage, "freezeTime");
				giveDamage.freezeTime = EditorGUILayout.FloatField("Freeze Time Given", giveDamage.freezeTime);
				if(EditorGUI.EndChangeCheck())
					EditorUtility.SetDirty(giveDamage);
			}
			GUI.enabled = true;
			
		}
		public static bool ImpactableInspector(Impactable impactable, SerializedProperty serializedProperty)
		{
			using (EditorGUILayoutGroup group = new EditorGUILayoutGroup(new GUIContent("Impactable", "Yeah, Impactable is a component that allow a gameobject to give impact effect. So for ex: you have spring, then you need to attach impactable component to body collider, then configure it. Ouh one more things, don't forget to setup Impactable Trigger Tag properly, you can found it at character."), serializedProperty.isExpanded, impactable)) {
				serializedProperty.isExpanded = group.isExpanding;
				if (group.isExpanding) {
					if (!impactable)
					{
						GUI.enabled = false;
						EditorGUILayout.IntField("Power", 0);
						EditorGUILayout.BeginHorizontal();
						EditorGUILayout.Vector3Field("Offset", Vector3.zero);
						GUILayout.Button("Edit", GUILayout.Width(35));
						EditorGUILayout.EndHorizontal();
						EditorGUILayout.IntPopup("ImpactType", 0, new string[1] { "" }, new int[] { 0 });

					}
					else
					{
						EditorGUI.BeginChangeCheck();
						Undo.RecordObject(impactable, "Power");
						impactable.power = EditorGUILayout.FloatField("Power", impactable.power);
						EditorGUILayout.BeginHorizontal();
						Undo.RecordObject(impactable, "Offset");
						impactable.offset = EditorGUILayout.Vector3Field("Offset", impactable.offset);
						if (GUILayout.Button(impactable.editSources ? "Done" : "Edit", GUILayout.Width(35)))
							impactable.editSources = !impactable.editSources;
						EditorGUILayout.EndHorizontal();
						Undo.RecordObject(impactable, "ImpactType");
						impactable.impactType = (Impactable.ImpactType)EditorGUILayout.EnumPopup("Impact Type", impactable.impactType);
						if(EditorGUI.EndChangeCheck())
							EditorUtility.SetDirty(impactable);
					}
					GUI.enabled = true;
				}
				return group.toggleValue;
			}
		}
		public static void ImpactableInspector(Impactable impactable)
		{
			if (!impactable)
			{
				GUI.enabled = false;
				EditorGUILayout.IntField("Power", 0);
				EditorGUILayout.BeginHorizontal();
				EditorGUILayout.Vector3Field("Offset", Vector3.zero);
				GUILayout.Button("Edit", GUILayout.Width(35));
				EditorGUILayout.EndHorizontal();
				EditorGUILayout.IntPopup("ImpactType", 0, new string[1] { "" }, new int[] { 0 });

			}
			else
			{
				EditorGUI.BeginChangeCheck();
				Undo.RecordObject(impactable, "Power");
				impactable.power = EditorGUILayout.FloatField("Power", impactable.power);
				EditorGUILayout.BeginHorizontal();
				Undo.RecordObject(impactable, "Offset");
				impactable.offset = EditorGUILayout.Vector3Field("Offset", impactable.offset);
				if (GUILayout.Button(impactable.editSources ? "Done" : "Edit", GUILayout.Width(35)))
					impactable.editSources = !impactable.editSources;
				EditorGUILayout.EndHorizontal();
				Undo.RecordObject(impactable, "ImpactType");
				impactable.impactType = (Impactable.ImpactType)EditorGUILayout.EnumPopup("Impact Type", impactable.impactType);
				if(EditorGUI.EndChangeCheck())
					EditorUtility.SetDirty(impactable);
			}
			GUI.enabled = true;
			
		}
		public static void ImpactableEditMode(Impactable impactable)
		{
			if (!impactable)
				return;
			if (impactable.editSources)
			{
				Handles.color = Color.magenta;
				Vector3 newPos = Handles.FreeMoveHandle(impactable.transform.position + impactable.offset, Quaternion.identity, HandleUtility.GetHandleSize(impactable.transform.position + impactable.offset) * .3f, InternalTool.GetSnapValue(), Handles.SphereHandleCap);
				if (newPos != impactable.offset - impactable.transform.position)
				{
					impactable.offset = newPos - impactable.transform.position;
					EditorUtility.SetDirty(impactable);
				}
				if (impactable.impactType == Impactable.ImpactType.LOCAL)
				{
					Handles.color = Color.magenta;
					Vector3 dir = (impactable.offset * -1).normalized;
					if (dir != Vector3.zero)
						Handles.ArrowHandleCap(0, impactable.offset + impactable.transform.position, Quaternion.LookRotation(dir), .5f, EventType.Repaint);
				}
			}
		}
		public static bool StompableInspector(Stompable stompable, SerializedProperty serializedProperty)
		{
			using (EditorGUILayoutGroup group = new EditorGUILayoutGroup(new GUIContent("Stompable", "Stompable Option, only applicable at Enemy type. if stompable is active, then Player can stomping on This gameobject, make sure your player Stompable Trigger Tag Option is set properly. "), serializedProperty.isExpanded, stompable)) {
				serializedProperty.isExpanded = group.isExpanding;
				if (group.isExpanding) {
					if (!stompable)
					{
						GUI.enabled = false;
						EditorGUILayout.IntField("Power", 0);
						EditorGUILayout.Toggle("Instant Kill", true);
					}
					else
					{
						EditorGUI.BeginChangeCheck();
						Undo.RecordObject(stompable, "Power");
						stompable.stompPower = EditorGUILayout.FloatField("Power", stompable.stompPower);
						Undo.RecordObject(stompable, "Instant Kill");
						stompable.killWhenStomped = EditorGUILayout.Toggle("Instant Kill", stompable.killWhenStomped);
						if (!stompable.killWhenStomped)
							stompable.stompDamage = EditorGUILayout.IntField("Stomp Damage", stompable.stompDamage);

						if(EditorGUI.EndChangeCheck())
							EditorUtility.SetDirty(stompable);
					}
					GUI.enabled = true;
				}
				return group.toggleValue;
			}
		}
#if PROFORMER3D
		public static void DrawColliderInspector(ref Collider collider, SerializedProperty serializedProperty, IsTriggerType isTriggerType = IsTriggerType.Chooseable)
#else
		public static void DrawColliderInspector(ref Collider2D collider, SerializedProperty serializedProperty, IsTriggerType isTriggerType = IsTriggerType.Chooseable)
#endif
		{
			EditorGUI.BeginChangeCheck();
			using (EditorGUILayoutGroup group = new EditorGUILayoutGroup(new GUIContent("Collider", "Show basic Collider option."), serializedProperty.isExpanded))
			{
				serializedProperty.isExpanded = group.isExpanding;
				if (group.isExpanding)
				{

					EditorGUILayout.BeginHorizontal();
					Undo.RecordObject(collider, "Collider");
#if PROFORMER3D
					collider = EditorGUILayout.ObjectField("Collider", collider, typeof(Collider), true) as Collider;
#else
					collider = EditorGUILayout.ObjectField("Collider", collider, typeof(Collider2D), true) as Collider2D;
#endif

					EditorGUILayout.EndHorizontal();

					if (collider)
					{
						//Show Info Collider
#if PROFORMER3D
						if (collider.GetType() == typeof(BoxCollider))
						{
							BoxColliderInspect((BoxCollider)collider, isTriggerType);
						}
						else if (collider.GetType() == typeof(SphereCollider))
						{
							SphereColliderInspect((SphereCollider)collider, isTriggerType);
						}
						else if (collider.GetType() == typeof(CapsuleCollider))
							CapsuleColliderInspect((CapsuleCollider)collider, isTriggerType);
#else
						if (collider.GetType() == typeof(BoxCollider2D))
						{
							BoxColliderInspect((BoxCollider2D)collider, isTriggerType);
						}
						else if (collider.GetType() == typeof(CircleCollider2D))
						{
							CircleColliderInspect((CircleCollider2D)collider, isTriggerType);
						}
						else if (collider.GetType() == typeof(PolygonCollider2D))
							PolygonColliderInspect((PolygonCollider2D)collider, isTriggerType);
#endif
					}
				}
			}
			if (EditorGUI.EndChangeCheck())
			{
				if (collider != null) {
					EditorUtility.SetDirty(collider);
				}
			}
		}

#if PROFORMER3D
		public static void DrawColliderInspector(Collider collider, string PrefsKey)
#else
		public static void DrawColliderInspector(Collider2D collider, string PrefsKey)
#endif
		{
			EditorGUI.BeginChangeCheck();
			EditorGUILayout.Space();
			using (EditorGUILayoutGroup group = new EditorGUILayoutGroup(new GUIContent("Collider", "Show basic Collider option."), PrefsKey))
			{
				if (group.isExpanding)
				{

					EditorGUILayout.BeginHorizontal();
#if PROFORMER3D
					collider = EditorGUILayout.ObjectField("Collider", collider, typeof(Collider), true) as Collider;
#else
					collider = EditorGUILayout.ObjectField("Collider", collider, typeof(Collider2D), true) as Collider2D;
#endif

					EditorGUILayout.EndHorizontal();

					if (collider)
					{
						//Show Info Collider
#if PROFORMER3D
						if (collider.GetType() == typeof(BoxCollider))
						{
							BoxColliderInspect((BoxCollider)collider);
						}
						else if (collider.GetType() == typeof(SphereCollider))
						{
							SphereColliderInspect((SphereCollider)collider);
						}
						else if (collider.GetType() == typeof(CapsuleCollider))
							CapsuleColliderInspect((CapsuleCollider)collider);
#else
						if (collider.GetType() == typeof(BoxCollider2D))
						{
							BoxColliderInspect((BoxCollider2D)collider);
						}
						else if (collider.GetType() == typeof(CircleCollider2D))
						{
							CircleColliderInspect((CircleCollider2D)collider);
						}
						else if (collider.GetType() == typeof(PolygonCollider2D))
							PolygonColliderInspect((PolygonCollider2D)collider);
#endif
					}
				}
			}
			if (EditorGUI.EndChangeCheck())
			{
				if (collider != null)
				{
					EditorUtility.SetDirty(collider);
				}
			}
		}
#if PROFORMER3D
		public static Collider PopUpColliderCreation(GameObject parent, bool colliderOnChildren) {
#else
		public static Collider2D PopUpColliderCreation(GameObject parent, bool colliderOnChildren) {
#endif
			int choosen = 0;
			List<string> colName = new List<string>();
			colName.Add("Create...");
			colName.Add("Box Collider");
#if PROFORMER3D
			colName.Add("Sphere Collider");
#else
			colName.Add("Circle Collider");
#endif
#if PROFORMER3D
			colName.Add("Capsule Collider");
#else
			colName.Add("Polygon Collider");
#endif
			choosen = EditorGUILayout.Popup("Collider", choosen, colName.ToArray());
#if PROFORMER3D
			if (colName[choosen] == "Box Collider")
			{
				GameObject bodyCollider;
				if (colliderOnChildren)
				{
					bodyCollider = new GameObject("Body");
					bodyCollider.transform.parent = parent.transform;
					bodyCollider.transform.localPosition = Vector3.zero;
					bodyCollider.layer = parent.gameObject.layer;
					bodyCollider.tag = parent.gameObject.tag;
				}
				else
					bodyCollider = parent;
				return Undo.AddComponent<BoxCollider>(bodyCollider);
			}
			else if (colName[choosen] == "Sphere Collider")
			{
				GameObject bodyCollider;
				if (colliderOnChildren)
				{
					bodyCollider = new GameObject("Body");
					bodyCollider.transform.parent = parent.transform;
					bodyCollider.transform.localPosition = Vector3.zero;
					bodyCollider.layer = parent.gameObject.layer;
					bodyCollider.tag = parent.gameObject.tag;
				}
				else
					bodyCollider = parent;
				return Undo.AddComponent<SphereCollider>(bodyCollider);
			}
			else if (colName[choosen] == "Capsule Collider")
			{
				GameObject bodyCollider;
				if (colliderOnChildren)
				{
					bodyCollider = new GameObject("Body");
					bodyCollider.transform.parent = parent.transform;
					bodyCollider.transform.localPosition = Vector3.zero;
					bodyCollider.layer = parent.gameObject.layer;
					bodyCollider.tag = parent.gameObject.tag;
				}
				else
					bodyCollider = parent;
				return Undo.AddComponent<CapsuleCollider>(bodyCollider);
			}
#else
			if (colName[choosen] == "Box Collider")
			{
				GameObject bodyCollider;
				if (colliderOnChildren)
				{
					bodyCollider = new GameObject("Body");
					bodyCollider.transform.parent = parent.transform;
					bodyCollider.transform.localPosition = Vector3.zero;
					bodyCollider.layer = parent.gameObject.layer;
					bodyCollider.tag = parent.gameObject.tag;
				}
				else
					bodyCollider = parent;
				return Undo.AddComponent<BoxCollider2D>(bodyCollider);
			}
			else if (colName[choosen] == "Circle Collider")
			{
				GameObject bodyCollider;
				if (colliderOnChildren)
				{
					bodyCollider = new GameObject("Body");
					bodyCollider.transform.parent = parent.transform;
					bodyCollider.transform.localPosition = Vector3.zero;
					bodyCollider.layer = parent.gameObject.layer;
					bodyCollider.tag = parent.gameObject.tag;
				}
				else
					bodyCollider = parent;
				return Undo.AddComponent<CircleCollider2D>(bodyCollider);
			}
			else if (colName[choosen] == "Polygon Collider")
			{
				GameObject bodyCollider;
				if (colliderOnChildren)
				{
					bodyCollider = new GameObject("Body");
					bodyCollider.transform.parent = parent.transform;
					bodyCollider.transform.localPosition = Vector3.zero;
					bodyCollider.layer = parent.gameObject.layer;
					bodyCollider.tag = parent.gameObject.tag;
				}
				else
					bodyCollider = parent;
				return Undo.AddComponent<PolygonCollider2D>(bodyCollider);
			}
#endif
			return null;
		}
#if PROFORMER3D
		public static void BoxColliderInspect(BoxCollider bc, IsTriggerType isTriggerType = IsTriggerType.Chooseable)
#else
		public static void BoxColliderInspect(BoxCollider2D bc, IsTriggerType isTriggerType = IsTriggerType.Chooseable)
#endif
		{
			if (Selection.gameObjects.Length == 1)
			{
				if (isTriggerType == IsTriggerType.Chooseable)
					bc.isTrigger = EditorGUILayout.Toggle("Is Trigger", bc.isTrigger);
				else {
					GUI.enabled = false;
					if (isTriggerType == IsTriggerType.False) 
						bc.isTrigger = EditorGUILayout.Toggle("Is Trigger", false);
					if (isTriggerType == IsTriggerType.True)
						bc.isTrigger = EditorGUILayout.Toggle("Is Trigger", true);
					GUI.enabled = true;
				}

#if PROFORMER3D
				bc.size = EditorGUILayout.Vector3Field("Size", bc.size);
				bc.center = EditorGUILayout.Vector3Field("Offset", bc.center);
#else
				bc.size = EditorGUILayout.Vector2Field("Size", bc.size);
				bc.offset = EditorGUILayout.Vector2Field("Offset", bc.offset);
#endif
			}
			
		}

#if PROFORMER3D
		public static void SphereColliderInspect(SphereCollider bc, IsTriggerType isTriggerType = IsTriggerType.Chooseable)
#else
		public static void CircleColliderInspect(CircleCollider2D bc, IsTriggerType isTriggerType = IsTriggerType.Chooseable)
#endif
		{
			if (Selection.gameObjects.Length != 1)
				return;
			if (isTriggerType == IsTriggerType.Chooseable)
				bc.isTrigger = EditorGUILayout.Toggle("Is Trigger", bc.isTrigger);
			else {
				GUI.enabled = false;
				if (isTriggerType == IsTriggerType.False)
					bc.isTrigger = EditorGUILayout.Toggle("Is Trigger", false);
				if (isTriggerType == IsTriggerType.True)
					bc.isTrigger = EditorGUILayout.Toggle("Is Trigger", true);
				GUI.enabled = true;
			}
#if PROFORMER3D
			bc.radius = EditorGUILayout.FloatField("Radius", bc.radius);
			bc.center = EditorGUILayout.Vector3Field("Offset", bc.center);
#else
			bc.radius = EditorGUILayout.FloatField("Radius", bc.radius);
			bc.offset = EditorGUILayout.Vector2Field("Offset", bc.offset);
#endif

		}

#if PROFORMER3D
		public static void CapsuleColliderInspect(CapsuleCollider bc, IsTriggerType isTriggerType = IsTriggerType.Chooseable)
#else
		public static void PolygonColliderInspect(PolygonCollider2D bc, IsTriggerType isTriggerType = IsTriggerType.Chooseable)
#endif
		{
			if (Selection.gameObjects.Length != 1)
				return;
			if (isTriggerType == IsTriggerType.Chooseable)
				bc.isTrigger = EditorGUILayout.Toggle("Is Trigger", bc.isTrigger);
			else {
				GUI.enabled = false;
				if (isTriggerType == IsTriggerType.False)
					bc.isTrigger = EditorGUILayout.Toggle("Is Trigger", false);
				if (isTriggerType == IsTriggerType.True)
					bc.isTrigger = EditorGUILayout.Toggle("Is Trigger", true);
				GUI.enabled = true;
			}
#if PROFORMER3D
			bc.radius = EditorGUILayout.FloatField("Radius", bc.radius);
			bc.height = EditorGUILayout.FloatField("Height", bc.height);
			bc.center = EditorGUILayout.Vector3Field("Offset", bc.center);
#else
			bc.offset = EditorGUILayout.Vector2Field("Offset", bc.offset);
#endif

		}
		
#endif
	}
}
