﻿using UnityEngine;
using UnityEditor;

namespace DI {
	public class EditorGUIBox
	{
		public static void Draw(Rect rect)
		{
			EditorGUI.DrawRect(rect, DIEditorHelper.getBoxColor);
		}
		public static void Draw(Rect rect, Color color)
		{
			EditorGUI.DrawRect(rect, color);
		}
		public static void Draw(Rect rect, bool header)
		{
			Draw(rect, header, false);
		}
		public static void Draw(Rect rect, bool border, bool header)
		{
			bool isFoldedDummy = false;
			Draw(rect, border, header, false, ref isFoldedDummy);
		}
		public static void Draw(Rect rect, bool border, bool header, ref bool isFolded)
		{
			Draw(rect, border, header, true, ref isFolded);
		}
		static void Draw(Rect rect, bool border, bool header, bool foldable, ref bool isFolded)
		{
			if (border)
			{
				if (!isFolded)
					EditorGUI.DrawRect(new Rect(rect.x - 1, rect.y - 1, rect.width + 2, rect.height + 2), Color.black);
				else
					EditorGUI.DrawRect(new Rect(rect.x - 1, rect.y - 1, rect.width + 2, EditorGUIUtility.singleLineHeight + EditorGUIUtility.standardVerticalSpacing + 2), Color.black);
			}

			if (foldable)
			{
				if (!isFolded)
					EditorGUI.DrawRect(rect, DIEditorHelper.getBoxColor);
			}

			if (header)
				EditorGUI.LabelField(new Rect(rect.x, rect.y, rect.width, EditorGUIUtility.singleLineHeight + EditorGUIUtility.standardVerticalSpacing), "", EditorStyles.toolbar);

			isFolded = !EditorGUI.Foldout(new Rect(rect.x + rect.width - 15, rect.y, 15, 15), !isFolded, "");
		}
	}
}