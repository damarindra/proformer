﻿using UnityEngine;
using System.Collections;
using System;
using UnityEditor;

public class EditorGUILayoutGroup : IDisposable
{
	bool foldRight, toggleLeft;
	public bool isExpanding = true, toggleValue = false;
	string foldID = "";
	public Rect headerRect;

	public EditorGUILayoutGroup(GUIContent content, string foldID, params GUILayoutOption[] option)
	{
		try
		{
			Setup(content, foldID, false, false);
			CreateHeader(content);
		}
		catch
		{
#if PROFORMER_FULL_LOG
			throw;
#endif
		}
	}
	public EditorGUILayoutGroup(GUIContent content, bool isExpanding, params GUILayoutOption[] option)
	{
		try
		{
			this.isExpanding = isExpanding;
			Setup(content, "", false, false);

			CreateHeader(content);
		}
		catch
		{
#if PROFORMER_FULL_LOG
			throw;
#endif
		}
	}
	public EditorGUILayoutGroup(GUIContent content, bool isExpanding, bool toggleValue, params GUILayoutOption[] option)
	{
		try
		{
			this.isExpanding = isExpanding;
			Setup(content, "", true, toggleValue, option);

			CreateHeader(content);
		}
		catch
		{
#if PROFORMER_FULL_LOG
			throw;
#endif
		}
	}
	public EditorGUILayoutGroup(GUIContent content, string foldID, bool toggleValue, params GUILayoutOption[] option)
	{
		try
		{
			Setup(content, foldID, true, toggleValue, option);
			CreateHeader(content);
		}
		catch
		{
#if PROFORMER_FULL_LOG
			throw;
#endif
		}
	}

	void Setup(GUIContent content, string foldID, bool toggleLeft, bool toggleValue, params GUILayoutOption[] option)
	{
		this.foldRight = true;
		this.foldID = foldID;
		this.toggleValue = toggleValue;
		this.toggleLeft = toggleLeft;
		GUIStyle box = new GUIStyle(GUI.skin.box);
		box.normal.background = DI.DIEditorHelper.MakeTexture(8, 4, DI.DIEditorHelper.getBoxColor, true);
		int indentValue = EditorGUI.indentLevel * 5;
		box.margin = new RectOffset();
		box.margin.left = indentValue;
		box.padding = new RectOffset();

		GUILayout.Space(3);
		EditorGUILayout.BeginVertical(box, option);
	}

	void CreateHeader(GUIContent content)
	{
		headerRect = GUILayoutUtility.GetRect(content, EditorStyles.textField);
		headerRect.x -= 3 + EditorGUI.indentLevel * 15;
		headerRect.width += 6 + EditorGUI.indentLevel * 15;
		headerRect.y -= 1;
		GUIStyle toolbarStyle = new GUIStyle(EditorStyles.toolbar);
		EditorGUI.LabelField(headerRect, "", toolbarStyle);
		GUIStyle labelStyle = new GUIStyle(EditorStyles.label);
		labelStyle.fontStyle = FontStyle.Bold;

		Rect labelRect = headerRect;
		if (toggleLeft)
			labelRect.x += 20;
		EditorGUI.LabelField(labelRect, content, labelStyle);
		if (foldRight)
		{
			Rect foldRect = headerRect;
			foldRect.x += foldRect.width - 5 - EditorGUI.indentLevel * 15;
			foldRect.size = new Vector2(15, 15);
			if (!string.IsNullOrEmpty(foldID))
			{
				EditorPrefs.SetBool(foldID, EditorGUI.Foldout(foldRect, EditorPrefs.GetBool(foldID, false), ""));
				isExpanding = EditorPrefs.GetBool(foldID, false);
			}
			else
			{
				isExpanding = EditorGUI.Foldout(foldRect, isExpanding, "");
			}
		}
		if (toggleLeft)
		{
			Rect fadeRect = headerRect;
			fadeRect.size = new Vector2(15, 15);
			toggleValue = EditorGUI.ToggleLeft(fadeRect, "", toggleValue);
		}
		
	}

	public void Dispose()
	{
		try
		{
			EditorGUILayout.EndVertical();
			GUILayout.Space(5);
		}
		catch {
#if PROFORMER_FULL_LOG
			throw;
#endif
		}
	}
}