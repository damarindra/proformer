﻿#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

namespace DI.Proformer.ToolHelper
{
	public class EditorSceneHelper
	{
		public static Vector3 DrawDIPositionInScene(DIPosition position, Handles.CapFunction handles = null)
		{
			if (handles == null)
				return Handles.PositionHandle(position.position, Quaternion.identity);
			else
				return Handles.FreeMoveHandle(position.position, Quaternion.identity, HandleUtility.GetHandleSize(position.position) * .2f, InternalTool.GetSnapValue(), handles);
		}

		public static Vector3[] DrawDIPositionArrayInScene(DIPosition[] position, Handles.CapFunction handles = null)
		{
			List<Vector3> positions = new List<Vector3>();
			foreach (DIPosition pos in position)
			{
				if (handles == null)
					positions.Add(Handles.PositionHandle(pos.position, Quaternion.identity));
				else
					positions.Add(Handles.FreeMoveHandle(pos.position, Quaternion.identity, HandleUtility.GetHandleSize(pos.position) * .2f, InternalTool.GetSnapValue(), handles));
			}

			return positions.ToArray();
		}

		public static void DrawArrow(Vector3 position, Vector3 direction, Color color, float size, Vector3 tangentDirection)
		{
			Color oriColor = Handles.color;
			Handles.color = color;
			Vector3[] lines = new Vector3[3] {
				position - direction * .15f + Vector3.up * size * .15f,
				position,
				position + direction * .15f + Vector3.up  * size * .15f
			};
			Handles.DrawAAPolyLine(size, lines);
			Handles.color = oriColor;
		}

		public static void DrawBezierCurve(Vector3 from, Vector3 to, float size, Color color, out Vector3 direction, out Vector3 tangentDirection)
		{
			direction = (to - from);
			tangentDirection = direction.y > 0 ? Vector3.up : Vector3.down;
			direction.y = 0;
			direction.Normalize();
			direction *= size;
			Handles.DrawBezier(from, to, from + tangentDirection * 3, to + Vector3.up * 3, color, null, size);
		}

		public static void DrawBezierCurve(Vector3 from, Vector3 to, float size, Color color)
		{
			Vector3 direction;
			Vector3 tangentDirection;
			DrawBezierCurve(from, to, size, color, out direction, out tangentDirection);
			Handles.DrawBezier(from, to, from - direction, to + direction, color, null, size);
		}
		public static void DrawBezierCurveAndArrow(Vector3 from, Vector3 to, float size, Color color)
		{
			Vector3 direction = Vector3.zero;
			Vector3 tangentDirection;
			DrawBezierCurve(from, to, size, color, out direction, out tangentDirection);
			DrawArrow(to, -direction, color, size, tangentDirection);
		}
		public static void DrawBezierCurveArrowAndLabel(Vector3 from, Vector3 to, float size, Color color, string label)
		{
			Vector3 direction = Vector3.zero;
			Vector3 tangentDirection;
			DrawBezierCurve(from, to, size, color, out direction, out tangentDirection);
			Handles.Label(to, label, EditorStyles.helpBox);
			DrawArrow(to, -direction, color, size, tangentDirection);
		}

	}
}
#endif
