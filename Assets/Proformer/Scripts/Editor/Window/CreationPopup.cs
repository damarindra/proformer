﻿#if UNITY_EDITOR
using UnityEngine;
using System.Collections;
using UnityEditor;
using DI.Proformer.Manager;

namespace DI.Proformer
{
	public class CreationPopup : EditorWindow
	{
		public GameObject renderObject;
		private GameObject lastRenderObject;
		public int levelPathPosition = 0;
		[Range(0, 1)]
		public float rawHorizontal = .5f;
		[Range(0, 1)]
		public float rawVertical = .5f;

		public static DIEntity entity;
		private static GameObject targetRender;
		private static bool onSetup = true;
		bool loaded = true;

		public static void CreationPopupWizard(DIEntity entity, GameObject targetRender) {
			CreationPopup.entity = entity;
			CreationPopup.targetRender = targetRender;
			onSetup = true;
			CreationPopup window = EditorWindow.GetWindow<CreationPopup>(true, "Create New", true);
			window.Show();
		}

		void OnInspectorUpdate()
		{
			Repaint();
		}

		void OnGUI() {
			if (Event.current.type == EventType.Layout)
				loaded = true;

			if (!loaded)
				return;
			if (entity != null)
			{
				//Check if position changed from Scene View
				if (entity.position.levelPath.GetFromRaw(rawHorizontal, rawVertical) != entity.transform.position)
				{
					Vector2 raw = entity.position.levelPath.GetRaw(entity.transform.position);
					rawHorizontal = raw.x;
					rawVertical = raw.y;
				}

				if (onSetup)
				{
					if (Selection.activeGameObject) {
						//Check if our selection is not prefab at project window
						if (PrefabUtility.GetPrefabType(Selection.activeGameObject) != PrefabType.Prefab)
						{
							renderObject = targetRender;
						}
						rawHorizontal = entity.position.rawHorizontal;
						rawVertical = entity.position.rawVertical;

					}
					else
						rawHorizontal = rawVertical = .5f;
					levelPathPosition = entity.position.levelPathPosition;
					entity.position.SetPositionAndFixIt(entity.position.levelPath.GetFromRaw(rawHorizontal, rawVertical));
					Selection.activeGameObject = entity.gameObject;
					onSetup = false;
				}


				//RENDERER
				lastRenderObject = renderObject;
				renderObject = EditorGUILayout.ObjectField("Renderer Object", renderObject, typeof(GameObject), true) as GameObject;
				if (renderObject)
				{
					if (!renderObject.GetComponentInChildren<Renderer>())
					{
						EditorGUILayout.HelpBox("Renderer Component not found in Renderer Object. Please replace the it with GameObject that contains Renderer Component", MessageType.Warning);
					}
					else
					{
						if (renderObject.transform.parent != entity.transform)
						{
							if (GUILayout.Button("Set As Renderer"))
							{
								if (entity.GetType().IsSubclassOf(typeof(DIObject)) || entity.GetType() == typeof(DIObject))
								{
									((DIObject)entity).rendererObject = renderObject;
									((DIObject)entity).renderer = renderObject.GetComponentInChildren<Renderer>();
								}
								else if (entity.GetType().IsSubclassOf(typeof(DIControllerMotor)) || entity.GetType() == typeof(DIControllerMotor))
								{
									((DIControllerMotor)entity).rendererObject = renderObject;
									((DIControllerMotor)entity).characterRenderer = renderObject.GetComponentInChildren<Renderer>();
								}

								renderObject.transform.parent = entity.transform;
								renderObject.transform.localPosition = Vector2.zero;
								renderObject.layer = entity.gameObject.layer;
								renderObject.tag = entity.gameObject.tag;
								if (lastRenderObject != renderObject)
									DestroyImmediate(lastRenderObject);
							}
						}
					}
				}

				//LEVELPATHPOS
				GUILayout.BeginHorizontal();
				levelPathPosition = EditorGUILayout.IntField("Level Path Position", levelPathPosition);
				if (GUILayout.Button("-", GUILayout.Width(20)))
				{
					levelPathPosition -= 1;
				}
				if (GUILayout.Button("+", GUILayout.Width(20)))
				{
					levelPathPosition += 1;
				}
				GUILayout.EndHorizontal();

				if (LevelBounds.instance)
				{
					if (levelPathPosition > LevelBounds.instance.levelPath.Count - 1)
						levelPathPosition = LevelBounds.instance.levelPath.Count - 1;
					else if (levelPathPosition < 0)
						levelPathPosition = 0;
				}
				if (levelPathPosition != entity.position.levelPathPosition)
				{
					entity.position.levelPathPosition = levelPathPosition;
					rawHorizontal = rawVertical = .5f;
					entity.position.SetPositionAndFixIt(entity.position.levelPath.GetFromRaw(rawHorizontal, rawVertical));
				}

				//Position
				float lastHorizontal = rawHorizontal;
				float lastVertical = rawVertical;
				rawHorizontal = EditorGUILayout.Slider("Horizontal", rawHorizontal, 0, 1);
				rawVertical = EditorGUILayout.Slider("Vertical", rawVertical, 0, 1);
				if (rawHorizontal != lastHorizontal || rawVertical != lastVertical)
				{
					entity.position.SetPositionAndFixIt(entity.position.levelPath.GetFromRaw(rawHorizontal, rawVertical));
				}
			}
			else {
				EditorGUILayout.HelpBox("Nothing to do here, reference is null.", MessageType.Info);
			}
			EditorGUILayout.Space();
			EditorGUILayout.Space();
			EditorGUILayout.Space();
			GUILayout.BeginHorizontal();
			EditorGUILayout.Space();
			if(GUILayout.Button("Done", GUILayout.Height(50), GUILayout.Width(100))){
				Close();
			}
			EditorGUILayout.Space();
			GUILayout.EndHorizontal();
			
		}

		void OnDestroy() {
			entity = null;
		}
	}
}
#endif