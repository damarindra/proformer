﻿using UnityEngine;
using DI.Proformer.Manager;
using System.IO;
using DI.PoolingSystem;
using System.Linq;
using DI.Proformer.Action;


#if UNITY_EDITOR
using UnityEditor;

namespace DI.Proformer.ToolHelper {
	public class ProformerMenu {

		[MenuItem("Window/Proformer/GameData")]
		static void UpdateGameData()
		{
			Selection.activeObject = GameData.Instance;
		}

#region Manager

		[MenuItem("GameObject/Proformer/Manager/Manager", false, 15)]
		static void CreateManager()
		{
			GameManager gm = Object.FindObjectOfType<GameManager>();
			GameObject go = gm ? gm.gameObject : null;
			if (go)
				Selection.activeGameObject = go;
			else {
				go = new GameObject("Manager");
				go.AddComponent<GameManager>();
				go.AddComponent<PoolingManager>();
				go.AddComponent<LoadLevelManager>();
				go.AddComponent<SoundManager>();
				// Set the user's selection to the new GameObject, so that they can start working with it
				Selection.activeGameObject = go;
			}
		}

		[MenuItem("GameObject/Proformer/Manager/Level Bounds", false, 16)]
		public static void CreateLevelBound()
		{
			LevelBounds lb = Object.FindObjectOfType<LevelBounds>();
			GameObject go = lb ? lb.gameObject : null;
			if (go)
				Selection.activeGameObject = go;
			else {
				go = new GameObject("LevelBounds");
				go.AddComponent<LevelBounds>();
				// Set the user's selection to the new GameObject, so that they can start working with it
				Selection.activeGameObject = go;
			}
		}

#endregion

#region Character
		static void CreateBasicCharacter(DIControllerMotor target, string tag, string layer)
		{
#if !PROFORMER3D
			//target.gameObject.AddComponent<Entity2DHelper>();
#endif
			target.gameObject.tag = tag;
			target.gameObject.layer = LayerMask.NameToLayer(layer);
			target.groundMask = 1 << LayerMask.NameToLayer("Grounds");
			/*
			if (Selection.activeGameObject != null)
			{
				target.rendererObject = Selection.activeGameObject;
				target.rendererObject.transform.parent = target.transform;
				target.rendererObject.transform.localPosition = Vector3.zero;
			}*/
			if (LevelBounds.instance != null)
				target.position.levelPathPosition = LevelBounds.instance.defaultLevelPathIndex;
			if (Selection.activeGameObject != null)
			{
				target.transform.position = target.position.CalculatePositionAndFixIt(Selection.activeGameObject.transform.position);
				EditorUtility.SetDirty(target.transform);
			}
			CreationPopup.CreationPopupWizard(target, Selection.activeGameObject);

			if (!GameObject.FindObjectOfType<LevelBounds>())
				CreateLevelBound();
			//Selection.activeGameObject = target.gameObject;

			GameObject.FindObjectOfType<LevelBounds>().GatherLevelPath();


			//SceneView.lastActiveSceneView.FrameSelected(true);

		}
		[MenuItem("GameObject/Proformer/Character/Player", false, 25)]
		static void CreatePlayer2D()
		{
			//Create core
			GameObject go = new GameObject("Player");
			DIPlayer behaviour = go.AddComponent<DIPlayer>();
			behaviour.controller = go.GetComponent<DIControllerMotor>();
			behaviour.controller.takeDamageTag = new string[1] { "Enemy" };
			behaviour.controller.impactTag = new string[1] { "Enemy" };
			CreateBasicCharacter(behaviour.controller, "Player", "Player");
		}
		[MenuItem("GameObject/Proformer/Character/Character - Follow Path", false, 27)]
		static void CreateEnemyFollowPath()
		{
			GameObject go = new GameObject("Character - Follow Path");
			DIControllerMotor behaviour = go.AddComponent<DIControllerMotor>();
			CreateBasicCharacter(behaviour, "Untagged", "Default");
			go.AddComponent<FollowPath>();
		}
		[MenuItem("GameObject/Proformer/Character/Character AI", false, 28)]
		static void CreateCharacterAI()
		{
			GameObject go = new GameObject("Simple AI");
			DICharacterFSM behaviour = go.AddComponent<DICharacterFSM>();
			go.AddComponent<DIControllerMotor>();
			behaviour.controller = go.GetComponent<DIControllerMotor>();
			behaviour.controller.ladderPermission = false;
			behaviour.controller.wallSlidePermission = false;
			CreateBasicCharacter(behaviour.controller, "Untagged", "Default");
		}
		//[MenuItem("Assets/Proformer/Set Selected As Enemy")]
		static void MakeAsEnemy()
		{
			GameObject selected = Selection.activeGameObject;
			if (!selected || Selection.objects.Length > 1)
			{
				Debug.LogError("Please Select oe GameObject at hierarchy");
				return;
			}
			DIControllerMotor cb = selected.GetComponent<DIControllerMotor>();
			if (!cb)
			{
				Debug.LogError("Selected gameobject is not type of Character");
				return;
			}
			cb.tag = "Enemy";
			cb.gameObject.layer = LayerMask.NameToLayer("Enemy");
			cb.characterBody.tag = "Enemy";
			cb.characterBody.gameObject.layer = cb.gameObject.layer;
			if (!cb.characterBody.GetComponent<GiveDamage>())
				cb.characterBody.gameObject.AddComponent<GiveDamage>();
			if (!cb.characterBody.GetComponent<Impactable>())
				cb.characterBody.gameObject.AddComponent<Impactable>();
		}
#endregion

#region Object
		[MenuItem("GameObject/Proformer/Object/Moving Platform", false, 31)]
		static void CreateMovingPlatform()
		{
			GameObject go = new GameObject("Moving Platform");
			MovingPlatform entt = go.AddComponent<MovingPlatform>();
			go.AddComponent<FollowPath>();
			go.layer = LayerMask.NameToLayer("Grounds");
			/*
			if (Selection.activeGameObject != null) {
				mp.rendererObject = Selection.activeGameObject;
				mp.rendererObject.transform.parent = go.transform;
				mp.rendererObject.transform.localPosition = Vector3.zero;
			}*/
			if (LevelBounds.instance != null)
				entt.position.levelPathPosition = LevelBounds.instance.defaultLevelPathIndex;
			if (Selection.activeGameObject != null)
			{
				entt.transform.position = entt.position.CalculatePositionAndFixIt(Selection.activeGameObject.transform.position);
				Path path = entt.GetComponent<Path>();
				path.pathList.Add(new DIPosition());
				path.pathList[0] = entt.position;
				EditorUtility.SetDirty(entt.transform);
			}
			CreationPopup.CreationPopupWizard(entt, Selection.activeGameObject);
			if (!GameObject.FindObjectOfType<LevelBounds>())
				CreateLevelBound();
			entt.position.levelPathPosition = LevelBounds.instance.defaultLevelPathIndex;
			// Set the user's selection to the new GameObject, so that they can start working with it
			//Selection.activeGameObject = go;
		}
		[MenuItem("GameObject/Proformer/Object/Pickable", false, 32)]
		static void CreatePickable()
		{
			GameObject go = new GameObject("Pickable");
			DIObject entt = go.AddComponent<DIObject>();
			go.AddComponent<Pickable>();
			/*
			if (Selection.activeGameObject != null)
			{
				entt.rendererObject = Selection.activeGameObject;
				entt.rendererObject.transform.parent = go.transform;
				entt.rendererObject.transform.localPosition = Vector3.zero;
			}*/
			if (LevelBounds.instance != null)
				entt.position.levelPathPosition = LevelBounds.instance.defaultLevelPathIndex;
			if (Selection.activeGameObject != null)
			{
				entt.transform.position = entt.position.CalculatePositionAndFixIt(Selection.activeGameObject.transform.position);
				EditorUtility.SetDirty(entt.transform);
			}
			CreationPopup.CreationPopupWizard(entt, Selection.activeGameObject);
			if (!GameObject.FindObjectOfType<LevelBounds>())
				CreateLevelBound();
			// Set the user's selection to the new GameObject, so that they can start working with it
			//Selection.activeGameObject = go;
		}
		[MenuItem("GameObject/Proformer/Object/Teleporter", false, 33)]
		static void CreateTeleporter()
		{
			GameObject go = new GameObject("Teleporter");
			DITeleporter entt = go.AddComponent<DITeleporter>();
			go.GetComponent<DITeleporter>()._autoRotateSinceCreation = false;
			go.GetComponent<DITeleporter>()._colliderOnChildren = false;
			if (LevelBounds.instance != null)
				entt.position.levelPathPosition = LevelBounds.instance.defaultLevelPathIndex;
			if (Selection.activeGameObject != null)
			{
				entt.transform.position = entt.position.CalculatePositionAndFixIt(Selection.activeGameObject.transform.position);
				EditorUtility.SetDirty(entt.transform);
			}
			CreationPopup.CreationPopupWizard(entt, Selection.activeGameObject);
			if (!GameObject.FindObjectOfType<LevelBounds>())
				CreateLevelBound();

			/*if (Selection.activeGameObject != null)
			{
				entt.rendererObject = Selection.activeGameObject;
				entt.rendererObject.transform.parent = go.transform;
				entt.rendererObject.transform.localPosition = Vector3.zero;
			}
			// Set the user's selection to the new GameObject, so that they can start working with it
			Selection.activeGameObject = go;*/
		}
		[MenuItem("GameObject/Proformer/Object/Treadmill", false, 33)]
		static void CreateTreadmill()
		{
			GameObject go = new GameObject("Treadmill");
			DITreadmill entt = go.AddComponent<DITreadmill>();
			/*if (Selection.activeGameObject != null)
			{
				entt.rendererObject = Selection.activeGameObject;
				entt.rendererObject.transform.parent = go.transform;
				entt.rendererObject.transform.localPosition = Vector3.zero;
			}*/
			if (LevelBounds.instance != null)
				entt.position.levelPathPosition = LevelBounds.instance.defaultLevelPathIndex;
			if (Selection.activeGameObject != null)
			{
				entt.transform.position = entt.position.CalculatePositionAndFixIt(Selection.activeGameObject.transform.position);
				EditorUtility.SetDirty(entt.transform);
			}
			CreationPopup.CreationPopupWizard(entt, Selection.activeGameObject);
			if (!GameObject.FindObjectOfType<LevelBounds>())
				CreateLevelBound();
			// Set the user's selection to the new GameObject, so that they can start working with it
			//Selection.activeGameObject = go;

		}
		[MenuItem("GameObject/Proformer/Object/Ladder", false, 33)]
		static void CreateLadder()
		{
			GameObject go = new GameObject("Ladder");
			DILadder entt = go.AddComponent<DILadder>();
			entt._colliderOnChildren = false;
			/*if (Selection.activeGameObject != null)
			{
				entt.rendererObject = Selection.activeGameObject;
				entt.rendererObject.transform.parent = go.transform;
				entt.rendererObject.transform.localPosition = Vector3.zero;
			}*/
			if (LevelBounds.instance != null)
				entt.position.levelPathPosition = LevelBounds.instance.defaultLevelPathIndex;
			if (Selection.activeGameObject != null)
			{
				entt.transform.position = entt.position.CalculatePositionAndFixIt(Selection.activeGameObject.transform.position);
				EditorUtility.SetDirty(entt.transform);
			}
			CreationPopup.CreationPopupWizard(entt, Selection.activeGameObject);
			if (!GameObject.FindObjectOfType<LevelBounds>())
				CreateLevelBound();
			// Set the user's selection to the new GameObject, so that they can start working with it
			//Selection.activeGameObject = go;

		}
		[MenuItem("GameObject/Proformer/Object/Go To Level", false, 34)]
		static void CreateGoToLevel()
		{
			GameObject go = new GameObject("Go To Level");
			DIEntity entt = go.AddComponent<GoToLevel>();
			go.GetComponent<GoToLevel>()._autoRotateSinceCreation = false;
			if (LevelBounds.instance != null)
				entt.position.levelPathPosition = LevelBounds.instance.defaultLevelPathIndex;
			if (Selection.activeGameObject != null)
			{
				entt.transform.position = entt.position.CalculatePositionAndFixIt(Selection.activeGameObject.transform.position);
				EditorUtility.SetDirty(entt.transform);
			}
			CreationPopup.CreationPopupWizard(entt, Selection.activeGameObject);
			if (!GameObject.FindObjectOfType<LevelBounds>())
				CreateLevelBound();
		}

#endregion

#region CAMERA

		[MenuItem("GameObject/Proformer/Camera/Main Camera", false, 40)]
		static void CreateCamera() {
			int depth = -1;
			GameObject go = new GameObject("Proformer Camera");
			go.AddComponent<Camera>();
			go.GetComponent<Camera>().depth = depth;
			go.GetComponent<Camera>().orthographic = false;
			go.AddComponent<FlareLayer>();
			go.AddComponent<GUILayer>();
			go.AddComponent<AudioListener>();
			go.AddComponent<DICamera>();
		}

		[MenuItem("GameObject/Proformer/Camera/Main Camera 2D", false, 41)]
		static void CreateCamera2D()
		{
			int depth = -1;
			GameObject go = new GameObject("Proformer Camera");
			go.AddComponent<Camera>();
			go.GetComponent<Camera>().depth = depth;
			go.GetComponent<Camera>().orthographic = true;
			go.AddComponent<FlareLayer>();
			go.AddComponent<GUILayer>();
			go.AddComponent<AudioListener>();
			go.AddComponent<DICamera2D>();
		}
		[MenuItem("GameObject/Proformer/Camera/UI Camera", false, 42)]
		static void CreateCameraUI()
		{
			GameObject go = null;
			if (Object.FindObjectOfType<GUIManager>()) {
				go = Object.FindObjectOfType<GUIManager>().gameObject;
				Selection.activeGameObject = go;
			}
			else {
				go = Object.Instantiate(AssetDatabase.LoadAssetAtPath("Assets/Proformer/Resources/Template/CameraUI.prefab", typeof(GameObject))) as GameObject;
				go.GetComponent<Camera>().depth = 1;
				go.name = "UI Camera";
				Selection.activeGameObject = go;
			}
		}

#endregion

#region MISC

		[MenuItem("GameObject/Proformer/Misc/Projectile", false, 36)]
		static void CreateProjectile()
		{
			GameObject go = new GameObject("Projectile");
			DIEntity entt = go.AddComponent<Projectile>();
			// Set the user's selection to the new GameObject, so that they can start working with it
			if (LevelBounds.instance != null)
				entt.position.levelPathPosition = LevelBounds.instance.defaultLevelPathIndex;
			if (Selection.activeGameObject != null)
			{
				entt.transform.position = entt.position.CalculatePositionAndFixIt(Selection.activeGameObject.transform.position);
				EditorUtility.SetDirty(entt.transform);
			}
			CreationPopup.CreationPopupWizard(entt, Selection.activeGameObject);
			if (!GameObject.FindObjectOfType<LevelBounds>())
				CreateLevelBound();
			//Selection.activeGameObject = go;
		}

		[MenuItem("GameObject/Proformer/Misc/Parallax", false, 37)]
		static void CreateParallax() {
			GameObject go = new GameObject("Parallax");
			go.AddComponent<DIParallax>();
			Selection.activeGameObject = go;
		}


		[MenuItem("GameObject/Proformer/Misc/Special Ground", false, 37)]
		static void CreateSpecialGround()
		{
			GameObject go = new GameObject("Special Ground");
			go.AddComponent<DISpecialGround>();
			go.layer = LayerMask.NameToLayer("Grounds");
			// Set the user's selection to the new GameObject, so that they can start working with it
			Selection.activeGameObject = go;
		}
		[MenuItem("GameObject/Proformer/Misc/Special Area", false, 37)]
		static void CreateSpecialArea()
		{
			GameObject go = new GameObject("Special Area");
			go.AddComponent<DISpecialArea>();
			// Set the user's selection to the new GameObject, so that they can start working with it
			Selection.activeGameObject = go;
		}

		#endregion

		/// <summary>
		/// ALL DISABLE IN THIS REGION!
		/// </summary>
		//[MenuItem("Window/Proformer/Ground Collider Type/2D Collider")]
		public static void Enable3DCollider()
		{
			Debug.LogWarning("Enabling 3D Collider");
			var defines = PlayerSettings.GetScriptingDefineSymbolsForGroup(EditorUserBuildSettings.selectedBuildTargetGroup);
			string newDefine = "PROFORMER3D";
			if (!defines.Contains(newDefine))
			{
				defines = string.Format("{0},{1}", defines, newDefine);
				PlayerSettings.SetScriptingDefineSymbolsForGroup(EditorUserBuildSettings.selectedBuildTargetGroup, defines);
				AssetDatabase.SaveAssets();
			}
		}
		

		//[MenuItem("Window/Proformer/Ground Collider Type/3D Collider")]
		public static void Enable2DCollider()
		{
			Debug.LogWarning("Enabling 2D Collider");
			var defines = PlayerSettings.GetScriptingDefineSymbolsForGroup(EditorUserBuildSettings.selectedBuildTargetGroup);
			string newDefine = "PROFORMER3D";
			if (defines.Contains(newDefine))
			{
				defines = defines.Replace(newDefine, "");
				PlayerSettings.SetScriptingDefineSymbolsForGroup(EditorUserBuildSettings.selectedBuildTargetGroup, defines);
				AssetDatabase.SaveAssets();
			}
		}
		
		
		[MenuItem("Window/Proformer/Flush Custom Action")]
		public static void FlushProformerActionLib() {
			GameData.Instance.customActionLibString.Clear();

			GameData.Instance.customActionLibString = InternalTool.GetScriptAssetsOfType<ProformerAction>().Select(i => i.ToString()).ToList();
			EditorUtility.SetDirty(GameData.Instance);
		}

#region Deprecated
		//[MenuItem("GameObject/Proformer/Make as/Collectable", false, 27)]
		static void MakeAsCollectable()
		{
			if (Selection.activeGameObject)
			{
				GameObject go = Selection.activeGameObject;
				go.AddComponent<Pickable>();

				if (!GameObject.FindObjectOfType<LevelBounds>())
					CreateLevelBound();
			}
			else
			{
				//WindowPopup.InitWindow ("Please Select your Candidate Impactable GameObject");
				Debug.LogError("Please Select your Candidate Impactable GameObject");
			}

			// Set the user's selection to the new GameObject, so that they can start working with it
		}
		//[MenuItem("GameObject/Proformer/Misc/Collider", false, 35)]
		/*static void CreateCollider()
		{
			GameObject go = new GameObject("Proformer Collider");
			go.AddComponent<ProformerSimpleCollider>();
			// Set the user's selection to the new GameObject, so that they can start working with it
			Selection.activeGameObject = go;
		}*/
		//[MenuItem("GameObject/Proformer/Make as/Moving Platform", false, 25)]
		static void MakeAsMovingPlatform()
		{
			if (Selection.activeGameObject)
			{
				GameObject go = Selection.activeGameObject;
				go.AddComponent<MovingPlatform>();

				if (!GameObject.FindObjectOfType<LevelBounds>())
					CreateLevelBound();
			}
			else
			{
				//WindowPopup.InitWindow ("Please Select your Candidate Moving Platform GameObject");
				Debug.LogError("Please Select your Candidate Moving Platform GameObject");
			}

			// Set the user's selection to the new GameObject, so that they can start working with it
		}
#endregion
	}
}
#endif