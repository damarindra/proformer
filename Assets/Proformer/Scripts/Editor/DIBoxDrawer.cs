﻿#if UNITY_EDITOR
using UnityEngine;
using System.Collections;
using UnityEditor;

namespace DI.Proformer.ToolHelper{
	/// <summary>
	/// Custom drawer for grouping member class. Just your inherit drawer class with DIBoxDrawer.
	/// Call Draw for drawing Box.
	/// Don't forget to hide your member using property.isExpanded
	/// </summary>
	public class DIBoxDrawer : PropertyDrawer {
		
		protected Rect totalRect;
		protected float spaceBottom = 0;

		public void Draw(SerializedProperty property, Rect rect, bool border, bool header) {
			Draw(property, rect, border, header, false);
		}

		public void Draw(SerializedProperty property, Rect rect, bool border, bool header, bool headerText, float bottomOffset = 0)
		{
			totalRect = rect;
			Rect realBoxRect = rect;
			realBoxRect.x += EditorGUI.indentLevel * 15;
			realBoxRect.width -= EditorGUI.indentLevel * 15;
			realBoxRect.height += bottomOffset;
			if (border) {
				if (property.isExpanded)
					EditorGUI.DrawRect(new Rect(realBoxRect.x - 1, realBoxRect.y - 1, realBoxRect.width + 2, realBoxRect.height + 2), Color.black);
				else
					EditorGUI.DrawRect(new Rect(realBoxRect.x - 1, realBoxRect.y - 1, realBoxRect.width + 2, EditorGUIUtility.singleLineHeight + EditorGUIUtility.standardVerticalSpacing + 2), Color.black);
			}
			
			if (property.isExpanded)
				EditorGUI.DrawRect(realBoxRect, DIEditorHelper.getBoxColor);
			
			if (header)
				EditorGUI.LabelField(new Rect(rect.x, rect.y, rect.width, EditorGUIUtility.singleLineHeight + EditorGUIUtility.standardVerticalSpacing), "", EditorStyles.toolbar);
			if (headerText) {
				GUIStyle labelStyle = new GUIStyle(EditorStyles.label);
				labelStyle.fontStyle = FontStyle.Bold;
				EditorGUI.LabelField(new Rect(rect.x, rect.y, rect.width, EditorGUIUtility.singleLineHeight + EditorGUIUtility.standardVerticalSpacing), property.displayName, labelStyle);
			}

			property.isExpanded = EditorGUI.Foldout(new Rect(realBoxRect.x + realBoxRect.width - EditorGUI.indentLevel*15 - 5, realBoxRect.y, 15, 15), property.isExpanded, "");
		}

		public override float GetPropertyHeight(SerializedProperty property, GUIContent label) {
			if (property.isExpanded)
				return totalRect.height + spaceBottom + 7;
			else
				return EditorGUIUtility.singleLineHeight + EditorGUIUtility.standardVerticalSpacing + 2 + spaceBottom + 5;
		}
	}
	
}
#endif