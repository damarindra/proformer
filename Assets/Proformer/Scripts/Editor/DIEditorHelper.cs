﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System;

namespace DI {
	public class DIEditorHelper {

		public static Texture2D MakeTexture(int width, int height, Color col, bool border = true)
		{
			Color[] pix = new Color[width * height];
			int currentIndex = 0;

			for (int j = 0; j < height; j++)
			{
				for (int i = 0; i < width; i++)
				{
					if (i == 0 || i == width - 1 || j == 0 || j == height - 1)
						pix[currentIndex] = Color.black;
					else
						pix[currentIndex] = col;
					currentIndex += 1;
				}
			}

			Texture2D result = new Texture2D(width, height);
			result.SetPixels(pix);
			result.Apply();
			return result;
		}

		public static Color getBoxColor
		{
			get
			{
				if (EditorGUIUtility.isProSkin)
					return new Color(78 / 255f, 78 / 255f, 78 / 255f, 1);
				else
					return new Color(177 / 255f, 177 / 255f, 177 / 255f, 1);
			}
		}
		public static Color getNormalColor
		{
			get
			{
				if (EditorGUIUtility.isProSkin)
					return new Color(50 / 255f, 50 / 255f, 50 / 255f, 1);
				else
					return new Color(200 / 255f, 200 / 255f, 200 / 255f, 1);
			}
		}

		public static void DrawHeaderToolbar(Rect rect, bool forMotorSubMenu = true)
		{
			if (forMotorSubMenu)
			{
				rect.y -= 1f;
				rect.width = EditorGUIUtility.currentViewWidth - 36;
				rect.x = 15;
			}
			GUI.Label(rect, GUIContent.none, EditorStyles.toolbar);
		}


		public static Rect EditorGUILayoutLabel(GUIContent content, GUIStyle style = null)
		{
			if (style == null)
				style = EditorStyles.label;
			Rect rect = GUILayoutUtility.GetRect(content, style);
			EditorGUI.LabelField(rect, content, style);
			return rect;
		}
		public static Rect EditorGUILayoutTextField(GUIContent content, ref string text, GUIStyle style = null)
		{
			if (style == null)
				style = EditorStyles.textField;
			Rect rect = GUILayoutUtility.GetRect(content, style);
			text = EditorGUI.TextField(rect, content, text, style);
			return rect;
		}
		public static Rect EditorGUILayoutObject(GUIContent content, ref UnityEngine.Object obj, Type type, bool allowSceneObjects)
		{
			Rect rect = GUILayoutUtility.GetRect(content, EditorStyles.objectField);
			obj = EditorGUI.ObjectField(rect, content, obj, type, allowSceneObjects);
			return rect;
		}
		public static Rect EditorGUILayoutPopup(GUIContent content, ref int index, GUIContent[] displayedNames)
		{
			Rect rect = GUILayoutUtility.GetRect(content, EditorStyles.objectField);
			index = EditorGUI.Popup(rect, content, index, displayedNames);
			return rect;
		}
		public static Rect EditorGUILayoutSlider(GUIContent content, ref float value, float min, float max)
		{
			Rect rect = GUILayoutUtility.GetRect(content, GUI.skin.horizontalSlider);
			value = EditorGUI.Slider(rect, content, value, min, max);
			return rect;
		}
		public static Rect EditorGUILayoutIntSlider(GUIContent content, ref int value, int min, int max)
		{
			Rect rect = GUILayoutUtility.GetRect(content, GUI.skin.horizontalSlider);
			value = EditorGUI.IntSlider(rect, content, value, min, max);
			return rect;
		}

		public static UnityEngine.Object RequiredObjectField(GUIContent content, UnityEngine.Object obj, Type objType, bool allowSceneObjects, params GUILayoutOption[] options) {
			return ColorObjectField(Color.red, content, obj, objType, allowSceneObjects, options);
		}
		public static UnityEngine.Object WarningObjectField(GUIContent content, UnityEngine.Object obj, Type objType, bool allowSceneObjects, params GUILayoutOption[] options)
		{
			return ColorObjectField(Color.yellow, content, obj, objType, allowSceneObjects, options);
		}

		public static UnityEngine.Object ColorObjectField(Color color, GUIContent content, UnityEngine.Object obj, Type objType, bool allowSceneObjects, params GUILayoutOption[] options)
		{

			Texture2D normalTex = EditorStyles.objectField.normal.background;
			Texture2D activeTex = EditorStyles.objectField.active.background;
			Texture2D focusedTex = EditorStyles.objectField.focused.background;
			Texture2D hoverTex = EditorStyles.objectField.hover.background;

			if (obj == null)
			{
				if (normalTex != null)
					EditorStyles.objectField.normal.background = MakeTexture(normalTex.width, normalTex.height, color);
				if (activeTex != null)
					EditorStyles.objectField.active.background = MakeTexture(activeTex.width, activeTex.height, color);
				if (focusedTex != null)
					EditorStyles.objectField.focused.background = MakeTexture(focusedTex.width, focusedTex.height, color);
				if (hoverTex != null)
					EditorStyles.objectField.hover.background = MakeTexture(hoverTex.width, hoverTex.height, color);
			}

			UnityEngine.Object result = EditorGUILayout.ObjectField(content, obj, objType, allowSceneObjects, options);


			if (obj == null)
			{
				if (normalTex != null)
					EditorStyles.objectField.normal.background = normalTex;
				if (activeTex != null)
					EditorStyles.objectField.active.background = activeTex;
				if (focusedTex != null)
					EditorStyles.objectField.focused.background = focusedTex;
				if (hoverTex != null)
					EditorStyles.objectField.hover.background = hoverTex;
			}

			return result;
		}
	}
}
