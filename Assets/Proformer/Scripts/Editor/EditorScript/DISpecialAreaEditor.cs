﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using DI.Proformer.ToolHelper;

namespace DI.Proformer {

	[CustomEditor(typeof(DISpecialArea), true)]
	public class DISpecialAreaEditor : Editor
	{
		DISpecialArea area;

		//public override void OnEnable()
		void OnEnable()
		{
			area = (DISpecialArea)target;
		}

		public override void OnInspectorGUI()
		{
			EditorGUILayout.HelpBox("DI Special Area only affect to ControllerMotor (such Player), other than character type not affected. (Follow Path not supported)", MessageType.Info);
			base.OnInspectorGUI();
			EditorGUI.BeginChangeCheck();
			using (EditorGUILayoutGroup group = new EditorGUILayoutGroup(new GUIContent("Move Speed Modify", "Modify move speed when character enter this Area"), serializedObject.FindProperty("moveSpeedModify").isExpanded, serializedObject.FindProperty("moveSpeedModify").FindPropertyRelative("permission").boolValue))
			{
				serializedObject.FindProperty("moveSpeedModify").isExpanded = group.isExpanding;
				serializedObject.FindProperty("moveSpeedModify").FindPropertyRelative("permission").boolValue = group.toggleValue;
				if (!serializedObject.FindProperty("moveSpeedModify").FindPropertyRelative("permission").boolValue)
					GUI.enabled = false;
				if (group.isExpanding)
				{
					EditorGUILayout.PropertyField(serializedObject.FindProperty("moveSpeedModify").FindPropertyRelative("value"));
					EditorGUILayout.PropertyField(serializedObject.FindProperty("moveSpeedModify").FindPropertyRelative("operation"));
				}
				GUI.enabled = true;
			}
			using (EditorGUILayoutGroup group = new EditorGUILayoutGroup(new GUIContent("Gravity Modify", "Modify gravity when character enter this Area"), serializedObject.FindProperty("gravityModify").isExpanded, serializedObject.FindProperty("gravityModify").FindPropertyRelative("permission").boolValue))
			{
				serializedObject.FindProperty("gravityModify").isExpanded = group.isExpanding;
				serializedObject.FindProperty("gravityModify").FindPropertyRelative("permission").boolValue = group.toggleValue;
				if (!serializedObject.FindProperty("gravityModify").FindPropertyRelative("permission").boolValue)
					GUI.enabled = false;
				if (group.isExpanding)
				{
					EditorGUILayout.PropertyField(serializedObject.FindProperty("gravityModify").FindPropertyRelative("value"));
					EditorGUILayout.PropertyField(serializedObject.FindProperty("gravityModify").FindPropertyRelative("operation"));
				}
				GUI.enabled = true;

			}
			using (EditorGUILayoutGroup group = new EditorGUILayoutGroup(new GUIContent("Force Horizontal", "Add force horizontal"), serializedObject.FindProperty("forcingHorizontal").isExpanded, serializedObject.FindProperty("forcingHorizontal").FindPropertyRelative("permission").boolValue))
			{
				serializedObject.FindProperty("forcingHorizontal").isExpanded = group.isExpanding;
				serializedObject.FindProperty("forcingHorizontal").FindPropertyRelative("permission").boolValue = group.toggleValue;
				if (!serializedObject.FindProperty("forcingHorizontal").FindPropertyRelative("permission").boolValue)
					GUI.enabled = false;
				if (group.isExpanding)
				{
					EditorGUILayout.PropertyField(serializedObject.FindProperty("forcingHorizontal").FindPropertyRelative("value"));
					EditorGUILayout.PropertyField(serializedObject.FindProperty("forcingHorizontal").FindPropertyRelative("maxValue"));
					EditorGUILayout.PropertyField(serializedObject.FindProperty("forcingHorizontal").FindPropertyRelative("operation"));
				}
				GUI.enabled = true;
			}

			EditorGUILayout.PropertyField(serializedObject.FindProperty("_rigidbody"));

			if (area._collider)
				EditorInspectorHelper.DrawColliderInspector(ref area._collider, serializedObject.FindProperty("_collider"));
			else
			{
				using (EditorGUILayoutGroup group = new EditorGUILayoutGroup(new GUIContent("Collider"), serializedObject.FindProperty("_collider").isExpanded))
				{
					serializedObject.FindProperty("_collider").isExpanded = group.isExpanding;
					if (group.isExpanding)
						area._collider = EditorInspectorHelper.PopUpColliderCreation(area.gameObject, false);
				}
			}
			if (EditorGUI.EndChangeCheck())
			{
				serializedObject.ApplyModifiedProperties();
				EditorUtility.SetDirty(area);
			}
		}
	}
}