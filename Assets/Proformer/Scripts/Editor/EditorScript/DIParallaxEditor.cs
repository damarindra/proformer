﻿using UnityEngine;
using System.Collections;
using UnityEditorInternal;
using UnityEditor;
using System.Linq;

namespace DI.Proformer {
	[CustomEditor(typeof(DIParallax), true)]
	public class ProformerParallaxEditor : Editor
	{
		DIParallax pp;

		ReorderableList parallaxList;
		DICameraBase camBase;

		void OnEnable()
		{
			pp = (DIParallax)target;
			if (camBase == null)
				camBase = FindObjectOfType<DICameraBase>();
			if (camBase)
			{
				foreach (DIParallax.Parallax p in pp.parallax)
				{
					if (p.parallaxObject)
						p.offsetPosition = p.parallaxObject.transform.position - camBase.transform.position;
				}
			}

			parallaxList = new ReorderableList(serializedObject, serializedObject.FindProperty("parallax"), true, true, true, true);
			parallaxList.drawElementCallback = drawElement;
			parallaxList.drawHeaderCallback = headerElement;
			parallaxList.elementHeight = EditorGUIUtility.singleLineHeight * 8f;
			parallaxList.onAddCallback = addElement;
			parallaxList.onRemoveCallback = removeElement;
		}

		private void headerElement(Rect rect)
		{
			EditorGUI.LabelField(rect, "Parallax");
		}

		private void removeElement(ReorderableList list)
		{
			pp.parallax.RemoveAt(parallaxList.index);
		}

		private void addElement(ReorderableList list)
		{
			pp.parallax.Add(new DIParallax.Parallax());
		}

		public override void OnInspectorGUI()
		{
			EditorGUILayout.HelpBox("Parallax is ONLY WORKS ON 2D Games!", MessageType.Info);

			if (camBase == null)
				camBase = FindObjectOfType<DICameraBase>();
			if (camBase == null)
			{
				EditorGUILayout.HelpBox("No Proformer Camera in Hierarchy, please create one", MessageType.Error);
				return;
			}
			parallaxList.DoLayoutList();
		}

		private void drawElement(Rect rect, int index, bool isActive, bool isFocused)
		{
			EditorGUI.BeginChangeCheck();
			var parallax = parallaxList.serializedProperty.GetArrayElementAtIndex(index);
			if (parallax.FindPropertyRelative("parallaxObject").objectReferenceValue == null)
			{
				Rect buttonRect = rect;
				buttonRect.y += EditorGUIUtility.singleLineHeight * 1.5f;
				buttonRect.height -= EditorGUIUtility.singleLineHeight * 2f;
				EditorGUI.PropertyField(new Rect(rect.x, rect.y, rect.width, EditorGUIUtility.singleLineHeight), parallax.FindPropertyRelative("parallaxSprite"));
				if (!parallax.FindPropertyRelative("parallaxSprite").objectReferenceValue)
					GUI.enabled = false;
				if (GUI.Button(buttonRect, "Create"))
				{
					GameObject parallaxObject = new GameObject(parallax.FindPropertyRelative("parallaxSprite").objectReferenceValue.name);
					parallaxObject.AddComponent<SpriteRenderer>();
					parallaxObject.GetComponent<SpriteRenderer>().sprite = (Sprite)parallax.FindPropertyRelative("parallaxSprite").objectReferenceValue;
					parallaxObject.transform.parent = pp.transform;
					parallax.FindPropertyRelative("parallaxObject").objectReferenceValue = parallaxObject;
				}
				GUI.enabled = true;
			}
			else
			{
				EditorGUI.PropertyField(new Rect(rect.x, rect.y, rect.width, EditorGUIUtility.singleLineHeight), parallax.FindPropertyRelative("parallaxObject"));
			}

			if (parallax.FindPropertyRelative("parallaxObject").objectReferenceValue)
			{
				GameObject pObj = (GameObject)parallax.FindPropertyRelative("parallaxObject").objectReferenceValue;
				SpriteRenderer sr = pObj.GetComponent<SpriteRenderer>();
				EditorGUI.BeginChangeCheck();
				var sortingLayerName = InternalTool.GetSortingLayerNames().ToList();
				EditorGUI.PropertyField(new Rect(rect.x, rect.y + EditorGUIUtility.singleLineHeight * 1.2f, rect.width, EditorGUIUtility.singleLineHeight), parallax.FindPropertyRelative("offsetPosition"));

				EditorGUI.LabelField(new Rect(rect.x, rect.y + 2 * EditorGUIUtility.singleLineHeight * 1.2f, 90, EditorGUIUtility.singleLineHeight), "Sorting Layer");
				sr.sortingLayerName = sortingLayerName[EditorGUI.Popup(new Rect(rect.x + 90, rect.y + 2 * EditorGUIUtility.singleLineHeight * 1.2f, rect.width - 190, EditorGUIUtility.singleLineHeight), sortingLayerName.IndexOf(sr.sortingLayerName), sortingLayerName.ToArray())];

				EditorGUI.LabelField(new Rect(rect.x + rect.width - 95, rect.y + 2 * EditorGUIUtility.singleLineHeight * 1.2f, 40, EditorGUIUtility.singleLineHeight), "Order");
				sr.sortingOrder = EditorGUI.IntField(new Rect(rect.x + rect.width - 55, rect.y + 2 * EditorGUIUtility.singleLineHeight * 1.2f, 55, EditorGUIUtility.singleLineHeight), sr.sortingOrder);
				if(EditorGUI.EndChangeCheck())
					EditorUtility.SetDirty(sr);

				parallax.FindPropertyRelative("speedHorizontal").floatValue = EditorGUI.Slider(new Rect(rect.x, rect.y + 3 * EditorGUIUtility.singleLineHeight * 1.2f, rect.width, EditorGUIUtility.singleLineHeight), "Speed Horizontal", parallax.FindPropertyRelative("speedHorizontal").floatValue, -3, 3);
				parallax.FindPropertyRelative("speedVertical").floatValue = EditorGUI.Slider(new Rect(rect.x, rect.y + 4 * EditorGUIUtility.singleLineHeight * 1.2f, rect.width, EditorGUIUtility.singleLineHeight), "Speed Vertical", parallax.FindPropertyRelative("speedVertical").floatValue, -3, 3);

				EditorGUI.LabelField(new Rect(rect.x, rect.y + 5 * EditorGUIUtility.singleLineHeight * 1.2f, 80, EditorGUIUtility.singleLineHeight), "Repeat X");
				parallax.FindPropertyRelative("repeatX").boolValue = EditorGUI.Toggle(new Rect(rect.x + 80, rect.y + 5 * EditorGUIUtility.singleLineHeight * 1.2f, rect.width / 2 - 80, EditorGUIUtility.singleLineHeight), parallax.FindPropertyRelative("repeatX").boolValue);

				//EditorGUI.LabelField(new Rect(rect.x + rect.width / 2, rect.y + 5 * EditorGUIUtility.singleLineHeight * 1.2f, 80, EditorGUIUtility.singleLineHeight), "Repeat Y");
				//parallax.FindPropertyRelative("repeatY").boolValue = EditorGUI.Toggle(new Rect(rect.x + rect.width / 2 + 80, rect.y + 5 * EditorGUIUtility.singleLineHeight * 1.2f, rect.width / 2 - 80, EditorGUIUtility.singleLineHeight), parallax.FindPropertyRelative("repeatY").boolValue);
			}

			if (EditorGUI.EndChangeCheck())
			{
				serializedObject.ApplyModifiedProperties();
				if (camBase && !Application.isPlaying)
				{
					GameObject pObj = (GameObject)parallax.FindPropertyRelative("parallaxObject").objectReferenceValue;
					pObj.transform.position = camBase.transform.position + camBase.transform.TransformDirection(parallax.FindPropertyRelative("offsetPosition").vector3Value);
					EditorUtility.SetDirty(pObj.transform);
				}
			}
		}
	}
}