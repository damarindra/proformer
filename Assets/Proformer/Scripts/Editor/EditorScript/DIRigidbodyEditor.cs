﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using DI.Proformer.ToolHelper;

namespace DI.Proformer {

	[CustomPropertyDrawer(typeof(DIRigidbody), true)]
	public class ProformerRigidbodyDrawer : DIBoxDrawer
	{

		Rect column0, column1, column2, column3;

		public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
		{
			//base.OnGUI(position, property, label);
			column0 = new Rect(position.x, position.y, position.width, EditorGUIUtility.singleLineHeight);
			column1 = column0;
			column1.y += EditorGUIUtility.singleLineHeight * 1.1f;
			column2 = column1;
			column2.y += EditorGUIUtility.singleLineHeight * 1.1f;
			column3 = column2;
			column3.y += EditorGUIUtility.singleLineHeight * 1.1f;
			Rect rbWithButton = column1;
			rbWithButton.width -= 60;
			Rect button = column1;
			button.x += rbWithButton.width;
			button.width = 55;

#if PROFORMER3D
			totalRect = new Rect(column0.x, column0.y, column0.width, column3.y - column0.y + column3.height);
			Draw(property, new Rect(column0.x, column0.y, column0.width, column3.y - column0.y + column3.height), true, true);
#else
			totalRect = new Rect(column0.x, column0.y, column0.width, column2.y - column0.y + column2.height);
			Draw(property, new Rect(column0.x, column0.y, column0.width, column2.y - column0.y + column2.height), true, true);
#endif

			EditorGUI.LabelField(column0, "Rigidbody", EditorStyles.boldLabel);
			if (property.isExpanded)
			{
#if PROFORMER3D
				property.FindPropertyRelative("rb").objectReferenceValue = EditorGUI.ObjectField(property.FindPropertyRelative("rb").objectReferenceValue ? column1 : rbWithButton, "Rigidbody", property.FindPropertyRelative("rb").objectReferenceValue, typeof(Rigidbody), true);
				if (property.FindPropertyRelative("rb").objectReferenceValue == null)
				{
					if (GUI.Button(button, "Create"))
					{
						if (Selection.gameObjects.Length > 1)
							Debug.LogError("Selection GameObject is not inspector gameobject");
						else if (Selection.gameObjects.Length == 1)
						{
							property.FindPropertyRelative("rb").objectReferenceValue = Selection.activeGameObject.AddComponent<Rigidbody>();
							((Rigidbody)(property.FindPropertyRelative("rb").objectReferenceValue)).isKinematic = true;
							((Rigidbody)(property.FindPropertyRelative("rb").objectReferenceValue)).useGravity = false;
						}
					}
					GUI.enabled = false;
					EditorGUI.Toggle(column2, "Is Kinematic", false);
					EditorGUI.Toggle(column3, "Use Gravity", false);
				}
				else
				{
					((Rigidbody)(property.FindPropertyRelative("rb").objectReferenceValue)).isKinematic = EditorGUI.Toggle(column2, "Is Kinematic", ((Rigidbody)(property.FindPropertyRelative("rb").objectReferenceValue)).isKinematic);
					((Rigidbody)(property.FindPropertyRelative("rb").objectReferenceValue)).useGravity = EditorGUI.Toggle(column3, "Use Gravity", ((Rigidbody)(property.FindPropertyRelative("rb").objectReferenceValue)).useGravity);
				}
#else
				property.FindPropertyRelative("rb").objectReferenceValue = EditorGUI.ObjectField(property.FindPropertyRelative("rb").objectReferenceValue ? column1 : rbWithButton, "Rigidbody", property.FindPropertyRelative("rb").objectReferenceValue, typeof(Rigidbody2D), true);
				if (property.FindPropertyRelative("rb").objectReferenceValue == null) {
					if(GUI.Button(button, "Create"))
					{
						property.FindPropertyRelative("rb").objectReferenceValue = ((MonoBehaviour)property.serializedObject.targetObject).gameObject.AddComponent<Rigidbody2D>();
						((Rigidbody2D)(property.FindPropertyRelative("rb").objectReferenceValue)).isKinematic = true;
					}
					GUI.enabled = false;
					EditorGUI.Toggle(column2, "isKinematic", false);
				}
				else{
					((Rigidbody2D)(property.FindPropertyRelative("rb").objectReferenceValue)).isKinematic = EditorGUI.Toggle(column2, "Is Kinematic", ((Rigidbody2D)(property.FindPropertyRelative("rb").objectReferenceValue)).isKinematic);
				}
#endif
			}
			GUI.enabled = true;
		}
	}
}