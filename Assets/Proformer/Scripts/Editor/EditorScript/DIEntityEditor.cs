﻿using UnityEngine;
using System.Collections;
using DI.Proformer.Manager;
using UnityEditor;
using DI.Proformer.ToolHelper;

namespace DI.Proformer {
	[CustomEditor(typeof(DIEntity), true), CanEditMultipleObjects]
	public class DIEntityEditor : Editor
	{
		public static DIEntityEditor instance = null;
		DIEntity pe;
		Path p;
		Collider2D col2d;

		bool isShowInfo;
		bool isShow;
		public bool disableMoveHandler = false;
		protected bool withoutDefault = false;
		protected bool _loaded = false;

		GUIStyle boxStyle;

		public virtual void OnEnable()
		{
			instance = this;
			pe = (DIEntity)target;
			p = pe.GetComponent<Path>();

			if (Application.isPlaying)
				return;
			pe.position.Init(pe.transform);
			Undo.undoRedoPerformed += undoRedoPerformed;

			if (Tools.current == Tool.Move)
				Tools.current = Tool.None;
			if (LevelBounds.instance == null && PrefabUtility.GetPrefabType(pe) != PrefabType.Prefab)
			{
				Debug.LogWarning("Level Bounds Not founded. Auto Create LevelBound");
				GameObject levelBound = new GameObject("LevelBounds");
				levelBound.AddComponent<LevelBounds>();
			}

			if (LevelBounds.instance)
			{
				///Gather all Level Path
				if (LevelBounds.instance.levelPath.Count == 0)
					LevelBounds.instance.GatherLevelPath();

				///Get Index Level Path, if there is any change at Level Bounds
				if (pe.position.levelPathPosition == -1)
				{
					if (LevelBounds.instance.levelPath.Count != 0)
						pe.position.SetLevelPathPosition(0);
				}
				else
				{
					if (pe.position.levelPathPosition >= LevelBounds.instance.levelPath.Count)
					{
						pe.position.SetLevelPathPosition(LevelBounds.instance.levelPath.Count - 1);
					}
				}
			}

		}
		void undoRedoPerformed()
		{
			if (pe == null)
				return;
			pe.transform.position += Vector3.up;
			pe.transform.position -= Vector3.up;
		}
		public virtual void OnDisable()
		{
			if (Application.isPlaying)
				return;
			Undo.undoRedoPerformed -= undoRedoPerformed;
			instance = null;
		}

		public override void OnInspectorGUI()
		{
			if (Event.current.type == EventType.Layout)
				_loaded = true;

			if (_loaded)
			{
				EditorGUILayout.Space();
				CustomInspectorGUI();

				if (!withoutDefault) {
					DrawBaseInspector();
				}
			}
		}

		public void DrawBaseInspector() {
			using (EditorGUILayoutGroup group = new EditorGUILayoutGroup(new GUIContent("Base Inspector"), "Base Inspector"))
			{
				if (group.isExpanding)
					base.OnInspectorGUI();
			}
		}

		public virtual void CustomInspectorGUI()
		{
			ShowTransformInspector();
		}

		protected void ShowTransformInspector()
		{
			if (Application.isPlaying)
			{
				pe = (DIEntity)target;
			}

			EditorGUI.BeginChangeCheck();

			using (EditorGUILayoutGroup group = new EditorGUILayoutGroup(new GUIContent("Position", "Position option is configure Entity Position. There is only 1 option show in here, Level Path Position."), serializedObject.FindProperty("position").isExpanded))
			{
				serializedObject.FindProperty("position").isExpanded = group.isExpanding;
				if (group.isExpanding)
				{
					if (LevelBounds.instance)
					{
						if (pe.position == null)
							pe.position = new DIPosition(0, pe.transform.position);
						if (group.isExpanding)
						{
							EditorGUILayout.BeginHorizontal();
							int lpPos = EditorGUILayout.IntField("Level Path Position", pe.position.levelPathPosition);
							if (GUILayout.Button("-", GUILayout.Width(20)))
							{
								lpPos -= 1;
							}
							if (GUILayout.Button("+", GUILayout.Width(20)))
							{
								lpPos += 1;
							}
							if (lpPos > LevelBounds.instance.levelPath.Count - 1)
								lpPos = LevelBounds.instance.levelPath.Count - 1;
							else if (lpPos < 0)
								lpPos = 0;
							if (pe.position.levelPathPosition != lpPos)
							{
								Undo.RecordObjects(new Object[2] { pe.transform, pe }, "Undo Change Position");
								pe.position.SetLevelPathPosition(lpPos);
								//pe.position.levelPathPosition = lpPos;
								EditorUtility.SetDirty(pe);
								//Vector3 newPos = pe.position.GetPositionFromRaw(pe.position.rawHorizontal, pe.position.rawVertical, lpPos);
								//pe.transform.position = newPos;
								EditorUtility.SetDirty(pe.transform);
							}
							EditorGUILayout.EndHorizontal();
						}

					}
				}

			}

			if (EditorGUI.EndChangeCheck())
			{
				serializedObject.ApplyModifiedProperties();
				EditorUtility.SetDirty(pe);
			}

		}

		public virtual void OnSceneGUI()
		{
			if (Application.isPlaying)
				return;
			if (Selection.gameObjects.Length > 1)
			{
				Tools.current = Tool.Move; return;
			}
			else if (Selection.gameObjects.Length == 1 && Tools.current == Tool.Move)
				Tools.current = Tool.None;

			if (pe == null)
				return;

			///Gather all Level Path
			if (LevelBounds.instance.levelPath.Count == 0)
				LevelBounds.instance.GatherLevelPath();

			if (pe._autoRotateSinceCreation && pe.transform.right != pe.position.levelPath.direction)
			{
				pe.transform.right = pe.position.levelPath.direction;
				//pe.transform.LookAt(new Vector3(pe.position.levelPath.to.x, pe.transform.position.y, pe.position.levelPath.to.z));
				EditorUtility.SetDirty(pe);
			}

			DrawLevelPathName();

			if (p)
			{
				return;
			}


			pe.position.position = pe.transform.position;

			//Find Position depend on raw with current real position
			Vector3 fPosDependOnRaw = pe.position.levelPath.from + pe.position.levelPath.direction * pe.position.rawHorizontal * pe.position.levelPath.length;
			fPosDependOnRaw.y = LevelBounds.instance.bottom + ((LevelBounds.instance.top - LevelBounds.instance.bottom) * pe.position.rawVertical);

			//Check Position if diference with the new one
			if (pe.position.levelPath != null && fPosDependOnRaw != pe.transform.position)
			{
				Undo.RecordObject(pe.transform, "Position");
				pe.transform.position = fPosDependOnRaw;
			}
			else if (pe.position.levelPath != null)
			{

				Handles.color = new Color(Color.red.r, Color.red.g, Color.red.b, .4f);

				Handles.color = new Color(Color.green.r, Color.green.g, Color.green.b, .4f);
				if (!disableMoveHandler && Tools.current == Tool.None)
				{
					#region XYHandler
					Vector3 newPosition = Handles.PositionHandle(pe.transform.position, pe.transform.rotation);
					if (newPosition != pe.transform.position)
					{
						float x = newPosition.x;
						if (pe.position.levelPath.direction.x >= 0)
						{
							if (x > pe.position.levelPath.to.x)
								x = pe.position.levelPath.to.x;
							else if (x < pe.position.levelPath.from.x)
								x = pe.position.levelPath.from.x;

						}
						else if (pe.position.levelPath.direction.x < 0)
						{
							if (x < pe.position.levelPath.to.x)
								x = pe.position.levelPath.to.x;
							else if (x > pe.position.levelPath.from.x)
								x = pe.position.levelPath.from.x;
						}
						float z = newPosition.z;
						if (pe.position.levelPath.direction.z >= 0)
						{
							if (z > pe.position.levelPath.to.z)
								z = pe.position.levelPath.to.z;
							else if (z < pe.position.levelPath.from.z)
								z = pe.position.levelPath.from.z;
						}
						else if (pe.position.levelPath.direction.z < 0)
						{
							if (z < pe.position.levelPath.to.z)
								z = pe.position.levelPath.to.z;
							else if (z > pe.position.levelPath.from.z)
								z = pe.position.levelPath.from.z;
						}

						Undo.RecordObject(pe.transform, "EntityPosition");
						pe.transform.position = pe.position.CalculatePositionAndFixIt(newPosition);
						pe.position.UpdatePosition();
						EditorUtility.SetDirty(pe);
					}
					#endregion
				}
			}



		}

		protected void DrawLevelPathName()
		{
			if (!ProformerSettings.showLevelBoundsInfo)
				return;
			//Draw LevelPath
			if (LevelBounds.instance)
			{
				for (int i = 0; i < LevelBounds.instance.points.Count; i++)
				{
					if (i < LevelBounds.instance.levelPath.Count) {
						//Handles.Label(LevelBounds.instance.points[i] + LevelBounds.instance.levelPath[i].direction * LevelBounds.instance.levelPath[i].length / 3, "Level Path : " + i.ToString(), EditorStyles.helpBox);
						Handles.BeginGUI();

						Vector3 worldPos = LevelBounds.instance.points[i] + LevelBounds.instance.levelPath[i].direction * LevelBounds.instance.levelPath[i].length / 3;
						worldPos.y = LevelBounds.instance.top;
						Vector2 worldToCam = Camera.current.WorldToScreenPoint(worldPos); worldToCam.y = Camera.current.pixelHeight - worldToCam.y;
						if (GUI.Button(new Rect(worldToCam, new Vector2(100, 20)), "Level Path : " + i.ToString())) {
							Undo.RecordObjects(new Object[2] { pe.transform, pe }, "Undo Change Position");
							pe.position.SetLevelPathPosition(i);
							//pe.position.levelPathPosition = lpPos;
							EditorUtility.SetDirty(pe);
							//Vector3 newPos = pe.position.GetPositionFromRaw(pe.position.rawHorizontal, pe.position.rawVertical, lpPos);
							//pe.transform.position = newPos;
							EditorUtility.SetDirty(pe.transform);
							//LevelBounds.instance.defaultLevelPathIndex = i;
							//EditorUtility.SetDirty(LevelBounds.instance);
						}
						Handles.EndGUI();
					}

				}
			}
		}
	}
}
