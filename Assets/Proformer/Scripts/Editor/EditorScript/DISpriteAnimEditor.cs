﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using UnityEditorInternal;
using System.Collections.Generic;

namespace DI.Proformer {
	[CustomEditor(typeof(DISpriteAnim))]
	public class DISpriteAnimEditor : Editor
	{

		DISpriteAnim anim;
		bool isPlaying;
		double nextTimeFrame;
		List<Sprite> spriteList = new List<Sprite>();
		ReorderableList sprites;

		void OnEnable()
		{
			anim = (DISpriteAnim)target;

			EditorApplication.update += update;
			sprites = new ReorderableList(serializedObject, serializedObject.FindProperty("sprites"), true, true, true, true);
			sprites.drawHeaderCallback = ((Rect rect) => {
				EditorGUI.LabelField(rect, "Sprite List");
			});
			sprites.drawElementCallback = ((Rect rect, int index, bool isActive, bool isFocused) => {
				EditorGUI.PropertyField(new Rect(rect.x, rect.y, rect.width, EditorGUIUtility.singleLineHeight), serializedObject.FindProperty("sprites").GetArrayElementAtIndex(index));
			});
		}

		void OnDisable()
		{
			EditorApplication.update -= update;
		}

		private void update()
		{
			if (Application.isPlaying)
				return;
			if (isPlaying)
				Animate();
		}

		public override void OnInspectorGUI()
		{
			EditorGUI.BeginChangeCheck();
			EditorPrefs.SetInt("DISpriteAnim-Toolbar", GUILayout.Toolbar(EditorPrefs.GetInt("DISpriteAnim-Toolbar", 0), new string[2] { "Sprites", "Settings" }));
			if (EditorPrefs.GetInt("DISpriteAnim-Toolbar", 0) == 0)
				sprites.DoLayoutList();
			else
				base.OnInspectorGUI();
			if (Application.isPlaying)
				return;
			if (GUILayout.Button(isPlaying ? "Stop" : "Play Test"))
			{
				isPlaying = !isPlaying;
				if (isPlaying)
				{
					nextTimeFrame = EditorApplication.timeSinceStartup + anim.frameTime;
					anim.sr.sprite = anim.sprites[0];
				}
				spriteList = new List<Sprite>(anim.sprites);
			}

			if (EditorGUI.EndChangeCheck())
			{
				serializedObject.ApplyModifiedProperties();
				EditorUtility.SetDirty(anim);
			}
		}

		void Animate()
		{
			if (EditorApplication.timeSinceStartup >= nextTimeFrame)
			{
				int currentIndex = spriteList.IndexOf(anim.sr.sprite);
				currentIndex += 1;
				if (currentIndex >= spriteList.Count)
				{
					if (anim.playMode == DISpriteAnim.PlayMode.Looping)
						currentIndex = 0;
					else if (anim.playMode == DISpriteAnim.PlayMode.PingPong)
					{
						spriteList.Reverse();
						if (spriteList.Count > 1)
							currentIndex = 1;
						else
							currentIndex = 0;
					}
					else
					{
						isPlaying = false;
						currentIndex = 0;
						return;
					}
				}
				anim.sr.sprite = spriteList[currentIndex];
				nextTimeFrame = EditorApplication.timeSinceStartup + anim.frameTime;
			}
		}
	}
}
