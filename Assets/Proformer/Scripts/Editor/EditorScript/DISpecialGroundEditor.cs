﻿using UnityEngine;
using System.Collections;
using UnityEditor;

namespace DI.Proformer {
	[CustomEditor(typeof(DISpecialGround)), CanEditMultipleObjects]
	public class DISpecialGroundEditor : Editor
	{
		DISpecialGround ground;

		void OnEnable()
		{
			ground = (DISpecialGround)target;
		}

		public override void OnInspectorGUI()
		{
			EditorGUI.BeginChangeCheck();
			EditorGUILayout.HelpBox("Special Ground is only affected to Controller (such Player). Other Type is not affect", MessageType.None);
			DrawDefaultInspector();
			if (ground.modifier == DISpecialGround.Operator.Scale)
			{
				if (ground.modifierType == DISpecialGround.ModifyType.Right_Velocity || ground.modifierType == DISpecialGround.ModifyType.Left_Velocity)
					ground.modifierType = DISpecialGround.ModifyType.FollowInput_Velocity;
				int choosenType = EditorGUILayout.Popup("Modifier Type", ground.modifierType == DISpecialGround.ModifyType.FollowInput_Velocity ? 0 : 1, new string[2] { "FollowInput_Velocity", "Acceleration Modify" });
				if (choosenType == 0)
					ground.modifierType = DISpecialGround.ModifyType.FollowInput_Velocity;
				else
					ground.modifierType = DISpecialGround.ModifyType.AccelerationModify;
			}
			else
				EditorGUILayout.PropertyField(serializedObject.FindProperty("modifierType"));

			if (EditorGUI.EndChangeCheck())
			{
				serializedObject.ApplyModifiedProperties();
				EditorUtility.SetDirty(ground);
			}
		}
	}
}
