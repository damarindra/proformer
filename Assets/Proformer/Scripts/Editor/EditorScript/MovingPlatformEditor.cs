﻿using UnityEngine;
using System.Collections;
using UnityEditor;

namespace DI.Proformer {

	[CustomEditor(typeof(MovingPlatform), true), CanEditMultipleObjects]
	public class MovingPlatformEditor : DIObjectEditor
	{

		MovingPlatform m;

		public override void OnEnable()
		{
			base.OnEnable();
			m = (MovingPlatform)target;
			withoutDefault = true;
		}

		public override void CustomInspectorGUI()
		{
			base.CustomInspectorGUI();
			EditorGUI.BeginChangeCheck();

			EditorGUILayout.Space();
			using (EditorGUILayoutGroup group = new EditorGUILayoutGroup(new GUIContent("Moving Platform", "Basic option Moving platform"), serializedObject.FindProperty("passengerMask").isExpanded))
			{
				serializedObject.FindProperty("passengerMask").isExpanded = group.isExpanding;
				if (group.isExpanding)
				{
					EditorGUILayout.PropertyField(serializedObject.FindProperty("passengerMask"));
					EditorGUILayout.PropertyField(serializedObject.FindProperty("specificPassenger"));
				}
			}

			if (EditorGUI.EndChangeCheck())
			{
				serializedObject.ApplyModifiedProperties();
				EditorUtility.SetDirty(m);
			}
		}

		public override void DrawColliderInspector()
		{
			EditorGUILayout.Space();
			EditorGUI.BeginChangeCheck();

			using (EditorGUILayoutGroup group = new EditorGUILayoutGroup(new GUIContent("Collider", "Basic option collider"), serializedObject.FindProperty("_bc").isExpanded))
			{
				serializedObject.FindProperty("_bc").isExpanded = group.isExpanding;
				if (group.isExpanding)
				{
					serializedObject.FindProperty("_bc").objectReferenceValue = EditorGUILayout.ObjectField("Collider",
#if PROFORMER3D
							serializedObject.FindProperty("_bc").objectReferenceValue, typeof(Collider),
#else
							serializedObject.FindProperty("_bc").objectReferenceValue, typeof(Collider2D),
#endif
							true);
#if PROFORMER3D
					if (!serializedObject.FindProperty("_bc").objectReferenceValue)
#else
					if (!serializedObject.FindProperty("_bc").objectReferenceValue)
#endif
					{
						if (GUILayout.Button("Create Body (Box Collider)"))
						{
#if PROFORMER3D
							BoxCollider bc = null;
							if (m.rendererObject != null)
							{
								bc = m.rendererObject.AddComponent<BoxCollider>();
							}
#else
							BoxCollider2D bc = null;
							if (m.rendererObject != null) {
								bc = m.rendererObject.AddComponent<BoxCollider2D>();
							}
#endif
							GameObject bodyCollider = new GameObject("Body");
							bodyCollider.transform.parent = m.transform;
							bodyCollider.transform.localPosition = Vector3.zero;
							bodyCollider.layer = bodyCollider.transform.parent.gameObject.layer;
#if PROFORMER3D
							BoxCollider boxCollider = bodyCollider.AddComponent<BoxCollider>();
#else
							BoxCollider2D boxCollider = bodyCollider.AddComponent<BoxCollider2D>();
#endif
							serializedObject.FindProperty("_bc").objectReferenceValue = boxCollider;
							if (bc != null)
							{
								boxCollider.size = bc.size;
#if PROFORMER3D
								boxCollider.center = bc.center;
#else
								boxCollider.offset = bc.offset;
#endif
								DestroyImmediate(bc);
							}
						}
					}
				}
			}

			if (EditorGUI.EndChangeCheck())
			{
				serializedObject.ApplyModifiedProperties();
				EditorUtility.SetDirty(m);
			}
		}
	}

}
