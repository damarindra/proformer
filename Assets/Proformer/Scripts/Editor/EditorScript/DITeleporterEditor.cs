﻿using UnityEngine;
using UnityEditor;

namespace DI.Proformer {
	[CustomEditor(typeof(DITeleporter))]
	public class DITeleporterEditor : DIObjectEditor
	{

		protected int choosen;

		public override void OnEnable()
		{
			((DITeleporter)target)._autoRotateSinceCreation = false;
			((DITeleporter)target)._colliderOnChildren = false;
			base.OnEnable();
		}

		public override void CustomInspectorGUI()
		{
			EditorGUI.BeginChangeCheck();
			serializedObject.Update();

			EditorPrefs.SetInt("ProformerTeleporterToolbar", GUILayout.Toolbar(EditorPrefs.GetInt("ProformerTeleporterToolbar", 0), new string[2] { "Setup", target.name }));
			choosen = EditorPrefs.GetInt("ProformerTeleporterToolbar", 0);
			if (choosen == 0)
				base.CustomInspectorGUI();
			else if (choosen == 1)
			{
				EditorGUILayout.PropertyField(serializedObject.FindProperty("destination"));
				EditorGUILayout.PropertyField(serializedObject.FindProperty("targetTag"));
				EditorGUILayout.PropertyField(serializedObject.FindProperty("executeMode"));
			}

			if (EditorGUI.EndChangeCheck())
			{
				serializedObject.ApplyModifiedProperties();
				EditorUtility.SetDirty((DITeleporter)target);
			}
		}
	}
}
