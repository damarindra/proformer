﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using UnityEditorInternal;
using System.Linq;
using System.Collections.Generic;
using DI.PoolingSystem;
using System;

namespace DI.Proformer.ToolHelper {
	[CustomEditor(typeof(GameData))]
	public class GameDataEditor : Editor {
		public GameData gameData;

		ReorderableList gameScreens, backgroundMusics, poolCandidate;

		void OnEnable() {
			gameData = (GameData)target;
			gameData.UpdateScenes();
			gameData.UpdateGameScreenType();
			gameData.UpdateBGM();

			if (gameData.sfxTemplate == null) {
				gameData.sfxTemplate = AssetDatabase.LoadAssetAtPath("Assets/Proformer/Resources/Template/SoundFX.prefab", typeof(GameObject)) as GameObject;
			}
			if (gameData.objectWillBePooled.Count == 0) {
				gameData.objectWillBePooled.Add(new PoolingSystem.PoolingManager.PoolCandidate());
				gameData.objectWillBePooled[0].go = gameData.sfxTemplate;
				gameData.objectWillBePooled[0].size = 15;
			}

			gameScreens = new ReorderableList(serializedObject, serializedObject.FindProperty("gameScreen"), false, true, false, false);
			gameScreens.drawHeaderCallback = (Rect rect) =>
			{
				EditorGUI.LabelField(new Rect(rect.x, rect.y, rect.width / 2, EditorGUIUtility.singleLineHeight), "Scene");
				EditorGUI.LabelField(new Rect(rect.x + rect.width / 2, rect.y, rect.width / 2, EditorGUIUtility.singleLineHeight), "Screen Type");
			};
			gameScreens.drawElementCallback =
				(Rect rect, int index, bool isActive, bool isFocused) =>
				{
					float width = rect.width / 2;
					EditorGUI.LabelField(new Rect(rect.x, rect.y, width, EditorGUIUtility.singleLineHeight), gameData.gameScreen[index].scene);
					gameData.gameScreen[index].gameScreenType = (GameData.GameScreenType)EditorGUI.EnumPopup(new Rect(rect.x + width, rect.y, width, EditorGUIUtility.singleLineHeight), gameData.gameScreen[index].gameScreenType);
					
				};

			backgroundMusics = new ReorderableList(serializedObject, serializedObject.FindProperty("backgroundMusics"), false, true, false, false);
			backgroundMusics.drawHeaderCallback = (Rect rect) =>
			{
				EditorGUI.LabelField(new Rect(rect.x, rect.y, rect.width / 2, EditorGUIUtility.singleLineHeight), "Scene");
				EditorGUI.LabelField(new Rect(rect.x + rect.width / 2, rect.y, rect.width / 2, EditorGUIUtility.singleLineHeight), "Clip");
			};
			backgroundMusics.drawElementCallback =
				(Rect rect, int index, bool isActive, bool isFocused) =>
				{
					float width = rect.width / 2;
					EditorGUI.LabelField(new Rect(rect.x, rect.y, width, EditorGUIUtility.singleLineHeight), gameData.backgroundMusics[index].scene);
					gameData.backgroundMusics[index].backgroundMusic = EditorGUI.ObjectField(new Rect(rect.x + width, rect.y, width, EditorGUIUtility.singleLineHeight), gameData.backgroundMusics[index].backgroundMusic, typeof(AudioClip), false) as AudioClip;
				};

			poolCandidate = new ReorderableList(serializedObject, serializedObject.FindProperty("objectWillBePooled"), false, true, true, true);
			poolCandidate.drawHeaderCallback = (Rect rect) =>
			{
				EditorGUI.LabelField(new Rect(rect.x + rect.width * .05f, rect.y, rect.width / 2, EditorGUIUtility.singleLineHeight), "Pooling Object");
				EditorGUI.LabelField(new Rect(rect.x + rect.width * .7f, rect.y, rect.width * .3f, EditorGUIUtility.singleLineHeight), "Size");
			};
			poolCandidate.drawElementCallback =
				(Rect rect, int index, bool isActive, bool isFocused) => {
					var element = poolCandidate.serializedProperty.GetArrayElementAtIndex(index);
					if (index == 0)
						GUI.enabled = false;
					gameData.objectWillBePooled[index].go = EditorGUI.ObjectField(new Rect(rect.x + rect.width * .05f, rect.y, rect.width * .65f, EditorGUIUtility.singleLineHeight), element.FindPropertyRelative("go").objectReferenceValue, typeof(GameObject), false) as GameObject;
					gameData.objectWillBePooled[index].size = EditorGUI.IntField(new Rect(rect.x + rect.width * .7f, rect.y, rect.width * .3f, EditorGUIUtility.singleLineHeight), element.FindPropertyRelative("size").intValue);
					
					if (index == 0)
						GUI.enabled = true;
				};
			poolCandidate.onAddCallback = (ReorderableList list) => {
				gameData.objectWillBePooled.Add(new PoolingSystem.PoolingManager.PoolCandidate());
			};
			
		}
		

		public override void OnInspectorGUI()
		{
			//base.OnInspectorGUI();
			EditorGUI.BeginChangeCheck();
			serializedObject.Update();
			EditorPrefs.SetBool("Check Point Type Fold",
			EditorGUILayout.Foldout(EditorPrefs.GetBool("Check Point Type Fold"), "Check Point Type"));
			if (EditorPrefs.GetBool("Check Point Type Fold"))
			{
				gameData.checkPointType = (GameData.CheckPointType)EditorGUILayout.EnumPopup("Check Point Type", gameData.checkPointType);
				EditorGUILayout.Space();
			}
			EditorGUILayout.BeginHorizontal();
			EditorPrefs.SetBool("Game Screen Type Fold",
			EditorGUILayout.Foldout(EditorPrefs.GetBool("Game Screen Type Fold"), "Game Scene Type"));
			if (EditorPrefs.GetBool("Game Screen Type Fold"))
			{
				EditorGUILayout.EndHorizontal();
				gameScreens.DoLayoutList();

				EditorGUILayout.Space();
			} else
				EditorGUILayout.EndHorizontal();
			EditorGUILayout.BeginHorizontal();
			EditorPrefs.SetBool("BGM Fold",
			EditorGUILayout.Foldout(EditorPrefs.GetBool("BGM Fold"), "BGM"));
			if (EditorPrefs.GetBool("BGM Fold"))
			{
				EditorGUILayout.EndHorizontal();
				backgroundMusics.DoLayoutList();
				EditorGUILayout.Space();
			} else
				EditorGUILayout.EndHorizontal();

			
			EditorPrefs.SetBool("Pooling Object Fold",
			EditorGUILayout.Foldout(EditorPrefs.GetBool("Pooling Object Fold"), "Pooling Object"));
			if (EditorPrefs.GetBool("Pooling Object Fold"))
			{
				poolCandidate.DoLayoutList();
				EditorGUILayout.HelpBox("Pooling Object Will be create at the Game Manager run. Pooling Object will be keep until the game closed\nObject must be instancing interface IPoolObject", MessageType.Info);
				EditorGUILayout.Space();
			}
			EditorPrefs.SetBool("Loading Preference Fold", 
			EditorGUILayout.Foldout(EditorPrefs.GetBool("Loading Preference Fold"), "Loading Preference"));
			if (EditorPrefs.GetBool("Loading Preference Fold")) {
				EditorGUI.indentLevel += 1;
				gameData.fadeDuration = EditorGUILayout.FloatField("Fade Transition Duration", gameData.fadeDuration);
				gameData.coroutineLoad = EditorGUILayout.Toggle("Load Level Coroutine", gameData.coroutineLoad);
				EditorGUI.indentLevel -= 1;
			}

			if (EditorGUI.EndChangeCheck()) {
				serializedObject.ApplyModifiedProperties();
				EditorUtility.SetDirty(gameData);
			}
		}
	}
}
