﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using DI.Proformer.ToolHelper;
using UnityEditorInternal;
using DI.Proformer.Manager;
using System.Reflection;
using System;
using System.Linq;
using System.Collections.Generic;

namespace DI.Proformer {

	[CustomEditor(typeof(Path)), CanEditMultipleObjects]
	public class PathEditor : Editor
	{

		public Path path;

		private ReorderableList pathRL;
		private DIEntity entity;
		bool setDirty = false;
		int lockedIndexPathHandle = -1;

		public void OnEnable()
		{
			if (Application.isPlaying)
				return;


			path = (Path)target;
			entity = path.GetComponent<DIEntity>();

			if (LevelBounds.instance == null && PrefabUtility.GetPrefabType(path) != PrefabType.Prefab)
			{
				Debug.LogWarning("Level Bounds Not founded. Auto Create LevelBound");
				GameObject levelBound = new GameObject("LevelBounds");
				levelBound.AddComponent<LevelBounds>();
			}
			if (LevelBounds.instance)
				LevelBounds.instance.GatherLevelPath();
			else
			{
				return;
			}
			Hidden = true;

			if (path.pathList.Count == 0)
			{
				path.pathList.Add(new DIPosition());
				path.pathList[0] = entity.position;
				//path.pathList[0].position = path.pathList[0].GetPositionFromRaw(.5f, .5f, 0);
			}

			pathRL = new ReorderableList(serializedObject, serializedObject.FindProperty("pathList"), true, true, true, true);
			pathRL.drawHeaderCallback = ((Rect rect) => {
				EditorGUI.LabelField(rect, "Path");
			});
			pathRL.drawElementCallback = ((Rect rect, int index, bool isActive, bool isFocused) => {
				DIPosition diPos = path.pathList[index];
				EditorGUI.LabelField(new Rect(rect.x, rect.y, 50, rect.height), "v " + index.ToString());
				diPos.position = EditorGUI.Vector3Field(new Rect(rect.x + 50, rect.y, rect.width - 50, rect.height), "", diPos.position);
			});
			pathRL.onSelectCallback = ((ReorderableList list) => {
				EditorPrefs.SetInt("Selected index path" + path.gameObject.GetInstanceID().ToString(), pathRL.index);
			});
			pathRL.onAddCallback = ((ReorderableList list) => {
				//Undo.RecordObject(path, "Undo Add");
				path.pathList.Add(new DIPosition(entity.position.levelPathPosition, path.pathList.Count == 0 ? entity.transform.position :
					path.pathList.Count == 1 ? path.pathList[0].position + path.pathList[0].levelPath.direction : path.pathList[path.pathList.Count - 1].position + (path.pathList[path.pathList.Count - 1].position - path.pathList[path.pathList.Count - 2].position).normalized));
				SceneView.RepaintAll();
			});
			pathRL.onRemoveCallback = ((ReorderableList list) => {
				//Undo.RecordObject(path, "Undo Remove");
				if (EditorPrefs.GetInt("Selected index path" + path.gameObject.GetInstanceID().ToString()) != 0)
					path.pathList.RemoveAt(EditorPrefs.GetInt("Selected index path" + path.gameObject.GetInstanceID().ToString()));
				SceneView.RepaintAll();
			});
		}
		public void OnDisable()
		{
			Hidden = false;

		}

		public override void OnInspectorGUI()
		{
			if (Application.isPlaying)
			{
				base.DrawDefaultInspector();
				return;
			}
			EditorGUILayout.Space();

			if (LevelBounds.instance)
			{
				if (LevelBounds.instance.levelPath.Count == 0)
				{
					LevelBounds.instance.GatherLevelPath();
				}
			}
			if (!entity)
				entity = path.GetComponent<DIEntity>();
			if (entity)
			{
				foreach (DIPosition diPos in path.pathList)
				{
					if (diPos.levelPathPosition != entity.position.levelPathPosition)
					{
						if(path.pathList.IndexOf(diPos) == 0)
							diPos.position = entity.position.position;
						else
							diPos.SetLevelPathPosition(entity.position.levelPathPosition);
						setDirty = true;
					}
				}
			}

			serializedObject.Update();
			EditorGUI.BeginChangeCheck();
			path.connetLastPoint = EditorGUILayout.Toggle("Connect Last Point", path.connetLastPoint);

			EditorGUILayout.BeginHorizontal();
			EditorGUILayout.EndHorizontal();
			if (LevelBounds.instance)
				pathRL.DoLayoutList();
			else
				EditorGUILayout.HelpBox("Hiding in prefabs mode", MessageType.Info);

			if (EditorGUI.EndChangeCheck() || setDirty)
			{
				serializedObject.ApplyModifiedProperties();
				EditorUtility.SetDirty(path);
				setDirty = false;
			}
		}

		public void DrawPath(bool withHandle)
		{
			Handles.color = Color.blue;
			for (int i = 0; i < path.pathList.Count; i++)
			{
				if (withHandle)
					Handles.SphereHandleCap(0, path.pathList[i].position, Quaternion.identity, .35f, EventType.Repaint);
				if (i == path.pathList.Count - 1 && path.connetLastPoint)
				{
					Handles.DrawLine(path.pathList[i].position, path.pathList[0].position);
					Vector3 dir = (path.pathList[0].position - path.pathList[i].position).normalized;
					if (dir != Vector3.zero)
						Handles.ArrowHandleCap(0, path.pathList[i].position, Quaternion.LookRotation(dir), 1f, EventType.Repaint);
				}
				else if (i < path.pathList.Count - 1)
				{
					Handles.DrawLine(path.pathList[i].position, path.pathList[i + 1].position);
					Vector3 dir = (path.pathList[i + 1].position - path.pathList[i].position).normalized;
					if (dir != Vector3.zero)
						Handles.ArrowHandleCap(0, path.pathList[i].position, Quaternion.LookRotation(dir), 1f, EventType.Repaint);
				}
			}

			if (Event.current.type == EventType.MouseUp && Event.current.button == 0 && lockedIndexPathHandle != -1)
			{
				lockedIndexPathHandle = -1;
			}

			List<float> distance = new List<float>();
			//evaluate the nearest one
			for (int i = 0; i < path.pathList.Count; i++)
			{
				Vector2 currentPathViewportPoint = Camera.current.WorldToScreenPoint(path.pathList[i].position);
				Vector2 mousePositionViewportPoint = new Vector2(Event.current.mousePosition.x, Camera.current.pixelHeight - Event.current.mousePosition.y);
				distance.Add(Vector3.Distance(currentPathViewportPoint, mousePositionViewportPoint));
			}
			int indexMin = distance.IndexOf(distance.Min());

			int iPath = 0;
			while (iPath < path.pathList.Count)
			{
				if (iPath >= path.pathList.Count)
				{
					iPath += 1;
					continue;
				}
				Vector3 currentPath = path.pathList[iPath].position;

				Handles.color = Color.green;
				Vector3 newPathPos = currentPath;
				if (indexMin == iPath && distance[indexMin] < 75 && lockedIndexPathHandle == -1)
				{
					if (Event.current.type == EventType.MouseDown && Event.current.button == 0)
					{
						lockedIndexPathHandle = iPath;
					}
					newPathPos = Handles.PositionHandle(currentPath, path.transform.rotation);
					SceneView.RepaintAll();
				}
				else if (lockedIndexPathHandle != -1 && lockedIndexPathHandle == iPath) {
					newPathPos = Handles.PositionHandle(currentPath, path.transform.rotation);
				}
				else
				{
					newPathPos = Handles.FreeMoveHandle(currentPath, Quaternion.identity, HandleUtility.GetHandleSize(currentPath) * .2f, InternalTool.GetSnapValue(), Handles.SphereHandleCap);
				}

				if (currentPath != newPathPos)
				{

					if (iPath != 0)
					{
						Undo.RecordObject(path, "Move");
						path.pathList[iPath].SetPositionAndFixIt(newPathPos);
					}
					else
					{
						Undo.RecordObject(path.transform, "Move");
						path.transform.position = path.pathList[iPath].CalculatePositionAndFixIt(newPathPos);
						path.pathList[iPath].UpdatePosition();
					}
					SceneView.RepaintAll();
					return;
				}
				iPath++;
			}
		}

		public void OnSceneGUI()
		{
			if (Application.isPlaying)
				return;
			bool isMoveHandleActive = true;
			if (entity != null && DIEntityEditor.instance != null)
			{
				if (DIEntityEditor.instance.disableMoveHandler)
					isMoveHandleActive = false;
			}
			if(isMoveHandleActive)
				DrawPath(false);
		}

		public static bool Hidden
		{
			get
			{
				return Tools.current == Tool.None;
			}
			set
			{
				Tools.current = value ? Tool.Move : Tool.None;
			}
		}
	}
}