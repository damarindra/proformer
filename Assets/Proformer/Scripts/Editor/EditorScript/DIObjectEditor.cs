﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using DI.Proformer.ToolHelper;

namespace DI.Proformer {
	[CustomEditor(typeof(DIObject), true), CanEditMultipleObjects]
	public class DIObjectEditor : DIEntityEditor
	{

		DIObject pObject;
		bool moveRenderer = false;
		protected IsTriggerType isTriggerType = IsTriggerType.Chooseable;

		public override void OnEnable()
		{
			pObject = (DIObject)target;
			base.OnEnable();
		}

		public override void CustomInspectorGUI()
		{
			EditorGUI.BeginChangeCheck();
			base.CustomInspectorGUI();
			DrawRendererInspector();
			DrawColliderInspector();

			if (EditorGUI.EndChangeCheck())
			{
				serializedObject.ApplyModifiedProperties();
				EditorUtility.SetDirty(pObject);
			}

			/*
			if (Selection.gameObjects.Length > 1) {
				DIObject[] obs = (DIObject[])targets;
				for (int i = 0; i < obs.Length; i++)
					((DIObject)targets[i]) = pObject;
			}*/
		}

		protected float GetHandleSize()
		{
			return HandleUtility.GetHandleSize(pObject.transform.position);
		}

		public virtual void DrawRendererInspector()
		{
			EditorGUI.BeginChangeCheck();
			serializedObject.Update();
			using (EditorGUILayoutGroup group = new EditorGUILayoutGroup(new GUIContent("Renderer", "Show basic Renderer option."), serializedObject.FindProperty("rendererObject").isExpanded))
			{
				serializedObject.FindProperty("rendererObject").isExpanded = group.isExpanding;
				if (group.isExpanding)
				{
					EditorGUILayout.BeginHorizontal();
					if (pObject.renderer)
						GUI.enabled = false;
					EditorGUILayout.PropertyField(serializedObject.FindProperty("rendererObject"));
					GUI.enabled = true;
					if (pObject.renderer != null && pObject.rendererObject != null)
					{
						if (GUILayout.Button("Remove", GUILayout.Width(60)))
						{
							Undo.DestroyObjectImmediate(pObject.rendererObject);
							return;
						}
					}
					else if (GUILayout.Button("Apply", GUILayout.Width(60)))
					{
						GameObject renderer = Instantiate(pObject.rendererObject, pObject.transform.position, Quaternion.identity) as GameObject;
						renderer.name = "Renderer";
						renderer.transform.parent = pObject.transform;
						pObject.rendererObject = renderer;
						renderer.layer = pObject.gameObject.layer;
						renderer.tag = pObject.gameObject.tag;
						Undo.RegisterCreatedObjectUndo(renderer, "Renderer");
					}
					EditorGUILayout.EndHorizontal();
					GUI.enabled = false;
					EditorGUILayout.PropertyField(serializedObject.FindProperty("renderer"));
					GUI.enabled = true;

					if (pObject.renderer != null)
					{
#if !PROFORMER3D
						Renderer renderer = pObject.rendererObject.GetComponent<Renderer>();
						if (renderer == null)
							renderer = pObject.rendererObject.GetComponentInChildren<Renderer>();
						if (renderer)
						{
							if (renderer.sortingLayerName == "Default")
								renderer.sortingLayerName = "Player";
							EditorGUI.indentLevel += 1;
							using (EditorGUILayoutGroup groupRenderer = new EditorGUILayoutGroup(new GUIContent("Renderer Opt"), serializedObject.FindProperty("renderer").isExpanded)) {
								serializedObject.FindProperty("renderer").isExpanded = groupRenderer.isExpanding;
								if (groupRenderer.isExpanding) {
									Editor rendererEditor = Editor.CreateEditor(renderer);
									rendererEditor.OnInspectorGUI();
								}
							}
							EditorGUI.indentLevel -= 1;
							/*Undo.RecordObject(pObject.rendererObject.GetComponent<Renderer>(), "Renderer");
							var sortingLayers = InternalTool.GetSortingLayerNames();
							renderer.sortingLayerName = sortingLayers[EditorGUILayout.Popup("Sorting Layer", InternalTool.GetSortingLayerIndex(renderer.sortingLayerName), sortingLayers)];
							renderer.sortingOrder = EditorGUILayout.IntField("Order in Layer", renderer.sortingOrder);
							*/
						}
#endif
						EditorGUI.indentLevel += 1;

						using (EditorGUILayoutGroup groupTransform = new EditorGUILayoutGroup(new GUIContent("Transform"), "TransformRendererFold")) {
							if (groupTransform.isExpanding) {
								Editor transformRenderer = Editor.CreateEditor(pObject.renderer.transform);
								transformRenderer.OnInspectorGUI();
								if (GUILayout.Button(moveRenderer ? "Done" : "Move"))
								{
									moveRenderer = !moveRenderer;
									disableMoveHandler = moveRenderer ? true : false;
								}
							}
						}
						EditorGUI.indentLevel -= 1;
						/*
						EditorGUILayout.BeginHorizontal();
						Undo.RecordObject(pObject.rendererObject.transform, "Local Position");
						pObject.rendererObject.transform.localPosition = EditorGUILayout.Vector3Field("Position", pObject.rendererObject.transform.localPosition);
						if (GUILayout.Button(moveRenderer ? "Done" : "Move", GUILayout.Width(60)))
						{
							moveRenderer = !moveRenderer;
							disableMoveHandler = moveRenderer ? true : false;
						}
						EditorGUILayout.EndHorizontal();
						Undo.RecordObject(pObject.rendererObject.transform, "Local Rotation");
						pObject.rendererObject.transform.localEulerAngles = EditorGUILayout.Vector3Field("Rotation", pObject.rendererObject.transform.localEulerAngles);
						Undo.RecordObject(pObject.rendererObject.transform, "Local Scale");
						pObject.rendererObject.transform.localScale = EditorGUILayout.Vector3Field("Scale", pObject.rendererObject.transform.localScale);*/
					}

				}
				if (EditorGUI.EndChangeCheck())
				{
					if (Selection.gameObjects.Length > 1)
					{
						for (int i = 0; i < targets.Length; i++)
						{
							((DIObject)targets[i]).rendererObject.GetComponentInChildren<Renderer>().sortingLayerName = pObject.renderer.sortingLayerName;
						}
					}
					serializedObject.ApplyModifiedProperties();
					EditorUtility.SetDirty(pObject);
				}
			}
		}

		public virtual void DrawColliderInspector()
		{
			if (pObject._collider != null)
			{
				EditorInspectorHelper.DrawColliderInspector(ref pObject._collider, serializedObject.FindProperty("_collider"), isTriggerType);
			}
			else {
				using (EditorGUILayoutGroup group = new EditorGUILayoutGroup(new GUIContent("Collider"), serializedObject.FindProperty("_collider").isExpanded))
				{
					serializedObject.FindProperty("_collider").isExpanded = group.isExpanding;
					if (group.isExpanding)
					{
						
						Undo.RecordObject(pObject, "Collider");
	#if PROFORMER3D
						pObject._collider = EditorGUILayout.ObjectField(pObject._collider, typeof(Collider), true) as Collider;
	#else
						pObject._collider = EditorGUILayout.ObjectField(pObject._collider, typeof(Collider2D), true) as Collider2D;
	#endif
						if (!pObject._collider)
						{
							pObject._collider = EditorInspectorHelper.PopUpColliderCreation(pObject.gameObject, pObject._colliderOnChildren);
						}


					}
				}
			
			}

		}

		public override void OnSceneGUI()
		{
			base.OnSceneGUI();
			if (moveRenderer)
			{
				Vector3 worldPosition = Handles.PositionHandle(pObject.rendererObject.transform.position, Quaternion.identity);
				if (worldPosition != pObject.rendererObject.transform.position)
				{
					Undo.RecordObject(pObject.rendererObject.transform, "Undo Move Renderer");
					pObject.rendererObject.transform.position = worldPosition;
				}
			}
		}
	}
}