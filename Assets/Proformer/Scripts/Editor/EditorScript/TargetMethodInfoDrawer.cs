﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using DI.Proformer.ToolHelper;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace DI.Proformer {

	[CustomPropertyDrawer(typeof(TargetMethodInfo))]
	public class TargetMethodInfoDrawer : DIBoxDrawer
	{

		protected Rect titleRect;
		protected Rect gObjectRect;
		protected Rect scriptRect;
		protected Rect methodRect;

		protected SerializedProperty property;

		public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
		{
			this.property = property;
			titleRect = new Rect(position.x, position.y, position.width, EditorGUIUtility.singleLineHeight);
			gObjectRect = new Rect(position.x, titleRect.y + EditorGUIUtility.singleLineHeight * 1.1f, position.width, EditorGUIUtility.singleLineHeight);
			scriptRect = new Rect(position.x, gObjectRect.y + EditorGUIUtility.singleLineHeight * 1.1f, position.width, EditorGUIUtility.singleLineHeight);
			methodRect = new Rect(position.x, scriptRect.y + EditorGUIUtility.singleLineHeight * 1.1f, position.width, EditorGUIUtility.singleLineHeight);

			spaceBottom = EditorGUIUtility.standardVerticalSpacing;
			totalRect = new Rect(titleRect.x, titleRect.y, titleRect.width, methodRect.y - titleRect.y + methodRect.height + EditorGUIUtility.standardVerticalSpacing);
			Draw(property, totalRect, true, true, true);
			if (property.isExpanded)
				ShowValues(property);
		}

		protected virtual void ShowValues(SerializedProperty value)
		{
			GameObject oldGO = value.FindPropertyRelative("target").objectReferenceValue as GameObject;
			EditorGUI.PropertyField(gObjectRect, value.FindPropertyRelative("target"));
			if (value.FindPropertyRelative("target").objectReferenceValue as GameObject != oldGO)
				value.FindPropertyRelative("monoScript").objectReferenceValue = null;

			if (value.FindPropertyRelative("target").objectReferenceValue != null)
			{
				//SCRIPT INFO
				MonoBehaviour oldMono = value.FindPropertyRelative("monoScript").objectReferenceValue as MonoBehaviour;
				List<MonoBehaviour> monoBehaviours = ((GameObject)value.FindPropertyRelative("target").objectReferenceValue).GetComponents<MonoBehaviour>().ToList();
				if (monoBehaviours.Count > 0)
				{
					if (value.FindPropertyRelative("monoScript").objectReferenceValue as MonoBehaviour == null && monoBehaviours.Count != 0)
						value.FindPropertyRelative("monoScript").objectReferenceValue = monoBehaviours[0];
					int iMonoScript = monoBehaviours.IndexOf(value.FindPropertyRelative("monoScript").objectReferenceValue as MonoBehaviour);
					iMonoScript = EditorGUI.Popup(scriptRect, "Script", iMonoScript, monoBehaviours.Select(i => i.ToString()).ToArray());
					if (iMonoScript < monoBehaviours.Count)
					{
						if (value.FindPropertyRelative("monoScript").objectReferenceValue as MonoBehaviour != monoBehaviours[iMonoScript])
							value.FindPropertyRelative("monoScript").objectReferenceValue = monoBehaviours[iMonoScript];
					}
					else
						iMonoScript = 0;
					if (value.FindPropertyRelative("monoScript").objectReferenceValue as MonoBehaviour != oldMono)
						value.FindPropertyRelative("methodInfoString").stringValue = "";
				}
				else
					value.FindPropertyRelative("monoScript").objectReferenceValue = null;

				DrawMethod(value.FindPropertyRelative("monoScript"), value.FindPropertyRelative("methodInfoString"));

			}
			else
			{
				GUI.enabled = false;
				value.FindPropertyRelative("monoScript").objectReferenceValue = EditorGUI.ObjectField(scriptRect, "Script", value.FindPropertyRelative("monoScript").objectReferenceValue, typeof(MonoBehaviour), true) as MonoBehaviour;
				EditorGUI.Popup(methodRect, "Method", 0, new string[] { "" });
				GUI.enabled = true;
			}
		}

		protected void DrawMethod(SerializedProperty script, SerializedProperty methodStr)
		{
			if (script.objectReferenceValue != null)
			{
				//FIELD INFO
				var methodInfos = script.objectReferenceValue.GetType().GetMethods(BindingFlags.Public | BindingFlags.Instance).ToList();
				List<string> infos = methodInfos.Select(i => i.Name.ToString()).ToList();
				infos.Insert(0, "             ");
				int totalRemoved = 0;
				int count = infos.Count;
				for (int i = 0; i < count; i++)
				{
					if (infos[i - totalRemoved].Contains("get_") || infos[i - totalRemoved].Contains("set_") || ProformerSettings.MonoBehaviourPublicMethod.Contains(infos[i - totalRemoved]))
					{
						infos.RemoveAt(i - totalRemoved);
						totalRemoved += 1;
					}
				}
				infos.Insert(1, "Enable GameObject");
				infos.Insert(2, "Disable GameObject");
				infos.Insert(3, "Enable Script");
				infos.Insert(4, "Disable Script");


				if (infos.Count > 0)
				{
					if (string.IsNullOrEmpty(methodStr.stringValue) && infos.Count != 0)
						methodStr.stringValue = infos[0].ToString();
					int iInfos = infos.IndexOf(methodStr.stringValue);
					if (iInfos == -1)
						iInfos = 0;
					iInfos = EditorGUI.Popup(methodRect, methodStr.displayName, iInfos, infos.ToArray());

					if (iInfos < infos.Count)
					{
						if (methodStr.stringValue != infos[iInfos])
						{
							methodStr.stringValue = infos[iInfos];
						}
					}
					else
						iInfos = 0;
				}
				else
				{
					methodStr.stringValue = "";
					EditorGUI.Popup(methodRect, methodStr.displayName, 0, new string[] { "" });
				}

			}
			else
			{
				EditorGUI.Popup(scriptRect, "Script", 0, new string[1] { "" });
				List<string> infos = new List<string>();
				infos.Insert(0, "             ");
				infos.Insert(1, "Enable GameObject");
				infos.Insert(2, "Disable GameObject");
				infos.Insert(3, "Enable Script");
				infos.Insert(4, "Disable Script");


				if (infos.Count > 0)
				{
					if (string.IsNullOrEmpty(methodStr.stringValue) && infos.Count != 0)
						methodStr.stringValue = infos[0].ToString();
					int iInfos = infos.IndexOf(methodStr.stringValue);
					if (iInfos == -1)
						iInfos = 0;
					iInfos = EditorGUI.Popup(methodRect, methodStr.displayName, iInfos, infos.ToArray());

					if (iInfos < infos.Count)
					{
						if (methodStr.stringValue != infos[iInfos])
						{
							methodStr.stringValue = infos[iInfos];
						}
					}
					else
						iInfos = 0;
				}
			}
		}
		protected void DrawMethod(SerializedProperty script, out string result, string propertyName, Rect position)
		{
			if (script.objectReferenceValue != null)
			{
				//FIELD INFO
				var methodInfos = script.objectReferenceValue.GetType().GetMethods(BindingFlags.Public | BindingFlags.Instance).ToList();
				List<string> infos = methodInfos.Select(i => i.Name.ToString()).ToList();
				infos.Insert(0, "             ");
				int totalRemoved = 0;
				int count = infos.Count;
				for (int i = 0; i < count; i++)
				{
					if (infos[i - totalRemoved].Contains("get_") || infos[i - totalRemoved].Contains("set_") || ProformerSettings.MonoBehaviourPublicMethod.Contains(infos[i - totalRemoved]))
					{
						infos.RemoveAt(i - totalRemoved);
						totalRemoved += 1;
					}
				}
				infos.Insert(1, "Enable GameObject");
				infos.Insert(2, "Disable GameObject");
				infos.Insert(3, "Enable Script");
				infos.Insert(4, "Disable Script");


				if (infos.Count > 0)
				{
					if (string.IsNullOrEmpty(property.FindPropertyRelative(propertyName).stringValue) && infos.Count != 0)
						property.FindPropertyRelative(propertyName).stringValue = infos[0].ToString();
					int iInfos = infos.IndexOf(property.FindPropertyRelative(propertyName).stringValue);
					if (iInfos == -1)
						iInfos = 0;
					iInfos = EditorGUI.Popup(position, property.FindPropertyRelative(propertyName).displayName, iInfos, infos.ToArray());

					if (iInfos < infos.Count)
					{
						if (property.FindPropertyRelative(propertyName).stringValue != infos[iInfos])
						{
							property.FindPropertyRelative(propertyName).stringValue = infos[iInfos];
						}
					}
					else
						iInfos = 0;
				}
				else
				{
					property.FindPropertyRelative(propertyName).stringValue = "";
					EditorGUI.Popup(position, property.FindPropertyRelative(propertyName).displayName, 0, new string[] { "" });
				}

			}
			else
			{
				EditorGUI.Popup(scriptRect, "Script", 0, new string[1] { "" });
				List<string> infos = new List<string>();
				infos.Insert(0, "             ");
				infos.Insert(1, "Enable GameObject");
				infos.Insert(2, "Disable GameObject");
				infos.Insert(3, "Enable Script");
				infos.Insert(4, "Disable Script");


				if (infos.Count > 0)
				{
					if (string.IsNullOrEmpty(property.FindPropertyRelative(propertyName).stringValue) && infos.Count != 0)
						property.FindPropertyRelative(propertyName).stringValue = infos[0].ToString();
					int iInfos = infos.IndexOf(property.FindPropertyRelative(propertyName).stringValue);
					if (iInfos == -1)
						iInfos = 0;
					iInfos = EditorGUI.Popup(position, property.FindPropertyRelative(propertyName).displayName, iInfos, infos.ToArray());

					if (iInfos < infos.Count)
					{
						if (property.FindPropertyRelative(propertyName).stringValue != infos[iInfos])
						{
							property.FindPropertyRelative(propertyName).stringValue = infos[iInfos];
						}
					}
					else
						iInfos = 0;
				}
			}
			result = property.FindPropertyRelative(propertyName).stringValue;
		}
	}
}
