﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using DI.Proformer.Action;

namespace DI.Proformer {
	[CustomEditor(typeof(DICharacterFSM))]
	public class DICharacterFSMEditor : Editor
	{
		DICharacterFSM ai;

		void OnEnable()
		{
			ai = (DICharacterFSM)target;

		}

		public override void OnInspectorGUI()
		{
			EditorGUI.BeginChangeCheck();
			MovementInspector();
			GunInspector();

			if (EditorGUI.EndChangeCheck())
			{
				serializedObject.ApplyModifiedProperties();
				EditorUtility.SetDirty(ai);
			}
		}

		void MovementInspector()
		{
			using (EditorGUILayoutGroup group = new EditorGUILayoutGroup(new GUIContent("Movement AI"), serializedObject.FindProperty("movePermission").isExpanded, serializedObject.FindProperty("movePermission").boolValue))
			{
				serializedObject.FindProperty("movePermission").boolValue = group.toggleValue;
				serializedObject.FindProperty("movePermission").isExpanded = group.isExpanding;
				if (group.isExpanding)
				{
					if (!ai.movePermission)
						GUI.enabled = false;
					EditorGUILayout.PropertyField(serializedObject.FindProperty("holeDetection"));
					EditorGUILayout.PropertyField(serializedObject.FindProperty("offsetHoleDetectorPosition"));
					EditorGUILayout.PropertyField(serializedObject.FindProperty("wallDetection"));
					EditorGUILayout.PropertyField(serializedObject.FindProperty("offsetWallDetectorLength"));
					EditorGUILayout.PropertyField(serializedObject.FindProperty("maxGroundHigh"));

					EditorGUILayout.Space();
					EditorGUILayout.LabelField("Move Range", EditorStyles.boldLabel);
					EditorGUILayout.PropertyField(serializedObject.FindProperty("maxMoveRange"));
					EditorGUILayout.BeginHorizontal();
					EditorGUILayout.LabelField("Move Range", GUILayout.Width(EditorGUIUtility.labelWidth));
					EditorGUILayout.MinMaxSlider(ref ai.minMoveTime, ref ai.maxMoveTime, 0, ai.maxMoveRange);
					EditorGUILayout.EndHorizontal();
					GUI.enabled = false;
					EditorGUILayout.PropertyField(serializedObject.FindProperty("maxMoveTime"));
					EditorGUILayout.PropertyField(serializedObject.FindProperty("minMoveTime"));
					if (ai.movePermission)
						GUI.enabled = true;

					EditorGUILayout.Space();
					EditorGUILayout.LabelField("Idle Range", EditorStyles.boldLabel);
					EditorGUILayout.PropertyField(serializedObject.FindProperty("maxIdleRange"));
					EditorGUILayout.BeginHorizontal();
					EditorGUILayout.LabelField("Idle Time Range", GUILayout.Width(EditorGUIUtility.labelWidth));
					EditorGUILayout.MinMaxSlider(ref ai.minIdleTime, ref ai.maxIdleTime, 0, ai.maxIdleRange);
					EditorGUILayout.EndHorizontal();
					GUI.enabled = false;
					EditorGUILayout.PropertyField(serializedObject.FindProperty("minIdleTime"));
					EditorGUILayout.PropertyField(serializedObject.FindProperty("maxIdleTime"));
					if (ai.movePermission)
						GUI.enabled = true;
				}

			}

			EditorGUILayout.Space();
			GUI.enabled = true;

		}

		void GunInspector()
		{
			bool gunAvailable = ai.GetComponent<GunAction>();
			using (EditorGUILayoutGroup group = new EditorGUILayoutGroup(new GUIContent("Gun Action"), serializedObject.FindProperty("shootRange").isExpanded, gunAvailable))
			{
				serializedObject.FindProperty("shootRange").isExpanded = group.isExpanding;
				if (group.isExpanding)
				{
					if (!group.toggleValue)
						GUI.enabled = false;
					EditorGUILayout.PropertyField(serializedObject.FindProperty("shootRange"));
					EditorGUILayout.PropertyField(serializedObject.FindProperty("shootTargetMask"));
					GUI.enabled = true;
				}
				if (gunAvailable != group.toggleValue)
				{
					if (!group.toggleValue)
						ai.GetComponent<GunAction>().KillComponent();
					else
						ai.gameObject.AddComponent<GunAction>();
				}
			}

		}
	}
}