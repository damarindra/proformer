﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using UnityEditorInternal;
using DI.Proformer.ToolHelper;
using System;
using DI.Proformer.Action;

namespace DI.Proformer {
	[CustomEditor(typeof(DIControllerMotor), true), CanEditMultipleObjects]
	public class PControllerMotorEditor : DIControllerEditor
	{
		DIControllerMotor pcMotor;
		//If contains follow path, our inspector will be disable!
		FollowPath followPath;
		GiveDamage giveDamage;
		Impactable impactable;
		Stompable stompable;
		bool moveRenderer = false;

		enum MenuMode
		{
			Setup, Inspector, Trigger
		}
		MenuMode menuMode = MenuMode.Setup;
		protected string animatorPath = "";
		ReorderableList takeDamageTrigger, impactableTrigger, pickableTrigger, stompableTag;

		public override void OnEnable()
		{
			base.OnEnable();
			pcMotor = (DIControllerMotor)target;
			followPath = pcMotor.GetComponent<FollowPath>();
			menuMode = (MenuMode)PlayerPrefs.GetInt("Menu Mode", 0);
			takeDamageTrigger = new ReorderableList(serializedObject, serializedObject.FindProperty("takeDamageTag"), false, true, true, true);
			impactableTrigger = new ReorderableList(serializedObject, serializedObject.FindProperty("impactTag"), false, true, true, true);
			pickableTrigger = new ReorderableList(serializedObject, serializedObject.FindProperty("pickableTag"), false, true, true, true);
			stompableTag = new ReorderableList(serializedObject, serializedObject.FindProperty("stompableTag"), false, true, true, true);

			ProformerMenu.FlushProformerActionLib();

			SetupReorderableList("Take Damage Tag", "TakeDamage", takeDamageTrigger);
			SetupReorderableList("Impactable Tag", "Impactable", impactableTrigger);
			SetupReorderableList("Pickable Tag", "Pickable", pickableTrigger);
			SetupReorderableList("Stompable Tag", "Stompable", stompableTag);
		}

		public override void CustomInspectorGUI()
		{
			if (_loaded)
			{
				PlayerPrefs.SetInt("Menu Mode", GUILayout.Toolbar((int)menuMode, new string[3] { "Setup", "Inspector", "Trigger" }));
				menuMode = (MenuMode)PlayerPrefs.GetInt("Menu Mode", 0);
				if (menuMode == MenuMode.Setup)
				{
					base.CustomInspectorGUI();
					DrawRendererInspector();
					if (!impactable && pcMotor.bodyCollider.collider != null)
						impactable = pcMotor.bodyCollider.collider.GetComponent<Impactable>();
					if (!giveDamage && pcMotor.bodyCollider.collider != null)
						giveDamage = pcMotor.bodyCollider.collider.GetComponent<GiveDamage>();
					if (!stompable && pcMotor.bodyCollider.collider != null)
						stompable = pcMotor.bodyCollider.collider.GetComponent<Stompable>();

					bool giveDamageAvailable = giveDamage;
					giveDamageAvailable = EditorInspectorHelper.GiveDamageInspector(giveDamage, serializedObject.FindProperty("_giveDamage"));
					if (giveDamageAvailable && !giveDamage)
						pcMotor.bodyCollider.collider.gameObject.AddComponent<GiveDamage>();
					else if (!giveDamageAvailable && giveDamage)
					{
						DestroyImmediate(giveDamage);
						giveDamage = null;
					}

					bool impactableAvailable = impactable;
					impactableAvailable = EditorInspectorHelper.ImpactableInspector(impactable, serializedObject.FindProperty("_impactable"));
					if (impactableAvailable && !impactable)
						pcMotor.bodyCollider.collider.gameObject.AddComponent<Impactable>();
					else if (!impactableAvailable && impactable)
					{
						DestroyImmediate(impactable);
						giveDamage = null;
					}

					//Stompable only apply on enemy
					if (pcMotor.gameObject.layer == LayerMask.NameToLayer("Enemy"))
					{
						EditorGUILayout.Space();

						bool stompableAvailable = stompable;

						stompableAvailable = EditorInspectorHelper.StompableInspector(stompable, serializedObject.FindProperty("_stompable"));

						if (stompableAvailable && !stompable)
						{
							pcMotor.bodyCollider.collider.gameObject.AddComponent<Stompable>();
						}
						else if (!stompableAvailable && stompable)
						{
							DestroyImmediate(stompable);
							stompable = null;
						}
					}
				}
				else if (menuMode == MenuMode.Inspector)
					DrawMotorInspector();
				else if (menuMode == MenuMode.Trigger)
					DrawTriggerInspector();

				if (pcMotor.bodyCollider.collider)
				{
					if (pcMotor.characterBody == null)
						pcMotor.characterBody = pcMotor.bodyCollider.collider.gameObject.AddComponent<DICharacterBody>();
					if (!pcMotor.rigidb.rb)
					{
#if PROFORMER3D
						pcMotor.rigidb.rb = pcMotor.bodyCollider.collider.GetComponent<Rigidbody>();
#else
						pcMotor.rigidb.rb = pcMotor.bodyCollider.collider.GetComponent<Rigidbody2D>();
#endif
						if (!pcMotor.rigidb.rb)
						{
#if PROFORMER3D
							pcMotor.rigidb = new DIRigidbody(pcMotor.bodyCollider.collider.gameObject.AddComponent<Rigidbody>());
#else
							pcMotor.rigidb = new DIRigidbody(pcMotor.bodyCollider.collider.gameObject.AddComponent<Rigidbody2D>());
#endif
							pcMotor.rigidb.rb.isKinematic = true;
						}
					}

				}

				if (!pcMotor.canRespawn && !pcMotor.infiniteLives)
					pcMotor.infiniteLives = true;
			}

		}
		
		private void DrawRendererInspector()
		{
			EditorGUI.BeginChangeCheck();


			using (EditorGUILayoutGroup group = new EditorGUILayoutGroup(new GUIContent("Renderer", "This option is show renderer object inspector, so you don't need to select renderer object to inspect them, we make it as simple as posible"), serializedObject.FindProperty("characterRenderer").isExpanded))
			{
				serializedObject.FindProperty("characterRenderer").isExpanded = group.isExpanding;
				if (group.isExpanding)
				{

					if (CreationPopup.entity != null)
					{
						EditorGUILayout.HelpBox("Renderer Disable currently because Creation Window is still opened. Please press Done or close the window to show Renderer Option", MessageType.Info);
					}
					else
					{
						EditorGUILayout.BeginHorizontal();
						if (pcMotor.characterRenderer && pcMotor.rendererObject != null) {
							if(pcMotor.rendererObject.transform.parent == pcMotor.transform)
								GUI.enabled = false;
						}
						EditorGUILayout.PropertyField(serializedObject.FindProperty("rendererObject"));
						GUI.enabled = true;
						try
						{
							if (pcMotor.characterRenderer != null && pcMotor.rendererObject != null && pcMotor.rendererObject.transform.parent == pcMotor.transform )
							{
									if (GUILayout.Button("Remove", GUILayout.Width(60)))
									{
										Undo.DestroyObjectImmediate(pcMotor.rendererObject);
										return;
									}
							}
							if (pcMotor.rendererObject)
							{
								if (pcMotor.rendererObject.transform.parent != pcMotor.transform)
								{
									if (GUILayout.Button("Apply", GUILayout.Width(60)))
									{
										GameObject renderer = Instantiate(pcMotor.rendererObject, pcMotor.transform.position, Quaternion.identity) as GameObject;
										renderer.name = "Renderer";
										renderer.transform.parent = pcMotor.transform;
										pcMotor.rendererObject = renderer;
										pcMotor.characterRenderer = renderer.GetComponentInChildren<Renderer>();
										pcMotor.animator = pcMotor.rendererObject.GetComponent<Animator>();
										if (pcMotor.animator)
											pcMotor.animator.applyRootMotion = false;
										renderer.layer = pcMotor.gameObject.layer;
										renderer.tag = pcMotor.gameObject.tag;
										Undo.RegisterCreatedObjectUndo(renderer, "Renderer");
									}
								}
							}
							EditorGUILayout.EndHorizontal();

							if (pcMotor.rendererObject)
							{
								GUI.enabled = false;
								if (pcMotor.rendererObject.transform.parent == pcMotor.transform)
								{
									EditorGUILayout.PropertyField(serializedObject.FindProperty("characterRenderer"));

									if (pcMotor.characterRenderer != null)
									{
										GUI.enabled = true;
	#if !PROFORMER3D
								Renderer renderer = pcMotor.rendererObject.GetComponent<Renderer>();
								if (renderer == null)
									renderer = pcMotor.rendererObject.GetComponentInChildren<Renderer>();
								if (renderer)
								{
									if (renderer.sortingLayerName == "Default")
										renderer.sortingLayerName = "Player";
									Undo.RecordObject(pcMotor.rendererObject.GetComponent<Renderer>(), "Renderer");
									var sortingLayers = InternalTool.GetSortingLayerNames();
									renderer.sortingLayerName = sortingLayers[EditorGUILayout.Popup("Sorting Layer", InternalTool.GetSortingLayerIndex(renderer.sortingLayerName), sortingLayers)];
									renderer.sortingOrder = EditorGUILayout.IntField("Order in Layer", renderer.sortingOrder);
								}
	#endif
										EditorGUILayout.BeginHorizontal();
										Undo.RecordObject(pcMotor.rendererObject.transform, "Local Position");
										pcMotor.rendererObject.transform.localPosition = EditorGUILayout.Vector3Field("Position", pcMotor.rendererObject.transform.localPosition);
										if (GUILayout.Button(moveRenderer ? "Done" : "Move", GUILayout.Width(60)))
										{
											moveRenderer = !moveRenderer;
											disableMoveHandler = moveRenderer ? true : false;
										}
										EditorGUILayout.EndHorizontal();
										Undo.RecordObject(pcMotor.rendererObject.transform, "Local Rotation");
										pcMotor.rendererObject.transform.localEulerAngles = EditorGUILayout.Vector3Field("Rotation", pcMotor.rendererObject.transform.localEulerAngles);
										Undo.RecordObject(pcMotor.rendererObject.transform, "Local Scale");
										pcMotor.rendererObject.transform.localScale = EditorGUILayout.Vector3Field("Scale", pcMotor.rendererObject.transform.localScale);
									}

									Rect animControllerRect = GUILayoutUtility.GetRect(new GUIContent(), EditorStyles.objectField);
									EditorGUILayout.BeginHorizontal();
									if (pcMotor.rendererObject)
									{
										if (pcMotor.characterRenderer == null)
											pcMotor.characterRenderer = pcMotor.rendererObject.GetComponentInChildren<Renderer>();
										if (pcMotor.animator)
										{
											//pcMotor.animator.runtimeAnimatorController = EditorGUILayout.ObjectField("Animator Controller", pcMotor.animator.runtimeAnimatorController, typeof(RuntimeAnimatorController), false) as RuntimeAnimatorController;
											float width = pcMotor.animator.runtimeAnimatorController != null ? animControllerRect.width : animControllerRect.width - 65;
											pcMotor.animator.runtimeAnimatorController = EditorGUI.ObjectField(new Rect(animControllerRect.x, animControllerRect.y, width, animControllerRect.height), "Animator Controller", pcMotor.animator.runtimeAnimatorController, typeof(RuntimeAnimatorController), false) as RuntimeAnimatorController;
											if (!pcMotor.animator.runtimeAnimatorController)
											{
												if (GUI.Button(new Rect(animControllerRect.x + animControllerRect.width - 60, animControllerRect.y, 60, animControllerRect.height), "Create"))
												{
													animatorPath = EditorUtility.SaveFilePanelInProject("Create Animator Controller", pcMotor.name, "controller", "Create Animator Controller Template");
													if (!string.IsNullOrEmpty(animatorPath))
													{
														FileUtil.DeleteFileOrDirectory(animatorPath);
														AssetDatabase.Refresh();
														FileUtil.CopyFileOrDirectory(Globals.animatorTemplatePath + "/" + Globals.animatorName, animatorPath);

														/*
														string[] animatorPathSplit = animatorPath.Split('/');

														
														string realPath = animatorPath.Remove(animatorPath.Length - animatorPathSplit[animatorPathSplit.Length - 1].Length);

														string fileName = animatorPathSplit[animatorPathSplit.Length - 1];
														fileName = fileName.Remove(fileName.Length - 11);
														
														string animsFolder = realPath  + fileName + "_clips";
														FileUtil.DeleteFileOrDirectory(realPath + fileName + "_clips");
														FileUtil.CopyFileOrDirectory(Globals.animationClipPath, realPath + fileName + "_clips");
														*/
														AssetDatabase.Refresh();
														AssetDatabase.SaveAssets();
														pcMotor.animator.runtimeAnimatorController = AssetDatabase.LoadAssetAtPath(animatorPath, typeof(RuntimeAnimatorController)) as RuntimeAnimatorController;
														
														/*
														SerializedProperty clipsSp = serializedObject.FindProperty("clips");
														int i = 1, until = serializedObject.FindProperty("clips").CountInProperty();
														while (i < until)
														{
															clipsSp.NextVisible(true);
															
															//First Get the clip
															UnityEngine.Object cl = AssetDatabase.LoadAssetAtPath(animsFolder + "/" + clipsSp.FindPropertyRelative("lastNameRegisteredInAnimatorController").stringValue + ".anim", typeof(AnimationClip));
															//then asign the clips field
															clipsSp.FindPropertyRelative("clip").objectReferenceValue = cl;
															//Then assign into animator controller
															//pcMotor.animator.ChangeAnimationClip(cl as AnimationClip, clipsSp.FindPropertyRelative("lastNameRegisteredInAnimatorController").stringValue);
															Debug.Log(pcMotor.animator.runtimeAnimatorController.animationClips[0]);
															pcMotor.animator.runtimeAnimatorController.animationClips[0] = cl as AnimationClip;
															Debug.Log("AFter" + pcMotor.animator.runtimeAnimatorController.animationClips[0]);

															i++;
														}*/
													}
												}
											}
										}
										else if (!pcMotor.animator)
										{
											pcMotor.animator = pcMotor.rendererObject.gameObject.GetComponent<Animator>();
											if (!pcMotor.animator)
											{
												if (GUI.Button(animControllerRect, "Add Animator"))
												{
													pcMotor.animator = pcMotor.rendererObject.gameObject.AddComponent<Animator>();
													pcMotor.animator.applyRootMotion = false;
												}
											}
										}
									}
									EditorGUILayout.EndHorizontal();
									/*
									if(pcMotor.animator.runtimeAnimatorController != null)
									{
										SerializedProperty clipsSp = serializedObject.FindProperty("clips");

										EditorGUI.indentLevel += 1;
										EditorGUILayout.PropertyField(clipsSp);
										Rect _lastRect = GUILayoutUtility.GetLastRect();
										EditorGUI.indentLevel += 1;

										int i = 1, until = serializedObject.FindProperty("clips").CountInProperty();
										while (i < until) {
											clipsSp.NextVisible(true);
											if (i % 2 == 1) {
												_lastRect.y += _lastRect.height + EditorGUIUtility.standardVerticalSpacing;
												EditorGUI.DrawRect(_lastRect, DIEditorHelper.getNormalColor);
											}

											EditorGUILayout.PropertyField(clipsSp);
											_lastRect = GUILayoutUtility.GetLastRect();

											i++;
										}
										EditorGUI.indentLevel -= 1;

										EditorGUI.indentLevel -= 1;

									}*/
								}
							}

						}
						catch
						{
#if PROFORMER_FULL_LOG
							throw;
#endif
						}
					}
				}
			}

			if (EditorGUI.EndChangeCheck())
			{
				serializedObject.ApplyModifiedProperties();
				EditorUtility.SetDirty(pcMotor);
			}
		}

		private void DrawMotorInspector()
		{
			EditorGUI.BeginChangeCheck();
			serializedObject.ApplyModifiedProperties();
			EditorGUILayout.Space();

			using (EditorGUILayoutGroup group = new EditorGUILayoutGroup(new GUIContent("Health", "Health option will configure Character Health, include damage effect and damage controll such invisible time, recover time etc."), serializedObject.FindProperty("maxHealth").isExpanded))
			{
				serializedObject.FindProperty("maxHealth").isExpanded = group.isExpanding;
				if (group.isExpanding)
				{
					EditorGUILayout.PropertyField(serializedObject.FindProperty("maxHealth"));
					serializedObject.FindProperty("health").intValue = EditorGUILayout.IntSlider(new GUIContent("Health"), serializedObject.FindProperty("health").intValue, 0, serializedObject.FindProperty("maxHealth").intValue);
					EditorGUILayout.PropertyField(serializedObject.FindProperty("invisibleTime"));
					EditorGUILayout.PropertyField(serializedObject.FindProperty("recoverTime"));
					EditorGUILayout.PropertyField(serializedObject.FindProperty("canRespawn"));
					EditorGUILayout.PropertyField(serializedObject.FindProperty("respawnTime"));
					EditorGUILayout.PropertyField(serializedObject.FindProperty("infiniteLives"));
					EditorGUILayout.PropertyField(serializedObject.FindProperty("livesCount"));
					EditorGUILayout.PropertyField(serializedObject.FindProperty("hitClip"));
					EditorGUILayout.PropertyField(serializedObject.FindProperty("flickeringWhenGetDamage"));
					serializedObject.FindProperty("disableEnableFlickerMethod").boolValue = EditorGUILayout.Popup("Flickering Method", serializedObject.FindProperty("disableEnableFlickerMethod").boolValue ? 0 : 1, new string[2] { "Disable Enable Renderer Object", "Color (Only Support for Sprite)" }) == 0;
					if (!serializedObject.FindProperty("disableEnableFlickerMethod").boolValue)
					{
						EditorGUILayout.PropertyField(serializedObject.FindProperty("colorFlicker"));
						if (!serializedObject.FindProperty("flickeringWhenGetDamage").boolValue)
							GUI.enabled = false;
					}
					serializedObject.FindProperty("transitionTimeFlickering").floatValue = EditorGUILayout.Slider(new GUIContent("Transition Time Flickering"), serializedObject.FindProperty("transitionTimeFlickering").floatValue, 0.02f, .5f);
					GUI.enabled = true;
					EditorGUILayout.PropertyField(serializedObject.FindProperty("transitionTimeFlickering"));
					EditorGUILayout.PropertyField(serializedObject.FindProperty("fadeOutWhenDie"));
					if (!serializedObject.FindProperty("fadeOutWhenDie").boolValue)
						GUI.enabled = false;
					if (!serializedObject.FindProperty("disableEnableFlickerMethod").boolValue)
						serializedObject.FindProperty("fadeOutSmoothing").floatValue = EditorGUILayout.Slider(new GUIContent("Fade Out Smoothing"), serializedObject.FindProperty("fadeOutSmoothing").floatValue, 0, 10);
					GUI.enabled = true;
				}
			}

			if (followPath)
				goto skip;

			using (EditorGUILayoutGroup group = new EditorGUILayoutGroup(new GUIContent("Movement", "Movement option will configure Character movement."), serializedObject.FindProperty("moveSpeed").isExpanded))
			{
				serializedObject.FindProperty("moveSpeed").isExpanded = group.isExpanding;
				if (group.isExpanding)
				{
					EditorGUILayout.PropertyField(serializedObject.FindProperty("moveSpeed"));
					EditorGUILayout.PropertyField(serializedObject.FindProperty("accelerationTimeGrounded"));
					EditorGUILayout.PropertyField(serializedObject.FindProperty("accelerationTimeAirborne"));
				}
			}
			using (EditorGUILayoutGroup group = new EditorGUILayoutGroup(new GUIContent("Jump", "Jump option will configure Character Jumping."), "Jump Inspector", serializedObject.FindProperty("jumpPermission").boolValue))
			{
				serializedObject.FindProperty("jumpPermission").boolValue = group.toggleValue;
				if (group.isExpanding)
				{
					EditorGUILayout.PropertyField(serializedObject.FindProperty("useGlobalGravity"));
					if (pcMotor.useGlobalGravity)
					{
						EditorGUILayout.PropertyField(serializedObject.FindProperty("maxJumpVelocity"));
						EditorGUILayout.PropertyField(serializedObject.FindProperty("minJumpVelocity"));
					}
					else
					{
						EditorGUILayout.PropertyField(serializedObject.FindProperty("maxJumpHeight"));
						EditorGUILayout.PropertyField(serializedObject.FindProperty("minJumpHeight"));
						EditorGUILayout.PropertyField(serializedObject.FindProperty("timeToJumpApex"));
						if (pcMotor.GetComponent<DIPlayer>())
							EditorGUILayout.PropertyField(serializedObject.FindProperty("setToGlobalGravity"));
						else if (serializedObject.FindProperty("setToGlobalGravity").boolValue)
							serializedObject.FindProperty("setToGlobalGravity").boolValue = false;

						GUI.enabled = false;
						EditorGUILayout.PropertyField(serializedObject.FindProperty("maxJumpVelocity"));
						EditorGUILayout.PropertyField(serializedObject.FindProperty("minJumpVelocity"));
						GUI.enabled = true;
						if (!Application.isPlaying)
							pcMotor.CalculateJump();
					}
					EditorGUILayout.PropertyField(serializedObject.FindProperty("numberJumpOnAir"));
					EditorGUILayout.PropertyField(serializedObject.FindProperty("jumpClip"));
					GUI.enabled = false;
					EditorGUILayout.PropertyField(serializedObject.FindProperty("gravity"));
					GUI.enabled = true;
				}
			}

			using (EditorGUILayoutGroup group = new EditorGUILayoutGroup(new GUIContent("Ladder", "Ladder option will configure Character Climbing Ladder."), "Ladder Inspector", serializedObject.FindProperty("ladderPermission").boolValue))
			{
				serializedObject.FindProperty("ladderPermission").boolValue = group.toggleValue;
				if (group.isExpanding)
				{
					EditorGUILayout.PropertyField(serializedObject.FindProperty("ladderSpeed"));
					EditorGUILayout.PropertyField(serializedObject.FindProperty("ladderJump"));
					EditorGUILayout.PropertyField(serializedObject.FindProperty("centeredWhenClimbing"));
					EditorGUILayout.PropertyField(serializedObject.FindProperty("unstickLadderWhenHorizontalMove"));
				}
			}

			using (EditorGUILayoutGroup group = new EditorGUILayoutGroup(new GUIContent("Wall Slide", "Wall Slide option will configure Character Wall Sliding, Wall Jump."), "Wall Slide Inspector", serializedObject.FindProperty("wallSlidePermission").boolValue))
			{
				serializedObject.FindProperty("wallSlidePermission").boolValue = group.toggleValue;
				if (group.isExpanding)
				{
					EditorGUILayout.PropertyField(serializedObject.FindProperty("wallJumpClimb"));
					EditorGUILayout.PropertyField(serializedObject.FindProperty("wallJumpOff"));
					EditorGUILayout.PropertyField(serializedObject.FindProperty("wallLeap"));
					EditorGUILayout.PropertyField(serializedObject.FindProperty("wallSlideSpeedMax"));
					EditorGUILayout.PropertyField(serializedObject.FindProperty("wallStickTime"));
					EditorGUILayout.PropertyField(serializedObject.FindProperty("unstickWhenDirTouchUp"));
					EditorGUILayout.PropertyField(serializedObject.FindProperty("reverseWallSlide"));
				}
			}

			using (EditorGUILayoutGroup group = new EditorGUILayoutGroup(new GUIContent("Crouch", "Crouch option will configure Character Crouching."), "Crouch Inspector", serializedObject.FindProperty("crouchPermission").boolValue))
			{
				serializedObject.FindProperty("crouchPermission").boolValue = group.toggleValue;
				if (group.isExpanding)
				{
					EditorGUILayout.PropertyField(serializedObject.FindProperty("speedCrouching"));
					if (!serializedObject.FindProperty("crouchPermission").boolValue)
						GUI.enabled = false;
					serializedObject.FindProperty("colliderScaller").floatValue = EditorGUILayout.Slider(new GUIContent("Speed Crouching"), serializedObject.FindProperty("colliderScaller").floatValue, 0.2f, .8f);
					GUI.enabled = true;
				}
			}

			using (EditorGUILayoutGroup group = new EditorGUILayoutGroup(new GUIContent("Custom Action", "Custom Action option shows all Custom Action, enabling action will allow character to use custom action."), "Custom Action Inspector"))
			{
				if (group.isExpanding)
				{
					if (GameData.Instance)
					{
						foreach (Type t in GameData.Instance.customActionLib())
						{
							ToggleAction(t.Name, (ProformerAction)pcMotor.GetComponent(t), t);
						}
					}
				}
			}

			skip:

			if (EditorGUI.EndChangeCheck())
			{
				if (serializedObject.FindProperty("health").intValue > serializedObject.FindProperty("maxHealth").intValue)
					serializedObject.FindProperty("health").intValue = serializedObject.FindProperty("maxHealth").intValue;
				serializedObject.ApplyModifiedProperties();
				EditorUtility.SetDirty(pcMotor);
			}
		}


		private void DrawTriggerInspector()
		{
			EditorGUI.BeginChangeCheck();
			takeDamageTrigger.DoLayoutList();
			impactableTrigger.DoLayoutList();
			pickableTrigger.DoLayoutList();
			stompableTag.DoLayoutList();

			if (EditorGUI.EndChangeCheck())
			{
				serializedObject.ApplyModifiedProperties();
				EditorUtility.SetDirty(pcMotor);
			}
		}

		/* OLD
		ProformerAction ToggleAction(string label, ProformerAction action, Type type) {
			bool enable = EditorGUILayout.Toggle(label, action);

			if (enable && !action)
				action = pcMotor.gameObject.AddComponent(type) as ProformerAction;
			else if (!enable && action) {
				action.KillComponent();
				action = null;
			}

			return action;
		}*/
		void ToggleAction(string label, ProformerAction action, Type type)
		{
			bool enable = EditorGUILayout.Toggle(label, action);

			if (enable && !action)
				action = pcMotor.gameObject.AddComponent(type) as ProformerAction;
			else if (!enable && action)
			{
				action.KillComponent();
				action = null;
			}
		}

		public override void OnSceneGUI()
		{
			base.OnSceneGUI();
			if (moveRenderer)
			{
				Vector3 worldPosition = Handles.PositionHandle(pcMotor.rendererObject.transform.position, Quaternion.identity);
				if (worldPosition != pcMotor.rendererObject.transform.position)
				{
					Undo.RecordObject(pcMotor.rendererObject.transform, "Undo Move Renderer");
					pcMotor.rendererObject.transform.position = worldPosition;
				}
			}
			EditorInspectorHelper.ImpactableEditMode(impactable);
		}


		void SetupReorderableList(string header, string elementName, ReorderableList list)
		{
			list.drawHeaderCallback = (Rect rect) => {
				EditorGUI.LabelField(rect, header);
			};
			list.drawElementCallback = (Rect rect, int index, bool isActive, bool isFocused) => {
				var element = list.serializedProperty.GetArrayElementAtIndex(index);
				EditorGUI.LabelField(new Rect(rect.x, rect.y, rect.width * .3f, EditorGUIUtility.singleLineHeight), elementName + " " + index);
				element.stringValue = EditorGUI.TagField(new Rect(rect.x + rect.width * .3f, rect.y, rect.width * .7f, EditorGUIUtility.singleLineHeight), element.stringValue);

				//EditorGUI.PropertyField(new Rect(rect.x, rect.y, rect.width, EditorGUIUtility.singleLineHeight), element);
			};
		}
	}
}