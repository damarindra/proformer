﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using DI.Proformer.ToolHelper;

namespace DI.Proformer
{
	[CustomPropertyDrawer(typeof(HeaderBackgroundAttribute))]
	public class HeaderBackgroundAttributeDrawer : DecoratorDrawer
	{
		public bool isExpanding = true, toggleValue = false;
		public Rect headerRect, totalRect;

		public override void OnGUI(Rect position)
		{
			HeaderBackgroundAttribute att = (HeaderBackgroundAttribute)attribute;
			this.totalRect = position;
			CreateHeader(att);
		}

		public override float GetHeight()
		{
			HeaderBackgroundAttribute att = (HeaderBackgroundAttribute)attribute;
			if (string.IsNullOrEmpty(att.headerLabel)) return att.margin.top; else return base.GetHeight() + att.margin.top;
		}

		void CreateHeader(HeaderBackgroundAttribute att)
		{
			headerRect = new Rect(totalRect.x + att.margin.left, totalRect.y + att.margin.top, totalRect.width - att.margin.right, EditorGUIUtility.singleLineHeight);
			if (att.border)
				EditorGUI.DrawRect(new Rect(headerRect.x - 1, headerRect.y - 1, headerRect.width + 2, (EditorGUIUtility.singleLineHeight + EditorGUIUtility.standardVerticalSpacing) * att.yCount + att.offsetBot), Color.black);

			GUIStyle toolbarStyle = new GUIStyle(EditorStyles.toolbar);
			EditorGUI.LabelField(headerRect, "", toolbarStyle);
			GUIStyle labelStyle = new GUIStyle(EditorStyles.label);
			labelStyle.fontStyle = FontStyle.Bold;
			EditorGUI.LabelField(headerRect, att.headerLabel, labelStyle);


			EditorGUI.DrawRect(new Rect(headerRect.x, headerRect.y + EditorGUIUtility.singleLineHeight, headerRect.width, (EditorGUIUtility.singleLineHeight + EditorGUIUtility.standardVerticalSpacing) * (att.yCount - 1) + att.offsetBot), DIEditorHelper.getBoxColor);

		}

	}
}