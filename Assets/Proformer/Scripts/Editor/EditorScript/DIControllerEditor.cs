﻿using UnityEngine;
using UnityEditor;

namespace DI.Proformer {

	[CustomEditor(typeof(DIController), true), CanEditMultipleObjects]
	public class DIControllerEditor : DIEntityEditor
	{
		DIController pcontroller;
		bool editColliderMode = false;

		public override void OnEnable()
		{
			base.OnEnable();
			withoutDefault = true;
			pcontroller = (DIController)target;
		}

		public override void CustomInspectorGUI()
		{
			base.CustomInspectorGUI();
			DrawControllerInspector();
			DrawColliderInspector();

		}

		void DrawControllerInspector()
		{
			EditorGUI.BeginChangeCheck();
			serializedObject.Update();

			using (EditorGUILayoutGroup group = new EditorGUILayoutGroup(new GUIContent("Controller", "Controller is option for configuring Controller Calculation."), serializedObject.FindProperty("horizontalRayCount").isExpanded))
			{
				serializedObject.FindProperty("horizontalRayCount").isExpanded = group.isExpanding;
				if (group.isExpanding)
				{
					EditorGUILayout.PropertyField(serializedObject.FindProperty("horizontalRayCount"));
					EditorGUILayout.PropertyField(serializedObject.FindProperty("verticalRayCount"));
					EditorGUILayout.PropertyField(serializedObject.FindProperty("slopeLimit"));
					float wallAngleMin = serializedObject.FindProperty("wallAngleMin").floatValue;
					float wallAngleMax = serializedObject.FindProperty("wallAngleMax").floatValue;
					EditorGUILayout.MinMaxSlider(new GUIContent("Wall Angle", "Determines is Wall"), ref wallAngleMin, ref wallAngleMax, 85, 95);
					serializedObject.FindProperty("wallAngleMin").floatValue = wallAngleMin;
					serializedObject.FindProperty("wallAngleMax").floatValue = wallAngleMax;
					GUI.enabled = false;
					EditorGUILayout.PropertyField(serializedObject.FindProperty("wallAngleMin"));
					EditorGUILayout.PropertyField(serializedObject.FindProperty("wallAngleMax"));
					GUI.enabled = true;
				}
			}

			if (EditorGUI.EndChangeCheck())
			{
				serializedObject.ApplyModifiedProperties();
				EditorUtility.SetDirty(pcontroller);
			}
		}

		protected virtual void DrawColliderInspector()
		{
			EditorGUI.BeginChangeCheck();
			serializedObject.Update();

			using (EditorGUILayoutGroup group = new EditorGUILayoutGroup(new GUIContent("Collider", "Body Collider is collider for character. Since Proformer using manual calculation (not using Rigidbody), body collider is the core of character movement calculation"), serializedObject.FindProperty("bodyCollider").isExpanded))
			{
				serializedObject.FindProperty("bodyCollider").isExpanded = group.isExpanding;
				if (group.isExpanding)
				{
					pcontroller.bodyCollider.VerifySettings();
					EditorGUILayout.BeginHorizontal();
#if PROFORMER3D
					pcontroller.bodyCollider.collider = DIEditorHelper.RequiredObjectField(new GUIContent("Body Collider", ""), pcontroller.bodyCollider.collider, typeof(BoxCollider), true) as BoxCollider;
#else
					pcontroller.bodyCollider.collider = DIEditorHelper.RequiredObjectField(new GUIContent("Body Collider", ""), pcontroller.bodyCollider.collider, typeof(BoxCollider2D), true) as BoxCollider2D;

#endif
					if (!pcontroller.bodyCollider.collider)
					{
						if (GUILayout.Button("Create", GUILayout.Width(60)))
						{
							GameObject bCollider = new GameObject("Body");
#if PROFORMER3D
							pcontroller.bodyCollider.collider = bCollider.AddComponent<BoxCollider>();
#else
							pcontroller.bodyCollider.collider = bCollider.AddComponent<BoxCollider2D>();
#endif
							bCollider.transform.parent = pcontroller.transform;
							bCollider.transform.localPosition = Vector3.zero;
							bCollider.transform.eulerAngles = Vector3.zero;
							bCollider.gameObject.layer = pcontroller.gameObject.layer;
							bCollider.gameObject.tag = pcontroller.gameObject.tag;
							Undo.RegisterCreatedObjectUndo(bCollider, "Undo Create Body");
						}
					}
					else
					{

						if (GUILayout.Button(editColliderMode ? "Done" : "Edit", GUILayout.Width(60)))
						{
							editColliderMode = !editColliderMode;
						}
					}
					EditorGUILayout.EndHorizontal();

					if (pcontroller.bodyCollider.collider)
					{
#if PROFORMER3D
						if (pcontroller.bodyCollider.center != Vector3.zero)
#else
						if (pcontroller.bodyCollider.center != Vector3.zero)
#endif
						{
							Rect errorRect = GUILayoutUtility.GetRect(new GUIContent("Offset changes detected! We do not recommend to changing offset of Body Collider, this may cause controller confuse when fliping direction. You can change Renderer Position instead of changing Body Collider Offset."), EditorStyles.helpBox);
							EditorGUI.HelpBox(new Rect(errorRect.x, errorRect.y, errorRect.width - 60, errorRect.height), "Offset changes detected! We do not recommend to changing offset of Body Collider, this may cause controller confuse when fliping direction. You can change Renderer Position instead of changing Body Collider Offset.", MessageType.Error);
							if (GUI.Button(new Rect(errorRect.x + errorRect.width - 60, errorRect.y, 60, errorRect.height), "Reset"))
#if PROFORMER3D
								pcontroller.bodyCollider.collider.center = Vector3.zero;
#else
								pcontroller.bodyCollider.collider.offset = Vector2.zero;
#endif
						}

						//Show Info Collider
#if PROFORMER3D
						//Undo.RecordObject(pcontroller.bodyCollider.collider, "Undo Collider Center");
						//pcontroller.bodyCollider.center = EditorGUILayout.Vector3Field("Center", pcontroller.bodyCollider.center);
						Undo.RecordObject(pcontroller.bodyCollider.collider, "Undo Collider Width");
						pcontroller.bodyCollider.width = EditorGUILayout.FloatField("Width", pcontroller.bodyCollider.width);
						Undo.RecordObject(pcontroller.bodyCollider.collider, "Undo Collider Height");
						pcontroller.bodyCollider.height = EditorGUILayout.FloatField("Height", pcontroller.bodyCollider.height);
						Undo.RecordObject(pcontroller.bodyCollider.collider, "Undo Collider Depth");
						pcontroller.bodyCollider.depth = EditorGUILayout.FloatField("Depth", pcontroller.bodyCollider.depth);
#else
						//Undo.RecordObject(pcontroller.bodyCollider.collider, "Undo Collider Center");
						//pcontroller.bodyCollider.center = EditorGUILayout.Vector2Field("Center", pcontroller.bodyCollider.center);
						Undo.RecordObject(pcontroller.bodyCollider.collider, "Undo Collider Width");
						pcontroller.bodyCollider.width = EditorGUILayout.FloatField("Width", pcontroller.bodyCollider.width);
						Undo.RecordObject(pcontroller.bodyCollider.collider, "Undo Collider Height");
						pcontroller.bodyCollider.height = EditorGUILayout.FloatField("Height", pcontroller.bodyCollider.height);
#endif
					}
				}
			}

			if (EditorGUI.EndChangeCheck())
			{
				serializedObject.ApplyModifiedProperties();
				EditorUtility.SetDirty(pcontroller);
			}
		}

		#region OnSceneGUI

		public override void OnSceneGUI()
		{
			base.OnSceneGUI();
			if (editColliderMode)
				EditCollider();
		}

		void EditCollider()
		{
			Vector3 worldCenter = pcontroller.bodyCollider.collider.transform.position + pcontroller.bodyCollider.center;
			Handles.color = new Color(Color.green.r, Color.green.g, Color.green.b, .5f);
			Vector3 resultWidth = Handles.FreeMoveHandle(worldCenter + pcontroller.position.levelPath.direction * pcontroller.bodyCollider.width / 2, Quaternion.identity, .1f, InternalTool.GetSnapValue(), Handles.CubeHandleCap);
			resultWidth.y = worldCenter.y;
			if (Vector3.Distance(resultWidth, worldCenter) != pcontroller.bodyCollider.width / 2)
			{
				Undo.RecordObject(pcontroller.bodyCollider.collider, "Undo Collider Width");
#if PROFORMER3D
				pcontroller.bodyCollider.collider.size = new Vector3(Vector3.Distance(resultWidth, worldCenter) * 2, pcontroller.bodyCollider.collider.size.y, pcontroller.bodyCollider.depth);
#else
				pcontroller.bodyCollider.collider.size = new Vector3(Vector3.Distance(resultWidth, worldCenter) * 2, pcontroller.bodyCollider.collider.size.y, pcontroller.bodyCollider.depth);
#endif
			}

			Vector3 resultHeight = Handles.FreeMoveHandle(worldCenter + Vector3.up * pcontroller.bodyCollider.height / 2, Quaternion.identity, .1f, InternalTool.GetSnapValue(), Handles.CubeHandleCap);
			if (resultHeight.y - worldCenter.y != pcontroller.bodyCollider.height / 2)
			{
				Undo.RecordObject(pcontroller.bodyCollider.collider, "Undo Collider Height");
#if PROFORMER3D
				pcontroller.bodyCollider.collider.size = new Vector3(pcontroller.bodyCollider.width, (resultHeight.y - worldCenter.y) * 2, pcontroller.bodyCollider.depth);
#else
				pcontroller.bodyCollider.collider.size = new Vector3(pcontroller.bodyCollider.collider.size.x, (resultHeight.y - worldCenter.y) * 2, pcontroller.bodyCollider.depth);
#endif
			}
#if !PROFORMER3D
			/*
			Vector3 resultCenter = Handles.FreeMoveHandle(worldCenter, Quaternion.identity, .1f, InternalTool.GetSnapValue(), Handles.CubeCap);
			if (resultCenter - pcontroller.bodyCollider.collider.transform.position != pcontroller.bodyCollider.center)
			{
				Undo.RecordObject(pcontroller.bodyCollider.collider, "Undo Collider Center");
				pcontroller.bodyCollider.collider.offset = (resultCenter - pcontroller.bodyCollider.collider.transform.position);
			pcontroller.bodyCollider.collider.center = (resultCenter - pcontroller.bodyCollider.collider.transform.position);
			}*/
#endif

			/*
#if PROFORMER3D
			//Draw Depth
			Vector3 resultDepth = Handles.FreeMoveHandle(worldCenter + pcontroller.transform.right * pcontroller.bodyCollider.depth / 2, Quaternion.identity, .1f, InternalTool.GetSnapValue(), Handles.CubeCap);
			resultDepth.y = worldCenter.y;
			if (Vector3.Distance(resultDepth, worldCenter) != pcontroller.bodyCollider.depth / 2)
			{
				Undo.RecordObject(pcontroller.bodyCollider.collider, "Undo Collider Depth");
				pcontroller.bodyCollider.collider.size = new Vector3(pcontroller.bodyCollider.width, pcontroller.bodyCollider.collider.size.y, Vector3.Distance(resultDepth, worldCenter) * 2);
			}
#endif
*/
		}

		#endregion
	}
}