﻿using UnityEngine;
using System.Collections;
using UnityEditor;

namespace DI.Proformer {

	[CustomEditor(typeof(DILadder)), CanEditMultipleObjects]
	public class DILadderEditor : DIObjectEditor
	{

		public override void OnEnable()
		{
			base.OnEnable();
			isTriggerType = IsTriggerType.True;
			withoutDefault = true;
		}

		public override void CustomInspectorGUI()
		{
			serializedObject.Update();
			int menu = GUILayout.Toolbar(EditorPrefs.GetInt("Ladder Toolbar", 0), new string[2] { "Setup", "Ladder" });
			EditorPrefs.SetInt("Ladder Toolbar", menu);
			if (menu == 0)
				base.CustomInspectorGUI();
			else if (menu == 1)
			{
				EditorGUILayout.PropertyField(serializedObject.FindProperty("autoGenerateTopLand"));
#if PROFORMER3D
				EditorGUILayout.PropertyField(serializedObject.FindProperty("ladderFaceDirection"));
#endif
			}
			serializedObject.ApplyModifiedProperties();
		}
	}
}