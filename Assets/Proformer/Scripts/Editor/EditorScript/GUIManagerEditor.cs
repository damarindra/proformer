﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using UnityEngine.UI;
using DI.Proformer.Manager;

namespace DI.Proformer
{
	[CustomEditor(typeof(GUIManager))]
	public class GUIManagerEditor : Editor
	{
		GUIManager gm;

		void OnEnable()
		{
			gm = (GUIManager)target;

		}

		public override void OnInspectorGUI()
		{
			//base.DrawDefaultInspector();
			EditorGUI.BeginChangeCheck();
			serializedObject.Update();
			EditorGUILayout.Space();
			EditorGUILayout.HelpBox("These Preference give you EZ mode. If you want to change anchor and other preference, you can edit manually", MessageType.Info);
			
			Undo.RecordObject(gm, "Use Char Preview");
			using (EditorGUILayoutGroup group = new EditorGUILayoutGroup(new GUIContent("Preview Character", "Preview Character Option"), "Preview Character Inspector", gm._usePreviewCharacter))
			{
				gm._usePreviewCharacter = group.toggleValue;
				if (GUI.Button(new Rect(group.headerRect.x + group.headerRect.width - 120, group.headerRect.y, 100, group.headerRect.height), gm.previewHide ? "Show In Editor" : "Hide in Editor"))
				{
					gm.previewHide = !gm.previewHide;
				}
				if (group.isExpanding)
				{
					if (gm._usePreviewCharacter)
					{
						if (!Application.isPlaying)
						{
							if (!gm.previewHide)
							{
								if (gm.characterPreviewBG)
									gm.characterPreviewBG.gameObject.SetActive(true);
								if (gm.characterPreview)
									gm.characterPreview.gameObject.SetActive(true);
							}
							else
							{
								if (gm.characterPreviewBG)
									gm.characterPreviewBG.gameObject.SetActive(false);
								if (gm.characterPreview)
									gm.characterPreview.gameObject.SetActive(false);
							}
						}
					}
					else
					{
						GUI.enabled = false;
						if (gm.characterPreviewBG)
							gm.characterPreviewBG.gameObject.SetActive(false);
						if (gm.characterPreview)
							gm.characterPreview.gameObject.SetActive(false);
					}
					EditorGUI.indentLevel += 1;
					ShowAndPreference(ref gm.characterPreviewBG, "Preview Background");
					ShowAndPreference(ref gm.characterPreview, "Character Preview");
					EditorGUI.indentLevel -= 1;
					GUI.enabled = true;
				}
			}
			Undo.RecordObject(gm, "Use Health Bar");
			using (EditorGUILayoutGroup group = new EditorGUILayoutGroup(new GUIContent("Health Bar", "Health Bar Option"), "Health Bar Preview Inspector", gm._useHealthBar))
			{
				gm._useHealthBar = group.toggleValue;
				if (GUI.Button(new Rect(group.headerRect.x + group.headerRect.width - 120, group.headerRect.y, 100, group.headerRect.height), gm.healthHide ? "Show In Editor" : "Hide in Editor"))
				{
					gm.healthHide = !gm.healthHide;
				}
				if (group.isExpanding)
				{
					if (gm._useHealthBar)
					{
						if (!Application.isPlaying)
						{
							if (!gm.healthHide)
							{
								if (gm.healthArea)
									gm.healthArea.gameObject.SetActive(true);
								if (gm.healthValue)
									gm.healthValue.gameObject.SetActive(true);
							}
							else if (gm.healthHide)
							{
								if (gm.healthArea)
									gm.healthArea.gameObject.SetActive(false);
								if (gm.healthValue)
									gm.healthValue.gameObject.SetActive(false);
							}
						}
					}
					else
					{
						GUI.enabled = false;
						if (gm.healthArea)
							gm.healthArea.gameObject.SetActive(false);
						if (gm.healthValue)
							gm.healthValue.gameObject.SetActive(false);
					}
					EditorGUI.indentLevel += 1;
					ShowAndPreference(ref gm.healthArea, "Health Background");
					ShowAndPreference(ref gm.healthValue, "Health Value");
					EditorGUI.indentLevel -= 1;
					GUI.enabled = true;
				}
			}
			using (EditorGUILayoutGroup group = new EditorGUILayoutGroup(new GUIContent("Loading Transition", "Loading Transition Option"), "Loading Transition Preview Inspector", gm._useLoading))
			{
				gm._useLoading = group.toggleValue;
				if (GUI.Button(new Rect(group.headerRect.x + group.headerRect.width - 120, group.headerRect.y, 100, group.headerRect.height), gm.loadingHide ? "Show In Editor" : "Hide in Editor"))
				{
					gm.loadingHide = !gm.loadingHide;
				}
				if (group.isExpanding)
				{
					if (gm._useLoading)
					{
						if (!Application.isPlaying)
						{
							if (!gm.loadingHide)
							{
								if (gm.loadingBG)
									gm.loadingBG.gameObject.SetActive(true);
								if (gm.loadingBarBg)
									gm.loadingBarBg.gameObject.SetActive(true);
								if (gm.loadingBar)
									gm.loadingBar.gameObject.SetActive(true);
							}
							else
							{
								if (gm.loadingBG)
									gm.loadingBG.gameObject.SetActive(false);
								if (gm.loadingBarBg)
									gm.loadingBarBg.gameObject.SetActive(false);
								if (gm.loadingBar)
									gm.loadingBar.gameObject.SetActive(false);
							}
						}
					}
					else
					{
						GUI.enabled = false;
						if (gm.loadingBG)
							gm.loadingBG.gameObject.SetActive(false);
						if (gm.loadingBarBg)
							gm.loadingBarBg.gameObject.SetActive(false);
						if (gm.loadingBar)
							gm.loadingBar.gameObject.SetActive(false);
					}
					EditorGUI.indentLevel += 1;
					ShowAndPreference(ref gm.loadingBG, "Loading Background");
					ShowAndPreference(ref gm.loadingBarBg, "Loading Bar BG");
					ShowAndPreference(ref gm.loadingBar, "Loading Bar");
					EditorGUI.indentLevel -= 1;
					GUI.enabled = true;
				}
			}
			using (EditorGUILayoutGroup group = new EditorGUILayoutGroup(new GUIContent("Game Over Panel", ""), "Game Over Panel Inspector"))
			{
				if (gm.gameOverPanel)
				{
					if (GUI.Button(new Rect(group.headerRect.x + group.headerRect.width - 120, group.headerRect.y, 100, group.headerRect.height), !gm.gameOverPanel.activeSelf ? "Show In Editor" : "Hide in Editor"))
						gm.gameOverPanel.SetActive(!gm.gameOverPanel.activeSelf);
				}
				if (group.isExpanding)
				{
					EditorGUILayout.PropertyField(serializedObject.FindProperty("gameOverPanel"));
				}
			}
			
			EditorGUI.indentLevel -= 1;

			if (EditorGUI.EndChangeCheck() && !Application.isPlaying)
			{
				serializedObject.ApplyModifiedProperties();
				EditorUtility.SetDirty(gm);
			}
		}

		void CreateButton()
		{
			EditorGUILayout.Space();
			EditorGUILayout.Space();
			EditorGUILayout.Space();
			EditorGUILayout.BeginHorizontal();
			GUILayout.FlexibleSpace();
			if (GUILayout.Button("Create New", GUILayout.Height(40), GUILayout.Width(150)))
			{
				gm.simpleGUITargets.Add(new SimpleGUITarget());
			}
			GUILayout.FlexibleSpace();
			EditorGUILayout.EndHorizontal();

		}

		void ShowAndPreference(ref Image image, string name)
		{
			using (EditorGUILayoutGroup group = new EditorGUILayoutGroup(new GUIContent(name), name + " Inspector"))
			{
				if (group.isExpanding)
				{
					EditorGUI.indentLevel += 1;
					Undo.RecordObject(gm, name + "Image");
					image = EditorGUILayout.ObjectField("Image", image, typeof(Image), true) as Image;
					if (image != null)
					{
						Sprite newSprite = EditorGUILayout.ObjectField(image.sprite, typeof(Sprite), false, GUILayout.Width(90), GUILayout.Height(60)) as Sprite;
						if (newSprite != image.sprite)
						{
							Undo.RecordObject(image, name + "Sprite");
							image.sprite = newSprite;
							EditorUtility.SetDirty(image);
						}
					}
					else
					{
						GUI.enabled = false;
						EditorGUILayout.ObjectField(null, typeof(Sprite), false, GUILayout.Width(90), GUILayout.Height(60));
						GUI.enabled = true;
					}

					if (image != null)
					{
						Rect lastRect = GUILayoutUtility.GetLastRect();
						Rect newRect = new Rect(lastRect.x + lastRect.width - 25, lastRect.y, (EditorGUIUtility.currentViewWidth * .95f - lastRect.width) / 2, EditorGUIUtility.singleLineHeight);
						Rect labelRect = new Rect(newRect.x, newRect.y, newRect.width / 2, newRect.height);
						Rect valueRect = new Rect(newRect.x + labelRect.width, newRect.y, newRect.width - labelRect.width, newRect.height);
						EditorGUI.LabelField(labelRect, "Pos X");
						float x = InternalTool.DragableFloatProperty(valueRect, labelRect, image.rectTransform.localPosition.x, EditorStyles.numberField);

						newRect = new Rect(newRect.x + newRect.width, newRect.y, newRect.width, newRect.height);
						labelRect = new Rect(newRect.x, newRect.y, newRect.width / 2, newRect.height);
						valueRect = new Rect(newRect.x + labelRect.width, newRect.y, newRect.width - labelRect.width, newRect.height);
						EditorGUI.LabelField(labelRect, "Pos Y");
						float y = InternalTool.DragableFloatProperty(valueRect, labelRect, image.rectTransform.localPosition.y, EditorStyles.numberField);

						if ((Vector2)image.rectTransform.localPosition != new Vector2(x, y))
						{
							Undo.RecordObject(image.rectTransform, "Position");
							image.rectTransform.localPosition = new Vector3(x, y, image.rectTransform.localPosition.z);
						}


						newRect = new Rect(lastRect.x + lastRect.width - 25, lastRect.y + newRect.height * 1.2f, (EditorGUIUtility.currentViewWidth * .95f - lastRect.width) / 2, EditorGUIUtility.singleLineHeight);
						labelRect = new Rect(newRect.x, newRect.y, newRect.width / 2, newRect.height);
						valueRect = new Rect(newRect.x + labelRect.width, newRect.y, newRect.width - labelRect.width, newRect.height);
						EditorGUI.LabelField(labelRect, "Width");
						float width = InternalTool.DragableFloatProperty(valueRect, labelRect, image.rectTransform.sizeDelta.x, EditorStyles.numberField);

						newRect = new Rect(newRect.x + newRect.width, newRect.y, newRect.width, newRect.height);
						labelRect = new Rect(newRect.x, newRect.y, newRect.width / 2, newRect.height);
						valueRect = new Rect(newRect.x + labelRect.width, newRect.y, newRect.width - labelRect.width, newRect.height);
						EditorGUI.LabelField(labelRect, "Height");
						float height = InternalTool.DragableFloatProperty(valueRect, labelRect, image.rectTransform.sizeDelta.y, EditorStyles.numberField);

						if (image.rectTransform.sizeDelta != new Vector2(width, height))
						{
							Undo.RecordObject(image.rectTransform, "Size");
							image.rectTransform.sizeDelta = new Vector2(width, height);
						}

						newRect = new Rect(lastRect.x + lastRect.width + 10, lastRect.y + newRect.height * 2.5f, (EditorGUIUtility.currentViewWidth * .86f - lastRect.width), EditorGUIUtility.singleLineHeight);
						if (GUI.Button(newRect, "Edit Manually"))
						{
							Selection.activeGameObject = image.gameObject;
						}
					}
					else
					{
						GUI.enabled = false;

						GUI.enabled = true;
					}
					EditorGUI.indentLevel -= 1;
				}
			}
		}
	}
}