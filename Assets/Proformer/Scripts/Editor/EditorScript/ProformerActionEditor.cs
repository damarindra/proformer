﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

namespace DI.Proformer.Action {
	[CustomEditor(typeof(ProformerAction), true), CanEditMultipleObjects]
	public class ProformerActionEditor : Editor
	{
		ProformerAction action;
		string actionName;
		protected bool actionsAsBaseInspector = true;

		public virtual void OnEnable()
		{
			action = (ProformerAction)target;
			actionName = action.ToString();
			if (action.permissionAction.otherActions == null)
			{
				action.permissionAction.otherActions = new List<ProformerAction>();
				action.permissionAction.otherActionPermission = new List<bool>();
			}
		}

		public override void OnInspectorGUI()
		{
			EditorGUI.BeginChangeCheck();
			serializedObject.Update();

			EditorPrefs.SetInt("Action Menu " + actionName, GUILayout.Toolbar(EditorPrefs.GetInt("Action Menu " + actionName, 0), new string[3] { "Permissions", "Actions", "Input" }));

			if (EditorPrefs.GetInt("Action Menu " + actionName, 0) == 0)
			{
				EditorPrefs.SetBool("PermissionActionFold" + action.GetType().Name, EditorGUILayout.Foldout(EditorPrefs.GetBool("PermissionActionFold" + action.GetType().Name), "Can Execute While"));
				if (EditorPrefs.GetBool("PermissionActionFold" + action.GetType().Name))
				{
					EditorGUI.indentLevel += 1;
					Rect topRect = GUILayoutUtility.GetLastRect();
					EditorGUILayout.PropertyField(serializedObject.FindProperty("permissionAction").FindPropertyRelative("grounded"), GUILayout.Width(EditorGUIUtility.currentViewWidth - 150));
					EditorGUILayout.PropertyField(serializedObject.FindProperty("permissionAction").FindPropertyRelative("jumping"), GUILayout.Width(EditorGUIUtility.currentViewWidth - 150));
					EditorGUILayout.PropertyField(serializedObject.FindProperty("permissionAction").FindPropertyRelative("wallSliding"), GUILayout.Width(EditorGUIUtility.currentViewWidth - 150));
					EditorGUILayout.PropertyField(serializedObject.FindProperty("permissionAction").FindPropertyRelative("ladder"), GUILayout.Width(EditorGUIUtility.currentViewWidth - 150));
					EditorGUILayout.PropertyField(serializedObject.FindProperty("permissionAction").FindPropertyRelative("crouch"), GUILayout.Width(EditorGUIUtility.currentViewWidth - 150));

					for (int i = 0; i < action.permissionAction.otherActions.Count; i++)
					{
						if (action.permissionAction.otherActions[i] == null)
						{
							Debug.LogWarning("Some Other Actions is removed, re-gather called and reset other actions permission");
							GatherOtherActions();
							return;
						}

						var strings = action.permissionAction.otherActions[i].ToString().Split(new char[1] { '.' });
						string name = strings[strings.Length - 1];
						name = name.Remove(name.Length - 1);

						action.permissionAction.otherActionPermission[i] = EditorGUILayout.Toggle(name, action.permissionAction.otherActionPermission[i]);
					}

					if (GUI.Button(new Rect(topRect.x + EditorGUIUtility.currentViewWidth - 150, topRect.y + EditorGUIUtility.singleLineHeight * 1.4f, 100, topRect.height * 5), "Flush / Refresh\nCustom Action"))
					{
						GatherOtherActions();
					}
					EditorGUI.indentLevel -= 1;
				}
			}
			else if (EditorPrefs.GetInt("Action Menu " + actionName, 0) == 1)
				DrawActionInspector();
			else
				DrawInputInspector();

			if (action._killComponent)
			{
				DestroyImmediate(action);
				return;
			}

			if (EditorGUI.EndChangeCheck())
			{
				serializedObject.ApplyModifiedProperties();
				Repaint();
				EditorUtility.SetDirty(action);
			}
		}
		public virtual void DrawActionInspector()
		{
			if (actionsAsBaseInspector)
				base.OnInspectorGUI();
		}
		public virtual void DrawInputInspector()
		{
			EditorGUILayout.PropertyField(serializedObject.FindProperty("targetInput"));
		}
		protected void GatherOtherActions()
		{

			action.permissionAction.otherActions.Clear();
			action.permissionAction.otherActionPermission.Clear();

			var actions = action.GetComponents<ProformerAction>();
			for (int i = 0; i < action.GetComponents<ProformerAction>().Length; i++)
			{
				if (actions[i] == action || action.permissionAction.otherActions.Contains(action))
					continue;
				else
				{
					action.permissionAction.otherActions.Add(actions[i]);
					action.permissionAction.otherActionPermission.Add(false);
				}
			}
		}
	}
}