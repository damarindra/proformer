﻿using UnityEngine;
using UnityEditor;
using DI.Proformer.ToolHelper;

namespace DI.Proformer.Action
{
	[CustomEditor(typeof(MeleeAction)), CanEditMultipleObjects]
	public class MeleeEditor : ProformerActionEditor
	{
		public MeleeAction melee;
		private GUIContent removeButtonContent = new GUIContent("-", "delete");
		private GUIContent addButtonContent = new GUIContent("+", "add");

		private GUILayoutOption mediumLayoutButton = GUILayout.Width(50f);
		SerializedProperty list;

		Transform moveAttTriggerTarget = null;

		public override void OnEnable()
		{
			base.OnEnable();
			melee = (MeleeAction)target;
			actionsAsBaseInspector = false;
			foreach (Impactable impactable in melee.GetComponentsInChildren<Impactable>())
				impactable.editSources = false;
		}

		public override void OnInspectorGUI()
		{
			base.OnInspectorGUI();
		}

		public override void DrawActionInspector()
		{
			base.DrawActionInspector();
			EditorGUI.BeginChangeCheck();
			serializedObject.Update();
			EditorGUILayout.PropertyField(serializedObject.FindProperty("stopWhenAttacking"));
			EditorGUILayout.PropertyField(serializedObject.FindProperty("attackRestTime"));
			if (melee._killComponent && Event.current.type == EventType.Repaint)
			{
				DestroyImmediate(melee);
				return;
			}
			list = serializedObject.FindProperty("attackCombo");
			ShowList(list);

			if (EditorGUI.EndChangeCheck())
			{
				serializedObject.ApplyModifiedProperties();
				EditorUtility.SetDirty(melee);
			}
		}

		public void ShowList(SerializedProperty list, bool showListSize = true, bool showListLabel = true)
		{
			if (showListLabel)
			{
				GUILayout.BeginHorizontal();
				EditorGUILayout.PropertyField(list);
				list.arraySize = EditorGUILayout.IntField(list.arraySize);

				if (GUILayout.Button(addButtonContent, mediumLayoutButton))
				{
					melee.attackCombo.Add(new MeleeAction.AttackCombo());
					return;
				}
				GUILayout.EndHorizontal();
			}
			if (list.isExpanded)
			{
				EditorGUI.indentLevel += 1;
				if (showListSize && !showListLabel)
				{
					if (EditorGUILayout.PropertyField(list.FindPropertyRelative("Array.size")))
						return;
				}

				for (int i = 0; i < list.arraySize; i++)
				{
					EditorGUILayout.Space();
					EditorGUI.DrawRect(GUILayoutUtility.GetLastRect(), new Color(0, 0, 0, .3f));
					if (i >= melee.attackCombo.Count)
					{
						melee.attackCombo.Add(new MeleeAction.AttackCombo());
					}

					EditorGUILayout.BeginHorizontal();
					EditorPrefs.SetBool("Attack Combo Fold " + i.ToString(), EditorGUILayout.Foldout(EditorPrefs.GetBool("Attack Combo Fold " + i.ToString()), "Combo " + (i + 1)));
					//EditorGUILayout.LabelField("Combo " + (i + 1).ToString(), EditorStyles.boldLabel);
					if (GUILayout.Button(removeButtonContent, mediumLayoutButton))
					{
						if (melee.attackCombo[i].areaEffect != null)
						{
							DestroyImmediate(melee.attackCombo[i].areaEffect.gameObject);
						}
						list.DeleteArrayElementAtIndex(i);
						return;
					}
					EditorGUI.indentLevel += 1;
					EditorGUILayout.EndHorizontal();
					if (!EditorPrefs.GetBool("Attack Combo Fold " + i.ToString()))
					{
						EditorGUI.indentLevel -= 1;
						continue;
					}
					Undo.RecordObject(melee, "Attack Time");
					melee.attackCombo[i].attackTime = EditorGUILayout.FloatField(new GUIContent("Attack Time", "Total Attack Time"), melee.attackCombo[i].attackTime);

					Undo.RecordObject(melee, "Trigger Start");
					melee.attackCombo[i].triggerStartTime = EditorGUILayout.Slider(new GUIContent("Trigger Start", "time when Damage Trigger Start"), melee.attackCombo[i].triggerStartTime, 0, melee.attackCombo[i].attackTime);

					Undo.RecordObject(melee, "Next Attack");
					melee.attackCombo[i].transitionToNextAttack = EditorGUILayout.Slider(new GUIContent("Next Attack", "start time transition. Next attack can start atleast reach this time"), melee.attackCombo[i].transitionToNextAttack, melee.attackCombo[i].triggerStartTime, melee.attackCombo[i].attackTime);

					Undo.RecordObject(melee, "Trigger End");
					melee.attackCombo[i].triggerEndTime = EditorGUILayout.Slider(new GUIContent("Trigger End", "time when Damage trigger will end"), melee.attackCombo[i].triggerEndTime, melee.attackCombo[i].triggerStartTime, melee.attackCombo[i].attackTime);

					EditorGUILayout.BeginHorizontal();
					EditorGUILayout.PropertyField(serializedObject.FindProperty("attackCombo").GetArrayElementAtIndex(i).FindPropertyRelative("attackClip"));
					EditorGUILayout.EndHorizontal();

					if (melee.attackCombo[i].areaEffect == null)
					{
						EditorGUILayout.BeginHorizontal();
#if PROFORMER3D
						melee.attackCombo[i].areaEffect = (Collider)EditorGUILayout.ObjectField(new GUIContent("Area Effect", "Area Effect. Damage radius"), melee.attackCombo[i].areaEffect, typeof(Collider), true);
#else
						melee.attackCombo[i].areaEffect = (Collider2D)EditorGUILayout.ObjectField(new GUIContent("Area Effect", "Area Effect. Damage radius"), melee.attackCombo[i].areaEffect, typeof(Collider2D), true);
#endif

						int colCode = 0;
#if PROFORMER3D
						string[] colliderStrings = new string[4] { "Create", "Sphere", "Box", "Capsule" };
#else
						string[] colliderStrings = new string[4] { "Create", "Circle", "Box", "Polygon" };
#endif
						colCode = EditorGUILayout.Popup(colCode, colliderStrings, GUILayout.Width(100));
#if PROFORMER3D
						if (colCode == 1)
							melee.attackCombo[i].areaEffect = CreateCollider(typeof(SphereCollider), "AttackTrigger-" + i);
						else if (colCode == 2)
							melee.attackCombo[i].areaEffect = CreateCollider(typeof(BoxCollider), "AttackTrigger-" + i);
						else if (colCode == 3)
							melee.attackCombo[i].areaEffect = CreateCollider(typeof(CapsuleCollider), "AttackTrigger-" + i);
#else
						if (colCode == 1)
							melee.attackCombo[i].areaEffect = CreateCollider(typeof(CircleCollider2D), "AttackTrigger-" + i);
						else if (colCode == 2)
							melee.attackCombo[i].areaEffect = CreateCollider(typeof(BoxCollider2D), "AttackTrigger-" + i);
						else if (colCode == 3)
							melee.attackCombo[i].areaEffect = CreateCollider(typeof(PolygonCollider2D), "AttackTrigger-" + i);
#endif
						EditorGUILayout.EndHorizontal();
					}
					else
					{
						EditorGUILayout.PropertyField(serializedObject.FindProperty("attackCombo").GetArrayElementAtIndex(i).FindPropertyRelative("areaEffect"));
						EditorGUILayout.BeginHorizontal();
						GUILayout.Space(45);
						if (moveAttTriggerTarget == null)
						{
							if (GUILayout.Button("Move"))
							{
								moveAttTriggerTarget = melee.attackCombo[i].areaEffect.transform;
								melee.transform.position += Vector3.up;
								melee.transform.position -= Vector3.up;
							}
						}
						else
						{
							if (GUILayout.Button("Done"))
							{
								moveAttTriggerTarget = null;
								melee.transform.position += Vector3.up;
								melee.transform.position -= Vector3.up;
							}
						}
						if (GUILayout.Button("Options"))
						{
							melee.attackCombo[i].showOptions = !melee.attackCombo[i].showOptions;
						}

						EditorGUILayout.EndHorizontal();
						if (melee.attackCombo[i].showOptions)
						{
							EditorGUILayout.LabelField("Collider", EditorStyles.boldLabel);
#if PROFORMER3D

							if (melee.attackCombo[i].areaEffect.GetType() == typeof(BoxCollider))
								EditorInspectorHelper.BoxColliderInspect((BoxCollider)melee.attackCombo[i].areaEffect);
							else if (melee.attackCombo[i].areaEffect.GetType() == typeof(SphereCollider))
								EditorInspectorHelper.SphereColliderInspect((SphereCollider)melee.attackCombo[i].areaEffect);
							else if (melee.attackCombo[i].areaEffect.GetType() == typeof(SphereCollider))
								EditorInspectorHelper.CapsuleColliderInspect((CapsuleCollider)melee.attackCombo[i].areaEffect);
#else
							if (melee.attackCombo[i].areaEffect.GetType() == typeof(BoxCollider2D))
								EditorInspectorHelper.BoxColliderInspect((BoxCollider2D)melee.attackCombo[i].areaEffect);
							else if (melee.attackCombo[i].areaEffect.GetType() == typeof(CircleCollider2D))
								EditorInspectorHelper.CircleColliderInspect((CircleCollider2D)melee.attackCombo[i].areaEffect);
							else if (melee.attackCombo[i].areaEffect.GetType() == typeof(PolygonCollider2D))
								EditorInspectorHelper.PolygonColliderInspect((PolygonCollider2D)melee.attackCombo[i].areaEffect);
#endif
							EditorGUILayout.LabelField("Give Damage", EditorStyles.boldLabel);
							EditorInspectorHelper.GiveDamageInspector(melee.attackCombo[i].areaEffect.GetComponent<GiveDamage>());
							EditorGUILayout.LabelField("Impactable", EditorStyles.boldLabel);
							EditorInspectorHelper.ImpactableInspector(melee.attackCombo[i].areaEffect.GetComponent<Impactable>());
						}
					}
					EditorGUI.indentLevel -= 1;
				}

				EditorGUI.indentLevel -= 1;
			}
		}

		public virtual void OnSceneGUI()
		{
			if (moveAttTriggerTarget != null)
			{
				Vector3 newPos = Handles.PositionHandle(moveAttTriggerTarget.position, Quaternion.identity);
				if (newPos != moveAttTriggerTarget.position)
				{
					moveAttTriggerTarget.position = newPos;
				}
			}

			foreach (Impactable impactable in melee.GetComponentsInChildren<Impactable>())
				EditorInspectorHelper.ImpactableEditMode(impactable);
		}
#if PROFORMER3D
		Collider CreateCollider(System.Type type, string name)
#else
		Collider2D CreateCollider(System.Type type, string name)
#endif
		{
			GameObject _target = new GameObject(name);
			_target.AddComponent(type);
			_target.transform.parent = melee.transform;
			_target.AddComponent<GiveDamage>();
			_target.transform.localRotation = Quaternion.identity;
			_target.transform.localPosition = Vector3.right;
			Impactable impact = _target.AddComponent<Impactable>();
			impact.offset = Vector3.zero;
			_target.tag = "Melee";
#if PROFORMER3D
			_target.GetComponent<Collider>().isTrigger = false;
			return _target.GetComponent<Collider>();
#else
			_target.GetComponent<Collider2D>().isTrigger = true;
			return _target.GetComponent<Collider2D>();
#endif
		}
		/*
		public void DropAreaGUI(int index)
		{
			Event evt = Event.current;
			Rect drop_area = GUILayoutUtility.GetRect(0f, 20, GUILayout.ExpandWidth(true));
			if (melee.attackCombo[index].areaEffect == null)
				GUI.Box(drop_area, "None (Collider). Drag Here");
			else
			{
				string type = melee.attackCombo[index].areaEffect.GetType().ToString();
				type = type.Remove(0, 12);
				GUI.Box(drop_area, melee.attackCombo[index].areaEffect.name + " (" + type + ")");
			}

			switch (evt.type)
			{
				case EventType.DragUpdated:
				case EventType.DragPerform:
					if (!drop_area.Contains(evt.mousePosition))
						return;

					DragAndDrop.visualMode = DragAndDropVisualMode.Copy;

					if (evt.type == EventType.DragPerform)
					{
						DragAndDrop.AcceptDrag();

						foreach (Object dragged_object in DragAndDrop.objectReferences)
						{
							// Do On Drag Stuff here
							GameObject go = dragged_object as GameObject;
							if (go.GetComponent<Collider>())
								melee.attackCombo[index].areaEffect = go.GetComponent<Collider>();
						}
					}
					break;
			}
			
		}*/

	}
}

