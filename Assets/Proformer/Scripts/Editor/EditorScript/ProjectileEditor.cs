﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using DI.Proformer.ToolHelper;

namespace DI.Proformer {

	[CustomEditor(typeof(Projectile), true), CanEditMultipleObjects]
	public class ProjectileEditor : DIObjectEditor
	{
		Projectile p { get { return (Projectile)target; } }
		bool editImpactable = false;
		GiveDamage giveDamage;
		Impactable impactable;

		public override void OnEnable()
		{
			base.OnEnable();
			withoutDefault = true;
		}

		public override void CustomInspectorGUI()
		{
			base.CustomInspectorGUI();
			if (!impactable && p._collider != null)
				impactable = p._collider.GetComponent<Impactable>();
			if (!giveDamage && p._collider != null)
				giveDamage = p._collider.GetComponent<GiveDamage>();

			serializedObject.Update();

			if (p.rendererObject != null)
			{
				if (p.rendererObject.transform.parent == p.transform)
					GUI.enabled = false;
			}
			GUI.enabled = true;
			if (p.rendererObject != null)
			{

				if (p.rendererObject.transform.parent != p.transform)
				{
					if (GUILayout.Button("Apply"))
					{
						p.rendererObject = Instantiate(p.rendererObject, p.transform.position, Quaternion.identity) as GameObject;
						p.rendererObject.transform.parent = p.transform;
					}

				}
				else if (p.rendererObject.transform.parent == p.transform)
				{

					if (p.tag != p.rendererObject.tag)
						p.rendererObject.tag = p.tag;
				}
			}

			EditorGUILayout.Space();

			using (EditorGUILayoutGroup group = new EditorGUILayoutGroup(new GUIContent("Projectile Properties", "Basic Projectile Properties"), serializedObject.FindProperty("speed").isExpanded))
			{
				serializedObject.FindProperty("speed").isExpanded = group.isExpanding;
				if (group.isExpanding)
				{
					EditorGUILayout.PropertyField(serializedObject.FindProperty("speed"));
					EditorGUILayout.PropertyField(serializedObject.FindProperty("collideMask"));
					EditorGUILayout.PropertyField(serializedObject.FindProperty("horizontalRayCount"));
					EditorGUILayout.PropertyField(serializedObject.FindProperty("verticalRayCount"));
				}
			}

			if (p._collider)
			{
				bool giveDamageAvailable = giveDamage;
				giveDamageAvailable = EditorInspectorHelper.GiveDamageInspector(giveDamage, serializedObject.FindProperty("collideMask"));
				if (giveDamageAvailable && !giveDamage)
					p._collider.gameObject.AddComponent<GiveDamage>();
				else if (!giveDamageAvailable && giveDamage)
				{
					DestroyImmediate(giveDamage);
					giveDamage = null;
				}

				bool impactableAvailable = impactable;
				impactableAvailable = EditorInspectorHelper.ImpactableInspector(impactable, serializedObject.FindProperty("horizontalRayCount"));
				if (impactableAvailable && !impactable)
					p._collider.gameObject.AddComponent<Impactable>();
				else if (!impactableAvailable && impactable)
				{
					DestroyImmediate(impactable);
					giveDamage = null;
				}
			}

			serializedObject.ApplyModifiedProperties();
		}

		public override void OnSceneGUI()
		{
			base.OnSceneGUI();
			ImpactableEditMode();
		}


		void GiveDamageInspector()
		{
			EditorGUILayout.LabelField("Give Damage", EditorStyles.boldLabel);
			EditorGUI.indentLevel += 1;

			GiveDamage giveDamage = p._collider.GetComponent<GiveDamage>();
			EditorGUILayout.BeginHorizontal();
			bool anyGiveDamage = EditorGUILayout.Toggle("Give Damage Sources", giveDamage);
			EditorGUILayout.EndHorizontal();

			if (anyGiveDamage != p._collider.GetComponent<GiveDamage>())
			{
				if (anyGiveDamage)
					giveDamage = p._collider.gameObject.AddComponent<GiveDamage>();
				else
					DestroyImmediate(giveDamage);

				anyGiveDamage = giveDamage;
			}
			if (!anyGiveDamage)
			{
				GUI.enabled = false;
				EditorGUILayout.IntField("Damage Given", 0);
				EditorGUILayout.IntField("Freeze Time Given", 0);
			}
			else
			{
				Undo.RecordObject(giveDamage, "damageGiven");
				giveDamage.damageGiven = EditorGUILayout.IntField("Damage Given", giveDamage.damageGiven);
				Undo.RecordObject(giveDamage, "freezeTime");
				giveDamage.freezeTime = EditorGUILayout.FloatField("Freeze Time Given", giveDamage.freezeTime);
			}
			GUI.enabled = true;


			EditorGUI.indentLevel -= 1;

		}
		void ImpactableInspector()
		{
			EditorGUILayout.LabelField("Impactable", EditorStyles.boldLabel);
			EditorGUI.indentLevel += 1;

			Impactable impactable = p._collider.GetComponent<Impactable>();
			EditorGUILayout.BeginHorizontal();
			bool anyImpactable = EditorGUILayout.Toggle("Impactable Sources", impactable);
			EditorGUILayout.EndHorizontal();

			if (anyImpactable != impactable)
			{
				if (anyImpactable)
					impactable = p._collider.gameObject.AddComponent<Impactable>();
				else
					DestroyImmediate(impactable);

				anyImpactable = impactable;
			}
			if (!anyImpactable)
			{
				GUI.enabled = false;
				EditorGUILayout.IntField("Power", 0);
				EditorGUILayout.BeginHorizontal();
				EditorGUILayout.Vector3Field("Offset", Vector3.zero);
				GUILayout.Button(editImpactable ? "Done" : "Edit", GUILayout.Width(35));
				EditorGUILayout.EndHorizontal();
				EditorGUILayout.IntPopup("ImpactType", 0, new string[1] { "" }, new int[] { 0 });
			}
			else
			{
				//EditorGUILayout.PropertyField(serializedObject.FindProperty("bodyChar").FindPropertyRelative("_gd"));

				Undo.RecordObject(impactable, "Power");
				impactable.power = EditorGUILayout.FloatField("Power", impactable.power);
				EditorGUILayout.BeginHorizontal();
				Undo.RecordObject(impactable, "Offset");
				impactable.offset = EditorGUILayout.Vector3Field("Offset", impactable.offset);
				if (GUILayout.Button(editImpactable ? "Done" : "Edit", GUILayout.Width(35)))
					editImpactable = !editImpactable;
				EditorGUILayout.EndHorizontal();
				Undo.RecordObject(impactable, "ImpactType");
				impactable.impactType = (Impactable.ImpactType)EditorGUILayout.EnumPopup("Impact Type", impactable.impactType);
			}
			GUI.enabled = true;
			EditorGUI.indentLevel -= 1;
		}
		private void ImpactableEditMode()
		{
			if (!editImpactable)
				return;
			if (!p._collider)
			{
				Debug.LogError("Collider Not Found !!");
				return;
			}
			Impactable impactable = p._collider.GetComponent<Impactable>();
			if (editImpactable && impactable)
			{
				Handles.color = Color.magenta;
				Vector3 newPos = Handles.FreeMoveHandle(impactable.transform.position + impactable.offset, Quaternion.identity, HandleUtility.GetHandleSize(impactable.transform.position + impactable.offset) * .3f, InternalTool.GetSnapValue(), Handles.SphereHandleCap);
				if (newPos != impactable.offset - impactable.transform.position)
				{
					impactable.offset = newPos - impactable.transform.position;
				}
				if (impactable.impactType == Impactable.ImpactType.LOCAL)
				{
					Handles.color = Color.magenta;
					Vector3 dir = (impactable.offset * -1).normalized;
					if (dir != Vector3.zero)
						Handles.ArrowHandleCap(0, impactable.offset + impactable.transform.position, Quaternion.LookRotation(dir), .5f, EventType.Repaint);
				}
			}
		}
	}
}