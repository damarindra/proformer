﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using DI.Proformer.Manager;
using DI.Proformer.ToolHelper;
using System.IO;
using UnityEditorInternal;
using System;

namespace DI.Proformer
{
	[CustomEditor(typeof(GameManager))]
	public class GameManagerEditor : Editor
	{

		public GameManager gm;

		private GUIContent removeButtonContent = new GUIContent("-", "delete");
		private GUIContent addButtonContent = new GUIContent("+", "add");

		ReorderableList checkPointList;

		void OnEnable()
		{
			gm = (GameManager)target;

			checkPointList = new ReorderableList(serializedObject, serializedObject.FindProperty("checkPoints"), true, true, true, true);
			checkPointList.drawHeaderCallback = ((Rect rect) => {
				EditorGUI.LabelField(rect, "Check Points");
			});
			checkPointList.drawElementCallback = ((Rect rect, int index, bool isActive, bool isFocused) => {
				EditorGUI.PropertyField(new Rect(rect.x, rect.y + EditorGUIUtility.standardVerticalSpacing / 2, rect.width, EditorGUIUtility.singleLineHeight), serializedObject.FindProperty("checkPoints").GetArrayElementAtIndex(index));
			});
			checkPointList.onAddCallback = ((ReorderableList list) => {
				CheckPoint cp = Instantiate(gm.checkPointTemplate).GetComponent<CheckPoint>();
				cp.name = "Check Point - " + list.count.ToString();
				cp.transform.parent = gm.transform;
				gm.checkPoints.Add(cp);
			});
			checkPointList.onRemoveCallback = ((ReorderableList list) =>
			{
				if (gm.checkPoints[list.index] != null)
					DestroyImmediate(((CheckPoint)serializedObject.FindProperty("checkPoints").GetArrayElementAtIndex(list.index).objectReferenceValue).gameObject);
				gm.checkPoints.RemoveAt(list.index);
			});
		}
		public override void OnInspectorGUI()
		{
			EditorGUI.BeginChangeCheck();
			serializedObject.Update();

			EditorGUILayout.Space();

			using (EditorGUILayoutGroup group = new EditorGUILayoutGroup(new GUIContent("Game Data", "Configuring all game data, such Scene Type, BGM, Pooling Object, Check Point Type"), "Game Data Inspector"))
			{
				EditorGUILayout.BeginHorizontal();
				GUI.enabled = false;
				EditorGUILayout.ObjectField("Game Data", GameData.Instance, typeof(GameData), false);
				GUI.enabled = true;
				
				if(GameData.Instance)
				{
					if (GUILayout.Button("Edit"))
					{
						Selection.activeObject = GameData.Instance;
					}
				}
				EditorGUILayout.EndHorizontal();
				if (GameData.Instance != null)
				{
					if (!GameData.Instance.scenes.Contains(SceneManagerTools.GetCurrentSceneName()))
					{
						Rect warningBox = GUILayoutUtility.GetRect(new GUIContent(SceneManagerTools.GetCurrentSceneName() + " scene is not set or Disable at Build Settings. Please Add to Build Settings"), EditorStyles.helpBox);
						EditorGUI.HelpBox(new Rect(warningBox.x, warningBox.y, warningBox.width - 100, warningBox.height), SceneManagerTools.GetCurrentSceneName() + " scene is not set or Disable at Build Settings. Please Add to Build Settings", MessageType.Warning);
						if (GUI.Button(new Rect(warningBox.x + warningBox.width - 100, warningBox.y, 100, warningBox.height), "Build Settings"))
							EditorWindow.GetWindow(Type.GetType("UnityEditor.BuildPlayerWindow,UnityEditor"));
					}
				}

				if (GameData.Instance == null)
					return;

				if (gm.showCheckPointProperty)
				{
					GameData.Instance.checkPointType = (GameData.CheckPointType)EditorGUILayout.EnumPopup("Check Point Type", GameData.Instance.checkPointType);
					if (Application.isPlaying)
					{
						GUI.enabled = false;
						gm.currentCheckPoint = (CheckPoint)EditorGUILayout.ObjectField("Current Check Point", gm.currentCheckPoint, typeof(CheckPoint), true);
						GUI.enabled = true;
					}
					else if (GameData.Instance.checkPointType == GameData.CheckPointType.Placing)
					{
						if (Application.isPlaying)
						{
							GUI.enabled = false;
							EditorGUILayout.PropertyField(serializedObject.FindProperty("currentCheckPoint"));
							GUI.enabled = true;
						}
						EditorGUILayout.BeginHorizontal();
						gm.checkPointTemplate = (GameObject)EditorGUILayout.ObjectField("Check Point Object", gm.checkPointTemplate, typeof(GameObject), false);
						if (gm.checkPointTemplate == null)
						{
							if (GUILayout.Button("Create"))
							{
								if (GameManager.Instance.player == null)
								{
									GameManager.Instance.player = FindObjectOfType<DIPlayer>();
									if (GameManager.Instance.player == null)
									{
										Debug.LogWarning("This scene not contains Player, Please Add a Player GameObject to the Scene!");
									}
								}
								if (GameManager.Instance.player)
								{
									gm.checkPointTemplate = CheckPoint.CreateCheckPoint(GameManager.Instance.player.controller);
									gm.checkPointTemplate.name = "CP-Template(Disabled When Play Mode)";
									gm.checkPointTemplate.transform.parent = gm.transform;
								}
							}
						}
						EditorGUILayout.EndHorizontal();
						if (gm.checkPointTemplate == null)
							EditorGUILayout.HelpBox("Please fill or create Check Point Object", MessageType.Error);

						if (gm.checkPointTemplate != null)
						{
							if (Application.isPlaying)
								gm.checkPointTemplate.SetActive(false);

							if (!gm.checkPointTemplate.GetComponent<CheckPoint>())
							{
								gm.checkPointTemplate = null;
							}
							//int checkPointCount = checkPoints.count;
							if (gm.checkPointTemplate != null)
							{
								checkPointList.DoLayoutList();
							}
						}
					}
				}
			}
			EditorGUILayout.Space();

			if (EditorGUI.EndChangeCheck())
			{
				serializedObject.ApplyModifiedProperties();
				EditorUtility.SetDirty(gm);
				EditorUtility.SetDirty(GameData.Instance);
			}
		}

		void RefreshCheckPoint()
		{
			CheckPoint[] cp = gm.GetComponentsInChildren<CheckPoint>();
			foreach (CheckPoint c in cp)
			{
				foreach (CheckPoint _c in gm.checkPoints)
				{
					if (c.Equals(_c))
						goto cont;
				}
				DestroyImmediate(c.gameObject);
				cont:
				continue;
			}
		}


		void ShowCheckPoints(SerializedProperty list)
		{
			EditorGUILayout.BeginHorizontal();
			EditorGUI.indentLevel += 1;

			EditorGUILayout.PropertyField(list);
			if (GUILayout.Button(addButtonContent))
			{
				CheckPoint cp = Instantiate(gm.checkPointTemplate).GetComponent<CheckPoint>();
				cp.name = "Check Point - " + list.arraySize.ToString();
				cp.transform.parent = gm.transform;
				gm.checkPoints.Add(cp);
			}

			EditorGUILayout.EndHorizontal();

			if (list.isExpanded)
			{
				EditorGUI.indentLevel += 1;
				for (int i = 0; i < list.arraySize; i++)
				{
					EditorGUILayout.BeginHorizontal();
					EditorGUILayout.PropertyField(list.GetArrayElementAtIndex(i));


					if (GUILayout.Button(removeButtonContent, GUILayout.Width(50)))
					{
						if (gm.checkPoints[i] != null)
						{
							GameObject g = gm.checkPoints[i].gameObject;
							DestroyImmediate(g);
						}
						gm.checkPoints.RemoveAt(i);
					}

					EditorGUILayout.EndHorizontal();
				}
				EditorGUI.indentLevel -= 1;

			}
			EditorGUI.indentLevel -= 1;
		}

	}
}
