﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using UnityEditorInternal;

namespace DI.Proformer.Manager {

	[CustomEditor(typeof(LevelBounds))]
	public class LevelBoundsEditor : Editor
	{

		LevelBounds levelBounds;
		private static GUIContent removeButtonContent = new GUIContent("-", "delete");
		private static GUILayoutOption miniLayoutButton = GUILayout.Width(20f);

		private ReorderableList paths;

		int controlID;

		public void OnEnable()
		{
			levelBounds = (LevelBounds)target;
			levelBounds.GatherLevelPath();

			controlID = GUIUtility.GetControlID(FocusType.Passive);
			//pathsSP = serializedObject.FindProperty("path");
			paths = new ReorderableList(serializedObject, serializedObject.FindProperty("points"), true, true, true, true);
			paths.drawHeaderCallback = (Rect rect) =>
			{
				EditorGUI.LabelField(rect, "Points");
			};
			paths.drawElementCallback =
				(Rect rect, int index, bool isActive, bool isFocused) => {
					var element = paths.serializedProperty.GetArrayElementAtIndex(index);
					rect.y += 2;
					//EditorGUI.PropertyField(rect, element, true);
					EditorGUI.LabelField(new Rect(rect.x, rect.y, rect.width / 4, EditorGUIUtility.singleLineHeight), "Point " + index.ToString());
					EditorGUI.LabelField(new Rect(rect.x + rect.width / 4, rect.y, 25, EditorGUIUtility.singleLineHeight), "X ");
					float x = EditorGUI.FloatField(new Rect(rect.x + rect.width / 4 + 25, rect.y, rect.width / 4, EditorGUIUtility.singleLineHeight), element.vector3Value.x);
					EditorGUI.LabelField(new Rect(rect.x + (rect.width / 4) * 2 + 25, rect.y, 25, EditorGUIUtility.singleLineHeight), "  Z ");
					float z = EditorGUI.FloatField(new Rect(rect.x + (rect.width / 4) * 2 + 50, rect.y, rect.width / 4, EditorGUIUtility.singleLineHeight), element.vector3Value.z);
					levelBounds.points[index] = new Vector3(x, levelBounds.points[index].y, z);
				};


			//SceneView.onSceneGUIDelegate += LevelBoundsUpdate;
		}

		public void OnDisable()
		{
			//SceneView.onSceneGUIDelegate -= LevelBoundsUpdate;
			levelBounds.GatherLevelPath();
		}

		public override void OnInspectorGUI()
		{
			EditorGUI.BeginChangeCheck();
			serializedObject.Update();
			EditorGUILayout.PropertyField(serializedObject.FindProperty("defaultLevelPathIndex"));
			if (serializedObject.FindProperty("defaultLevelPathIndex").intValue < 0)
				serializedObject.FindProperty("defaultLevelPathIndex").intValue = 0;
			else if (serializedObject.FindProperty("defaultLevelPathIndex").intValue >= levelBounds.levelPath.Count && levelBounds.loop)
				serializedObject.FindProperty("defaultLevelPathIndex").intValue = levelBounds.levelPath.Count - 1;
			else if (serializedObject.FindProperty("defaultLevelPathIndex").intValue >= levelBounds.levelPath.Count - 1 && !levelBounds.loop)
				serializedObject.FindProperty("defaultLevelPathIndex").intValue = levelBounds.levelPath.Count - 1;
			GUILayout.BeginHorizontal();
			GUILayout.Label("Connect Last");
			levelBounds.loop = EditorGUILayout.Toggle(levelBounds.loop);
			GUILayout.EndHorizontal();
			EditorGUILayout.PropertyField(serializedObject.FindProperty("lineColor"));
			EditorGUILayout.PropertyField(serializedObject.FindProperty("top"));
			EditorGUILayout.PropertyField(serializedObject.FindProperty("bottom"));
			EditorGUILayout.PropertyField(serializedObject.FindProperty("topBound"));
			EditorGUILayout.PropertyField(serializedObject.FindProperty("bottomBound"));
			
			paths.DoLayoutList();
			if (GUILayout.Button("Align Entity"))
			{
				Undo.RecordObjects(FindObjectsOfType<Transform>(), "Undo Object");
				foreach (DIEntity cb in GameObject.FindObjectsOfType<DIEntity>())
				{
					cb.transform.position = cb.position.GetPositionFromRaw(cb.position.rawHorizontal, cb.position.rawVertical, cb.position.levelPath.index);
				}
			}
			if (EditorGUI.EndChangeCheck())
				serializedObject.ApplyModifiedProperties();
			SceneView.RepaintAll();

		}

		void UpdatePathsIfChanged()
		{
			if (levelBounds.levelPath.Count == 0)
				levelBounds.GatherLevelPath();

			for (int i = 0; i < paths.count; i++)
			{
				paths.serializedProperty.GetArrayElementAtIndex(i).vector3Value = EditorGUILayout.Vector3Field("Path-" + (i + 1).ToString(), paths.serializedProperty.GetArrayElementAtIndex(i).vector3Value);
				if (GUILayout.Button(removeButtonContent, miniLayoutButton))
				{
					paths.serializedProperty.DeleteArrayElementAtIndex(i);
				}
				//EditorGUILayout.PropertyField(list.GetArrayElementAtIndex(i));
			}
		}

		public virtual void OnSceneGUI()
		{
			if (Application.isPlaying)
				return;

			Event e = Event.current;


			int i = 0;
			while (i < paths.count)
			{
				Vector3 currentPath = paths.serializedProperty.GetArrayElementAtIndex(i).vector3Value;
				if (currentPath.y != (levelBounds.top + levelBounds.bottom) / 2)
				{
					currentPath.y = (levelBounds.top + levelBounds.bottom) / 2;
					levelBounds.points[i] = currentPath;
					levelBounds.GatherLevelPath();
				}

				Handles.color = Color.green;

				if (i != paths.serializedProperty.arraySize - 1)
					Handles.DrawDottedLine(currentPath, paths.serializedProperty.GetArrayElementAtIndex(i + 1).vector3Value, 2);

				if (e.alt)
					break;

				if (i < levelBounds.levelPath.Count && ProformerSettings.showLevelBoundsInfo)
				{
					if (i == levelBounds.levelPath.Count - 1 && levelBounds.loop)
					{
						//Handles.Label(currentPath + levelBounds.levelPath[i].direction * levelBounds.levelPath[i].length / 3, "Level Path : " + i.ToString(), EditorStyles.helpBox);
						Handles.BeginGUI();
						Vector2 worldToCam = Camera.current.WorldToScreenPoint(LevelBounds.instance.points[i] + LevelBounds.instance.levelPath[i].direction * LevelBounds.instance.levelPath[i].length / 3);
						worldToCam.y = Camera.current.pixelHeight - worldToCam.y;
						if (GUI.Button(new Rect(worldToCam, new Vector2(100, 20)), "Level Path : " + i.ToString())) {
							levelBounds.defaultLevelPathIndex = i;
							EditorUtility.SetDirty(levelBounds);
						}
						Handles.EndGUI();
					}
					else {
						//Handles.Label(currentPath + levelBounds.levelPath[i].direction * levelBounds.levelPath[i].length / 3, "Level Path : " + i.ToString(), EditorStyles.helpBox);
						Handles.BeginGUI();
						Vector3 worldPos = LevelBounds.instance.points[i] + LevelBounds.instance.levelPath[i].direction * LevelBounds.instance.levelPath[i].length / 3;
						worldPos.y = LevelBounds.instance.top;
						Vector2 worldToCam = Camera.current.WorldToScreenPoint(worldPos);
						worldToCam.y = Camera.current.pixelHeight - worldToCam.y;
						if (GUI.Button(new Rect(worldToCam, new Vector2(100, 20)), "Level Path : " + i.ToString()))
						{
							levelBounds.defaultLevelPathIndex = i;
							EditorUtility.SetDirty(levelBounds);
						}
						Handles.EndGUI();
					}

				}
				Vector3 newPathPos = Handles.FreeMoveHandle(currentPath, Quaternion.identity, .4f, InternalTool.GetSnapValue(), Handles.SphereHandleCap);
				if (currentPath != newPathPos && !e.alt)
				{
					Undo.RecordObject(levelBounds, "Move");
					newPathPos.y = (levelBounds.top + levelBounds.bottom) / 2;
					levelBounds.points[i] = newPathPos;
					levelBounds.GatherLevelPath();
					return;
				}

				i++;
			}

			if (e.control)
			{
				if (e.GetTypeForControl(controlID) == EventType.MouseDown)
				{
					Undo.RecordObject(levelBounds, "Create");
					Vector3 dir = (levelBounds.points[levelBounds.points.Count - 1] - levelBounds.points[levelBounds.points.Count - 2]).normalized;
					levelBounds.points.Add(levelBounds.points[levelBounds.points.Count - 1] + dir * 4);
					levelBounds.GatherLevelPath();
					return;
				}
			}
			else if (e.alt)
			{
				i = 0;
				while (i < paths.count)
				{
					Vector3 currentPath = paths.serializedProperty.GetArrayElementAtIndex(i).vector3Value;
					if (Vector2.Distance((Vector2)Camera.current.WorldToScreenPoint(currentPath), new Vector2(e.mousePosition.x, Camera.current.pixelHeight - e.mousePosition.y)) < 10f)
					{
						Handles.color = Color.red;
						if (e.GetTypeForControl(controlID) == EventType.MouseDown)
						{
							Undo.RecordObject(levelBounds, "Delete");
							Debug.Log("Delete Level Path index : " + i);
							levelBounds.points.RemoveAt(i);
							levelBounds.GatherLevelPath();
							return;
						}
					}
					else
					{
						Handles.color = Color.blue;
					}
					Handles.DrawSolidArc(currentPath, (Camera.current.transform.position - currentPath).normalized, currentPath - Vector3.right, 360, .2f);

					i++;
				}
			}

		}
	}
}