﻿using UnityEngine;
using UnityEditor;

namespace DI.Proformer {

	//[CustomEditor(typeof(Pickable)), CanEditMultipleObjects]
	public class PickableEditor : Editor
	{

		public override void OnInspectorGUI()
		{
			EditorGUI.BeginChangeCheck();
			base.OnInspectorGUI();
			EditorGUILayout.Space();

			/*
			using (EditorGUILayoutGroup group = new EditorGUILayoutGroup(new GUIContent(serializedObject.FindProperty("varOperator").displayName), serializedObject.FindProperty("varOperator").isExpanded))
			{
				if (group.isExpanding)
				{
					EditorGUILayout.PropertyField(serializedObject.FindProperty("varOperator").FindPropertyRelative("targetProperty"));
					EditorGUILayout.PropertyField(serializedObject.FindProperty("varOperator").FindPropertyRelative("_operator"));
					EditorGUILayout.PropertyField(serializedObject.FindProperty("varOperator").FindPropertyRelative("_intValue"));
					EditorGUILayout.PropertyField(serializedObject.FindProperty("varOperator").FindPropertyRelative("_floatValue"));
					EditorGUILayout.PropertyField(serializedObject.FindProperty("varOperator").FindPropertyRelative("_stringValue"));
				}
				serializedObject.FindProperty("varOperator").isExpanded = group.isExpanding;
			}
			*/

			if (EditorGUI.EndChangeCheck())
			{
				serializedObject.ApplyModifiedProperties();
				EditorUtility.SetDirty((Pickable)target);
			}
		}
	}
}
