﻿using UnityEngine;
using System.Collections;
using UnityEditor;

namespace DI.Proformer {
	[CustomEditor(typeof(Impactable)), CanEditMultipleObjects]
	public class ImpactableEditor : Editor
	{
		public Impactable impactable;

		bool editMode = false;

		public void OnEnable()
		{
			impactable = (Impactable)target;
			if (Application.isPlaying)
				return;
			SceneView.onSceneGUIDelegate += ImpactableUpdate;

		}

		public void OnDisable()
		{
			SceneView.onSceneGUIDelegate -= ImpactableUpdate;
			if (Application.isPlaying)
				return;
		}

		public override void OnInspectorGUI()
		{
			base.OnInspectorGUI();
			if (impactable == null)
				return;
			string mode = "Edit Source Impact";
			if (editMode)
				mode = "Done";
			if (GUILayout.Button(mode))
			{
				editMode = !editMode;
				Tools.current = editMode ? Tool.None : Tool.Move;
			}

		}

		private void ImpactableUpdate(SceneView sceneView)
		{
			if (impactable == null)
				return;

			if (editMode)
			{
				Handles.color = Color.magenta;
				//Vector3 newPos = Handles.FreeMoveHandle(impactable.transform.position + impactable.offset, Quaternion.identity, HandleUtility.GetHandleSize(impactable.transform.position + impactable.offset) * .3f, InternalTool.GetSnapValue(), Handles.SphereCap);
				Vector3 newPos = Handles.PositionHandle(impactable.transform.position + impactable.offset, Quaternion.identity);
				if (newPos != impactable.offset - impactable.transform.position)
				{
					Undo.RecordObject(impactable, "Edit Offset");
					impactable.offset = newPos - impactable.transform.position;
				}
			}
			if (impactable.impactType == Impactable.ImpactType.LOCAL)
			{
				Handles.color = Color.magenta;
				Vector3 dir = (impactable.offset * -1).normalized;
				if (dir != Vector3.zero)
					Handles.ArrowHandleCap(0, impactable.offset + impactable.transform.position, Quaternion.LookRotation(dir), .5f, EventType.Repaint);
			}
		}

	}
}