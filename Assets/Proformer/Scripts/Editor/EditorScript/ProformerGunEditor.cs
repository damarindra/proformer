﻿using UnityEngine;
using UnityEditor;

namespace DI.Proformer.Action
{
	[CustomEditor(typeof(GunAction), true), CanEditMultipleObjects]
	public class ProformerGunEditor : ProformerActionEditor
	{
		public GunAction gun;
		bool editPositionProjectile = false;

		public override void OnEnable()
		{
			base.OnEnable();
			gun = (GunAction)target;
			actionsAsBaseInspector = false;
		}

		public override void OnInspectorGUI()
		{
			base.OnInspectorGUI();
		}

		void OnSceneGUI()
		{
			if (editPositionProjectile)
			{
				Vector3 newPosition = Handles.PositionHandle(gun.transform.position + gun.transform.TransformDirection(gun.projectilePosition), gun.transform.rotation);
				gun.projectilePosition = gun.transform.InverseTransformDirection(newPosition - gun.transform.position);
			}
		}


		public override void DrawActionInspector()
		{
			base.DrawActionInspector();
			EditorGUI.BeginChangeCheck();
			serializedObject.Update();
			EditorGUILayout.PropertyField(serializedObject.FindProperty("projectile"));
			EditorGUILayout.BeginHorizontal();
			EditorGUILayout.PropertyField(serializedObject.FindProperty("projectilePosition"));
			if (GUILayout.Button(editPositionProjectile ? "Done" : "Move", GUILayout.Width(60)))
			{
				editPositionProjectile = !editPositionProjectile;
			}
			EditorGUILayout.EndHorizontal();
			EditorGUILayout.PropertyField(serializedObject.FindProperty("projectilePoolSize"));
			EditorGUILayout.PropertyField(serializedObject.FindProperty("continuous"));

			if (gun.continuous)
				GUI.enabled = true;
			else
				GUI.enabled = false;
			EditorGUILayout.PropertyField(serializedObject.FindProperty("fireRate"));
			GUI.enabled = true;
			EditorGUILayout.PropertyField(serializedObject.FindProperty("unlimitedAmmo"));
			if (serializedObject.FindProperty("unlimitedAmmo").boolValue == true && serializedObject.FindProperty("canReload").boolValue == true)
				serializedObject.FindProperty("canReload").boolValue = false;
			EditorGUILayout.PropertyField(serializedObject.FindProperty("canReload"));
			EditorGUILayout.PropertyField(serializedObject.FindProperty("reloadTime"));
			EditorGUILayout.PropertyField(serializedObject.FindProperty("autoReload"));
			EditorGUILayout.PropertyField(serializedObject.FindProperty("maxEachMagazine"));
			EditorGUILayout.PropertyField(serializedObject.FindProperty("totalMagazine"));
			if (serializedObject.FindProperty("maxEachMagazine").intValue > serializedObject.FindProperty("totalMagazine").intValue)
				serializedObject.FindProperty("maxEachMagazine").intValue = serializedObject.FindProperty("totalMagazine").intValue;

			EditorGUILayout.PropertyField(serializedObject.FindProperty("shootClip"));

			if (EditorGUI.EndChangeCheck())
			{
				serializedObject.ApplyModifiedProperties();
				EditorUtility.SetDirty(gun);
			}
		}

		public override void DrawInputInspector()
		{
			base.DrawInputInspector();
			EditorGUI.BeginChangeCheck();

			if (serializedObject.FindProperty("canReload").boolValue)
				EditorGUILayout.PropertyField(serializedObject.FindProperty("reloadInput"));

			if (EditorGUI.EndChangeCheck())
			{
				serializedObject.ApplyModifiedProperties();
				EditorUtility.SetDirty(gun);
			}
		}
	}
}