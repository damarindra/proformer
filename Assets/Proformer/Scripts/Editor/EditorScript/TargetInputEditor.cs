﻿using UnityEngine;
using UnityEditor;
using DI.Proformer.ToolHelper;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace DI.Proformer {
	[CustomPropertyDrawer(typeof(TargetInput), true)]
	public class TargetInputDrawer : DIBoxDrawer
	{

		Rect targetRect, scriptRect, inputRect, titleRect, inputTriggerRect;

		public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
		{
			titleRect = new Rect(position.x, position.y, position.width, EditorGUIUtility.singleLineHeight);
			targetRect = new Rect(position.x, titleRect.y + EditorGUIUtility.singleLineHeight * 1.1f, position.width, EditorGUIUtility.singleLineHeight);
			scriptRect = new Rect(position.x, targetRect.y + EditorGUIUtility.singleLineHeight * 1.1f, position.width, EditorGUIUtility.singleLineHeight);
			inputRect = new Rect(position.x, scriptRect.y + EditorGUIUtility.singleLineHeight * 1.1f, position.width, EditorGUIUtility.singleLineHeight);
			inputTriggerRect = new Rect(position.x, inputRect.y + EditorGUIUtility.singleLineHeight * 1.1f, position.width, EditorGUIUtility.singleLineHeight);
			spaceBottom = EditorGUIUtility.standardVerticalSpacing;
			if (property.FindPropertyRelative("hideInputTrigger").boolValue)
				totalRect = new Rect(titleRect.x, titleRect.y, titleRect.width, inputRect.y - titleRect.y + inputRect.height + EditorGUIUtility.standardVerticalSpacing);
			else
				totalRect = new Rect(titleRect.x, titleRect.y, titleRect.width, inputTriggerRect.y - titleRect.y + inputTriggerRect.height + EditorGUIUtility.standardVerticalSpacing);
			Draw(property, totalRect, true, true, true);

			ShowValue(property);
		}

		void ShowValue(SerializedProperty value)
		{
			if (value.isExpanded)
			{
				GameObject oldGO = value.FindPropertyRelative("target").objectReferenceValue as GameObject;
				EditorGUI.PropertyField(targetRect, value.FindPropertyRelative("target"));
				if (value.FindPropertyRelative("target").objectReferenceValue as GameObject != oldGO)
					value.FindPropertyRelative("script").objectReferenceValue = null;

				if (value.FindPropertyRelative("target").objectReferenceValue != null)
				{
					//SCRIPT INFO
					MonoBehaviour oldMono = value.FindPropertyRelative("script").objectReferenceValue as MonoBehaviour;
					List<MonoBehaviour> monoBehaviours = ((GameObject)value.FindPropertyRelative("target").objectReferenceValue).GetComponents<MonoBehaviour>().ToList();
					if (monoBehaviours.Count > 0)
					{
						if (value.FindPropertyRelative("script").objectReferenceValue as MonoBehaviour == null && monoBehaviours.Count != 0)
							value.FindPropertyRelative("script").objectReferenceValue = monoBehaviours[0];
						int iMonoScript = monoBehaviours.IndexOf(value.FindPropertyRelative("script").objectReferenceValue as MonoBehaviour);
						iMonoScript = EditorGUI.Popup(scriptRect, "Script", iMonoScript, monoBehaviours.Select(i => i.ToString()).ToArray());
						if (iMonoScript < monoBehaviours.Count)
						{
							if (value.FindPropertyRelative("script").objectReferenceValue as MonoBehaviour != monoBehaviours[iMonoScript])
								value.FindPropertyRelative("script").objectReferenceValue = monoBehaviours[iMonoScript];
						}
						else
							iMonoScript = 0;
						if (value.FindPropertyRelative("script").objectReferenceValue as MonoBehaviour != oldMono)
							value.FindPropertyRelative("inputName").stringValue = "";
					}
					else
						value.FindPropertyRelative("script").objectReferenceValue = null;


					if (value.FindPropertyRelative("script").objectReferenceValue != null)
					{
						//FIELD INFO
						List<string> infos = value.FindPropertyRelative("script").objectReferenceValue.GetType().GetFields(BindingFlags.Public | BindingFlags.Instance).ToList().Select(i => i.Name.ToString()).ToList();

						int totalRemoved = 0;
						List<string> types = value.FindPropertyRelative("script").objectReferenceValue.GetType().GetFields(BindingFlags.Public | BindingFlags.Instance).ToList().Select(i => i.FieldType.ToString()).ToList();
						for (int i = 0; i < types.Count; i++)
						{
							string[] typeNames = types[i].Split(new char[1] { '.' });
							string result = typeNames[typeNames.Length - 1];
							if (result.Substring(result.Length - 1) == "]")
								result.Remove(result.Length - 1);
							if (!result.Contains("StandartInput"))
							{
								infos.RemoveAt(i - totalRemoved);
								totalRemoved += 1;
							}
						}


						if (infos.Count > 0)
						{
							if ((value.FindPropertyRelative("inputName").stringValue == null || value.FindPropertyRelative("inputName").stringValue == "") && infos.Count != 0)
								value.FindPropertyRelative("inputName").stringValue = infos[0].ToString();
							int iInfos = infos.IndexOf(value.FindPropertyRelative("inputName").stringValue);
							iInfos = EditorGUI.Popup(inputRect, "Input", iInfos, infos.ToArray());
							if (iInfos < infos.Count && iInfos != -1)
							{
								if (value.FindPropertyRelative("inputName").stringValue != infos[iInfos])
								{
									value.FindPropertyRelative("inputName").stringValue = infos[iInfos];
								}
							}
							else
								iInfos = 0;
						}
						else
						{
							value.FindPropertyRelative("inputName").stringValue = "";
							EditorGUI.Popup(inputRect, "Input", 0, new string[] { "" });
						}

					}
				}
				else
				{
					GUI.enabled = false;
					value.FindPropertyRelative("script").objectReferenceValue = EditorGUI.ObjectField(scriptRect, "Script", value.FindPropertyRelative("script").objectReferenceValue, typeof(MonoBehaviour), true) as MonoBehaviour;
					//value.FindPropertyRelative("typeAllowed").stringValue = EditorGUI.TextField(allowedType, "Type", value.FindPropertyRelative("typeAllowed").stringValue);
					EditorGUI.Popup(inputRect, "Input", 0, new string[] { "" });
					GUI.enabled = true;
				}
				if (!value.FindPropertyRelative("hideInputTrigger").boolValue)
					EditorGUI.PropertyField(inputTriggerRect, value.FindPropertyRelative("inputTrigger"));
			}

		}
	}
}
