﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System.Collections.Generic;
using System;
using DI.Proformer.Manager;

namespace DI.Proformer.ToolHelper {
	[InitializeOnLoad]
	public class HierarchyIcon {

		static Texture2D characterIcon, cameraIcon, managerIcon, levelBoundIcon, groundIcon, pickableIcon, uiIcon, parallaxIcon;
		static List<int> characterId, cameraId, groundId, pickableId, managerId, levelBoundId, uiId, parallaxId;

		static HierarchyIcon() {
			characterIcon = AssetDatabase.LoadAssetAtPath("Assets/Proformer/Resources/Images/charactericon.png", typeof(Texture2D)) as Texture2D;
			cameraIcon = AssetDatabase.LoadAssetAtPath("Assets/Proformer/Resources/Images/cameraIcon.png", typeof(Texture2D)) as Texture2D;
			managerIcon = AssetDatabase.LoadAssetAtPath("Assets/Proformer/Resources/Images/managerIcon.png", typeof(Texture2D)) as Texture2D;
			levelBoundIcon = AssetDatabase.LoadAssetAtPath("Assets/Proformer/Resources/Images/levelBoundsIcon.png", typeof(Texture2D)) as Texture2D;
			groundIcon = AssetDatabase.LoadAssetAtPath("Assets/Proformer/Resources/Images/groundsIcon.png", typeof(Texture2D)) as Texture2D;
			pickableIcon = AssetDatabase.LoadAssetAtPath("Assets/Proformer/Resources/Images/pickableIcon.png", typeof(Texture2D)) as Texture2D;
			uiIcon = AssetDatabase.LoadAssetAtPath("Assets/Proformer/Resources/Images/uiIcon.png", typeof(Texture2D)) as Texture2D;
			parallaxIcon = AssetDatabase.LoadAssetAtPath("Assets/Proformer/Resources/Images/parallaxIcon.png", typeof(Texture2D)) as Texture2D;

			EditorApplication.update += update;
			EditorApplication.hierarchyWindowItemOnGUI += hierarchyItem;
		}

		private static void hierarchyItem(int instanceID, Rect selectionRect)
		{
			if (!ProformerSettings.showHierarchyIcon)
				return;
			if (characterId == null)
				return;
			Rect rect = new Rect(selectionRect);
			rect.x = rect.width - 5;
			rect.width = 18;

			if (characterId.Contains(instanceID))
				DrawIcon(rect, characterIcon);
			else if (cameraId.Contains(instanceID))
				DrawIcon(rect, cameraIcon);
			else if (managerId.Contains(instanceID))
				DrawIcon(rect, managerIcon);
			else if (levelBoundId.Contains(instanceID))
				DrawIcon(rect, levelBoundIcon);
			else if (groundId.Contains(instanceID))
				DrawIcon(rect, groundIcon);
			else if (pickableId.Contains(instanceID))
				DrawIcon(rect, pickableIcon);
			else if (uiId.Contains(instanceID))
				DrawIcon(rect, uiIcon);
			else if (parallaxId.Contains(instanceID))
				DrawIcon(rect, parallaxIcon);
		}


		static void DrawIcon(Rect rect, Texture2D texture) {
			GUI.Label(rect, texture);
		}

		static void update() {
			if (!ProformerSettings.showHierarchyIcon)
				return;
			DIControllerMotor[] motors = Resources.FindObjectsOfTypeAll(typeof(DIControllerMotor)) as DIControllerMotor[];
			characterId = new List<int>();
			foreach (DIControllerMotor motor in motors)
				characterId.Add(motor.gameObject.GetInstanceID());

			DICameraBase[] cameras = Resources.FindObjectsOfTypeAll(typeof(DICameraBase)) as DICameraBase[];
			cameraId = new List<int>();
			foreach (DICameraBase cam in cameras)
				cameraId.Add(cam.gameObject.GetInstanceID());

			GameManager[] gm = Resources.FindObjectsOfTypeAll(typeof(GameManager)) as GameManager[];
			managerId = new List<int>();
			foreach (GameManager g in gm)
				managerId.Add(g.gameObject.GetInstanceID());

			LevelBounds[] levelbounds = Resources.FindObjectsOfTypeAll(typeof(LevelBounds)) as LevelBounds[];
			levelBoundId = new List<int>();
			foreach (LevelBounds lb in levelbounds)
				levelBoundId.Add(lb.gameObject.GetInstanceID());

			GameObject[] groundObjects = Resources.FindObjectsOfTypeAll(typeof(GameObject)) as GameObject[];
			groundId = new List<int>();
			foreach (GameObject ground in groundObjects) {
				if(ground.layer == LayerMask.NameToLayer("Grounds"))
					groundId.Add(ground.GetInstanceID());
			}

			Pickable[] pickables = Resources.FindObjectsOfTypeAll(typeof(Pickable)) as Pickable[];
			pickableId = new List<int>();
			foreach (Pickable pickable in pickables)
				pickableId.Add(pickable.gameObject.GetInstanceID());

			GUIManager[] ui = Resources.FindObjectsOfTypeAll(typeof(GUIManager)) as GUIManager[];
			uiId = new List<int>();
			foreach (GUIManager u in ui)
				uiId.Add(u.gameObject.GetInstanceID());

			DIParallax[] parallax = Resources.FindObjectsOfTypeAll(typeof(DIParallax)) as DIParallax[];
			parallaxId = new List<int>();
			foreach (DIParallax p in parallax)
				parallaxId.Add(p.gameObject.GetInstanceID());
		}
	}
}
