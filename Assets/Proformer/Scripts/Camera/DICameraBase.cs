﻿using UnityEngine;
using System.Collections;

namespace DI.Proformer {
	public class DICameraBase : MonoBehaviour {
		public Vector3 leftEdge { get { return transform.position - transform.right * _camera.aspect * _camera.orthographicSize; } }
		public Vector3 rightEdge { get { return transform.position + transform.right * _camera.aspect * _camera.orthographicSize; } }
		public Camera _camera { get { return GetComponent<Camera>(); } }
	}
}
