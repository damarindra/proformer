﻿using UnityEngine;
using System.Collections.Generic;

namespace DI.Proformer {
	/// <summary>Parallax (2D Only) </summary>
	public class DIParallax : MonoBehaviour
	{

		[System.Serializable]
		public class Parallax {
			public Sprite parallaxSprite;
			public GameObject parallaxObject;
			public Vector3 offsetPosition = Vector3.zero;
			public float speedHorizontal = 1;
			public float speedVertical = 0;
			public bool repeatX = true;
			public Transform left, right;
			private List<Transform> parallaxs = new List<Transform>();

			public void Update(Vector3 moveAmount, DICameraBase cam) {
				if (parallaxs.Count == 0) {
					parallaxs.Add(parallaxObject.transform);
					left = parallaxObject.transform;
					right = parallaxObject.transform;
				}
				
				moveAmount.x *= speedHorizontal;
				moveAmount.z *= speedHorizontal;
				moveAmount.y *= speedVertical;
				if (repeatX) {
					if (left.position.x + moveAmount.x - (left.gameObject.GetComponent<SpriteRenderer>().sprite.rect.width / left.gameObject.GetComponent<SpriteRenderer>().sprite.pixelsPerUnit) * .5f > cam.leftEdge.x)
						CreateNewParallax(ref left, left.position - cam.transform.right * (left.gameObject.GetComponent<SpriteRenderer>().sprite.rect.width / left.gameObject.GetComponent<SpriteRenderer>().sprite.pixelsPerUnit));
					if(right.position.x + moveAmount.x + (right.gameObject.GetComponent<SpriteRenderer>().sprite.rect.width / right.gameObject.GetComponent<SpriteRenderer>().sprite.pixelsPerUnit) * .5f < cam.rightEdge.x)
						CreateNewParallax(ref right, right.position + cam.transform.right * (right.gameObject.GetComponent<SpriteRenderer>().sprite.rect.width / right.gameObject.GetComponent<SpriteRenderer>().sprite.pixelsPerUnit));
				}

				foreach (Transform tr in parallaxs)
					tr.Translate(moveAmount, Space.World);

			}

			void CreateNewParallax(ref Transform goEdge, Vector3 position) {
				GameObject newParallax = Instantiate(goEdge.gameObject, position, goEdge.transform.rotation) as GameObject;
				parallaxs.Add(newParallax.transform);
				newParallax.transform.parent = FindObjectOfType<DIParallax>().transform;
				goEdge = newParallax.transform;
			}
		}
		public List<Parallax> parallax = new List<Parallax>();

		DICameraBase cam;
		Vector3 camLastPosition;

		void Start() {
			cam = FindObjectOfType<DICameraBase>();
			camLastPosition = cam.transform.position;
		}

		void LateUpdate() {
			Vector3 moveAmount = cam.transform.position - camLastPosition;

			foreach (Parallax p in parallax) {
				if (p.parallaxObject != null) {
					p.Update(moveAmount, cam);
				}
			}

			camLastPosition = cam.transform.position;
		}

	}
}
