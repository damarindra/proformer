﻿using UnityEngine;
using System.Collections;
using DI.Proformer.Manager;

namespace DI.Proformer {
	/// <summary>Camera2D Control </summary>
	public class DICamera2D : DICameraBase
	{

		/// <summary>distance </summary>
		[SerializeField]
		private float distance = 10;
		/// <summary>Forward distance </summary>
		[SerializeField]
		private float distanceDirection = 2;
		
		/// <summary>Area Focus </summary>
		[SerializeField]
		private Vector2 areaFocus = Vector2.one;
		private Vector2 centerFocus;
		private Bounds areaBounds = new Bounds();
		
		/// <summary>Acceleration Time </summary>
		[SerializeField]
		private float accelerationTime = .2f;
		private Vector3 currentSmoothMoveAmount;
		/// <summary>Rotation damping </summary>
		[SerializeField, HideInInspector]
		private float rotationDamping;

		Camera cam;
		Vector3 velocity = new Vector3();

		// Use this for initialization
		void Start () {
			cam = GetComponent<Camera>();
			cam.orthographic = true;
			transform.position = GameManager.Instance.player.transform.position + InternalTool.RotateDirection(GameManager.Instance.player.controller.position.levelPath.direction, new Vector3(0, 90, 0)) * distance;

			GameManager.Instance.player.controller.onRespawn += (() => {
				transform.position = GameManager.Instance.player.transform.position + InternalTool.RotateDirection(GameManager.Instance.player.controller.position.levelPath.direction, new Vector3(0, 90, 0)) * distance;
			});

			transform.position += CalculateMovement();

			centerFocus = GameManager.Instance.player.transform.position;
			areaBounds.min = (Vector3)centerFocus - transform.right * (areaFocus.x) - transform.up * (areaFocus.y);
			areaBounds.max = (Vector3)centerFocus + transform.right * (areaFocus.x) + transform.up * (areaFocus.y);
		}

		bool isInArea() {
			if (!areaBounds.Contains(GameManager.Instance.player.transform.position)) {
				float moveX = 0, moveZ = 0, moveY = 0;
				if (Vector3.Distance(GameManager.Instance.player.transform.position, new Vector3(areaBounds.max.x, GameManager.Instance.player.transform.position.y, areaBounds.max.z)) > Vector3.Distance(GameManager.Instance.player.transform.position, new Vector3(areaBounds.min.x, GameManager.Instance.player.transform.position.y, areaBounds.min.z)))
				{
					moveX = GameManager.Instance.player.transform.position.x - areaBounds.min.x;
					moveZ = GameManager.Instance.player.transform.position.z - areaBounds.min.z;
				}
				else if(Vector3.Distance(GameManager.Instance.player.transform.position, new Vector3(areaBounds.max.x, GameManager.Instance.player.transform.position.y, areaBounds.max.z)) < Vector3.Distance(GameManager.Instance.player.transform.position, new Vector3(areaBounds.min.x, GameManager.Instance.player.transform.position.y, areaBounds.min.z)))
				{

					moveX = GameManager.Instance.player.transform.position.x - areaBounds.max.x;
					moveZ = GameManager.Instance.player.transform.position.z - areaBounds.max.z;
				}

				if (GameManager.Instance.player.transform.position.y < areaBounds.min.y)
					moveY = GameManager.Instance.player.transform.position.y - areaBounds.min.y;
				else if (GameManager.Instance.player.transform.position.y > areaBounds.max.y)
					moveY = GameManager.Instance.player.transform.position.y - areaBounds.max.y;
					
				areaBounds.center += new Vector3(moveX, moveY, moveZ);
				return false;
			}
			return true;
		}

		Vector3 CalculateMovement() {
			Vector3 movement;
			//Find target position with offset direction
			Vector3 targetPosition = GameManager.Instance.player.transform.position + GameManager.Instance.player.controller.direction * distanceDirection + InternalTool.RotateDirection(GameManager.Instance.player.controller.position.levelPath.direction, new Vector3(0, 90, 0)) * distance;
			//find distance between target position and current position
			Vector3 distanceTarget = targetPosition - transform.position;
			/*
			if (Vector3.Distance(new Vector3(transform.position.x, GameManager.Instance.player.transform.position.y, transform.position.z) + transform.forward * backward, GameManager.Instance.player.transform.position) > distanceDirection ) { }
			else if (GameManager.Instance.player.controller.motorInput.standartInput.directionalInput.x == 0 && Vector3.Distance(new Vector3(transform.position.x, GameManager.Instance.player.transform.position.y, transform.position.z) + transform.forward * backward, GameManager.Instance.player.transform.position) < distanceDirection + areaFocus.x * 2)
			{
				distanceTarget.x *= GameManager.Instance.player.controller.getPercentageVelocityHorizontal();
				distanceTarget.z *= GameManager.Instance.player.controller.getPercentageVelocityHorizontal();
			}
			else if (GameManager.Instance.player.transform.position == targetPositionLastFrame && Vector3.Distance(new Vector3(transform.position.x, GameManager.Instance.player.transform.position.y, transform.position.z) + transform.forward * backward, GameManager.Instance.player.transform.position) < distanceDirection + areaFocus.x * 2)
			{
				distanceTarget.x = Mathf.SmoothDamp(distanceTarget.x, 0, ref currentSmoothMoveAmount.x, accelerationTime);
				distanceTarget.z = Mathf.SmoothDamp(distanceTarget.z, 0, ref currentSmoothMoveAmount.z, accelerationTime);
			}*/
			if (GameManager.Instance.player.controller.getHorizontalMovement != 0) {
				distanceTarget.x *= ((Mathf.Abs(GameManager.Instance.player.controller.getHorizontalMovement) / Time.deltaTime) / GameManager.Instance.player.controller.moveSpeed);
				distanceTarget.z *= ((Mathf.Abs(GameManager.Instance.player.controller.getHorizontalMovement) / Time.deltaTime) / GameManager.Instance.player.controller.moveSpeed);
			}
			
			movement = Vector3.SmoothDamp(Vector3.zero, distanceTarget, ref currentSmoothMoveAmount, accelerationTime);

			float bottomEdge = transform.position.y + movement.y - cam.orthographicSize;
			float topEdge = bottomEdge + cam.orthographicSize * 2;

			if (bottomEdge < LevelBounds.instance.bottom)
				movement.y -= bottomEdge - LevelBounds.instance.bottom;
			else if (topEdge > LevelBounds.instance.top)
				movement.y -= topEdge - LevelBounds.instance.top;

			if (!LevelBounds.instance.loop)
			{
				if (GameManager.Instance.player.controller.position.levelPath.index == 0 || GameManager.Instance.player.controller.position.levelPath.index == LevelBounds.instance.levelPath.Count - 2) {
					Vector3 leftEdge = LevelBounds.instance.levelPath[0].from + InternalTool.RotateDirection(LevelBounds.instance.levelPath[0].direction, new Vector3(0, 90, 0)) * distance;
					Vector3 rightEdge = LevelBounds.instance.levelPath[LevelBounds.instance.levelPath.Count - 1].to + InternalTool.RotateDirection(LevelBounds.instance.levelPath[LevelBounds.instance.levelPath.Count - 1].direction, new Vector3(0, 90, 0)) * distance;
					Vector3 camLeftEdgeAfterMoveAmount = cam.transform.position - LevelBounds.instance.levelPath[0].direction * cam.aspect * cam.orthographicSize + movement;
					Vector3 camRightEdgeAfterMoveAmount = cam.transform.position + LevelBounds.instance.levelPath[LevelBounds.instance.levelPath.Count - 1].direction * cam.aspect * cam.orthographicSize + movement;
					leftEdge.y = rightEdge.y = camLeftEdgeAfterMoveAmount.y = camRightEdgeAfterMoveAmount.y = 0;

					if ((camLeftEdgeAfterMoveAmount - leftEdge).normalized == -LevelBounds.instance.levelPath[0].direction)
					{
						movement.x -= camLeftEdgeAfterMoveAmount.x - leftEdge.x;
						movement.z -= camLeftEdgeAfterMoveAmount.z - leftEdge.z;
					}
					else if ((camRightEdgeAfterMoveAmount - rightEdge).normalized == LevelBounds.instance.levelPath[LevelBounds.instance.levelPath.Count - 1].direction)
					{
						movement.x -= camRightEdgeAfterMoveAmount.x - rightEdge.x;
						movement.z -= camRightEdgeAfterMoveAmount.z - rightEdge.z;
					}
				}
			}
			return movement;
		}

		Quaternion CalculateRotation() {

			//get target rotation
			float wantedRotationAngle = Quaternion.LookRotation(GameManager.Instance.player.controller.position.levelPath.direction).eulerAngles.y - 90;// GameManager.Instance.player.transform.eulerAngles.y - 90;

			//get current rotation
			float currentAngle = transform.rotation.eulerAngles.y;

			currentAngle = Mathf.LerpAngle(currentAngle, wantedRotationAngle, rotationDamping);

			return Quaternion.Euler(new Vector3(transform.rotation.eulerAngles.x, currentAngle, transform.rotation.eulerAngles.z));
		}

		// Update is called once per frame
		void LateUpdate () {
			if (isInArea() && Vector3.Distance(new Vector3(transform.position.x, GameManager.Instance.player.transform.position.y, transform.position.z) + transform.forward * distance, GameManager.Instance.player.transform.position) < distanceDirection + areaFocus.x*2) {
				return;
			}

			velocity = CalculateMovement();

			transform.Translate(velocity, Space.World);
			transform.rotation = CalculateRotation();
		}

		void OnDrawGizmos()
		{
			Gizmos.color = Color.cyan;
			Vector3 topLeft = (Application.isPlaying ? areaBounds.center : transform.position) - transform.right * areaFocus.x + transform.up * areaFocus.y;
			Vector3 topRight = (Application.isPlaying ? areaBounds.center : transform.position) + transform.right * areaFocus.x + transform.up * areaFocus.y;
			Vector3 bottomLeft = (Application.isPlaying ? areaBounds.center : transform.position) - transform.right * areaFocus.x - transform.up * areaFocus.y;
			Vector3 bottomRight = (Application.isPlaying ? areaBounds.center : transform.position) + transform.right * areaFocus.x - transform.up * areaFocus.y;
			Gizmos.DrawLine(topLeft, topRight);
			Gizmos.DrawLine(topLeft, bottomLeft);
			Gizmos.DrawLine(topRight, bottomRight);
			Gizmos.DrawLine(bottomLeft, bottomRight);
			//Gizmos.DrawCube(areaBounds.center, areaBounds.extents * 2);
		}
	}
}
