﻿using UnityEngine;
using System.Collections;
using DI.Proformer.Manager;
using DI.Proformer.ToolHelper;

namespace DI.Proformer {
	/// <summary>
	/// Camera control
	/// </summary>
	public class DICamera : DICameraBase
	{
		/// <summary> The distance in the x-z plane to the target</summary>
		[SerializeField]
		private float distance = 6.0f;
		/// <summary>the height we want the camera to be above the target</summary>
		public float height = 2.0f;
		/// <summary>Forward disatance</summary>
		public float forwardDistance = 1f;

		/// <summary>use rotation X </summary>
		[SerializeField]
		private bool rotationX = false;
		/// <summary>use rotation Y </summary>
		[SerializeField]
		private bool rotationY = false;

		/// <summary>Stop following player </summary>
		bool followPlayer = true;
		private Vector3 movementSmoothing;
		/// <summary>Acceleration time when moving </summary>
		public float accelerationTime = .4f;
		private Vector3 smoothingEulerAngle;
		/// <summary>Acceleration time when rotate </summary>
		public float accelerationRotation = .4f;

		/// <summary>get if Camera follow player </summary>
		public bool isFollowPlayer { get { return followPlayer; } protected set { followPlayer = value; } }

		// Update is called once per frame
		void LateUpdate()
		{
			// Early out if we don't have a target
			if (!GameManager.Instance.player || !isFollowPlayer)
				return;

			//Calculate Position
			Vector3 targetPosition = GameManager.Instance.player.transform.position + InternalTool.RotateDirection(GameManager.Instance.player.controller.position.levelPath.direction, new Vector3(0, 90, 0)) * distance;
			targetPosition += GameManager.Instance.player.controller.stateAdv.isWallSliding ? -GameManager.Instance.player.transform.right * forwardDistance : GameManager.Instance.player.transform.right * forwardDistance;
			targetPosition += Vector3.up * height;
			Vector3 movement = targetPosition - transform.position;
			movement = Vector3.SmoothDamp(Vector3.zero, movement, ref movementSmoothing, accelerationTime);
			float currentDistance = Vector3.Distance(transform.position + movement, GameManager.Instance.player.transform.position);
			if (currentDistance < distance)
			{
				Vector3 backDirection = (transform.position + movement - GameManager.Instance.player.transform.position).normalized;
				float distanceLeft = distance - currentDistance;
				movement += backDirection * distanceLeft;
			}
			transform.Translate(movement, Space.World);

			//Calculate Rotation
			Vector3 targetLookDirection = (GameManager.Instance.player.transform.position - transform.position).normalized;
			if (!rotationX)
				targetLookDirection.y = 0;
			if (!rotationY) {
				Vector3 direction = InternalTool.RotateDirection(GameManager.Instance.player.controller.position.levelPath.direction, new Vector3(0, -90, 0));
				targetLookDirection.x = direction.x;
				targetLookDirection.z = direction.z;
			}
			Vector3 eulerAngle = Vector3.SmoothDamp(transform.forward, targetLookDirection, ref smoothingEulerAngle, accelerationRotation);
			transform.rotation = Quaternion.LookRotation(eulerAngle);
		}

		/// <summary>
		/// Stop following player
		/// </summary>
		public void StopFollow()
		{
			isFollowPlayer = false;
		}
		/// <summary>
		/// Start / Continue following Player
		/// </summary>
		public void FollowPlayer()
		{
			isFollowPlayer = true;
		}
		
	}
}
