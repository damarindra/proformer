﻿using UnityEngine;
using DI.Proformer.ToolHelper;
using DI.Proformer.Manager;

namespace DI.Proformer {
	[ExecuteInEditMode]
	/// <summary>
	/// Go To Level is script Listener for Player which want to load new Scene
	/// </summary>
	public class GoToLevel : DIEntity {
		[SceneString]
		public string levelName;
		[Tag]
		public string targetTag = "Player";

		protected override void Start()
		{
			if (!Application.isPlaying) {
#if PROFORMER3D
			if (GetComponent<Collider>())
				GetComponent<Collider>().isTrigger = true;
			else {
				BoxCollider box = gameObject.AddComponent<BoxCollider>();
				box.isTrigger = true;
			}
#else
				if (GetComponent<Collider2D>())
					GetComponent<Collider2D>().isTrigger = true;
				else
				{
					BoxCollider2D box = gameObject.AddComponent<BoxCollider2D>();
					box.isTrigger = true;
				}
#endif
				return;
			}
			base.Start();
		}
#if PROFORMER3D
		void OnTriggerEnter(Collider col) {
#else
		void OnTriggerEnter2D(Collider2D col) {
#endif
			if(col.tag.Equals(targetTag))
				LoadLevelManager.Instance.LoadLevel(levelName, GameData.Instance.fadeDuration);
		}
	}
}