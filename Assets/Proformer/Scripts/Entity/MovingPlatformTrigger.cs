﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DI.Proformer.ToolHelper;
using System;

namespace DI.Proformer
{
	public class MovingPlatformTrigger : DIAreaCollector
	{
		DIEntity entity;
		MovingPlatform mp;
		FollowPath fp;

#if PROFORMER3D
		[SerializeField]
		BoxCollider bodyCollider;
		BoxCollider triggerColTop;
#else
		[SerializeField]
		BoxCollider2D bodyCollider;
		BoxCollider2D triggerColTop;
#endif
		[SerializeField]
		private float raySpace = .3f;
		private float _raySpace;
		int horizontalRayCount { get { return (int)Mathf.Ceil((bodyCollider.size.y - skinWidth * 2) / raySpace); } }
		///<summary>CheckTop</summary>
		Vector3 topLeft { get { return bodyCollider.transform.position - entity.position.levelPath.direction * bodyCollider.size.x / 2 * bodyCollider.transform.localScale.x + Vector3.up * bodyCollider.size.y / 2 + Vector3.up * skinWidth; } }
		///<summary>Check Right</summary>
		Vector3 topRight { get { return bodyCollider.transform.position + entity.position.levelPath.direction * bodyCollider.size.x / 2 * bodyCollider.transform.localScale.x + Vector3.up * bodyCollider.size.y / 2 + Vector3.right * skinWidth; } }
		///<summary>Check Bottom</summary>
		Vector3 bottomRight { get { return bodyCollider.transform.position + entity.position.levelPath.direction * bodyCollider.size.x / 2 * bodyCollider.transform.localScale.x - Vector3.up * bodyCollider.size.y / 2 + Vector3.left * skinWidth + Vector3.up * skinWidth; } }
		///<summary>Check Left</summary>
		Vector3 bottomLeft { get { return bodyCollider.transform.position - entity.position.levelPath.direction * bodyCollider.size.x / 2 * bodyCollider.transform.localScale.x - Vector3.up * bodyCollider.size.y / 2 + Vector3.right * skinWidth + Vector3.up * skinWidth; } }

		List<Transform> passengers = new List<Transform>();
		List<Transform> passengersFromRay = new List<Transform>();
		List<DIController> passengersController = new List<DIController>();
		List<DIController> passengersControllerFromRay = new List<DIController>();

		public Vector3 velocity;
		public Vector2 rawVelocity { get {
				Vector3 _velocity = velocity;
				_velocity.y = 0;
				if (_velocity.magnitude == 0)
					return Vector2.zero;
				Vector3 dir = _velocity.normalized;
				if (Vector3.Angle(dir, mp.position.levelPath.direction) < 90)
					return Vector2.right * _velocity.magnitude;
				else
					return -Vector2.right * _velocity.magnitude;
			} }
		public LayerMask passenger;

		void Start()
		{
			entity = GetComponentInParent<DIEntity>();
			mp = GetComponentInParent<MovingPlatform>();
			fp = GetComponentInParent<FollowPath>();
			bodyCollider = mp.bc;
			_raySpace = (bodyCollider.size.y - skinWidth * 2) / (horizontalRayCount - 1);

			layerTarget = mp.passengerMask;
			Init(bodyCollider);
		}

		private void addVelocityStepOn(ref Vector3 velocity, DIController controller)
		{
			Vector3 addVelocity = fp.VelocityToVector2(mp.position.levelPathPosition);
			addVelocity.y = 0;
			velocity += addVelocity;
		}


		void Update()
		{
			HorizontalRaycast(velocity);
			if (fp) {
				if (fp.isMoving)
				{
					foreach (Transform entity in passengers)
					{
						entity.Translate(velocity, Space.World);
					}
					foreach (DIController controller in passengersController)
						controller.transform.Translate(new Vector3(0, velocity.y, 0), Space.World);
					foreach (DIController controller in passengersControllerFromRay) {
						DIControllerMotor motor = (DIControllerMotor)controller;
						if (motor != null && motor.stateAdv.isWallSliding) { }
						else {
							Vector3 currentVelocity = rawVelocity;
							controller.UpdateLevelPath(ref currentVelocity);
							controller.transform.Translate(new Vector3(currentVelocity.x, 0, currentVelocity.z), Space.World);
						}
					}
				}
			}
		}

		void HorizontalRaycast(Vector3 velocity) {
			passengersFromRay.Clear();
			foreach (DIController cont in passengersControllerFromRay)
				cont.onBeforeCalculation -= addVelocityStepOn;
			passengersControllerFromRay.Clear();
			Vector3 rayOrigin = Vector3.Angle(velocity.normalized, entity.position.levelPath.direction) < 90 ? bottomRight : bottomLeft;
			Vector3 rayDirection = Vector3.Angle(velocity.normalized, entity.position.levelPath.direction) < 90 ? entity.position.levelPath.direction : -entity.position.levelPath.direction;
			float rayLength = skinWidth + Mathf.Abs(velocity.x) + Mathf.Abs(velocity.z);

			for (int i = 0; i < horizontalRayCount; i++)
			{
				Ray ray = new Ray(rayOrigin, rayDirection);
				ProformerRaycastHit[] hits;
				hits = InternalTool.ProformerRaycastAll(ray, rayLength, passenger);
				InternalTool.DrawRay(rayOrigin, rayDirection * rayLength);

				foreach (ProformerRaycastHit hit in hits)
				{
					if (hit.collider.GetComponent<DICharacterBody>())
					{
						if (!passengersControllerFromRay.Contains(hit.collider.GetComponent<DICharacterBody>().motor) && !hit.collider.GetComponent<DICharacterBody>().motor.stateAdv.isWallSliding) {
							passengersControllerFromRay.Add(hit.collider.GetComponent<DICharacterBody>().motor);
							//hit.collider.GetComponent<DICharacterBody>().motor.onBeforeCalculation += addVelocityStepOn;
						}
					}
					else if (!passengers.Contains(hit.collider.transform))
					{
						if (mp.specificPassenger != MovingPlatform.SpecificPassenger.ALL)
						{
							if ((mp.specificPassenger & MovingPlatform.SpecificPassenger.RIGIDBODY_ONLY) == MovingPlatform.SpecificPassenger.RIGIDBODY_ONLY)
							{
#if PROFORMER3D
								Rigidbody rb = hit.collider.GetComponent<Rigidbody>() ? hit.collider.GetComponent<Rigidbody>() : hit.collider.GetComponentInParent<Rigidbody>();
								if (rb && !passengers.Contains(rb.transform))
									passengersFromRay.Add(rb.transform);
#else
								Rigidbody2D rb = hit.collider.GetComponent<Rigidbody2D>() ? hit.collider.GetComponent<Rigidbody2D>() : hit.collider.GetComponentInParent<Rigidbody2D>();
								if (rb && !passengers.Contains(rb.transform))
										passengersFromRay.Add(rb.transform);
#endif
								return;
							}
							if ((mp.specificPassenger & MovingPlatform.SpecificPassenger.ENTITY_ONLY) == MovingPlatform.SpecificPassenger.ENTITY_ONLY)
							{
								DIEntity ett = hit.collider.GetComponent<DIEntity>() ? hit.collider.GetComponent<DIEntity>() : hit.collider.GetComponentInParent<DIEntity>();
								if (ett && !passengers.Contains(ett.transform))
									passengersFromRay.Add(ett.transform);
								return;
							}
						}
						else
							passengers.Add(hit.collider.transform);

					}
				}

				rayOrigin += Vector3.up * _raySpace;
			}
		}
		
#if PROFORMER3D
		public override void OnTriggerEnter(Collider col)
#else
		public override void OnTriggerEnter2D(Collider2D col)
#endif
		{
			if (col.transform == transform.parent)
				return;

			int layerObj = 1 << col.gameObject.layer;
			if ((layerTarget.value & layerObj) > 0)
			{
				if (col.GetComponent<DICharacterBody>())
				{
					if (!passengersController.Contains(col.GetComponent<DICharacterBody>().motor))
					{
						passengersController.Add(col.GetComponent<DICharacterBody>().motor);
						col.GetComponent<DICharacterBody>().motor.onBeforeCalculation += addVelocityStepOn;
					}/*
					if (!passengers.Contains(col.GetComponent<DICharacterBody>().motor.transform)) {
						col.GetComponent<DICharacterBody>().motor.isAffectedMovingPlatform = true;
						passengers.Add(col.GetComponent<DICharacterBody>().motor.transform);
					}*/
				}
				else if (!passengers.Contains(col.transform)) {
					if (mp.specificPassenger != MovingPlatform.SpecificPassenger.ALL) {
						if ((mp.specificPassenger & MovingPlatform.SpecificPassenger.RIGIDBODY_ONLY) == MovingPlatform.SpecificPassenger.RIGIDBODY_ONLY)
						{
#if PROFORMER3D
							Rigidbody rb = col.GetComponent<Rigidbody>() ? col.GetComponent<Rigidbody>() : col.GetComponentInParent<Rigidbody>();
							if (rb && !passengers.Contains(rb.transform))
								passengers.Add(rb.transform);
#else
							Rigidbody2D rb = col.GetComponent<Rigidbody2D>() ? col.GetComponent<Rigidbody2D>() : col.GetComponentInParent<Rigidbody2D>();
							if (rb && !passengers.Contains(rb.transform))
								passengers.Add(rb.transform);
#endif
							return;
						}
						if ((mp.specificPassenger & MovingPlatform.SpecificPassenger.ENTITY_ONLY) == MovingPlatform.SpecificPassenger.ENTITY_ONLY) {
							DIEntity ett = col.GetComponent<DIEntity>() ? col.GetComponent<DIEntity>() : col.GetComponentInParent<DIEntity>();
							if (ett && !passengers.Contains(ett.transform)) {
								passengers.Add(ett.transform);
							}
							return;
						}
					}
					else
						passengers.Add(col.transform);

				}
			}

		}

#if PROFORMER3D
		public override void OnTriggerExit(Collider col)
#else
		public override void OnTriggerExit2D(Collider2D col)
#endif
		{
			if (col.transform == transform.parent)
				return;

			int layerObj = 1 << col.gameObject.layer;
			if ((layerTarget.value & layerObj) > 0)
			{
				if (col.GetComponent<DICharacterBody>())
				{
					if (passengersController.Contains(col.GetComponent<DICharacterBody>().motor))
					{
						passengersController.Remove(col.GetComponent<DICharacterBody>().motor);
						col.GetComponent<DICharacterBody>().motor.onBeforeCalculation -= addVelocityStepOn;
					}/*
					if (passengers.Contains(col.GetComponent<DICharacterBody>().motor.transform)) {
						col.GetComponent<DICharacterBody>().motor.isAffectedMovingPlatform = false;
						passengers.Remove(col.GetComponent<DICharacterBody>().motor.transform);
					}*/
				}
#if PROFORMER3D
				if (col.GetComponent<Rigidbody>())
				{
					if (passengers.Contains(col.GetComponent<Rigidbody>().transform))
						passengers.Remove(col.GetComponent<Rigidbody>().transform);
				}
				else if (col.GetComponentInParent<Rigidbody>())
				{
					if (passengers.Contains(col.GetComponentInParent<Rigidbody>().transform))
						passengers.Remove(col.GetComponentInParent<Rigidbody>().transform);
				}
#else
				else if (col.GetComponent<Rigidbody2D>())
				{
					if (passengers.Contains(col.GetComponent<Rigidbody2D>().transform))
						passengers.Remove(col.GetComponent<Rigidbody2D>().transform);
				}
				else if (col.GetComponentInParent<Rigidbody2D>())
				{
					if (passengers.Contains(col.GetComponentInParent<Rigidbody2D>().transform))
						passengers.Remove(col.GetComponentInParent<Rigidbody2D>().transform);
				}
#endif
				else if (col.GetComponent<DIEntity>())
				{
					if (passengers.Contains(col.GetComponent<DIEntity>().transform)) {
						passengers.Remove(col.GetComponent<DIEntity>().transform);
					}
				}
				else if (col.GetComponentInParent<DIEntity>())
				{
					if (passengers.Contains(col.GetComponentInParent<DIEntity>().transform)) {
						passengers.Remove(col.GetComponentInParent<DIEntity>().transform);
					}
				}
				else if (passengers.Contains(col.transform))
					passengers.Remove(col.transform);
			}
		}

	}
}
