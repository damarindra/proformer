﻿using UnityEngine;
using System;
using DI.Proformer.Manager;
using DI.Proformer.ToolHelper;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace DI.Proformer
{
	[ExecuteInEditMode]
	/// <summary>
	/// Pickable item, this base class for Pickable item.
	/// Set tag for type of pickable item. (ex : health)
	/// Character Basic side, set Pickable Trigger Tag
	/// </summary>
	public class Pickable : MonoBehaviour
	{

		//[HeaderBackground(3, .3f, "Pickable")]
		public bool disableWhenPicked = true;
		public AudioClip pickedSFX;

		protected DIObject pObj;

		public virtual void Triggered()
		{
			if (pickedSFX)
				SoundManager.Instance.PlaySFX(pickedSFX);

			if (disableWhenPicked)
			{
				gameObject.SetActive(false);
			}
		}

		protected void Update()
		{
			if (!Application.isPlaying)
			{
				if (pObj == null)
				{
					pObj = GetComponent<DIObject>();
				}
			}
		}
		
	}
}