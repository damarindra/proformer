﻿using UnityEngine;
using System.Collections;

namespace DI.Proformer {
	/// <summary>DISpecialGround is a Ground Component for modified velocity DIController.</summary>
	public class DISpecialGround : MonoBehaviour
	{
		public enum Operator { Add, Substract, Scale }
		public Operator modifier = Operator.Add;
		public float modifierValue = 1.5f;
		public enum ModifyType { AccelerationModify, MoveSpeedModify, FollowInput_Velocity, Right_Velocity, Left_Velocity }
		[HideInInspector]
		public ModifyType modifierType;

		public void Modify(ref Vector3 velocity, DIController modifiedController)
		{
			if (modifierType != ModifyType.AccelerationModify && modifierType != ModifyType.MoveSpeedModify)
			{
				Vector3 dir = Vector3.zero;
				if (modifier == Operator.Scale)
					dir = Vector2.right;
				else if (modifierType == ModifyType.FollowInput_Velocity && modifiedController.standartInput.directionalInput.x != 0)
					dir = modifiedController.standartInput.directionalInput;
				else if (modifierType == ModifyType.Right_Velocity)
					dir = Vector2.right;
				else if (modifierType == ModifyType.Left_Velocity)
					dir = Vector2.left;

				Vector3 addition = dir * modifierValue;

				if (modifier != Operator.Scale)
					addition *= Time.deltaTime;

				if (modifier == Operator.Add)
				{
					velocity += addition;
				}
				else if (modifier == Operator.Substract)
				{
					velocity -= addition;
				}
				else if (modifier == Operator.Scale)
				{
					velocity = Vector3.Scale(velocity, addition);
				}
			}
			else {
				if (modifiedController.GetType() == typeof(DIControllerMotor))
				{
					DIControllerMotor motor = (DIControllerMotor)modifiedController;
					if (!motor.state.lastFrameSpecialGrounded)
						motor.velocityXSmoothingSpecialGround = motor.oldVelocity.x;

					if (modifierType == ModifyType.AccelerationModify)
					{
						float acceleration = motor.accelerationTimeGrounded;
						if (modifier == Operator.Scale)
							acceleration *= modifierValue;
						else if (modifier == Operator.Add)
							acceleration += modifierValue;
						else if (modifier == Operator.Substract)
							acceleration -= modifierValue;

						motor.velocity.x = Mathf.SmoothDamp(motor.oldVelocity.x, motor.motorInput.standartInput.directionalInput.x * motor.moveSpeed, ref motor.velocityXSmoothingSpecialGround, acceleration);
						velocity.x = motor.velocity.x * Time.deltaTime;
					}
					else if (modifierType == ModifyType.MoveSpeedModify)
					{
						float moveSpeed = motor.moveSpeed;
						if (modifier == Operator.Scale)
							moveSpeed *= modifierValue;
						else if (modifier == Operator.Add)
							moveSpeed += modifierValue;
						else if (modifier == Operator.Substract)
							moveSpeed -= modifierValue;
						motor.velocity.x = Mathf.SmoothDamp(motor.oldVelocity.x, motor.motorInput.standartInput.directionalInput.x * moveSpeed, ref motor.velocityXSmoothingSpecialGround, motor.accelerationTimeGrounded);
						velocity.x = motor.velocity.x * Time.deltaTime;
					}
				}
				else
					Debug.LogWarning("Currently Special Ground : Follow Velocity modifier only work on DIControllerMotor!");
			}
		}
	}
}
