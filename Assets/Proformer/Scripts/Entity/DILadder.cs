﻿using UnityEngine;
using System.Collections;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace DI.Proformer {
	public class DILadder : DIObject {

#if PROFORMER3D
		public enum LadderDirection { Backward, Forward, Right, Left }
		public LadderDirection ladderFaceDirection = LadderDirection.Backward;
#endif
		public bool autoGenerateTopLand = false;

		void CreateLandTop() {
			if (_collider) {
				Vector3 size = _collider.bounds.size;
				Vector3 center = _collider.bounds.center;
				GameObject land = new GameObject("Top Land");
				land.transform.parent = transform;
				land.transform.position = center + Vector3.up * size.y /2 + Vector3.down * .025f;
#if PROFORMER3D
				BoxCollider landCollider = land.AddComponent<BoxCollider>();
				landCollider.size = new Vector3(size.x, .08f, size.z);
				land.tag = "OneWay";
				land.layer = LayerMask.NameToLayer("Grounds");
#else
				BoxCollider2D landCollider = land.AddComponent<BoxCollider2D>();
				landCollider.size = new Vector3(size.x, .08f, size.z);
				land.tag = "OneWay";
				land.layer = LayerMask.NameToLayer("Grounds");
#endif
			}
		}

		public float getTopY() {
			return _collider.bounds.center.y + _collider.bounds.size.y / 2;
		}

		protected override void Start() {
			base.Start();
			if (autoGenerateTopLand)
				CreateLandTop();
			if (_collider)
			{
				_collider.tag = "Ladder";
			}
		}
	}
}
