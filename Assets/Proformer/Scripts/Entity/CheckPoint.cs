﻿using UnityEngine;
using DI.Proformer.Manager;
using DI.Proformer.ToolHelper;

namespace DI.Proformer {
	/// <summary>
	/// Check Point, used for respawning Player when Die
	/// There is 2 Type of check Point, Placing and Last Stand (Configure at Game Data)
	/// </summary>
	public class CheckPoint : DIEntity {
		public DIControllerMotor owner;
		[HideInInspector]
		public bool followOwner = false;
		public float spawnYOffset = 1;
		protected override void Start() {
			base.Start();
		}
		void FixedUpdate() {
			if (followOwner && owner != null) {
				if (owner.state.colliderBelow && !owner.state.colliderBelow.GetComponentInParent<FollowPath>())
				{
					if (!owner.state.lastFrameCollisionBelow) {
						transform.position = owner.transform.position;
					}
					if (position.levelPath != owner.position.levelPath) {
						position.SetLevelPathPosition(owner.position.levelPathPosition);
						transform.position = owner.transform.position;
					}
					else
						transform.position = Vector3.Lerp(transform.position, owner.transform.position, 10.0f * Time.deltaTime);
				}
			}
		}

#if PROFORMER3D
		void OnTriggerEnter(Collider col) {
#else
		void OnTriggerEnter2D(Collider2D col) {
#endif
			if (GameData.Instance.checkPointType == GameData.CheckPointType.LastStand)
				return;

			if (col.tag.Equals("Player")) {
				GameManager.Instance.currentCheckPoint = this;
				GameManager.Instance.player.controller.respawnPosition = this;
			}
		}

		public static GameObject CreateCheckPoint(DIControllerMotor owner)
		{
			GameObject go = new GameObject("Check Point");
			go.AddComponent<CheckPoint>();
			go.GetComponent<CheckPoint>().owner = owner;
			go.GetComponent<CheckPoint>()._autoRotateSinceCreation = false;
			go.GetComponent<CheckPoint>().position = new DIPosition();
			go.GetComponent<CheckPoint>().position.SetLevelPathPosition(0);
			go.GetComponent<CheckPoint>().position.SetPositionAndFixIt(GameManager.Instance.transform.position);
#if PROFORMER3D
			go.AddComponent<BoxCollider>();
			go.GetComponent<BoxCollider>().isTrigger = true;
#else
			go.AddComponent<BoxCollider2D>();
			go.GetComponent<BoxCollider2D>().isTrigger = true;
#endif
			return go;
		}
	}
	
}
