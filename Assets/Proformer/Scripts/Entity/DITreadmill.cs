﻿using UnityEngine;
using System.Collections;
using System;
using DI.Proformer.ToolHelper;

namespace DI.Proformer {
	[ExecuteInEditMode]
	public class DITreadmill : DIObject {

		public enum TreadmillDirection {
			Right, Left
		}
		[HeaderBackground(headerLabel = "Treadmill", yCount = 4, offsetBot = 0)]
		public TreadmillDirection threadmillDirection;
		public LayerMask passengers;
		/// <summary>Set if treadmill execute or not.</summary>
		public bool Execute {
			get { return _execute; }
			set { _execute = value; if (!_execute) areaCollector.gameObjectCollected.Clear(); }
		}
		private bool _execute = true;
		/// <summary>get transform.forward</summary>
		public Vector3 RightDirection { get { return transform.right; } }
		public float speed = 5;

		DIAreaCollector areaCollector;

		protected override void Start() {
			if (!Application.isPlaying)
			{
				colliderAllowed = ColliderAllowed.Box;
			}
			else {
				base.Start();
				GameObject triggerObj = new GameObject("Area Trigger Collector");

				triggerObj.transform.parent = transform;
				areaCollector = triggerObj.AddComponent<DIAreaCollector>();
#if !PROFORMER3D
				areaCollector.Init((BoxCollider2D)_collider);
#else
				areaCollector.Init((BoxCollider)_collider);
#endif
				areaCollector.layerTarget = passengers;
				areaCollector.onGameObjectEnter += addVelocityMotor;
				areaCollector.onGameObjectExit += removeVelocityMotor;
			}
		}

		void Update()
		{
			if (!Application.isPlaying)
				return;
			foreach (GameObject passenger in areaCollector.gameObjectCollected) {
				if (passenger.GetComponent<DICharacterBody>())
				{
					//DO NOTHING
					//PROCESS On DELEGATE EVENT
				}
				else
				{
					passenger.transform.Translate(threadmillDirection == TreadmillDirection.Right ? RightDirection * speed * Time.deltaTime : -RightDirection * speed * Time.deltaTime, Space.World);
				}
			}
		}

		private void addVelocityMotor(GameObject go)
		{
			if (go.GetComponent<DICharacterBody>())
			{
				DICharacterBody cBody = go.GetComponent<DICharacterBody>();
				cBody.motor.onBeforeCalculation += addVelocityMotor;
			}
		}
		private void removeVelocityMotor(GameObject go)
		{
			if (go.GetComponent<DICharacterBody>())
			{
				DICharacterBody cBody = go.GetComponent<DICharacterBody>();
				cBody.motor.onBeforeCalculation -= addVelocityMotor;
			}
		}

		private void addVelocityMotor(ref Vector3 velocity, DIController controller)
		{
			if (Execute) {
				velocity += threadmillDirection == TreadmillDirection.Right ? RightDirection * speed * Time.deltaTime : -RightDirection * speed * Time.deltaTime;
			}
		}
	}
}
