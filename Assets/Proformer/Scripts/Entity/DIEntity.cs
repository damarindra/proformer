﻿using UnityEngine;
using DI.Proformer.Manager;
using DI.Proformer.ToolHelper;
namespace DI.Proformer {
	/// <summary>
	/// This class is base of our entity in game world.
	/// </summary>
	public class DIEntity : MonoBehaviour {
		[HideInInspector]
		public DIPosition position = new DIPosition();

		protected Vector3 velocityLastFrame { get { return transform.position - lastFramePosition; } }
		Vector3 lastFramePosition;

		[HideInInspector]
		public bool _autoRotateSinceCreation = true;

		protected virtual void LateUpdate() {
			lastFramePosition = transform.position;
		}

		/// <summary>
		/// Translate Horizontal Velocity to X and Z Velocity
		/// </summary>
		protected void AlongPath(ref Vector3 velocity)
		{
			velocity.z = velocity.x;
			velocity.x *= position.levelPath.direction.x;
			velocity.z *= position.levelPath.direction.z;
		}

		public void RotateTo(bool rightDirection)
		{
			if (rightDirection)
				transform.right = position.levelPath.direction;
			//transform.LookAt(new Vector3(levelPath.to.x, transform.position.y, levelPath.to.z));
			else
				transform.right = -position.levelPath.direction;
			//transform.LookAt(new Vector3(levelPath.from.x, transform.position.y, levelPath.from.z));
		}
		/// <summary>Get direction player, if right -> levelPath.direction, and otherwise</summary>
		public Vector3 direction { get { return transform.right; } }


		#region virtual
		/// <summary>
		/// Initialize Entity
		/// </summary>
		protected virtual void Start() {
			if (LevelBounds.instance.levelPath.Count == 0 || position.levelPathPosition > LevelBounds.instance.levelPath.Count)
				LevelBounds.instance.GatherLevelPath();
			position.Init(transform);
		}
		#endregion

		#region Public Method

		/// <summary>
		/// Update the position to given Parameter. Only Use when using LevelBounds
		/// </summary>
		/// <param name="levelPath">Level Path</param>
		/// <param name="horizontal">Horizontal Position</param>
		/// <param name="vertical">Vertical Position</param>
		public void UpdatePositionTo(LevelPath levelPath, float horizontal, float vertical) {
			transform.position = position.GetPositionFromRaw(horizontal, vertical, levelPath.index);
		}

		/*
		public void DisableGameObject()
		{
			gameObject.SetActive(false);
		}
		public void EnableGameObject()
		{
			gameObject.SetActive(true);
		}
		public void DisableScript() {
			enabled = false;
		}
		public void EnableScript() {
			enabled = true;
		}*/
#endregion
		
	}
}
