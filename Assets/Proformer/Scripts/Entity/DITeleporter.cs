﻿using UnityEngine;
using DI.Proformer.ToolHelper;
using System.Collections.Generic;

namespace DI.Proformer{
	public class DITeleporter : DIObject {
		[HideInInspector]
		public DIEntity destination;
		[Tag, HideInInspector]
		public string targetTag;
		enum ExecuteMode { OnEnter, CallMethod }
		[SerializeField, HideInInspector]
		ExecuteMode executeMode = ExecuteMode.OnEnter;

		bool disabledTeleport = false;

		List<DIEntity> entityWillBeTeleported = new List<DIEntity>();
		List<Transform> transformWillBeTeleported = new List<Transform>();

#if PROFORMER3D
		void OnTriggerEnter(Collider col) {
#else
		void OnTriggerEnter2D(Collider2D col) {

#endif
			if (!col.tag.Equals(targetTag) || !enabled)
				return;

			if (disabledTeleport && executeMode == ExecuteMode.OnEnter) {
				disabledTeleport = false;
				return;
			}
			if (destination.GetType() == typeof(DITeleporter)) {
				if(((DITeleporter)destination).executeMode == ExecuteMode.OnEnter)
					((DITeleporter)destination).disabledTeleport = true;
			}

			DIEntity entity = col.GetComponentInParent<DICharacterBody>() ? col.GetComponent<DICharacterBody>().motor : col.GetComponentInParent<DIEntity>() ? col.GetComponentInParent<DIEntity>() : col.GetComponent<DIEntity>();
			if (entity)
			{
				if (executeMode == ExecuteMode.OnEnter)
					entity.position.SetPosition(destination.position.levelPathPosition, destination.position.position);
				else if(!entityWillBeTeleported.Contains(entity))
					entityWillBeTeleported.Add(entity);
			}
			else{
				if (executeMode == ExecuteMode.OnEnter)
					col.transform.position = destination.position.position;
				else if(!transformWillBeTeleported.Contains(col.transform))
					transformWillBeTeleported.Add(col.transform);
			}
		}
#if PROFORMER3D
		void OnTriggerExit(Collider col) {
#else
		void OnTriggerExit2D(Collider2D col) {
#endif
			DIEntity entity = col.GetComponentInParent<DICharacterBody>() ? col.GetComponent<DICharacterBody>().motor : col.GetComponentInParent<DIEntity>() ? col.GetComponentInParent<DIEntity>() : col.GetComponent<DIEntity>();
			if (entity)
			{
				if (entityWillBeTeleported.Contains(entity))
					entityWillBeTeleported.Remove(entity);
			}
			else
			{
				if (transformWillBeTeleported.Contains(col.transform))
					transformWillBeTeleported.Remove(col.transform);
			}
		}


		public void DoTeleport() {
			foreach(DIEntity entity in entityWillBeTeleported)
				entity.position.SetPosition(destination.position.levelPathPosition, destination.position.position);
			foreach (Transform tr in transformWillBeTeleported)
				tr.position = destination.position.position;
			entityWillBeTeleported.Clear();
			transformWillBeTeleported.Clear();
		}
	}
}
