﻿using UnityEngine;
using System.Collections;
using DI.Proformer.ToolHelper;
using System.Collections.Generic;
using System;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace DI.Proformer {
	public class DISpecialArea : MonoBehaviour {

		[SerializeField, HideInInspector]
#pragma warning disable 0649
		private SpecialAreaActionExtended moveSpeedModify, gravityModify, forcingHorizontal;
#pragma warning restore 0649

		Dictionary<DIControllerMotor, float> moveSpeedOriginalValue = new Dictionary<DIControllerMotor, float>();
		Dictionary<DIControllerMotor, float> gravityOriginalValue = new Dictionary<DIControllerMotor, float>();
		Dictionary<DIController, Vector3> forcingHorizontalMember = new Dictionary<DIController, Vector3>();

		[System.Serializable]
		public class SpecialAreaAction {
			public bool permission;
			public float value;

			public enum Operation { Add, Substract, Multiply }
			public Operation operation;
		}
		[System.Serializable]
		public class SpecialAreaActionExtended : SpecialAreaAction
		{
			public float maxValue;
		}

		[HideInInspector]
		public DIRigidbody _rigidbody;
#if PROFORMER3D
		[HideInInspector]
		public Collider _collider;
#else
		[HideInInspector]
		public Collider2D _collider;
#endif

#if PROFORMER3D
		void OnTriggerEnter(Collider col) {
#else
		void OnTriggerEnter2D(Collider2D col) {
#endif
			DICharacterBody body = col.GetComponent<DICharacterBody>();
			if (body) {
				if (moveSpeedModify.permission) {
					moveSpeedOriginalValue.Add(body.motor, body.motor.moveSpeed);
					if (moveSpeedModify.operation == SpecialAreaAction.Operation.Add)
						body.motor.moveSpeed += moveSpeedModify.value;
					else if (moveSpeedModify.operation == SpecialAreaAction.Operation.Substract)
						body.motor.moveSpeed -= moveSpeedModify.value;
					else if(moveSpeedModify.operation == SpecialAreaAction.Operation.Multiply)
						body.motor.moveSpeed *= moveSpeedModify.value;

				}
				if (gravityModify.permission)
				{
					gravityOriginalValue.Add(body.motor, body.motor._gravity);
					if (gravityModify.operation == SpecialAreaAction.Operation.Add)
						body.motor._gravity += gravityModify.value;
					else if (gravityModify.operation == SpecialAreaAction.Operation.Substract)
						body.motor._gravity -= gravityModify.value;
					else if (gravityModify.operation == SpecialAreaAction.Operation.Multiply)
						body.motor._gravity *= gravityModify.value;
				}
				if (forcingHorizontal.permission) {
					forcingHorizontalMember.Add(body.motor, Vector3.zero);
					body.motor.onBeforeCalculation += applyForceHorizontal;
				}
			}
		}

		private void applyForceHorizontal(ref Vector3 velocity, DIController controller)
		{
			forcingHorizontalMember[controller] += Vector3.right * forcingHorizontal.value * Time.deltaTime;
			if (forcingHorizontalMember[controller].magnitude > (Vector3.right * forcingHorizontal.maxValue).magnitude)
				forcingHorizontalMember[controller] = Vector3.right * forcingHorizontal.maxValue;
			velocity += forcingHorizontalMember[controller] * Time.deltaTime;
		}


#if PROFORMER3D
		void OnTriggerExit(Collider col) {
#else
		void OnTriggerExit2D(Collider2D col)
		{
#endif
			DICharacterBody body = col.GetComponent<DICharacterBody>();
			if (body)
			{
				if(moveSpeedOriginalValue.ContainsKey(body.motor)){
					body.motor.moveSpeed = moveSpeedOriginalValue[body.motor];
					moveSpeedOriginalValue.Remove(body.motor);
				}
				if (gravityOriginalValue.ContainsKey(body.motor))
				{
					body.motor._gravity = gravityOriginalValue[body.motor];
					gravityOriginalValue.Remove(body.motor);
				}
				if (forcingHorizontalMember.ContainsKey(body.motor))
				{
					body.motor.onBeforeCalculation -= applyForceHorizontal;
					forcingHorizontalMember.Remove(body.motor);
				}
			}
		}
	}
}
