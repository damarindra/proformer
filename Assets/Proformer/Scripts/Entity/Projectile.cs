﻿using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

using DI.PoolingSystem;

namespace DI.Proformer {
	public class Projectile : DIObject, IPoolObject
	{
		public float speed = 5f;
		public LayerMask collideMask;
		public int horizontalRayCount = 2, verticalRayCount = 2;

		float rayHorLength, rayVertLength;

		[HideInInspector]
		public Vector3 projectileDir = Vector3.right;

		protected override void Start()
		{
			base.Start();
			OnSetup();
			GiveDamage gd = GetComponentInChildren<GiveDamage>();
			if (gd)
				gd.triggered = ((c) => gameObject.SetActive(false));
		}

		void Update() {
			CollideCast();
			transform.Translate(projectileDir * speed * Time.deltaTime, Space.World);
		}

		void CollideCast() {
			for (int i = 0; i < 4; i++) {
				Vector3 rayOrigin = transform.position ;
				Vector3 rayDirection = projectileDir;
				if (i == 0)
					rayDirection = projectileDir;
				else if (i == 1)
					rayDirection = -projectileDir;
				else if (i == 2)
					rayDirection = Vector3.up;
				else if (i == 3)
					rayDirection = Vector3.down;

				int rayCount = i < 2 ? horizontalRayCount : verticalRayCount;
				if (i < 2)
				{
					if (rayCount > 1)
						rayOrigin -= Vector3.up * (_collider.bounds.size.y / 2);
				}
				else {
					if (rayCount > 1)
						rayOrigin -= projectileDir * (_collider.bounds.size.x / 2);
				}
				for (int rayIndex = 0; rayIndex < rayCount; rayIndex++) {
					Ray ray = new Ray(rayOrigin, rayDirection);
					ProformerRaycastHit hit;
					if (i < 2)
					{
						InternalTool.ProformerRaycast(ray, out hit, rayHorLength, collideMask);
						Debug.DrawRay(rayOrigin, rayDirection * rayHorLength);

						rayOrigin.y += (_collider.bounds.size.y / (horizontalRayCount-1));
					}
					else {
						InternalTool.ProformerRaycast(ray, out hit, rayVertLength, collideMask);
						Debug.DrawRay(rayOrigin, rayDirection * rayVertLength);

						rayOrigin += projectileDir * (_collider.bounds.size.x / (verticalRayCount - 1));
					}

					if (hit.collider == _collider)
						continue;

					if (hit.collider != null) {
						if(!hit.collider.GetComponent<DICharacterBody>() || hit.collider.GetComponent<DICharacterBody>().motor.isInvisible)
							Disable();
						return;
					}
					
				}
			}
		}

		public void OnObjectReuse(UnityEngine.Object obj)
		{
			rayHorLength = _collider.bounds.size.x * .5f;
			rayVertLength = _collider.bounds.size.y * .5f;
			DIControllerMotor motor = (DIControllerMotor)obj;
			transform.rotation = motor.transform.rotation;

			if (motor != null) {
				projectileDir = motor.direction;
			}
		}

		public void OnSetup()
		{

		}

		public void Disable()
		{
			gameObject.SetActive(false);
		}
	}
}