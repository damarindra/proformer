﻿using UnityEngine;
using System.Collections;
using System;

namespace DI.PoolingSystem{
	public class PoolObject : MonoBehaviour, IPoolObject {
		
		public int size = 10;

		public virtual void Disable()
		{
		}

		public virtual void OnObjectReuse(UnityEngine.Object obj)
		{
		}

		public virtual void OnSetup()
		{
		}

	}
}
