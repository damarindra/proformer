﻿using UnityEngine;
using DI.Proformer.Manager;

namespace DI.Proformer {
	public class DIObject : DIEntity {

		[HideInInspector]
		public GameObject rendererObject;
#pragma warning disable 0109
		[HideInInspector]
		public new Renderer renderer;
#pragma warning restore 0109

		[HideInInspector]
#if PROFORMER3D
		public Collider _collider;
#else
		public Collider2D _collider;
#endif
		public enum ColliderAllowed {
			None = 0, Box = 1, CircleSphere = 2, PolygonCapsule = 4, All = Box | CircleSphere | PolygonCapsule
		}
		/// <summary>Editor Things!</summary>
		[HideInInspector]
		public ColliderAllowed colliderAllowed = ColliderAllowed.All;
		/// <summary>Editor Things!</summary>
		[HideInInspector]
		public bool _colliderOnChildren = true;

		protected override void Start()
		{
			base.Start();
		}

		public bool FartherThan(DICharacterBody body)
		{
			return FartherThan(body.motor);
		}

		public bool FartherThan(DIEntity pEntity)
		{
			if (position.levelPath == pEntity.position.levelPath)
			{
				return position.rawHorizontal > pEntity.position.rawHorizontal;
			}
			else
			{
				if (GameManager.Instance.levelBound.loop)
				{
					int dir = position.levelPathPosition - pEntity.position.levelPathPosition;
					if (Mathf.Abs(dir) > GameManager.Instance.levelBound.levelPath.Count / 2)
					{
						if (dir > 0)
						{
							return false;
						}
						else
						{
							return true;
						}
					}
					else
					{
						if (dir > 0)
						{
							return true;
						}
						else
						{
							return false;
						}
					}
				}
				else
				{
					return position.levelPathPosition > pEntity.position.levelPathPosition;
				}
			}
		}
	}
}