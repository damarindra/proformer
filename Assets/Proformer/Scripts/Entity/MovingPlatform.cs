﻿using UnityEngine;
using DI.Proformer.ToolHelper;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace DI.Proformer {
	[ExecuteInEditMode]
	/// <summary>
	/// Moving Platform
	/// </summary>
	public class MovingPlatform : DIObject {

		[System.Flags]
		public enum SpecificPassenger {
			ALL = 0, RIGIDBODY_ONLY = 1, ENTITY_ONLY = 2, RIGIDBODY_AND_ENTITY = RIGIDBODY_ONLY | ENTITY_ONLY
		}
		/// <summary>Set the specific passenger.</summary>
		public SpecificPassenger specificPassenger = SpecificPassenger.RIGIDBODY_AND_ENTITY;

		/// <summary>Passenger Layer Mask</summary>
		[HideInInspector]
		public LayerMask passengerMask;
#if PROFORMER3D
		public BoxCollider bc { get { if (_bc) return _bc;
				else {
					_bc = _collider.GetComponent<BoxCollider>();
					return _bc;
				} } }
		[SerializeField]
		BoxCollider _bc;
#else
		public BoxCollider2D bc { get { if (_bc) return _bc;
				else {
					_bc = _collider.GetComponent<BoxCollider2D>();
					return _bc;
				} } }
		[SerializeField]
		BoxCollider2D _bc;
#endif
		GameObject triggerObj;
		FollowPath fp;
		MovingPlatformTrigger mpt;

		protected override void Start()
		{
			if (!Application.isPlaying)
				passengerMask = 1 << LayerMask.NameToLayer("Player");
			else {
				base.Start();
				fp = GetComponent<FollowPath>();
				triggerObj = new GameObject("Trigger");
				triggerObj.transform.rotation = bc.transform.rotation;
				triggerObj.transform.position = bc.transform.position;
				triggerObj.transform.parent = transform;
				triggerObj.transform.localScale = Vector3.one;
				mpt = triggerObj.AddComponent<MovingPlatformTrigger>();
				mpt.passenger = passengerMask;
			}
		}

		void Update() {
			if (!Application.isPlaying)
				return;
			mpt.velocity = fp.velocity;
		}
	}
}
