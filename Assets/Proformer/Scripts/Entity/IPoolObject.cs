﻿using UnityEngine;
using System.Collections;

namespace DI.PoolingSystem{
	public interface IPoolObject{
		void OnSetup ();
		void OnObjectReuse (Object obj);
		void Disable ();

	}
}
