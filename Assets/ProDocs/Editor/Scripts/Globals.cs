﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DI.ProDocs {
	public static class Globals  {
		public static string PRO_DOCS_DATA_EDITOR_LOCATION = "Assets/ProDocs/Editor/Data/Prefs.asset";
	}
}
