﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace DI.ProDocs
{
	public class ProDocsEditorData : ScriptableObject
	{
		public Texture2D titleDocsImage;
		public Color headerColor = new Color(60 / 255f, 60 / 255f, 60 / 255f, 1);
		public Color navigationColor = Color.white;
		public Color mainColor = Color.white;
		public string docsLocation = "Assets/ProDocs/Editor/Data/Docs";
		public FontStyle header1 = new FontStyle(null, 60, true);
		public FontStyle header2 = new FontStyle(null, 48, true);
		public FontStyle header3 = new FontStyle(null, 36, true);
		public FontStyle header4 = new FontStyle(null, 26, true);
		public FontStyle header5 = new FontStyle(null, 18, true);
		public FontStyle header6 = new FontStyle(null, 12, true);
		public FontStyle normalText = new FontStyle(null, 12, false, false, true, true);
		public FontStyle editorText = new FontStyle(null, 12, false, false, true);
		public List<DocsFile> docs = new List<DocsFile>();

		public static ProDocsEditorData Instance
		{
			get {return Init(); }
		}
		static ProDocsEditorData _instance = null;
		static ProDocsEditorData Init()
		{
			ProDocsEditorData data = _instance;
			if (data == null) {
				//Find at the location
				data = AssetDatabase.LoadAssetAtPath(Globals.PRO_DOCS_DATA_EDITOR_LOCATION, typeof(ProDocsEditorData)) as ProDocsEditorData;

				if (data == null) {
					//create
					data = ScriptableObject.CreateInstance<ProDocsEditorData>();
					AssetDatabase.CreateAsset(data, Globals.PRO_DOCS_DATA_EDITOR_LOCATION);
					AssetDatabase.SaveAssets();
					AssetDatabase.Refresh();
				}
				_instance = data;
			}
			return _instance;
		}

		[PreferenceItem("Pro Docs Editor")]
		public static void ProDocsPreferences()
		{
			if (Selection.activeObject != Instance)
				Selection.activeObject = Instance;
			else
				return;
			if(ProDocsEditorDataEditor._editor != null)
				ProDocsEditorDataEditor._editor.OnInspectorGUI();
			//Undo.RecordObject(Instance, "Pro Docs Data");
			/*
			EditorGUILayout.LabelField("Decoration", EditorStyles.boldLabel);
			ProDocsEditorData.Instance.titleDocsImage = EditorGUILayout.ObjectField("Title Image", Instance.titleDocsImage, typeof(Texture2D), false) as Texture2D;
			Instance.headerColor = EditorGUILayout.ColorField("Header", Instance.headerColor);
			Instance.navigationColor = EditorGUILayout.ColorField("Navigation", Instance.navigationColor);
			Instance.mainColor = EditorGUILayout.ColorField("Main", Instance.mainColor);

			EditorGUILayout.LabelField("Text Rich", EditorStyles.boldLabel);*/

			EditorUtility.SetDirty(Instance);
		}
	}

	[System.Serializable]
	public class FontStyle {
		public Font font;
		public int fontSize = 12;
		public bool bold, italic, wordWarp, richText;

		public FontStyle(Font font, int fontSize, bool bold = false, bool italic = false, bool wordWarp = false, bool richText = false) {
			this.font = font;
			this.fontSize = fontSize;
			this.bold = bold;
			this.italic = italic;
			this.wordWarp = wordWarp;
			this.richText = richText;
		}

		public GUIStyle ConvertToGUIStyle(GUIStyle from = null) {
			GUIStyle result = null;
			if (from != null)
				result = new GUIStyle(from);
			else result = new GUIStyle();

			if (this.font != null)
				result.font = this.font;
			result.fontStyle = bold && italic ? UnityEngine.FontStyle.BoldAndItalic : bold ? UnityEngine.FontStyle.Bold : italic ? UnityEngine.FontStyle.Italic : UnityEngine.FontStyle.Normal;
			result.fontSize = fontSize;
			result.wordWrap = wordWarp;
			result.richText = richText;

			return result;
		}
		public GUIStyle FlattenGUIStyleToNormalStyle(GUIStyle from = null)
		{
			GUIStyle result = ConvertToGUIStyle(from);
			result.active.textColor = result.normal.textColor;
			result.focused.textColor = result.normal.textColor;
			return result;
		}
	}

	[CustomEditor(typeof(ProDocsEditorData))]
	public class ProDocsEditorDataEditor : Editor {

		public static ProDocsEditorDataEditor _editor;
		ProDocsEditorData pded;

		void OnEnable()
		{
			if (pded == null)
				pded = (ProDocsEditorData)target;
			if (_editor == null)
				_editor = this;
		}

		public override void OnInspectorGUI()
		{
			base.OnInspectorGUI();
			if (pded == null)
				return;
			EditorGUI.BeginChangeCheck();

			//serializedObject.FindProperty("header1").isExpanded = EditorGUILayout.Foldout(serializedObject.FindProperty("header1").isExpanded, serializedObject.FindProperty("header1").displayName);
			EditorGUI.indentLevel += 1;

			EditorGUI.indentLevel -= 1;
			
			if (EditorGUI.EndChangeCheck()) {
				serializedObject.ApplyModifiedProperties();
			}
		}
	}
}