﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace DI.ProDocs {
	public class ProDocsWindow : EditorWindow {

		public static ProDocsWindow _window;

		int headerHeight = 60;
		int searchWidth = 300;
		int navigationWidth = 250;
		Vector2 navigationScrollPosition, mainScrollPosition, mainEditorScrollPosition;

		Rect navigationViewSize = new Rect(0,0,1,1);
		Rect mainViewSize = new Rect(0,0,1,1);

		DocsFile docSelected = null;

		List<string> docTree = new List<string>();
		List<bool> foldStatus = new List<bool>();

		float considerAsWritingTime = 1.5f;
		double nextStoppedWriting;

		string searchStrTemp = "";
		string searchStr = "";
		bool showSearch = false;

		int getMainViewWidth { get {
				return Screen.width - navigationWidth - 8;
			} }
		int getMainViewHeight
		{
			get { return Screen.height - headerHeight - 20; }
		}

		[MenuItem("Window/ProDocs/ProDocs Window")]
		static void Init()
		{
			_window = (ProDocsWindow)EditorWindow.GetWindow(typeof(ProDocsWindow));
			_window.titleContent = new GUIContent("Pro Docs","");
			_window.Show();
		}

		void OnGUI()
		{
			if (docTree.Count == 0)
				GenerateFoldInfo(ProDocsEditorData.Instance.docs.ToArray());
			///Layout Header
			//Reserve header size
			Rect headerRect = GUILayoutUtility.GetRect(EditorGUIUtility.currentViewWidth, headerHeight);
			//draw bg
			EditorGUI.DrawRect(headerRect, ProDocsEditorData.Instance.headerColor);
			//Show Title Image
			if (ProDocsEditorData.Instance.titleDocsImage != null) {
				//draw title
				EditorGUI.DrawPreviewTexture(new Rect(headerRect.x + 20, headerRect.y + 4, ProDocsEditorData.Instance.titleDocsImage.width, headerRect.height - 8), ProDocsEditorData.Instance.titleDocsImage, null);
			}
			//draw search
			if (!EditorPrefs.GetBool("ProDocsEditorMode")) {
				GUI.SetNextControlName("Search Field");
				searchStrTemp = GUI.TextField(new Rect(headerRect.x + headerRect.width - searchWidth, headerRect.y + headerRect.height - EditorGUIUtility.singleLineHeight - 4, searchWidth, EditorGUIUtility.singleLineHeight), searchStrTemp, GUI.skin.FindStyle("ToolbarSeachTextField"));

				if (Event.current.keyCode == KeyCode.Return && GUI.GetNameOfFocusedControl() == "Search Field") {
					//Search
					searchStr = searchStrTemp;
					showSearch = true;
					GUIUtility.keyboardControl = 0;
					DrawSearchResult(searchStr);
					Repaint();
					return;
				}
			}
			//Draw refresh
#if PRO_DOCS_EDITOR_MODE

			if (GUI.Button(new Rect(headerRect.x + headerRect.width - searchWidth - 80, headerRect.y + headerRect.height - 34, 30, 30), "o"))
			{
				LoadDocsAndGenerateFoldStatus();
			}
			EditorPrefs.SetBool("ProDocsEditorMode", EditorGUI.Foldout(new Rect(headerRect.x + headerRect.width - searchWidth - 80 - 30, headerRect.y + headerRect.height - 34, 30, 30), EditorPrefs.GetBool("ProDocsEditorMode"), ""));
#endif
			
			//BEGIN MANUAL LAYOUT

			//GUI.BeginScrollView is calculate manually, all view rect, scroll position is manual.
			EditorGUI.DrawRect(new Rect(navigationWidth, headerHeight, 4, getMainViewHeight), ProDocsEditorData.Instance.headerColor);

			if (navigationViewSize.y == 0)
			{
				navigationViewSize.y = headerHeight;
			}

			Rect navigationScrollViewPos = new Rect(0, headerHeight, navigationWidth, getMainViewHeight);
			EditorGUI.DrawRect(new Rect(0, headerHeight, navigationWidth, getMainViewHeight), ProDocsEditorData.Instance.navigationColor);
			navigationScrollPosition = GUI.BeginScrollView(navigationScrollViewPos, navigationScrollPosition, navigationViewSize);
			DrawNavigation();
			
			GUI.EndScrollView();
			
			if (mainViewSize.y == 0)
			{
				mainViewSize.y = headerHeight;
			}
			if (mainViewSize.x == 0) {
				mainViewSize.x = navigationWidth + 4;
			}
			if (mainViewSize.width == 1)
				mainViewSize.width = Screen.width - navigationWidth - 20;
			EditorGUI.DrawRect(new Rect(navigationWidth + 4, headerHeight, getMainViewWidth, getMainViewHeight), ProDocsEditorData.Instance.mainColor);

			if (docSelected != null) {
#if PRO_DOCS_EDITOR_MODE
				if (EditorPrefs.GetBool("ProDocsEditorMode"))
					DrawMainEditor();
				else {
#endif
					Rect mainScrollViewPos = new Rect(navigationWidth + 4, headerHeight, getMainViewWidth, getMainViewHeight);
					mainScrollPosition = GUI.BeginScrollView(mainScrollViewPos, mainScrollPosition, mainViewSize);

					if (!showSearch)
						DrawMainPreview();
					else
						DrawSearchResult(searchStr);
					GUI.EndScrollView();
#if PRO_DOCS_EDITOR_MODE
				}
#endif

			}

			//END MANUAL LAYOUT

		}

		void DrawSearchResult(string key)
		{
			List<DocsFile> resultsTitle = new List<DocsFile>();
			List<DocsFile> resultsContent = new List<DocsFile>();

			foreach (DocsFile df in ProDocsEditorData.Instance.docs) {
				//Find from Title
				if (df.title.Contains(key)) {
					resultsTitle.Add(df);
				}
				//Find from Content
				if (df.content.Contains(key))
					resultsContent.Add(df);

			}

			GUIStyle btnLabelStyle = new GUIStyle(EditorStyles.label);
			btnLabelStyle.fontSize = (int)(ProDocsEditorData.Instance.normalText.fontSize * 1.5f);

			Rect lastMainSetupPosition = new Rect(mainViewSize.x + 20, mainViewSize.y, 1, 1);

			Vector2 titleSize = ProDocsEditorData.Instance.header3.FlattenGUIStyleToNormalStyle(EditorStyles.label).CalcSize(new GUIContent("Search From Title"));
			GUI.Label(new Rect(lastMainSetupPosition.position, titleSize), "Search From Title", ProDocsEditorData.Instance.header3.FlattenGUIStyleToNormalStyle(EditorStyles.label));
			lastMainSetupPosition.position = NextLine(lastMainSetupPosition.position, titleSize);

			foreach (DocsFile title in resultsTitle) {

				Vector2 _size = btnLabelStyle.CalcSize(new GUIContent(title.title));
				Rect _btnRect = new Rect(lastMainSetupPosition.position, _size);


				if (GUI.Button(_btnRect, title.title, btnLabelStyle)) {
					//DO Something
					docSelected = title;
					showSearch = false;
				}
				lastMainSetupPosition.position = NextLine(lastMainSetupPosition.position, _size);

			}

			titleSize = ProDocsEditorData.Instance.header3.FlattenGUIStyleToNormalStyle(EditorStyles.label).CalcSize(new GUIContent("Search From Content"));
			GUI.Label(new Rect(lastMainSetupPosition.position, titleSize), "Search From Content", ProDocsEditorData.Instance.header3.FlattenGUIStyleToNormalStyle(EditorStyles.label));
			lastMainSetupPosition.position = NextLine(lastMainSetupPosition.position, titleSize);

			foreach (DocsFile content in resultsContent)
			{

				Vector2 _size = btnLabelStyle.CalcSize(new GUIContent(content.content));
				Rect _btnRect = new Rect(lastMainSetupPosition.position, _size);

				if (GUI.Button(_btnRect, content.content, btnLabelStyle))
				{
					//DO Something
					docSelected = content;
					showSearch = false;
				}
				lastMainSetupPosition.position = NextLine(lastMainSetupPosition.position, _size);

			}
		}

		void DrawMainEditor()
		{

			//DrawTextWarp(new GUIStyle(EditorStyles.textArea), docSelected.content, ref lastMainSetupPosition);
			Rect contentRect = new Rect(mainViewSize.x, mainViewSize.y + EditorGUIUtility.singleLineHeight + EditorGUIUtility.standardVerticalSpacing, getMainViewWidth - 15, getMainViewHeight - (EditorGUIUtility.singleLineHeight + EditorGUIUtility.standardVerticalSpacing));
			DoTextEditor(ref docSelected.content, contentRect);
			//DoTextEditorGUILayout(ref docSelected.content);
		}
		
		void DrawMainPreview()
		{

			Rect lastMainSetupPosition = new Rect(mainViewSize.x + 20, mainViewSize.y, 1, 1);

			//Split all text
			var contents = docSelected.content.Split('\n');

			foreach (string cont in contents)
			{
				if (cont.StartsWith("###### "))
				{
					string _cont = cont.Substring(7);
					DrawHeader(ProDocsEditorData.Instance.header6.FlattenGUIStyleToNormalStyle(EditorStyles.label), _cont, ref lastMainSetupPosition, false);
				}
				else if (cont.StartsWith("##### "))
				{
					string _cont = cont.Substring(6);
					DrawHeader(ProDocsEditorData.Instance.header5.FlattenGUIStyleToNormalStyle(EditorStyles.label), _cont, ref lastMainSetupPosition, false);
				}
				else if (cont.StartsWith("#### "))
				{
					string _cont = cont.Substring(5);
					DrawHeader(ProDocsEditorData.Instance.header4.FlattenGUIStyleToNormalStyle(EditorStyles.label), _cont, ref lastMainSetupPosition, false);
				}
				else if (cont.StartsWith("### "))
				{
					string _cont = cont.Substring(4);
					DrawHeader(ProDocsEditorData.Instance.header3.FlattenGUIStyleToNormalStyle(EditorStyles.label), _cont, ref lastMainSetupPosition, false);
				}
				else if (cont.StartsWith("## "))
				{
					string _cont = cont.Substring(3);
					DrawHeader(ProDocsEditorData.Instance.header2.FlattenGUIStyleToNormalStyle(EditorStyles.label), _cont, ref lastMainSetupPosition, true);
				}
				else if (cont.StartsWith("# "))
				{
					string _cont = cont.Substring(2);
					DrawHeader(ProDocsEditorData.Instance.header1.FlattenGUIStyleToNormalStyle(EditorStyles.label), _cont, ref lastMainSetupPosition, true);
				}
				else if (cont.StartsWith("[center]"))
				{
					GUIStyle style = ProDocsEditorData.Instance.normalText.FlattenGUIStyleToNormalStyle(EditorStyles.label);
					style.alignment = TextAnchor.UpperCenter;
					DrawTextWarp(style, cont.Substring(8, cont.Length - 8), ref lastMainSetupPosition);
				}
				else if (cont.StartsWith("[right]"))
				{
					GUIStyle style = ProDocsEditorData.Instance.normalText.FlattenGUIStyleToNormalStyle(EditorStyles.label);
					style.alignment = TextAnchor.UpperRight;
					DrawTextWarp(style, cont.Substring(7, cont.Length - 7), ref lastMainSetupPosition);
				}
				else if (cont.StartsWith("<img=") && cont.EndsWith(">"))
				{
					string modifier = cont.Substring(1, cont.IndexOf('>') - 1);
					string[] modifiers = modifier.Split(',');
					Texture2D image = null;
					double scale = 1;
					string align = "left";
					foreach (string mod in modifiers)
					{
						if (mod.StartsWith("img="))
						{
							//Load Image
							image = AssetDatabase.LoadAssetAtPath(ProDocsEditorData.Instance.docsLocation + "/" + mod.Substring(5, mod.Length - 6), typeof(Texture2D)) as Texture2D;
						}
						else if (mod.StartsWith("scale=") || mod.StartsWith(" scale="))
						{
							//scale the image
							if (!System.Double.TryParse(mod.Substring(mod.Length - 1), out scale))
								scale = 1;
						}
						else if (mod.StartsWith("align=") || mod.StartsWith(" align="))
						{
							//align
							align = mod.Substring(mod.IndexOf('=') + 1);
						}
					}
					if (image != null)
					{
						Vector2 size = new Vector2(image.width * (float)scale, image.height * (float)scale);
						Vector2 pos = lastMainSetupPosition.position;
						if (align == "center")
							pos.x = lastMainSetupPosition.x + getMainViewWidth / 2 - size.x / 2;
						else if (align == "right")
							pos.x = getMainViewWidth - size.x / 2;
						EditorGUI.DrawPreviewTexture(new Rect(pos, size), image);
						lastMainSetupPosition.position = NextLine(lastMainSetupPosition.position, size);
					}
					else
					{
						EditorGUI.LabelField(new Rect(lastMainSetupPosition.x, lastMainSetupPosition.y, 200, EditorGUIUtility.singleLineHeight), "Image Not Found!");
						lastMainSetupPosition.position = NextLine(lastMainSetupPosition.position, new Vector2(200, EditorGUIUtility.singleLineHeight));
					}
				}
				else
				{
					DrawTextWarp(ProDocsEditorData.Instance.normalText.FlattenGUIStyleToNormalStyle(EditorStyles.label), cont, ref lastMainSetupPosition);
				}

			}

			mainViewSize.size = lastMainSetupPosition.size;
			mainViewSize.width += 50;
		}

		
		void DoTextEditorGUILayout(ref string text)
		{
			//This trick is really DIRTY!, but what can I do?
			//OTHER USING MANUAL CALCULATION (GUI & EDITORGUI), ONLY THIS ONE WHICH USING AUTO LAYOUTING.
			EditorGUILayout.BeginHorizontal();
			EditorGUILayout.LabelField("", GUILayout.Width(navigationWidth), GUILayout.Height(getMainViewHeight));
			//Debug.Log(GUILayoutUtility.GetRect(navigationWidth, getMainViewHeight));
			

			GUIStyle textArea = new GUIStyle(EditorStyles.textArea);
			textArea.wordWrap = true;
			mainEditorScrollPosition = EditorGUILayout.BeginScrollView(mainEditorScrollPosition);
			text = EditorGUILayout.TextArea(text, textArea);
			EditorGUILayout.EndScrollView();
			EditorGUILayout.EndHorizontal();
		}

		//[System.Obsolete("Hard to setup manually, use DoTextEditorGUI Instead")]
		void DoTextEditor(ref string text, Rect contentRect)
		{

			EditorGUI.LabelField(new Rect(mainViewSize.x, mainViewSize.y, getMainViewWidth, EditorGUIUtility.singleLineHeight), "", EditorStyles.toolbar);
			GUIStyle textArea = new GUIStyle(EditorStyles.textArea);
			textArea.wordWrap = true;

			//Still writing
			if (EditorApplication.timeSinceStartup < nextStoppedWriting)
				nextStoppedWriting = EditorApplication.timeSinceStartup + considerAsWritingTime;
			//Stopped writing
			else
			{
				nextStoppedWriting = EditorApplication.timeSinceStartup + considerAsWritingTime;
				//Save Text
				EditorUtility.SetDirty(docSelected);
				//Undo.RecordObject(docSelected, "Edit Content");	
			}

			text = GUI.TextArea(contentRect, text, textArea);

			TextEditor textEditor = (TextEditor)GUIUtility.GetStateObject(typeof(TextEditor), GUIUtility.keyboardControl);
			if (Event.current != null) {
				if (Event.current.type == EventType.ValidateCommand) {

					//Cut
					if (Event.current.commandName == "Cut")
					{
						//textEditor.Cut();
						//docSelected.content = docSelected.content.Remove(textEditor.selectIndex, GUIUtility.systemCopyBuffer.Length);
						Event.current.Use();
						Repaint();
					}
					//Copy
					else if (Event.current.commandName == "Copy")
					{
						//textEditor.Copy();
						Event.current.Use();
						Repaint();
					}
					//Paste
					else if (Event.current.commandName == "Paste")
					{
						//docSelected.content = docSelected.content.Insert(textEditor.cursorIndex, GUIUtility.systemCopyBuffer);
						Event.current.Use();
						Repaint();
					}
					//Select ALl
					else if (Event.current.commandName == "SelectAll")
					{
						//textEditor.SelectAll();
						Event.current.Use();
						Repaint();
					}
				}
				else if (Event.current.type == EventType.ExecuteCommand)
				{
					//Cut
					if (Event.current.commandName == "Cut")
					{
						textEditor.Cut();
						docSelected.content = docSelected.content.Remove(textEditor.cursorIndex < textEditor.selectIndex ? textEditor.cursorIndex : textEditor.selectIndex, GUIUtility.systemCopyBuffer.Length);
						Event.current.Use();
						Repaint();
					}
					//Copy
					else if (Event.current.commandName == "Copy")
					{
						textEditor.Copy();
						Event.current.Use();
						Repaint();
					}
					//Paste
					else if (Event.current.commandName == "Paste")
					{
						if(textEditor.SelectedText.Length != 0)
							docSelected.content = docSelected.content.Remove(textEditor.cursorIndex < textEditor.selectIndex ? textEditor.cursorIndex : textEditor.selectIndex, textEditor.SelectedText.Length);
						
						docSelected.content = docSelected.content.Insert(textEditor.cursorIndex < textEditor.selectIndex ? textEditor.cursorIndex : textEditor.selectIndex, GUIUtility.systemCopyBuffer);
						Event.current.Use();
						Repaint();
					}
					//Select ALl
					else if (Event.current.commandName == "SelectAll")
					{
						textEditor.SelectAll();
						Event.current.Use();
						Repaint();
					}
				}
			}
			
			
			//BEGIN SCROLL AREA
			//calculate the max height
			float scrollHeight = textArea.CalcHeight(new GUIContent(docSelected.content), contentRect.width);
			if (scrollHeight < contentRect.height)
				scrollHeight = contentRect.height;
			//Check if doing mouse scroll
			if (Event.current != null)
			{
				if (Event.current.type == EventType.scrollWheel) {
					if (contentRect.Contains(Event.current.mousePosition))
					{
						if (Event.current.type == EventType.repaint)
						{
							Event.current.Use();
						}
						textEditor.scrollOffset.y += Event.current.delta.y;
						Repaint();
					}
				}
			}

			textEditor.scrollOffset.y = GUI.VerticalScrollbar(new Rect(contentRect.x + contentRect.width, contentRect.y, 15, contentRect.height), textEditor.scrollOffset.y, contentRect.height, 0, scrollHeight);

			//END SCROLL AREA
		}


		void DrawHeader(GUIStyle style, string content, ref Rect lastSetupPosition, bool withLine = false)
		{
			Rect contentRect = DrawText(style, content, ref lastSetupPosition);

			style.fontSize = (int)(style.fontSize * .6f);
			Rect space = new Rect(lastSetupPosition.position, style.CalcSize(new GUIContent(content)));
			space.width = Screen.width - navigationWidth - 100;
			if (space.width < contentRect.width)
				space.width = contentRect.width;
			if (withLine)
			{
				//Draw line at 1/3 from top
				EditorGUI.DrawRect(new Rect(space.x, space.y + space.height * .2f, space.width, 1), new Color(50 / 255f, 50 / 255f, 50 / 255f, 1));
			}
			lastSetupPosition.position = NextLine(lastSetupPosition.position, new Vector2(0, space.height));
			lastSetupPosition.height = lastSetupPosition.y - headerHeight;

			if (lastSetupPosition.width < space.width)
				lastSetupPosition.width = (int)(space.width);
		}
		/// <summary>
		/// Draw Text with wordwarping
		/// </summary>
		/// <param name="style"></param>
		/// <param name="content"></param>
		/// <param name="lastSetupPosition"></param>
		/// <returns>return Text Rect</returns>
		Rect DrawTextWarp(GUIStyle style, string content, ref Rect lastSetupPosition)
		{
			GUIStyle textArea = new GUIStyle(EditorStyles.textArea);
			textArea.fontSize = style.fontSize;
			textArea.font = style.font;
			textArea.wordWrap = style.wordWrap;
			textArea.fontStyle = style.fontStyle;
			textArea.richText = style.richText;

			Rect contentRect = new Rect(lastSetupPosition.position.x, lastSetupPosition.y, getMainViewWidth - 50, textArea.CalcHeight(new GUIContent(content), getMainViewWidth - 50));
			
			///UNDERLINE
			/** STILL DON'T KNOW IT WORKS OR NOT. BUT CURRENTLY ONLY WORK ONE LINE TEXT
			while (content.IndexOf("<u>") != -1 && content.IndexOf("</u>") != -1)
			{
				Vector2 spaceXNeedToRemove = textArea.CalcSize(new GUIContent(""));
				string prefix = content.IndexOf("<u>") == 0 ? "" : content.Substring(0, content.IndexOf("<u>"));
				//First calculate if prefix height using CalcHeight and max width
				float realHeight = textArea.CalcHeight(new GUIContent(content), getMainViewWidth - 50);
				//Second calculalte the single line size
				Vector2 prefixSize = textArea.CalcSize(new GUIContent(prefix));
				//check if single line and realheight is different?
				if(prefixSize.y != realHeight)
				{
					//
				}

				prefixSize.x -= spaceXNeedToRemove.x;

				string _underline = content.Substring(content.IndexOf("<u>") + 3, content.IndexOf("</u>") - 3 - prefix.Length);
				EditorGUI.DrawRect(new Rect(lastSetupPosition.x + prefixSize.x, lastSetupPosition.y + prefixSize.y - EditorGUIUtility.standardVerticalSpacing, textArea.CalcSize(new GUIContent(_underline)).x - spaceXNeedToRemove.x, 1), Color.black);

				string tail = content.IndexOf("</u>") == content.Length - 4 ? "" : content.Substring(content.IndexOf("</u>") + 4);
				
				content = prefix + _underline + tail;
			}*/
			
			EditorGUI.SelectableLabel(contentRect, content, style);
			lastSetupPosition.position = NextLine(lastSetupPosition.position, new Vector2(0, contentRect.height));
			lastSetupPosition.height = lastSetupPosition.y - headerHeight;

			if (lastSetupPosition.width < contentRect.width)
				lastSetupPosition.width = (int)(contentRect.width);


			return contentRect;

		}
		/// <summary>
		/// Draw Text without worwarping
		/// </summary>
		/// <param name="style"></param>
		/// <param name="content"></param>
		/// <param name="lastSetupPosition"></param>
		/// <returns>return Text Rect</returns>
		Rect DrawText(GUIStyle style, string content, ref Rect lastSetupPosition)
		{
			GUIStyle textArea = new GUIStyle(EditorStyles.textArea);
			textArea.fontSize = style.fontSize;
			textArea.font = style.font;
			textArea.wordWrap = style.wordWrap;
			textArea.fontStyle = style.fontStyle;

			Rect contentRect = new Rect(lastSetupPosition.position, textArea.CalcSize(new GUIContent(content)));
			EditorGUI.SelectableLabel(contentRect, content, style);
			lastSetupPosition.position = NextLine(lastSetupPosition.position, new Vector2(0, contentRect.height));
			lastSetupPosition.height = lastSetupPosition.y - headerHeight;

			if (lastSetupPosition.width < contentRect.width)
				lastSetupPosition.width = (int)(contentRect.width);

			return contentRect;

		}

		void DrawNavigation()
		{
			if (docTree.Count == 0 || foldStatus.Count == 0)
				return;
			Vector2 lastNavSetupPosition = new Vector2(5, navigationViewSize.y);
			int xMax = 0;
			for (int i = 0; i < ProDocsEditorData.Instance.docs.Count; i++)
			{
				if (ProDocsEditorData.Instance.docs[i] != null) {
					string[] loc = ProDocsEditorData.Instance.docs[i].location.Substring(ProDocsEditorData.Instance.docsLocation.Length + 1).Split('/');
					string[] locBefore = i == 0 ? new string[]{ } : ProDocsEditorData.Instance.docs[i - 1].location.Substring(ProDocsEditorData.Instance.docsLocation.Length + 1).Split('/');

					lastNavSetupPosition.x = 0;

					int lastIndent = 0;
					int counter = 0;
					string currentPath = loc[0];
					while(counter < loc.Length)
					{
						if(counter != 0 && counter < loc.Length -1)
							currentPath += "/" + loc[counter];
						if (counter == loc.Length - 1)
						{
							//Rect btnRect = GUILayoutUtility.GetRect(new GUIContent(loc[counter]), EditorStyles.label);
							Rect btnRect = new Rect(new Vector2(0, lastNavSetupPosition.y), EditorStyles.label.CalcSize(new GUIContent(ProDocsEditorData.Instance.docs[i].title)));
							lastNavSetupPosition = NextLine(lastNavSetupPosition, btnRect.size);
							btnRect.x += 5 + lastIndent * 25;
							if(GUI.Button(btnRect, ProDocsEditorData.Instance.docs[i].title, EditorStyles.label))
							{
								docSelected = ProDocsEditorData.Instance.docs[i];
								if (showSearch)
									showSearch = false;
							}
							if (xMax < btnRect.x + btnRect.width)
								xMax = (int)btnRect.x + (int)btnRect.width;
						}
						else {
							bool doFold = false;
							if (locBefore.Length != 0 && locBefore.Length > counter) {
								if (loc[counter] != locBefore[counter]) {
									doFold = true;
								}
							}
							else
							{
								doFold = true;
							}
							Rect foldRect = new Rect(new Vector2(0, lastNavSetupPosition.y), EditorStyles.foldout.CalcSize(new GUIContent(loc[counter])));
							foldRect.x += 5 + lastIndent * 25;

							if (doFold) {
								//Rect foldRect = GUILayoutUtility.GetRect(new GUIContent(loc[counter]), EditorStyles.foldout);
								try {
									foldStatus[docTree.IndexOf(currentPath)] = EditorGUI.Foldout(foldRect, foldStatus[docTree.IndexOf(currentPath)], loc[counter]);
								}
								catch {
									//Catch if docs data in ProDocsEditorData is not equal with window data. Re-generateFoldInfo will fix it
									GenerateFoldInfo(ProDocsEditorData.Instance.docs.ToArray());
									goto stoploop;
								}
								
								lastNavSetupPosition = NextLine(lastNavSetupPosition, foldRect.size);
							}

							lastNavSetupPosition.x = foldRect.x;
							
							if (!foldStatus[docTree.IndexOf(currentPath)]) {
								break;
							}

							lastIndent += 1;
						}
						counter++;
					}
				}
				
			}

			stoploop:
			navigationViewSize = new Rect(0, headerHeight, navigationWidth, getMainViewHeight);
			navigationViewSize.y = headerHeight;
			navigationViewSize.width = xMax;
			navigationViewSize.height = lastNavSetupPosition.y;
		}

	
		public static void LoadDocs()
		{
			string[] docFiles = System.IO.Directory.GetFiles(ProDocsEditorData.Instance.docsLocation, "*.asset", System.IO.SearchOption.AllDirectories);
			ProDocsEditorData.Instance.docs.Clear();

			foreach (string docPath in docFiles)
			{
				DocsFile df = AssetDatabase.LoadAssetAtPath(docPath, typeof(DocsFile)) as DocsFile;
				if (df != null)
				{
					ProDocsEditorData.Instance.docs.Add(df);
				}
			}

			EditorUtility.SetDirty(ProDocsEditorData.Instance);
		}
		void LoadDocsAndGenerateFoldStatus()
		{
			LoadDocs();
			GenerateFoldInfo(ProDocsEditorData.Instance.docs.ToArray());
		}

		//TODO : GENERATE ALL PATH, current problem is we only now final path into docs file, not each branch, so we need to find each branch and setup the foldout things
		void GenerateFoldInfo(DocsFile[] paths)
		{
			List<string> docTree = new List<string>();
			List<bool> foldStatus = new List<bool>();

			foreach (DocsFile p in paths) {
				if (p == null)
					continue;
				string realPath = p.location.Substring(ProDocsEditorData.Instance.docsLocation.Length + 1);
				string[] slicedPath = realPath.Split('/');

				int i = 0;
				string pathCheck = slicedPath[i];
				do
				{
					if (docTree.IndexOf(pathCheck) == -1)
					{
						docTree.Add(pathCheck);
						if(this.docTree.IndexOf(pathCheck) != -1)
							foldStatus.Add(this.foldStatus[this.docTree.IndexOf(pathCheck)]);
						else
							foldStatus.Add(false);
					}
					i++;
					if(i < slicedPath.Length)
						pathCheck += "/" + slicedPath[i];

				} while (i <= slicedPath.Length);
			}

			this.docTree = docTree;
			this.foldStatus = foldStatus;
		}

		Vector2 NextLine(Vector2 current, Vector2 size)
		{
			current.y += size.y;
			current.y += EditorGUIUtility.standardVerticalSpacing;

			return current;
		}

#region Docs Creation

#if PRO_DOCS_EDITOR_MODE

		[MenuItem("Window/ProDocs/Create Doc")]
		public static void CreateAsset()
		{
			CreateDocs("docs");
		}

		[MenuItem("CONTEXT/MonoBehaviour/Create Docs")]
		private static void AddDocs(MenuCommand menuCommand)
		{
			CreateDocs(menuCommand.context.GetType().ToString());
		}

		static void CreateDocs(string name)
		{
			string locs = ProDocsEditorData.Instance.docsLocation == "" ? "Assets/ProDocs/Editor/Data/Docs" : ProDocsEditorData.Instance.docsLocation;
			if (!AssetDatabase.IsValidFolder(locs))
				locs = "Assets";
			string filePath = EditorUtility.SaveFilePanel("Pro Docs File", locs, name, "asset");
			if (!string.IsNullOrEmpty(filePath))
			{
				if (filePath.StartsWith(Application.dataPath))
				{
					filePath = "Assets" + filePath.Substring(Application.dataPath.Length);
				}
				Object _overwrite = AssetDatabase.LoadAssetAtPath(filePath, typeof(Object));
				if (_overwrite != null)
					DestroyImmediate(_overwrite, true);

				DocsFile data = ScriptableObject.CreateInstance<DocsFile>();
				var path = filePath.Split('/');
				string _name = path[path.Length - 1].Remove(6);
				data.title = _name;
				data.content = "## " + _name;
				AssetDatabase.CreateAsset(data, filePath);
				AssetDatabase.SaveAssets();
				AssetDatabase.Refresh();
			}
		}

		[MenuItem("Assets/Set Docs Location Here")]
		private static void SetAsDocsLocation()
		{
			ProDocsEditorData.Instance.docsLocation = AssetDatabase.GetAssetPath(Selection.activeObject);
			LoadDocs();
			EditorUtility.SetDirty(ProDocsEditorData.Instance);
		}
		[MenuItem("Assets/Set Docs Location Here", true)]
		private static bool SetAsDocsLocationValidation()
		{
			return AssetDatabase.IsValidFolder(AssetDatabase.GetAssetPath(Selection.activeObject));
		}

#endif
#endregion

	}
}
