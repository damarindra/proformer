﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace DI.ProDocs {
	public class DocsFile : ScriptableObject {

		public string title;
		[TextArea]
		public string content;
		public string location {
			get {
				return AssetDatabase.GetAssetPath(this);
			}
		}

	}
}
