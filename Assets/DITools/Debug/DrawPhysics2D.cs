﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DI.ToolHelper {
	public class DrawPhysics2D : MonoBehaviour {

		public bool boxCollider = true;
		public Color boxColliderColor = Color.green;

		public BoxCollider2D[] bcArray = null;
		
		public bool polygonCollider = true;
		public Color polygonColliderColor = Color.blue;

		public PolygonCollider2D[] pgArray = null;

#if UNITY_EDITOR
		// Use this for initialization
		void Start () {
			if (boxCollider)
				bcArray = FindObjectsOfType<BoxCollider2D>();
			if (polygonCollider) {
				pgArray = FindObjectsOfType<PolygonCollider2D>();
			}
		}
	
		// Update is called once per frame
		void Update () {
			foreach (BoxCollider2D bc in bcArray)
				DebugBoxCollider2D(bc);
			foreach (PolygonCollider2D pg in pgArray) {
				DebugPolygonCollider2D(pg);
			}
		}
#endif

		void DebugPolygonCollider2D(PolygonCollider2D pg2d)
		{
			for (int i = 1; i < pg2d.points.Length; i++) {
				Vector2 v1 = pg2d.points[i - 1];
				v1.x = (v1.x) * pg2d.transform.lossyScale.x + pg2d.transform.position.x;
				v1.y = (v1.y) * pg2d.transform.lossyScale.y + pg2d.transform.position.y;
				v1 = RotatePointAroundPivot(v1, (Vector2)pg2d.transform.position, pg2d.transform.eulerAngles);
				Vector2 v2 = pg2d.points[i];
				v2.x = (v2.x) * pg2d.transform.lossyScale.x + pg2d.transform.position.x;
				v2.y = (v2.y) * pg2d.transform.lossyScale.y + pg2d.transform.position.y;
				v2 = RotatePointAroundPivot(v2, (Vector2)pg2d.transform.position, pg2d.transform.eulerAngles);
				Debug.DrawLine(v1, v2, polygonColliderColor);

				if(i == 1)
				{


					v1 = pg2d.points[0];
					v1.x = (v1.x) * pg2d.transform.lossyScale.x + pg2d.transform.position.x;
					v1.y = (v1.y) * pg2d.transform.lossyScale.y + pg2d.transform.position.y;
					v1 = RotatePointAroundPivot(v1, (Vector2)pg2d.transform.position, pg2d.transform.eulerAngles);
					v2 = pg2d.points[pg2d.points.Length-1];
					v2.x = (v2.x) * pg2d.transform.lossyScale.x + pg2d.transform.position.x;
					v2.y = (v2.y) * pg2d.transform.lossyScale.y + pg2d.transform.position.y;
					v2 = RotatePointAroundPivot(v2, (Vector2)pg2d.transform.position, pg2d.transform.eulerAngles); Debug.DrawLine(v1, v2, polygonColliderColor);
				}
			}
		}

		void DebugBoxCollider2D(BoxCollider2D bc2d) {
			Vector3 centerPos = (Vector2)bc2d.transform.position + bc2d.offset;
			Vector2 size = Vector3.Scale(bc2d.size, (Vector2)bc2d.transform.lossyScale);
			Vector3 euler = bc2d.transform.eulerAngles;
			/*
			Vector3 topLeft = RotatePointAroundPivot(centerPos + -bc2d.transform.right * size.x * .5f + bc2d.transform.up * size.y * .5f, bc2d.transform.position, bc2d.transform.eulerAngles);
			Vector3 topRight = RotatePointAroundPivot(centerPos + bc2d.transform.right * size.x * .5f + bc2d.transform.up * size.y * .5f, bc2d.transform.position, bc2d.transform.eulerAngles);
			Vector3 bottomLeft = RotatePointAroundPivot(centerPos + -bc2d.transform.right * size.x * .5f - bc2d.transform.up * size.y * .5f, bc2d.transform.position, bc2d.transform.eulerAngles);
			Vector3 bottomRight = RotatePointAroundPivot(centerPos + bc2d.transform.right * size.x * .5f - bc2d.transform.up * size.y * .5f, bc2d.transform.position, bc2d.transform.eulerAngles);
			*/
			Vector3 topLeft = centerPos + -bc2d.transform.right * size.x * .5f + bc2d.transform.up * size.y * .5f;
			Vector3 topRight = centerPos + bc2d.transform.right * size.x * .5f + bc2d.transform.up * size.y * .5f;
			Vector3 bottomLeft = centerPos + -bc2d.transform.right * size.x * .5f - bc2d.transform.up * size.y * .5f;
			Vector3 bottomRight = centerPos + bc2d.transform.right * size.x * .5f - bc2d.transform.up * size.y * .5f;

			Debug.DrawLine(topLeft, topRight, boxColliderColor);
			Debug.DrawLine(topLeft, bottomLeft, boxColliderColor);
			Debug.DrawLine(bottomRight, topRight, boxColliderColor);
			Debug.DrawLine(bottomRight, bottomLeft, boxColliderColor);
		}

		Vector3 RotatePointAroundPivot(Vector3 point, Vector3 pivot, Vector3 euler)
		{
			Vector3 dir = point - pivot;
			dir = Quaternion.Euler(euler) * dir;
			point = dir + pivot;
			return point;
		}
	}
}
