﻿using UnityEngine;
using System.Collections;
using DI.Proformer.ToolHelper;
using DI.VisualScripting;
using System;
using DI.Proformer;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace DI.VisualScripting.Proformer
{
	public class DIAreaTrigger : DIObject, VisualScripting.IVisualScripting
	{
		[SerializeField, HideInInspector]
		private DIRigidbody rb;

		[Tag]
		public string targetTag;
		public bool listenToSpecificsGameObject = false;
		[Conditional("Hide", "listenToSpecificsGameObject", true, false)]
		public GameObject listenTo;
		public VisualScripting.DIRootComponent onTriggerEnter, onTriggerExit, onTriggerStay;

		public MonoBehaviour getMono
		{
			get
			{
				return this;
			}
		}

		public string visualScriptingName
		{
			get
			{
				return "Area Trigger";
			}
		}

		protected override void Start()
		{
			base.Start();
			_collider.isTrigger = true;
		}

#if PROFORMER2D
		protected virtual void OnTriggerEnter2D(Collider2D col)
#else
		protected virtual void OnTriggerEnter(Collider col) 
#endif
		{
			bool result = false;
			if (listenToSpecificsGameObject)
				result = col.gameObject == listenTo;
			else
				result = col.tag.Equals(targetTag);
			if (result) {
				if (onTriggerEnter != null)
					onTriggerEnter.Do();
			}
		}


#if PROFORMER2D
		protected virtual void OnTriggerStay2D(Collider2D col)
#else
		protected virtual void OnTriggerStay(Collider col)
#endif
		{
			bool result = false;
			if (listenToSpecificsGameObject)
				result = col.gameObject == listenTo;
			else
				result = col.tag.Equals(targetTag);
			if (result)
			{
				if (onTriggerStay != null)
					onTriggerStay.Do();
			}
		}

#if PROFORMER2D
		protected virtual void OnTriggerExit2D(Collider2D col)
#else
		protected virtual void OnTriggerExit(Collider col)
#endif
		{
			bool result = false;
			if (listenToSpecificsGameObject)
				result = col.gameObject == listenTo;
			else
				result = col.tag.Equals(targetTag);
			if (result)
			{
				if (onTriggerExit != null)
					onTriggerExit.Do();
			}
		}

		public DIRootComponent[] rootComponents
		{
			get
			{
				return new DIRootComponent[3] { onTriggerEnter, onTriggerExit, onTriggerStay};
			}

			set
			{
				if (value == null)
				{
					onTriggerEnter = null;
					onTriggerExit = null;
					onTriggerStay = null;
					return;
				}
				if (value.Length > 0)
				{
					onTriggerEnter = value[0];
				}
				if (value.Length > 1)
					onTriggerExit = value[1];
				if (value.Length > 2)
					onTriggerStay = value[2];
			}
		}
	}
}