﻿using UnityEngine;
using System;
using DI.Proformer;
using DI.Proformer.Manager;
using DI.Proformer.ToolHelper;
using DI.VisualScripting;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace DI.VisualScripting.Proformer
{
	[ExecuteInEditMode]
	/// <summary>
	/// Pickable item, this base class for Pickable item.
	/// Set tag for type of pickable item. (ex : health)
	/// Character Basic side, set Pickable Trigger Tag
	/// </summary>
	public class PickableVS : Pickable, VisualScripting.IVisualScripting
	{

		//[HeaderBackground(3, .3f, "Pickable")]
		public VisualScripting.DIRootComponent pickedAction;
		

		public MonoBehaviour getMono
		{
			get
			{
				return this;
			}
		}

		public string visualScriptingName
		{
			get
			{
				return "Pickable";
			}
		}

		public override void Triggered()
		{
			if (pickedAction != null)
				pickedAction.Do();
			base.Triggered();
		}

		public DIRootComponent[] rootComponents
		{
			get
			{
				return new DIRootComponent[1] { pickedAction };
			}

			set
			{
				if (value == null)
				{
					pickedAction = null;
					return;
				}
				if (value.Length > 0)
				{
					pickedAction = value[0];
				}
			}
		}
	}
}