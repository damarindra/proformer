﻿using UnityEngine;
using System.Collections;
using DI.Proformer;
using UnityEditor;
using DI.Proformer.ToolHelper;

namespace DI.VisualScripting.Proformer {

	[CustomEditor(typeof(DIAreaTrigger), true)]
	public class DIAreaTriggerEditor : DIObjectEditor
	{

		DIAreaTrigger trigger;
		protected int choosen;

		public override void OnEnable()
		{
			trigger = (DIAreaTrigger)target;
			withoutDefault = true;
			base.OnEnable();
		}

		public override void CustomInspectorGUI()
		{
			EditorGUI.BeginChangeCheck();
			serializedObject.Update();

			EditorPrefs.SetInt("ProformerBasicToolbarInspector", GUILayout.Toolbar(EditorPrefs.GetInt("ProformerBasicToolbarInspector", 0), new string[2] { "Setup", "Inspector" }));
			choosen = EditorPrefs.GetInt("ProformerBasicToolbarInspector", 0);
			if (choosen == 0)
			{
				ShowTransformInspector();
				DrawRendererInspector();
				DrawColliderInspector();
				EditorGUILayout.PropertyField(serializedObject.FindProperty("rb"));

			}
			else if (choosen == 1)
			{
				DrawBaseInspector();
			}

			if (EditorGUI.EndChangeCheck())
			{
				serializedObject.ApplyModifiedProperties();
				EditorUtility.SetDirty(trigger);
			}
		}

		public override void OnSceneGUI()
		{
			base.OnSceneGUI();

		}
	}
}
