﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using DI.Proformer;
using DI.Proformer.Manager;
using DI.Proformer.ToolHelper;

namespace DI.VisualScripting.Proformer {
	public class VSProformerMenu {

		[MenuItem("GameObject/VisualScripting/Proformer/Area Trigger")]
		static void CreateAreaTrigger()
		{
			GameObject go = new GameObject("AreaTrigger");
			go.AddComponent<DIAreaTrigger>();
			Selection.activeGameObject = go;
		}

		[MenuItem("GameObject/VisualScripting/Proformer/Pickable", false, 32)]
		static void CreatePickable()
		{
			GameObject go = new GameObject("PickableVS");
			DIObject entt = go.AddComponent<DIObject>();
			go.AddComponent<PickableVS>();
			/*
			if (Selection.activeGameObject != null)
			{
				entt.rendererObject = Selection.activeGameObject;
				entt.rendererObject.transform.parent = go.transform;
				entt.rendererObject.transform.localPosition = Vector3.zero;
			}*/
			if (LevelBounds.instance != null)
				entt.position.levelPathPosition = LevelBounds.instance.defaultLevelPathIndex;
			if (Selection.activeGameObject != null)
			{
				entt.transform.position = entt.position.CalculatePositionAndFixIt(Selection.activeGameObject.transform.position);
				EditorUtility.SetDirty(entt.transform);
			}
			CreationPopup.CreationPopupWizard(entt, Selection.activeGameObject);
			if (!GameObject.FindObjectOfType<LevelBounds>())
				ProformerMenu.CreateLevelBound();
			// Set the user's selection to the new GameObject, so that they can start working with it
			//Selection.activeGameObject = go;
		}
	}
}
